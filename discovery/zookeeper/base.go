package zookeeper

import (
	"gitee.com/h79/goutils/discovery/config"
	"gitee.com/h79/goutils/discovery/service"
	"github.com/go-zookeeper/zk"
	"time"
)

type Base struct {
	conn *zk.Conn // zk的客户端连接
}

func NewClientZk(eps *config.EndPoints, serviceNode config.Node, timeout int) (*zk.Conn, error) {
	// 连接服务器
	conn, _, err := zk.Connect(eps.Servers.Array(), time.Duration(timeout)*time.Second)
	if err != nil {
		return nil, err
	}
	// 创建服务根节点
	exists, _, err := conn.Exists(serviceNode.Root())
	if err != nil {
		conn.Close()
		return nil, err
	}
	if exists {
		return conn, nil
	}
	_, err = conn.Create(serviceNode.Root(), []byte(""), 0, zk.WorldACL(zk.PermAll))
	if err != nil && err != zk.ErrNodeExists {
		conn.Close()
		return nil, err
	}
	return conn, nil
}

// Set interface
func (base *Base) Set(data service.Data) error {

	return nil
}

// Get interface
func (base *Base) Get(key service.Key) ([]*service.Data, error) {

	return nil, nil
}
