package zookeeper

import (
	"encoding/json"
	"gitee.com/h79/goutils/common/logger"
	"gitee.com/h79/goutils/common/server"
	"gitee.com/h79/goutils/discovery/config"
	"gitee.com/h79/goutils/discovery/resolver"
	"gitee.com/h79/goutils/discovery/resolver/builder"
	"gitee.com/h79/goutils/discovery/service"
	"github.com/go-zookeeper/zk"
)

var _ service.Client = (*zkClient)(nil)
var _ builder.Builder = (*zkClient)(nil)

type zkClient struct {
	Base
	node config.Node
}

func NewZkClient(conf config.Config, eps config.EndPoints, resol resolver.Resolver) (service.Client, error) {

	cli, err := NewClientZk(&eps, conf.Node, 30)
	if err != nil {
		logger.Error("Zookeeper.client: create client failure,err= %v", err)
		return nil, err
	}
	logger.Info("Zookeeper.client: create client")

	return NewZkClientWith(cli, &conf)
}

func NewZkClientWith(cli *zk.Conn, conf *config.Config) (service.Client, error) {

	b := &zkClient{
		Base: Base{
			conn: cli,
		},
		node: conf.Node,
	}
	resolver.Register(b)
	return b, nil
}

func (cli *zkClient) Start() error {
	return nil
}

func (cli *zkClient) Stop() {
}

// Resolve service.Client and resolver.Builder interface
// server config https://github.com/grpc/grpc/blob/master/doc/service_config.md
func (cli *zkClient) Resolve(target builder.Target) []builder.Address {

	path := target.NameWith("")

	// 获取字节点名称
	childs, _, err := cli.conn.Children(path)
	if err != nil {
		if err == zk.ErrNoNode {
			return nil
		}
	}

	var adders []builder.Address
	for _, child := range childs {
		fullPath := path + "/" + child
		data, _, er := cli.conn.Get(fullPath)
		if er != nil {
			if er == zk.ErrNoNode {
				continue
			}
			return nil
		}
		s := server.Address{}
		er = json.Unmarshal(data, &s)
		if er != nil {
			return nil
		}
		s.Adjust()
		address := builder.Address{Addr: s.To(), ServerName: target.SchemeWith("")}
		adders = append(adders, address)
	}
	return adders
}

// Build
// builder.Builder interface
func (cli *zkClient) Build(target builder.Target, cc builder.Connector) (builder.Resolver, error) {

	logger.Info("def.Client: Build target: %+v", target)

	cr := resolver.NewResolver(target, cc, cli)

	return cr, nil
}

// Scheme
// builder.Builder interface
func (cli *zkClient) Scheme() string {
	return cli.node.Scheme
}

// Type
// builder.Builder interface
func (cli *zkClient) Type() string {
	return cli.node.Type
}
