package etcd

import (
	"context"
	"encoding/json"
	"gitee.com/h79/goutils/common/logger"
	"gitee.com/h79/goutils/common/server"
	"gitee.com/h79/goutils/discovery/config"
	"gitee.com/h79/goutils/discovery/resolver"
	"gitee.com/h79/goutils/discovery/resolver/builder"
	"gitee.com/h79/goutils/discovery/service"
	"gitee.com/h79/goutils/discovery/watch"
	clientv3 "go.etcd.io/etcd/client/v3"
)

var _ service.Client = (*edClient)(nil)
var _ builder.Builder = (*edClient)(nil)

type edClient struct {
	Base
	node config.Node
}

func NewClient(conf config.Config, eps config.EndPoints, resol resolver.Resolver) (service.Client, error) {

	cli, err := NewClient3(&eps, 30)
	if err != nil {
		logger.Error("etcd.client: create client failure,err= %v", err)
		return nil, err
	}
	logger.Info("etcd.client: create client")

	return NewClientWith(cli, &conf)
}

func NewClientWith(cli *clientv3.Client, conf *config.Config) (service.Client, error) {

	b := &edClient{
		Base: Base{
			client:  cli,
			watcher: nil,
		},
		node: conf.Node,
	}
	resolver.Register(b)
	return b, nil
}

func (cli *edClient) Start() error {
	return nil
}

func (cli *edClient) Stop() {
}

// Resolve service.Client and resolver.Builder interface
//
//	https://github.com/grpc/grpc/blob/master/doc/service_config.md
func (cli *edClient) Resolve(target builder.Target) []builder.Address {

	resp, err := cli.client.Get(context.Background(), target.NameWith(""), clientv3.WithPrefix())
	if err != nil {
		return nil
	}
	addrs := cli.extractAdders(resp)

	var adders []builder.Address
	for _, data := range addrs {
		s := server.Address{}
		err = json.Unmarshal([]byte(data), &s)
		if err != nil {
			return nil
		}
		s.Adjust()
		address := builder.Address{Addr: s.To(), ServerName: target.SchemeWith("")}
		adders = append(adders, address)
	}
	return adders
}

func (cli *edClient) extractAdders(resp *clientv3.GetResponse) []string {
	adders := make([]string, 0)
	if resp == nil || resp.Kvs == nil {
		return adders
	}
	for i := range resp.Kvs {
		kvs := resp.Kvs[i]
		if v := kvs.Value; v != nil {
			if cli.watcher != nil {
				data := service.Data{Key: service.NewKey(string(kvs.Key)), Value: string(kvs.Value)}
				cli.watcher.Changed(watch.NewChanged(data, watch.Put))
			}
			adders = append(adders, string(v))
		}
	}
	return adders
}

// Build
// builder.Builder interface
func (cli *edClient) Build(target builder.Target, cc builder.Connector) (builder.Resolver, error) {

	logger.Info("def.Client: Build target: %+v", target)

	cr := resolver.NewResolver(target, cc, cli)

	return cr, nil
}

// Scheme
// builder.Builder interface
func (cli *edClient) Scheme() string {
	return cli.node.Scheme
}

// Type
// builder.Builder interface
func (cli *edClient) Type() string {
	return cli.node.Type
}
