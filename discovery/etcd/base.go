package etcd

import (
	"context"
	"errors"
	"gitee.com/h79/goutils/discovery/config"
	"gitee.com/h79/goutils/discovery/service"
	clientv3 "go.etcd.io/etcd/client/v3"
	"time"
)

type Base struct {
	client  *clientv3.Client
	watcher *Watcher
}

func NewClient3(eps *config.EndPoints, timeout int) (*clientv3.Client, error) {

	config := clientv3.Config{
		Endpoints:   eps.Servers.Array(),
		DialTimeout: time.Duration(timeout) * time.Second,
	}
	return clientv3.New(config)
}

// Set
// Config interface
func (base *Base) Set(data service.Data) error {
	if base.client == nil {
		return errors.New("etcd: set failure, because client is nil")
	}
	_, err := base.client.Put(context.Background(), data.ToKey(), data.Value)
	return err
}

// Get
// Config interface
func (base *Base) Get(key service.Key) ([]*service.Data, error) {
	if base.client == nil {
		return nil, errors.New("etcd: get failure, because client is nil")
	}
	_, err := base.client.Get(context.Background(), key.ToKey(), clientv3.WithPrefix())
	if err != nil {
		return nil, err
	}
	return nil, nil
}

func (base *Base) AttachWatcher(watcher *Watcher) {
	base.watcher = watcher
}
