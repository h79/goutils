package etcd

import (
	"context"
	"gitee.com/h79/goutils/common/system"
	"gitee.com/h79/goutils/discovery/service"
	"gitee.com/h79/goutils/discovery/watch"
	"go.etcd.io/etcd/api/v3/mvccpb"
	clientv3 "go.etcd.io/etcd/client/v3"
)

type Watcher struct {
	client *clientv3.Client
	wc     watch.Chan
	ctx    context.Context
	cancel context.CancelFunc
}

func NewWatcher(client *clientv3.Client, sizeChan int) *Watcher {
	ctx, cancel := context.WithCancel(context.Background())
	return &Watcher{
		client: client,
		wc:     make(watch.Chan, sizeChan),
		cancel: cancel,
		ctx:    ctx,
	}
}

func (watcher *Watcher) Watch(key watch.Key) (watch.Chan, error) {
	wc := watcher.client.Watch(context.Background(), key.ToKey(), clientv3.WithPrefix())
	system.ChildRunning(func() {
		select {
		case resp := <-wc:
			for _, ev := range resp.Events {
				data := service.Data{Key: service.NewKey(string(ev.Kv.Key)), Value: string(ev.Kv.Value)}
				switch ev.Type {
				case mvccpb.PUT:
					watcher.wc <- watch.NewChanged(data, watch.Put)

				case mvccpb.DELETE:
					watcher.wc <- watch.NewChanged(data, watch.Delete)
				}
			}
		case <-watcher.ctx.Done():
			return

		case <-system.Closed():
			return
		}
	})
	return watcher.wc, nil
}

func (watcher *Watcher) Changed(cmd watch.Changed) {
	watcher.wc <- cmd
}

func (watcher *Watcher) Stop() error {
	watcher.cancel()
	return nil
}
