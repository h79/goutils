package local

import (
	"gitee.com/h79/goutils/common/logger"
	"gitee.com/h79/goutils/common/server"
	"gitee.com/h79/goutils/discovery/config"
	"gitee.com/h79/goutils/discovery/errors"
	"gitee.com/h79/goutils/discovery/resolver"
	"gitee.com/h79/goutils/discovery/resolver/builder"
	"gitee.com/h79/goutils/discovery/service"
)

var _ service.Client = (*defClient)(nil)
var _ builder.Builder = (*defClient)(nil)

type defClient struct {
	node      config.Node
	server    server.Address
	endPoints config.EndPoints
	resolver  resolver.Resolver
}

func NewDefaultClient(conf config.Config, points config.EndPoints, resol resolver.Resolver) (service.Client, error) {

	build := &defClient{
		node:      conf.Node,
		server:    conf.Server,
		endPoints: points,
		resolver:  resol,
	}
	resolver.Register(build)
	return build, nil
}

func (cli *defClient) Start() error {
	return nil
}

func (cli *defClient) Stop() {
}

// Set service.Config interface
func (cli *defClient) Set(data service.Data) error {
	return errors.ErrNotSupported
}

// Get service.Config interface
func (cli *defClient) Get(key service.Key) ([]*service.Data, error) {
	return nil, errors.ErrNotSupported
}

// Resolve service.Client and resolver.Builder interface
func (cli *defClient) Resolve(target builder.Target) []builder.Address {
	if cli.resolver != nil {
		return cli.resolver.Resolve(target)
	}
	return cli.defResolve(target)
}

func (cli *defClient) defResolve(target builder.Target) []builder.Address {
	var adders []builder.Address
	var addFn = func(a server.Address) {
		if !a.IsValid() {
			return
		}
		s := server.Address{
			Scheme: a.Scheme,
			Host:   a.Host,
			Port:   a.Port,
		}
		s.Adjust()
		address := builder.Address{Addr: s.To(), ServerName: target.SchemeWith("")}
		adders = append(adders, address)
	}
	for i := range cli.endPoints.Servers {
		addFn(cli.endPoints.Servers[i])
	}
	addFn(cli.server)
	return adders
}

// Build
// builder.Builder interface
func (cli *defClient) Build(target builder.Target, cc builder.Connector) (builder.Resolver, error) {

	logger.Info("def.Client: Build target: %+v", target)

	cr := resolver.NewResolver(target, cc, cli)

	return cr, nil
}

// Scheme
// builder.Builder interface
func (cli *defClient) Scheme() string {
	return cli.node.Scheme
}

// Type
// builder.Builder interface
func (cli *defClient) Type() string {
	return cli.node.Type
}
