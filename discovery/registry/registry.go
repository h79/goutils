package registry

import (
	"gitee.com/h79/goutils/discovery/config"
)

// Registry service registry interface.
type Registry interface {
	Register(service *config.Service, points config.EndPoints) error
	Deregister(service *config.Service, points config.EndPoints) error
}
