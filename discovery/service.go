package discovery

import (
	"gitee.com/h79/goutils/discovery/config"
	"gitee.com/h79/goutils/discovery/consul"
	"gitee.com/h79/goutils/discovery/etcd"
	"gitee.com/h79/goutils/discovery/local"
	"gitee.com/h79/goutils/discovery/registry"
	"gitee.com/h79/goutils/discovery/service"
	"gitee.com/h79/goutils/discovery/zookeeper"
)

func NewService(cfg config.Config, points config.EndPoints, registry registry.Registry) (service.Service, error) {
	if nsf, ok := _service[points.Type]; ok {
		return nsf(cfg, points, registry)
	}
	switch points.Type {
	case "consul":
		return consul.NewService(cfg, points, registry)
	case "zookeeper":
		return zookeeper.NewService(cfg, points, registry)
	case "etcd":
		return etcd.NewService(cfg, points, registry)
	default:
		return local.NewService(cfg, points, registry)
	}
}

/*
 * example
	//points, err := server.ReadEndPoints(endpointFileName)
	//if err != nil {
	//	return nil, err
	//}
	//
	//health, err := server.ReadHealth("")

	//cfg := config.Config{
	//	Node:   conf.Node,
	//	Server: conf.Server,
	//	HealthCheck:  conf.HealthCheck,
	//	Meta:   conf.Meta,
	//	Tags:   conf.Tags,
	//}

	// register service to consul,zk,dubbo,etcd...
	service, err := NewService(*conf, points)
	if err != nil {
		return nil, err
	}
	service.Start()
*/
