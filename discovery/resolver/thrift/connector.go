package thrift

import (
	"gitee.com/h79/goutils/discovery/resolver/builder"
	thriftresolver "gitee.com/h79/goutils/thrift/resolver"
)

// connector
// 一个与thrift桥接的实现
type connector struct {
	cc thriftresolver.Connector
}

// UpdateState builder.Connector interface
func (cr *connector) UpdateState(state builder.State) {
	if cr.cc == nil {
		return
	}
	s := thriftresolver.State{
		Addresses:  toThriftAddress(state.Addresses),
		Attributes: state.Attributes,
	}
	cr.cc.UpdateState(s)
}

// ReportError builder.Connector interface
func (cr *connector) ReportError(er error) {
	if cr.cc == nil {
		return
	}
	cr.cc.ReportError(er)
}

func toThriftAddress(addr []builder.Address) []thriftresolver.Address {
	thriftAddress := make([]thriftresolver.Address, 0)
	for _, a := range addr {
		aa := thriftresolver.Address{
			Addr:       a.Addr,
			ServerName: a.ServerName,
			Attributes: a.Attributes,
		}
		thriftAddress = append(thriftAddress, aa)
	}
	return thriftAddress
}
