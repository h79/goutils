package thrift

import (
	"fmt"
	"gitee.com/h79/goutils/common/logger"
	"gitee.com/h79/goutils/discovery/resolver/builder"
	thriftresolver "gitee.com/h79/goutils/thrift/resolver"
)

// priBuilder 为thrift桥接
type priBuilder struct {
	myBuilder builder.Builder
}

func Register(builder builder.Builder) {
	b := &priBuilder{
		myBuilder: builder,
	}
	// 注册到thrift中
	thriftresolver.Register(b)
}

// Build grpc  interface
func (cb *priBuilder) Build(target thriftresolver.Target, cc thriftresolver.Connector) (thriftresolver.Resolver, error) {

	if cc == nil {
		return nil, fmt.Errorf("[Thrift Build] client connect is nil")
	}
	logger.Debug("Thrift: Build target= %+v", target)
	tar := builder.Target{
		Scheme:    target.Scheme,
		Authority: target.Authority,
		Endpoint:  target.Endpoint,
	}

	cr := &connector{
		cc: cc,
	}

	rr, err := cb.myBuilder.Build(tar, cr)
	if err != nil {
		return nil, err
	}

	rr0 := &priResolver{
		myResolver: rr,
	}

	return rr0, nil
}

// Scheme thrift  interface
func (cb *priBuilder) Scheme() string {
	return cb.myBuilder.Scheme()
}

type priResolver struct {
	myResolver builder.Resolver
}

// ResolveNow thrift.Resolver interface
func (cr *priResolver) ResolveNow() {
	cr.myResolver.ResolveNow()
}

// Close thrift.Resolver interface
func (cr *priResolver) Close() {
	cr.myResolver.Close()
}
