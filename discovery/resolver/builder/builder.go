package builder

import (
	"gitee.com/h79/goutils/common/attributes"
	"gitee.com/h79/goutils/common/random"
	"sort"
	"strings"
)

// Target
//   - "dns://some_authority/foo.bar"
//     Target{Scheme: "dns", Authority: "some_authority", Endpoint: "foo.bar"}
type Target struct {
	// "grpc", "thrift", "local"...
	Type      string `json:"type"`
	Scheme    string `json:"scheme"`
	Authority string `json:"authority"`
	Endpoint  string `json:"endpoint"`
}

const KDefBuild = "/"

func (target *Target) SchemeWith(build string) string {
	addr := ""
	if len(build) == 0 {
		build = KDefBuild
	}
	if len(target.Scheme) > 0 {
		addr = target.Scheme + "://"
	}
	if len(target.Authority) > 0 {
		addr += target.Authority
	}
	if len(target.Endpoint) > 0 {
		if len(addr) > 0 {
			addr += build
		}
		addr += target.Endpoint
	}
	return addr
}

func (target *Target) NameWith(build string) string {
	addr := ""
	if len(build) == 0 {
		build = KDefBuild
	}
	if len(target.Authority) > 0 {
		addr += target.Authority
	}
	if len(target.Endpoint) > 0 {
		if len(addr) > 0 {
			addr += build
		}
		addr += target.Endpoint
	}
	return addr
}

func TargetWithGrpc(scheme, authority, endpoint string) Target {
	return TargetWith("grpc", scheme, authority, endpoint)
}

func TargetWithThrift(scheme, authority, endpoint string) Target {
	return TargetWith("thrift", scheme, authority, endpoint)
}

func TargetWithLocal(scheme, authority, endpoint string) Target {
	return TargetWith("local", scheme, authority, endpoint)
}

func TargetWith(ty, scheme, authority, endpoint string) Target {
	return Target{
		Type:      ty,
		Scheme:    scheme,
		Authority: authority,
		Endpoint:  endpoint,
	}
}

// split2 returns the values from strings.SplitN(s, sep, 2).
// If sep is not found, it returns ("", "", false) instead.
func split2(s, sep string) (string, string, bool) {
	spl := strings.SplitN(s, sep, 2)
	if len(spl) < 2 {
		return "", "", false
	}
	return spl[0], spl[1], true
}

// ParseTarget splits target into a resolver.Target struct containing scheme,
// authority and endpoint.
//
// If target is not a valid scheme://authority/endpoint, it returns {Endpoint:
// target}.
func ParseTarget(target string) (ret Target) {
	var ok bool
	ret.Scheme, ret.Endpoint, ok = split2(target, "://")
	if !ok {
		return Target{Endpoint: target}
	}
	ret.Authority, ret.Endpoint, ok = split2(ret.Endpoint, KDefBuild)
	if !ok {
		return Target{Endpoint: target}
	}
	return ret
}

// Builder creates a resolver that will be used to watch name resolution updates.
type Builder interface {
	// Build creates a new resolver for the given target.
	//
	// gRPC dial calls Build synchronously, and fails if the returned error is
	// not nil.
	Build(target Target, cc Connector) (Resolver, error)

	// Scheme returns the scheme supported by this resolver.
	Scheme() string

	Type() string
}

// Resolver watches for the updates on the specified target.
// Updates include address updates and service config updates.
type Resolver interface {
	// ResolveNow will be called by gRPC to try to resolve the target name
	// again. It's just a hint, resolver can ignore this if it's not necessary.
	//
	// It could be called multiple times concurrently.
	ResolveNow()

	// Close closes the resolver.
	Close()
}

type Connector interface {
	// UpdateState updates the state of the ClientConn appropriately.
	UpdateState(State)

	// ReportError notifies the ClientConn that the Resolver encountered an
	// error.  The ClientConn will notify the load balancer and begin calling
	// ResolveNow on the Resolver with exponential backoff.
	ReportError(error)
}

// Address Notice: This type is EXPERIMENTAL and may be changed or removed in a
// later release.
type Address struct {
	// Addr is the server address on which a connection will be established.
	Addr string

	// ServerName is the name of this address.
	// If non-empty, the ServerName is used as the transport certification authority for
	// the address, instead of the hostname from the Dial target string. In most cases,
	// this should not be set.
	//
	// If Method is GRPCLB, ServerName should be the name of the remote load
	// balancer, not the name of the backend.
	//
	// WARNING: ServerName must only be populated with trusted values. It
	// is insecure to populate it with data from untrusted inputs since untrusted
	// values could be used to bypass the authority checks performed by TLS.
	ServerName string

	// Attributes contains arbitrary data about this address intended for
	// consumption by the load balancing policy.
	Attributes *attributes.Attributes

	// Weight sort max in front
	Weight int64
}

// State contains the current Resolver state relevant to the ClientConn.
type State struct {
	// Addresses is the latest set of resolved addresses for the target.
	Addresses Addresses

	// Attributes contains arbitrary data about the resolver intended for
	// consumption by the load balancing policy.
	Attributes *attributes.Attributes
}

type Addresses []Address

func (a Addresses) Len() int {
	return len(a)
}

func (a Addresses) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

// Less 权重大的在前面
func (a Addresses) Less(i, j int) bool {
	return a[i].Weight >= a[j].Weight
}

func (a Addresses) ToWeight() {
	r := random.Random()
	for index, _ := range a {
		a[index].Weight = r.Int63n(1000)
	}
	sort.Sort(a)
}
