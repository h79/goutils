package grpc

import (
	"fmt"
	"gitee.com/h79/goutils/common/logger"
	"gitee.com/h79/goutils/discovery/resolver/builder"
	grpcresolver "google.golang.org/grpc/resolver"
)

// priBuilder 为grpc桥接
type priBuilder struct {
	myBuilder builder.Builder
}

func Register(builder builder.Builder) {
	b := &priBuilder{
		myBuilder: builder,
	}

	// 注册到GRPC中
	grpcresolver.Register(b)
}

// Build
// grpc  interface
func (cb *priBuilder) Build(target grpcresolver.Target,
	cc grpcresolver.ClientConn, opts grpcresolver.BuildOptions) (grpcresolver.Resolver, error) {

	if cc == nil {
		return nil, fmt.Errorf("GRPC: client connect is nil")
	}
	logger.Info("GRPC: Build target: %+v", target)

	// - "dns://some_authority/foo.bar"
	//   Target{Scheme: "dns", Authority: "some_authority", Endpoint: "foo.bar"}
	// - "foo.bar"
	//   Target{Scheme: resolver.GetDefaultScheme(), Endpoint: "foo.bar"}
	// - "unknown_scheme://authority/endpoint"
	//   Target{Scheme: resolver.GetDefaultScheme(), Endpoint: "unknown_scheme://authority/endpoint"}
	endpoints := target.URL.Path
	if len(endpoints) <= 0 {
		endpoints = target.URL.Opaque
	}
	tar := builder.Target{
		Scheme:    target.URL.Scheme,
		Authority: target.URL.Host,
		Endpoint:  endpoints,
	}

	cr := &connector{
		cc: cc,
	}

	rr, err := cb.myBuilder.Build(tar, cr)
	if err != nil {
		return nil, err
	}

	return &priResolver{myResolver: rr}, nil
}

// Scheme
// grpc  interface
func (cb *priBuilder) Scheme() string {
	return cb.myBuilder.Scheme()
}

type priResolver struct {
	myResolver builder.Resolver
}

func (cr *priResolver) ResolveNow(grpcresolver.ResolveNowOptions) {
	cr.myResolver.ResolveNow()
}

func (cr *priResolver) Close() {
	cr.myResolver.Close()
}
