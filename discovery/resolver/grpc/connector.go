package grpc

import (
	"gitee.com/h79/goutils/common/attributes"
	"gitee.com/h79/goutils/discovery/resolver/builder"
	grpcAttributes "google.golang.org/grpc/attributes"
	grpcResolver "google.golang.org/grpc/resolver"
	"sort"
)

// connector
// 一个与grpc桥接的实现
type connector struct {
	cc grpcResolver.ClientConn
}

// UpdateState builder.Connector interface
func (cr *connector) UpdateState(state builder.State) {
	if cr.cc == nil {
		return
	}
	s := grpcResolver.State{
		ServiceConfig: nil,
		Addresses:     toGrpcAddress(state.Addresses),
		Attributes:    toGrpcAttribute(state.Attributes),
	}
	_ = cr.cc.UpdateState(s)
}

// ReportError resolver.Connector interface
func (cr *connector) ReportError(er error) {
	if cr.cc == nil {
		return
	}
	cr.cc.ReportError(er)
}

func toGrpcAttribute(attrs *attributes.Attributes) *grpcAttributes.Attributes {
	if attrs == nil {
		return nil
	}
	var attr *grpcAttributes.Attributes
	attrs.For(func(key interface{}, val interface{}) {
		attr = attr.WithValue(key, val)
	})
	return attr
}

func toGrpcAddress(addr builder.Addresses) []grpcResolver.Address {
	sort.Sort(addr)
	grpcAddress := make([]grpcResolver.Address, 0)
	for i := range addr {
		aa := grpcResolver.Address{
			Addr:       addr[i].Addr,
			ServerName: addr[i].ServerName,
			Attributes: toGrpcAttribute(addr[i].Attributes),
		}
		grpcAddress = append(grpcAddress, aa)
	}
	return grpcAddress
}
