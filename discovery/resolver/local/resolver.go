package local

import "gitee.com/h79/goutils/discovery/resolver/builder"

var (
	// m is a map from scheme to resolver builder.
	m = make(map[string]builder.Builder)
)

func Register(b builder.Builder) {
	m[b.Scheme()] = b
}

// Get returns the resolver builder registered with the given scheme.
//
// If no builder is register with the scheme, nil will be returned.
func Get(scheme string) builder.Builder {
	if b, ok := m[scheme]; ok {
		return b
	}
	return nil
}
