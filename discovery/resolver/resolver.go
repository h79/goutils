package resolver

import (
	"gitee.com/h79/goutils/common/logger"
	"gitee.com/h79/goutils/discovery/resolver/builder"
)

type Resolver interface {
	Resolve(target builder.Target) []builder.Address
}

var _ builder.Resolver = (*Bridge)(nil)

type Bridge struct {
	build  Builder
	target builder.Target
	conn   builder.Connector
}

// ResolveNow builder.Resolver interface
func (cr *Bridge) ResolveNow() {
	logger.Info("Bridge: ResolveNow, target= '%v'", cr.target)
	cr.update()
}

// Close builder.Resolver interface
func (cr *Bridge) Close() {
	logger.Info("Bridge: Close, target= '%v'", cr.target)
}

func (cr *Bridge) update() {
	if addrs := cr.build.Resolve(cr.target); len(addrs) > 0 {
		cr.conn.UpdateState(builder.State{Addresses: addrs, Attributes: nil})
	}
}

func NewResolver(target builder.Target, cc builder.Connector, builder Builder) builder.Resolver {
	r := &Bridge{
		target: target,
		conn:   cc,
		build:  builder,
	}
	r.update()
	return r
}
