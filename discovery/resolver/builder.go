package resolver

import (
	"gitee.com/h79/goutils/discovery/resolver/builder"
	"gitee.com/h79/goutils/discovery/resolver/grpc"
	"gitee.com/h79/goutils/discovery/resolver/local"
	"gitee.com/h79/goutils/discovery/resolver/thrift"
)

type Builder interface {
	Resolve(target builder.Target) []builder.Address
}

func Register(b builder.Builder) {
	switch b.Type() {
	case "grpc":
		grpc.Register(b)
	case "thrift":
		thrift.Register(b)
	case "local":
		local.Register(b)
	}
}
