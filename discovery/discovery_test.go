package discovery

import (
	"gitee.com/h79/goutils/common/server"
	"gitee.com/h79/goutils/discovery/config"
	"gitee.com/h79/goutils/discovery/resolver/builder"
	"testing"
)

func TestDiscovery(t *testing.T) {
	conf := config.WithTarget(builder.TargetWithGrpc("dim", "server", ""), server.Health{})
	NewService(conf, config.EndPoints{Type: "local"}, nil)

	NewClient(conf, config.EndPoints{Type: "local"}, nil)

}
