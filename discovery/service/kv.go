package service

import "strings"

// Key
/** 一个KEY 值是由路径+服务名称组合 */
type Key struct {
	Path    string
	Service string
}

func (k Key) ToKey() string {
	if len(k.Service) > 0 {
		return k.Path + "/" + k.Service
	}
	return k.Path
}

func (k Key) ToMap() map[string]interface{} {
	//{"type": "service", "service":""}
	//{"type": "keyprefix", "prefix": k.ToKey()}
	return map[string]interface{}{"type": "key", "key": k.ToKey()}
}

func NewKey(key string) Key {
	index := strings.LastIndexByte(key, '/')
	if index == -1 {
		return Key{Service: key}
	}
	return Key{key[0:index], key[index+1:]}
}

type Data struct {
	Key
	Value string //json format
}

type Config interface {
	Set(data Data) error
	Get(key Key) ([]*Data, error)
}
