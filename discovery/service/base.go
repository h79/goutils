package service

type Base interface {
	Start() error
	Stop()
}
