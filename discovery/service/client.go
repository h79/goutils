package service

import (
	"gitee.com/h79/goutils/discovery/resolver"
)

type Client interface {
	Base
	Config
	resolver.Resolver
}
