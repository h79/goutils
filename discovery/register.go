package discovery

import (
	"gitee.com/h79/goutils/discovery/config"
	"gitee.com/h79/goutils/discovery/registry"
	"gitee.com/h79/goutils/discovery/resolver"
	"gitee.com/h79/goutils/discovery/service"
)

var (
	_service map[string]NewServiceFunc
	_client  map[string]NewClientFunc
)

type NewServiceFunc func(cfg config.Config, points config.EndPoints, registry registry.Registry) (service.Service, error)
type NewClientFunc func(cfg config.Config, points config.EndPoints, resolver resolver.Resolver) (service.Client, error)

func init() {
	_service = make(map[string]NewServiceFunc)
	_client = make(map[string]NewClientFunc)
}

func ClientUse(key string, ncf NewClientFunc) {
	if ncf == nil || len(key) <= 0 {
		return
	}
	_client[key] = ncf
}

func ServiceUse(key string, nsf NewServiceFunc) {
	if nsf == nil || len(key) <= 0 {
		return
	}
	_service[key] = nsf
}
