package discovery

import (
	"gitee.com/h79/goutils/discovery/service"
	"sync"
)

// 需要对services

type Services struct {
	lock     sync.Mutex
	Services map[string]service.Base
}

func NewServices() *Services {
	return &Services{
		Services: make(map[string]service.Base),
	}
}

func (s *Services) Add(name string, base service.Base) {
	s.lock.Lock()
	defer s.lock.Unlock()
	s.Services[name] = base
}

func (s *Services) Del(name string) {
	s.lock.Lock()
	defer s.lock.Unlock()
	delete(s.Services, name)
}

func (s *Services) Get(name string) (service.Base, bool) {
	s.lock.Lock()
	defer s.lock.Unlock()
	it, ok := s.Services[name]
	return it, ok
}
