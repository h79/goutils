package watch

type Key interface {
	ToKey() string
	ToMap() map[string]interface{} // for consul
}

type Watcher interface {
	Watch(key Key) (Chan, error)
	Changed(cmd Changed)
	Stop() error
}

type Chan chan Changed

type Changed struct {
	D   interface{}
	Cmd int
}

func NewChanged(data interface{}, cmd int) Changed {
	return Changed{
		D:   data,
		Cmd: cmd,
	}
}

// Cmd=
const (
	Delete = 1
	Put    = 2
)
