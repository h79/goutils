package discovery

import (
	"gitee.com/h79/goutils/discovery/config"
	"gitee.com/h79/goutils/discovery/consul"
	"gitee.com/h79/goutils/discovery/etcd"
	"gitee.com/h79/goutils/discovery/local"
	"gitee.com/h79/goutils/discovery/resolver"
	"gitee.com/h79/goutils/discovery/service"
	"gitee.com/h79/goutils/discovery/zookeeper"
)

func NewClient(conf config.Config, points config.EndPoints, resolver resolver.Resolver) (service.Client, error) {
	if ncf, ok := _client[points.Type]; ok {
		return ncf(conf, points, resolver)
	}
	switch points.Type {
	case "consul":
		return consul.NewConsulClient(conf, points, resolver)
	case "zookeeper":
		return zookeeper.NewZkClient(conf, points, resolver)
	case "etcd":
		return etcd.NewClient(conf, points, resolver)
	default:
		return local.NewDefaultClient(conf, points, resolver)
	}
}

/*
 * example
	//points, err := server.ReadEndPoints(endpointFileName)
	//if err != nil {
	//	return nil, err
	//}
	//
	//health, err := server.ReadHealth("")

	//cfg := config.Config{
	//	Node:   ,
	//	Server: ,
	//	HealthCheck:  health,
	//	Meta:   ,
	//	Tags:   ,
	//}

	client, err := NewClient(*conf, points)
	if err != nil {
		return nil, err
	}
*/
