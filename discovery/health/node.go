package health

import "gitee.com/h79/goutils/common/server"

type Unpack func(body []byte, node *Node) error
type CheckFunc func(node *Node) error

type Node struct {
	Health server.Health
	// http检测后，服务器返回的body，由业务根据情况不同去解释
	Unpack Unpack
	// 可以配自己的检测函数
	Check CheckFunc
	// 结点状态
	Status int
	// 错误几次
	errCount int
}

func (n Node) URL() string {
	return n.Health.URL.To()
}

func (n Node) Id() string {
	return n.Health.URL.To()
}
