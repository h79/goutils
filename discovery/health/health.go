package health

import (
	"context"
	"fmt"
	"gitee.com/h79/goutils/alarm"
	commonhttp "gitee.com/h79/goutils/common/http"
	"gitee.com/h79/goutils/common/logger"
	"gitee.com/h79/goutils/common/result"
	"gitee.com/h79/goutils/common/system"
	"gitee.com/h79/goutils/discovery/service"
	"net/http"
	"strings"
	"sync"
	"time"
)

var _ service.Base = (*Health)(nil)

type Health struct {
	conf   Config
	locker sync.RWMutex
	nodes  map[string]*Node
	stop   chan bool
}

func New(conf Config) *Health {
	if conf.MaxCount <= 0 {
		conf.MaxCount = 3
	}
	if conf.Interval <= 0 {
		conf.Interval = 30
	}
	return &Health{
		conf:  conf,
		stop:  make(chan bool),
		nodes: make(map[string]*Node),
	}
}

func (ch *Health) AddNode(id string, node *Node) {
	ch.locker.Lock()
	defer ch.locker.Unlock()
	if _, ok := ch.nodes[id]; ok {
		return
	}
	ch.nodes[id] = node
}

func (ch *Health) DelNode(id string) {
	ch.locker.Lock()
	defer ch.locker.Unlock()
	delete(ch.nodes, id)
}

func (ch *Health) Start() error {
	system.ChildRunning(ch.check)
	return nil
}

func (ch *Health) Stop() {
	system.Stop(time.Second, ch.stop)
}

func (ch *Health) check() {
	ticker := time.NewTicker(time.Second * ch.conf.Interval)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			ch.run()

		case <-ch.stop:
			ch.stop <- true
			return

		case <-system.Closed():
			return
		}
	}
}

func (ch *Health) run() {
	nodes := make(map[string]*Node)
	ch.locker.RLock()
	for id, node := range ch.nodes {
		nodes[id] = node
	}
	ch.locker.RUnlock()

	for _, node := range nodes {
		if err := ch.checkNode(node); err != nil {
			node.errCount++
			logger.Error("Health: check failure, err= '%v'", err)
		} else {
			node.errCount = 0
		}
		if node.errCount > ch.conf.MaxCount {
			logger.Error("Health: check failure, err count > max count(%v)", node.errCount, ch.conf.MaxCount)
			//报警
			ch.alarm(node)
		}
	}
}

func (ch *Health) alarm(node *Node) {
	if !ch.conf.EnableAlarm {
		return
	}
	alarm.Important(context.Background(), result.ErrTimeout, "discovery", "", fmt.Sprintf("Health: node health check error, node= %+v", node), nil)
}

func (ch *Health) checkNode(node *Node) error {
	if node.Check != nil {
		return node.Check(node)
	}
	if strings.Contains(node.Health.Protocol, "http") {
		return ch.http(node)
	}
	return result.RErrNotSupport
}

func (ch *Health) http(node *Node) error {
	hp := commonhttp.Http{}
	body, err := hp.Do(node.Health.Method, node.URL(), nil, func(h *http.Header) {
	})
	if err != nil {
		return err
	}
	if node.Unpack != nil {
		return node.Unpack(body, node)
	}
	return nil
}
