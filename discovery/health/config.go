package health

import "time"

type Config struct {
	MaxCount int `json:"maxCount"`
	// 检测秒
	Interval    time.Duration `json:"interval"`
	EnableAlarm bool          `json:"enableAlarm"`
}
