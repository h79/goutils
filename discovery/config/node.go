package config

import (
	"gitee.com/h79/goutils/discovery/resolver/builder"
)

type Node struct {
	builder.Target
}

func (node *Node) Root() string {
	return node.Authority
}
