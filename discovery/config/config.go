package config

import (
	"gitee.com/h79/goutils/common/server"
	"gitee.com/h79/goutils/discovery/resolver/builder"
	"net"
)

type Service struct {
	// The node name that the endpoint belongs to.
	Node Node `json:"node"`

	// The network address at which the service can be reached.
	Server server.Address `json:"server"`

	// The list of tags associated with the service.
	Tags []string `json:"tags"`

	// The set of metadata associated with the node on which the service is
	// running.
	Meta map[string]string `json:"meta"`
}

func (s *Service) Adjust(addr net.Addr, pubIp string) {
	s.Server = server.ParseV2(addr, pubIp)
}

type Config struct {
	Service
	Check server.Health
}

func WithTarget(target builder.Target, health server.Health) Config {
	return Config{
		Service: Service{
			Node: Node{
				Target: target,
			},
		},
		Check: health,
	}
}
