package config

import (
	"gitee.com/h79/goutils/common/config"
	"gitee.com/h79/goutils/common/json"
	"gitee.com/h79/goutils/common/server"
)

type EndPoints struct {
	Servers server.Addresses `json:"servers"`
	//consul,etcd,dubbo, local注册
	Type string `json:"type"`
}

// ReadEndPoints
// filename 相对文件名
func ReadEndPoints(filename string) (EndPoints, error) {
	if len(filename) <= 0 {
		filename = "endpoints.json"
	}
	filename = config.FileNameInEnvPath(filename)
	ep := EndPoints{}
	err := json.Read(filename, &ep)
	return ep, err
}
