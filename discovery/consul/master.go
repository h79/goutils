package consul

import (
	"gitee.com/h79/goutils/common/logger"
	"gitee.com/h79/goutils/common/server"
	"gitee.com/h79/goutils/discovery/config"
	"gitee.com/h79/goutils/discovery/resolver"
	"gitee.com/h79/goutils/discovery/resolver/builder"
	"gitee.com/h79/goutils/discovery/service"
	consul "github.com/hashicorp/consul/api"
)

var _ service.Client = (*clClient)(nil)
var _ builder.Builder = (*clClient)(nil)

type clClient struct {
	Base
	node config.Node
}

func NewConsulClient(conf config.Config, endPoints config.EndPoints, resol resolver.Resolver) (service.Client, error) {
	cli, err := NewClientWithPoints(endPoints)
	if err != nil {
		logger.Error("Consul.client: create consul failure, err= %+v", err)
		return nil, err
	}
	logger.Info("Consul.client: create consul client")
	b := &clClient{
		Base: Base{
			client:  cli,
			watcher: nil,
		},
		node: conf.Node,
	}
	resolver.Register(b)
	return b, nil
}

func (cli *clClient) Start() error {
	return nil
}

func (cli *clClient) Stop() {
}

// Resolve service.Client and resolver.Builder interface
func (cli *clClient) Resolve(target builder.Target) []builder.Address {

	entries, _, err := cli.client.Health().Service(target.NameWith(""), "", true, &consul.QueryOptions{})
	if err != nil {
		logger.Error("Consul.client: Resolve service(%s) entry, err= %+v", target.NameWith("/"), err)
		return nil
	}

	if len(entries) == 0 {
		return nil
	}

	var adders []builder.Address
	for _, entry := range entries {

		logger.Debug("Consul.client: entry: %v", entry)
		addr := server.Address{
			Host: entry.Service.Address,
			Port: entry.Service.Port,
		}
		addr.Adjust()
		address := builder.Address{Addr: addr.To(), ServerName: target.SchemeWith("")}
		adders = append(adders, address)
	}
	return adders
}

// Build
// builder.Builder interface
func (cli *clClient) Build(target builder.Target, cc builder.Connector) (builder.Resolver, error) {

	logger.Info("def.Client: Build target: %+v", target)

	cr := resolver.NewResolver(target, cc, cli)

	return cr, nil
}

// Scheme
// builder.Builder interface
func (cli *clClient) Scheme() string {
	return cli.node.Scheme
}

// Type
// builder.Builder interface
func (cli *clClient) Type() string {
	return cli.node.Type
}
