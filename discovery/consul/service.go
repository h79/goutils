/**
 * by huqiuyun
 * 启动一个服务时，注册到consul中心，从而可以收到配置信息的变化通知
 */
package consul

import (
	"fmt"
	"gitee.com/h79/goutils/common/logger"
	"gitee.com/h79/goutils/common/result"
	"gitee.com/h79/goutils/common/server"
	"gitee.com/h79/goutils/common/system"
	"gitee.com/h79/goutils/discovery/config"
	"gitee.com/h79/goutils/discovery/registry"
	"gitee.com/h79/goutils/discovery/service"
	consul "github.com/hashicorp/consul/api"
	"time"
)

var _ service.Service = (*clService)(nil)

type clService struct {
	Base
	id          string
	serviceName string
	conf        config.Config
	stop        chan bool
}

func NewService(cfg config.Config, points config.EndPoints, registry registry.Registry) (service.Service, error) {

	// initial consul client config
	cli, err := NewClientWithPoints(points)
	if err != nil {
		return nil, err
	}
	logger.Info("Consul.service: create consul client")
	id := fmt.Sprintf("service: %s", cfg.Server.To())
	serviceName := cfg.Node.NameWith("")

	reg := &consul.AgentServiceRegistration{
		ID:      id,
		Name:    serviceName,
		Tags:    cfg.Tags,
		Port:    cfg.Server.Port,
		Address: cfg.Server.Host,
	}
	logger.Info("Consul.service: register info= %+v", reg)
	if err = cli.Agent().ServiceRegister(reg); err != nil {
		return nil, result.Error(result.ErrServiceInternal,
			fmt.Sprintf("CONSUL.SERVICE: register service= %v", err))
	}

	err = registerHealth(cfg.Check, id, serviceName, cli)
	if err != nil {
		return nil, result.Error(result.ErrServiceInternal,
			fmt.Sprintf("[CONSUL.SERVICE] register service check : %v", err))
	}

	ser := &clService{
		Base: Base{
			client:  cli,
			watcher: nil,
		},
		id:          id,
		serviceName: serviceName,
		conf:        cfg,
		stop:        make(chan bool)}

	return ser, nil
}

func registerHealth(conf server.Health, id string, name string, client *consul.Client) error {
	// initial register service check
	asc := consul.AgentServiceCheck{Status: consul.HealthPassing}

	duration := fmt.Sprintf("%ds", conf.Interval)

	if conf.Ttl {
		asc.TTL = duration
	} else {
		asc.Interval = duration
		switch conf.Protocol {
		case server.KGRpcProtocol:
			asc.GRPC = conf.URL.To()
		case server.KHttpProtocol:
			asc.HTTP = conf.URL.To()
		case server.KTCPProtocol:
			asc.TCP = conf.URL.To()
		}
	}
	check := &consul.AgentCheckRegistration{
		ID:                id,
		ServiceID:         id,
		Name:              name,
		AgentServiceCheck: asc,
	}
	return client.Agent().CheckRegister(check)
}

// Start
/**
* Service interface
* 使用 goroutine  go Start
 */
func (s *clService) Start() error {
	system.ChildRunning(s.keepAlive)
	return nil
}

// Stop Service interface
func (s *clService) Stop() {
	system.Stop(time.Second, s.stop)
}

func (s *clService) keepAlive() {
	ticker := time.NewTicker(time.Second * s.conf.Check.Interval)
	defer ticker.Stop()
	for {
		select {
		case <-s.stop:
			s.revoke()
			s.stop <- true
			return

		case <-ticker.C:
			if err := s.client.Agent().UpdateTTL(s.id, "", consul.HealthPassing); err != nil {
				logger.Error("Consul.service: update ttl err= %v", err)
			}

		case <-system.Closed():
			logger.Error("Consul.service: server stop because system closed")
			s.revoke()
			return
		}
	}
}

func (s *clService) revoke() {

	err := s.client.Agent().ServiceDeregister(s.id)
	if err != nil {
		logger.Error("Consul.service: deregister service err= %v", err)
	}

	err = s.client.Agent().CheckDeregister(s.id)
	if err != nil {
		logger.Error("Consul.service: deregister check err= %v", err)
	}
	logger.Info("Consul.service: deregister")
}
