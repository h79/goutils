package perf

import (
	"context"
	"fmt"
	"gitee.com/h79/goutils/alarm"
	"gitee.com/h79/goutils/common/random"
	"gitee.com/h79/goutils/common/result"
	"gitee.com/h79/goutils/common/timer"
	"go.uber.org/zap"
	"path/filepath"
	"runtime"
	"time"
)

type Config struct {
	DisableLog  bool `json:"disableLog" yaml:"disableLog"`
	DisableWarn bool `json:"disableWarn" yaml:"disableWarn"`
}

type RunTime struct {
	warn  int64
	start int64
}

func Begin(warn int64) *RunTime {
	if warn == 0 {
		warn = 3000
	}
	return &RunTime{start: timer.CurrentMS(), warn: warn}
}

func (t *RunTime) Start() int64 {
	return t.start
}

func (t *RunTime) End() int64 {
	end := timer.CurrentMS()
	diff := end - t.start
	if diff >= t.warn {
		return diff
	}
	return 0
}

type Perf struct {
	Config
	id      string
	model   string
	funName string
	file    string
	line    int
	warn    int64
	start   int64
	mid     int64
}

func (p *Perf) EnableLog(enable bool) *Perf {
	p.DisableLog = !enable
	return p
}

func (p *Perf) EnableWarn(enable bool) *Perf {
	p.DisableWarn = !enable
	return p
}

func (p *Perf) WithModel(model string) *Perf {
	p.model = model
	return p
}

func (p *Perf) WithWarn(t int64) *Perf {
	p.warn = t
	return p
}

func (p *Perf) WithPerfId(id string) *Perf {
	p.id = id
	return p
}

func (p *Perf) Start() *Perf {
	p.start = timer.CurrentMS()
	p.mid = p.start
	return p
}

func (p *Perf) End() {
	end := timer.CurrentMS()
	dif := end - p.start
	p.mid = end
	p.waring(end, dif, "end")
}

func (p *Perf) Out(detail string) {
	end := timer.CurrentMS()
	dif := end - p.mid
	p.mid = end
	p.waring(end, dif, detail)
}

func (p *Perf) log(end, dif int64, detail string) {
	zap.L().WithOptions(zap.AddCallerSkip(2)).Debug("Perf",
		zap.String("perfId", p.id),
		zap.String("model", p.model),
		zap.String("detail", detail),
		zap.Int64("start", p.start),
		zap.Int64("end", end),
		zap.Duration("dif", time.Duration(dif)*time.Millisecond),
		zap.String("func", p.funName),
		zap.String("file", p.file),
		zap.Int("line", p.line))
}

func (p *Perf) waring(end, dif int64, detail string) {
	//执行时间太长，告警
	wa := p.warn
	if wa <= 0 {
		wa = 2000
	}
	if p.DisableWarn {
		if !p.DisableLog {
			p.log(end, dif, detail)
		}
		return
	}
	if dif >= p.warn+1000 {
		p.log(end, dif, detail)
		alarm.Tight(context.Background(), result.ErrTimeout,
			"perf", "",
			fmt.Sprintf("%s, detail=> '%s', run time too long=> %dms, start=> %d,end=> %d", p.info(), detail, dif, p.start, end), nil)
	} else if dif >= p.warn {
		p.log(end, dif, detail)
		alarm.Important(context.Background(), result.ErrTimeout,
			"perf", "",
			fmt.Sprintf("%s, detail=> '%s', run time too long=> %dms,start=> %d,end=> %d", p.info(), detail, dif, p.start, end), nil)
	}
}

func (p *Perf) info() string {
	return fmt.Sprintf("perfId: '%s', model: '%s',funcName: '%s', fileName: '%s', line: %d",
		p.id, p.model, p.funName, p.file, p.line)
}

// T OWNER
func T() *Perf {
	pc, file, line, _ := runtime.Caller(1)
	return &Perf{
		id:      random.GenerateNumString(16),
		model:   "",
		warn:    2000,
		funName: filepath.Base(runtime.FuncForPC(pc).Name()),
		file:    filepath.Base(file),
		line:    line,
	}
}

// C caller
func C() *Perf {
	pc, file, line, _ := runtime.Caller(2)
	return &Perf{
		id:      random.GenerateNumString(16),
		model:   "",
		warn:    2000,
		funName: filepath.Base(runtime.FuncForPC(pc).Name()),
		file:    filepath.Base(file),
		line:    line,
	}
}
