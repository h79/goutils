package perf

import (
	"testing"
)

func TestCallerName(t *testing.T) {
	perf := T().Start()
	defer perf.End()
	for i := 0; i < 1000; i++ {
		t.Logf("%d", i)
		perf.Out("for")
	}
}
