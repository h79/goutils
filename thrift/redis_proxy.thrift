namespace java com.vibo.redis.thrift
namespace go redis_proxy

const i32 RES_OK = 0 //正确返回
const i32 RES_FAIL = 1 //错误返回
const i32 RES_WRONG_TYPE = 2 //类型错误
const i32 RES_NOT_NULL = 3 //key的值存在，当NX才有效
const i32 RES_CONNECTION_REFUSED = 501 //和集群的某些节点断开连接
const i32 RES_CANNOT_LOAD_SLOTS = 502 //获取slot信息失败，多出现在启动时连不上集群
const i32 RES_CLUSTERDOWN = 503 //集群宕机
const i32 RES_MOVED = 301 //当被MOVE的节点失效时出现
const i32 RES_ASK = 302 //当被Ask的节点失效时出现

struct response {
    1:i32 code,
    2:string msg,
    3:string res,
}

struct mResponse{
    1:i32 code,
    2:string msg,
    3:map<string,string> res,
}

struct lResponse{
    1:i32 code,
    2:string msg,
    3:list<string> res,
}

struct intResponse{
    1:i32 code,
    2:string msg,
    3:i64 res,
}

struct mIntResponse{
    1:i32 code,
    2:string msg,
    3:map<string,i64> res,
}

struct floatResponse{
    1:i32 code,
    2:string msg,
    3:double res,
}

struct bytesResponse{
    1:i32 code,
    2:string msg,
    3:binary res,
}

struct mBytesResponse{
    1:i32 code,
    2:string msg,
    3:map<string,binary> res,
}

struct lBytesResponse{
    1:i32 code,
    2:string msg,
    3:list<binary> res,
}

struct Z {
    1:double score,
    2:string member,
}

struct ZResponse {
    1:i32 code,
    2:string msg,
    3:list<Z> res,
}

service redis_proxy{
    /*ping*/
    i32 Ping(1:i32 echo)

    i32 redis_proxy_ping(1:i32 echo)

    /*string*/

    //GetSet获取并更新value到key
    response GetSet(1:string appid, 2:string key, 3:string value)
    bytesResponse GetSetBytes(1:string appid, 2:string key, 3:binary value)

    //Set 将字符串值 value 关联到 key 。
    response Set(1:string appid, 2:string key, 3:string value, 4:i64 ttl)
    response SetBytes(1:string appid, 2:string key, 3:binary value, 4:i64 ttl)

    //SetNX 当key不存在时 将字符串值 value 关联到 key 。
    response SetNX(1:string appid, 2:string key, 3:string value, 4:i64 ttl)
    response SetNXBytes(1:string appid, 2:string key, 3:binary value, 4:i64 ttl)

    //Get 返回 key 所关联的字符串值。
    response Get(1:string appid, 2:string key)
    bytesResponse GetBytes(1:string appid, 2:string key)

    //MSet 同时设置一个或多个 key-value 对。
    response MSet(1:string appid, 2:map<string,string> reqs)
    response MSetBytes(1:string appid, 2:map<string,binary> reqs)

    //MGet 返回所有(一个或多个)给定 key 的值。
    mResponse MGet(1:string appid, 2:list<string> keys)
    mBytesResponse MGetBytes(1:string appid, 2:list<string> keys)

    /*keys*/
    //Del 删除给定的一个 key 。
    intResponse Del(1:string appid, 2:string key)

    //MDel 删除给定的多个 key 。
    intResponse MDel(1:string appid, 2:list<string> keys)

    //Keys 查找所有符合给定模式 pattern 的 key 。
    lResponse Keys(1:string appid, 2:string key)

    //Expire 为给定 key 设置生存时间，当 key 过期时(生存时间为 0 )，它会被自动删除。单位是秒
    response Expire(1:string appid, 2:string key, 3:i64 ttl)

    //TTL 以秒为单位，返回给定 key 的剩余生存时间(TTL, time to live)
    intResponse TTL(1:string appid, 2:string key)

    response Exists(1:string appid, 2:string key)

    /*Hash*/
    //HSet 将哈希表 key 中的域 field 的值设为 value 。
    response HSet(1:string appid, 2:string key, 3:string field, 4:string value)
    response HSetBytes(1:string appid, 2:string key, 3:string field, 4:binary value)

    //HSetNX 当可以不存在时， 将哈希表 key 中的域 field 的值设为 value 
    response HSetNX(1:string appid, 2:string key, 3:string field, 4:string value)
    response HSetNXBytes(1:string appid, 2:string key, 3:string field, 4:string value)

    //HGet 返回哈希表 key 中给定域 field 的值
    response HGet(1:string appid, 2:string key, 3:string field)
    bytesResponse HGetBytes(1:string appid, 2:string key, 3:string field)

    response HMSet(1:string appid, 2:string key, 3:map<string,string> fields)
    response HMSetBytes(1:string appid, 2:string key, 3:map<string,binary> fields)

    mResponse HMGet(1:string appid, 2:string key, 3:list<string> fields)
    mBytesResponse HMGetBytes(1:string appid, 2:string key, 3:list<string> fields)

    mResponse HGetAll(1:string appid, 2:string key)

    intResponse HDel(1:string appid, 2:string key, 3:string field)

    intResponse MHDel(1:string appid, 2:string key, 3:list<string> fields)

    lResponse HKeys(1:string appid, 2:string key)

    lResponse HVals(1:string appid, 2:string key)

    intResponse HIncrBy(1:string appid, 2:string key, 3:string field, 4:i64 inrc)

    floatResponse HIncrByFloat(1:string appid, 2:string key, 3:string field, 4:double inrc)

    response HExists(1:string appid, 2:string key, 3:string field)

    /*sortedSet*/
    //ZAdd 将一个 member 元素及其 score 值加入到有序集 key 当中
    intResponse ZAdd(1:string appid, 2:string key, 3:Z member)

    //MZAdd 将多个 member 元素及其 score 值加入到有序集 key 当中
    intResponse MZAdd(1:string appid, 2:string key, 3:list<Z> members)

    //ZCard 返回有序集 key 的基数。
    intResponse ZCard(1:string appid, 2:string key)

    //ZCount 返回有序集 key 中， score 值在 min 和 max 之间(默认包括 score 值等于 min 或 max )的成员的数量，
    //默认闭区间开一在min和max前添加 ( 来使用开区间，min 和 max 可以是 -inf 和 +inf
    intResponse ZCount(1:string appid, 2:string key, 3:string min, 4:string max)//支持-inf,+inf 和 (

    //ZRangeWithScores  ZRANGE ××× WITHSCORES 返回有序集 key 中成员 member 的排名。其中有序集成员按 score 值递增(从小到大)顺序排列。
    //区间分别以下标参数 start 和 stop 指出，包含 start 和 stop 在内。
    //下标参数 start 和 stop 都以 0 为底，也就是说，以 0 表示有序集第一个成员，以 1 表示有序集第二个成员，以此类推。
    //你也可以使用负数下标，以 -1 表示最后一个成员， -2 表示倒数第二个成员，以此类推。
    ZResponse ZRangeWithScores(1:string appid, 2:string key, 3:i64 start, 4:i64 stop)

    //ZRangeByScoreWithScores  ZRANGEBYSCORE key min max WITHSCORES LIMIT offset count	返回有序集 key 中，所有 score 值介于 min 和 max 之间(包括等于 min 或 max )的成员。
    //有序集成员按 score 值递增(从小到大)次序排列。
    //默认闭区间开一在min和max前添加 ( 来使用开区间，min 和 max 可以是 -inf 和 +inf
    //offset和count置0为无效
    ZResponse ZRangeByScoreWithScores(1:string appid, 2:string key, 3:string min, 4:string max, 5:i64 offset, 6:i64 count)

    //ZRem 移除有序集 key 中的一个成员，不存在的成员将被忽略。
    intResponse ZRem(1:string appid, 2:string key, 3:string member)

    //MZRem 移除有序集 key 中的多个成员，不存在的成员将被忽略。
    intResponse MZRem(1:string appid, 2:string key, 3:list<string> members)

    //ZRemRangeByRank 移除有序集 key 中，指定排名(rank)区间内的所有成员。
    //区间分别以下标参数 start 和 stop 指出，包含 start 和 stop 在内。
    //下标参数 start 和 stop 都以 0 为底，也就是说，以 0 表示有序集第一个成员，以 1 表示有序集第二个成员，以此类推。
    //你也可以使用负数下标，以 -1 表示最后一个成员， -2 表示倒数第二个成员，以此类推。
    intResponse ZRemRangeByRank(1:string appid, 2:string key, 3:i64 start, 4:i64 stop)
    
    //ZRemRangeByScore 移除有序集 key 中，所有 score 值介于 min 和 max 之间(包括等于 min 或 max )的成员。
    //有序集成员按 score 值递增(从小到大)次序排列。
    //默认闭区间开一在min和max前添加 ( 来使用开区间，min 和 max 可以是 -inf 和 +inf
    intResponse ZRemRangeByScore(1:string appid, 2:string key, 3:string min, 4:string max)

    //ZIncrBy 为有序集 key 的成员 member 的 score 值加上增量 increment 。可以为负数
    floatResponse ZIncrBy(1:string appid, 2:string key, 3:double increment, 4:string member)

    //ZScore 返回有序集 key 中，成员 member 的 score 值。
    floatResponse ZScore(1:string appid, 2:string key, 3:string member)

    /* set*/
    intResponse SAdd(1:string appid, 2:string key, 3:string member)

    intResponse MSAdd(1:string appid, 2:string key, 3:list<string> members)

    intResponse SCard(1:string appid, 2:string key)

    response SIsMember(1:string appid, 2:string key, 3:string member)

    lResponse SMembers(1:string appid, 2:string key)

    response SRandMember(1:string appid, 2:string key)

    lResponse SRandMemberN(1:string appid, 2:string key, 3:i64 n)

    intResponse SRem(1:string appid, 2:string key, 3:string member)

    intResponse MSRem(1:string appid, 2:string key, 3:list<string> member)

    /*list*/

    //将指定的值插入到存于 key 的列表的头部。如果 key 不存在，那么在进行 push 操作前会创建一个空列表
    intResponse LPush(1:string appid, 2:string key, 3:string value)
    intResponse LPushByte(1:string appid, 2:string key, 3:binary value)

    //向存于 key 的列表的尾部插入指定的值。如果 key 不存在，那么会创建一个空的列表然后再进行 push 操作
    intResponse RPush(1:string appid, 2:string key, 3:string value)
    intResponse RPushByte(1:string appid, 2:string key, 3:binary value)

    //将多个指定的值插入到存于 key 的列表的头部。如果 key 不存在，那么在进行 push 操作前会创建一个空列表
    intResponse MLPush(1:string appid, 2:string key, 3:list<string> value)
    intResponse MLPushWithTTL(1:string appid, 2:string key, 3:list<string> value, 4:i64 ttl)
    intResponse MLPushByteWithTTL(1:string appid, 2:string key, 3:list<binary> value, 4:i64 ttl)

    //向存于 key 的列表的尾部插入多个指定的值。如果 key 不存在，那么会创建一个空的列表然后再进行 push 操作
    intResponse MRPushWithTTL(1:string appid, 2:string key, 3:list<string> value, 4:i64 ttl)
    intResponse MRPushByteWithTTL(1:string appid, 2:string key, 3:list<binary> value, 4:i64 ttl)

    //移除并且返回 key 对应的 list 的第一个元素。
    response LPop(1:string appid, 2:string key)
    bytesResponse LPopByte(1:string appid, 2:string key)

    //移除并返回存于 key 的 list 的最后一个元素。
    response RPop(1:string appid, 2:string key)
    bytesResponse RPopByte(1:string appid, 2:string key)

    intResponse LInsertBefore(1:string appid, 2:string key, 3:string pivot, 4:string value)

    intResponse LInsertAfter(1:string appid, 2:string key, 3:string pivot, 4:string value)

    //返回存储在 key 里的list的长度。 如果 key 不存在，那么就被看作是空list，并且返回长度为 0。
    //当存储在 key 里的值不是一个list的话，会返回error。
    intResponse LLen(1:string appid, 2:string key)

    //LLen的多参数版本
    mIntResponse MLLen(1:string appid, 2:list<string> keys)

    response LSet(1:string appid, 2:string key, 3:i64 index, 4:string value)

    response LIndex(1:string appid, 2:string key, 3:i64 index)

    lResponse LRange(1:string appid, 2:string key, 3:i64 start, 4:i64 stop)

    // 修剪(trim)一个已存在的 list，这样 list 就会只包含指定范围的指定元素。
    // start 和 stop 都是由0开始计数的， 这里的 0 是列表里的第一个元素（表头），1 是第二个元素，以此类推。
    response LTrim(1:string appid, 2:string key, 3:i64 start, 4:i64 stop)

    /*cluster*/
    response ClusterNodes(1:string appid)

    response ClusterInfo(1:string appid)

    /*server*/
    mResponse Info(1:string appid, 2:list<string> sections)
    response NodeInfo(1:string appid, 2:string addr, 3:list<string> sections)
}