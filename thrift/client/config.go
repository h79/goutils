package client

type Config struct {
	Target   string `json:"target"`
	Protocol string `json:"protocol"`
}
