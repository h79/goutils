package client

import (
	"fmt"
	"gitee.com/h79/goutils/common/server"
	"gitee.com/h79/goutils/common/system"
	"gitee.com/h79/goutils/thrift/resolver"
	"gitee.com/h79/goutils/thrift/thriftutil"
	"github.com/apache/thrift/lib/go/thrift"
	"sync"
)

var TransInvalidateERR = fmt.Errorf("transport invalidate")

type Client struct {
	target       string
	parsedTarget resolver.Target

	firstResolveEvent *system.Event
	mu                sync.RWMutex
	resolverWrapper   *ccResolverWrapper

	tpf        thrift.TProtocolFactory
	tTransport thrift.TTransport
	tClient    thrift.TClient
}

func newClient(tpf thrift.TProtocolFactory, target string) (*Client, error) {
	cli := &Client{
		tpf:    tpf,
		target: target,
	}
	if err := cli.newTransport(); err != nil {
		return nil, err
	}
	return cli, nil
}

func (c *Client) Open() error {
	if c.tTransport != nil {
		return c.tTransport.Open()
	}
	return c.build()
}

func (c *Client) Close() error {
	if c.tTransport != nil {
		return c.tTransport.Close()
	}
	return TransInvalidateERR
}

func (c *Client) IsOpen() bool {
	if c.tTransport != nil {
		return c.tTransport.IsOpen()
	}
	return false
}

func (c *Client) FramedTransport() {
	if c.tTransport != nil {
		c.tTransport = thrift.NewTFramedTransport(c.tTransport)
	}
}

func (c *Client) SetHeader(headers map[string]string) {
	if len(headers) == 0 {
		panic(fmt.Errorf(""))
		return
	}
	if httpTrans, ok := c.tTransport.(*thrift.THttpClient); ok {
		for key, value := range headers {
			httpTrans.SetHeader(key, value)
		}
	}
}

func (c *Client) In() thrift.TProtocol {
	if c.tTransport != nil {
		return nil
	}
	return c.tpf.GetProtocol(c.tTransport)
}

func (c *Client) Out() thrift.TProtocol {
	return c.In()
}

func (c *Client) TClient() thrift.TClient {
	if c.tTransport != nil {
		return thrift.NewTStandardClient(c.In(), c.Out())
	}
	return nil
}

func (c *Client) getResolver(scheme string) resolver.Builder {
	if scheme == "" {
		return nil
	}
	return resolver.Get(scheme)
}

func (c *Client) build() error {
	return c.newTransport()
}

func (c *Client) newTransport() error {

	c.parsedTarget = thriftutil.ParseTarget(c.target)
	builder := c.getResolver(c.parsedTarget.Scheme)
	if builder == nil {
		addr, er := server.Parse(c.target)
		if er != nil {
			return er
		}
		var err error
		var trans thrift.TTransport

		if addr.IsHttp() {
			trans, err = thrift.NewTHttpClient(addr.To())
		} else if (addr.HasIP() || addr.HasDomain()) && addr.HasPort() {
			trans, err = thrift.NewTSocket(addr.To())
		}
		if err != nil {
			return err
		}
		c.tTransport = trans
	} else if c.resolverWrapper == nil {
		resolverWrapper, err := newCCResolverWrapper(c, builder)
		if err != nil {
			return err
		}
		c.mu.Lock()
		c.resolverWrapper = resolverWrapper
		c.mu.Unlock()
	} else {
		c.mu.Lock()
		r := c.resolverWrapper
		c.mu.Unlock()
		r.resolveNow()
	}
	return nil
}

func (c *Client) updateResolverState(s resolver.State, err error) error {
	return nil
}
