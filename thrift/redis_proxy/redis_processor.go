package redis_proxy

import (
	"context"
	"github.com/apache/thrift/lib/go/thrift"
)

type processorBase struct {
	cmd string
}

func (p *processorBase) Read(ctx context.Context, in, out thrift.TProtocol, args thrift.TStruct, seqId int32) (success bool, err thrift.TException) {
	var err2 error
	if err2 = args.Read(ctx, in); err2 != nil {
		in.ReadMessageEnd(ctx)
		exc := thrift.NewTApplicationException(thrift.PROTOCOL_ERROR, err2.Error())
		in.WriteMessageBegin(ctx, p.cmd, thrift.EXCEPTION, seqId)
		exc.Write(ctx, in)
		in.WriteMessageEnd(ctx)
		in.Flush(ctx)
		return false, thrift.WrapTException(err2)
	}
	in.ReadMessageEnd(ctx)
	return true, nil
}

func (p *processorBase) CmdException(ctx context.Context, in, out thrift.TProtocol, seqId int32, err2 error) (success bool, err thrift.TException) {
	x := thrift.NewTApplicationException(thrift.INTERNAL_ERROR, "Internal error processing "+p.cmd+": "+err2.Error())
	in.WriteMessageBegin(ctx, p.cmd, thrift.EXCEPTION, seqId)
	x.Write(ctx, in)
	in.WriteMessageEnd(ctx)
	in.Flush(ctx)
	return false, thrift.WrapTException(err2)
}

func (p *processorBase) Cmd(ctx context.Context, in, out thrift.TProtocol, result thrift.TStruct, seqId int32) (success bool, err thrift.TException) {
	var err2 error
	if err2 = in.WriteMessageBegin(ctx, p.cmd, thrift.REPLY, seqId); err2 != nil {
		err = thrift.WrapTException(err2)
	}
	if err2 = result.Write(ctx, in); err == nil && err2 != nil {
		err = thrift.WrapTException(err2)
	}
	if err2 = in.WriteMessageEnd(ctx); err == nil && err2 != nil {
		err = thrift.WrapTException(err2)
	}
	if err2 = in.Flush(ctx); err == nil && err2 != nil {
		err = thrift.WrapTException(err2)
	}
	if err != nil {
		return
	}
	return true, err
}

type processorPing struct {
	processorBase
	handler RedisProxy
}

func (p *processorPing) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {

	args := PingArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := PingResult{}
	if ret, err2 := p.handler.Ping(ctx, args.Echo); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = &ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorRedisProxyPing struct {
	processorBase
	handler RedisProxy
}

func (p *processorRedisProxyPing) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := RedisProxyPingArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}

	result := RedisProxyPingResult{}
	if ret, err2 := p.handler.Ping(ctx, args.Echo); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = &ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorGetSet struct {
	processorBase
	handler RedisProxy
}

func (p *processorGetSet) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := GetSetArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}

	result := GetSetResult{}
	if ret, err2 := p.handler.GetSet(ctx, args.Appid, args.Key, args.Value); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorGetSetBytes struct {
	processorBase
	handler RedisProxy
}

func (p *processorGetSetBytes) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := GetSetBytesArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}

	result := GetSetBytesResult{}

	if ret, err2 := p.handler.GetSetBytes(ctx, args.Appid, args.Key, args.Value); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorSet struct {
	processorBase
	handler RedisProxy
}

func (p *processorSet) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := SetArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}

	result := SetResult{}
	if ret, err2 := p.handler.Set(ctx, args.Appid, args.Key, args.Value, args.TTL); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorSetBytes struct {
	processorBase
	handler RedisProxy
}

func (p *processorSetBytes) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := SetBytesArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}

	result := RedisProxySetBytesResult{}
	if ret, err2 := p.handler.SetBytes(ctx, args.Appid, args.Key, args.Value, args.TTL); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorSetNX struct {
	processorBase
	handler RedisProxy
}

func (p *processorSetNX) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := SetNXArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := SetNXResult{}
	if ret, err2 := p.handler.SetNX(ctx, args.Appid, args.Key, args.Value, args.TTL); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorSetNXBytes struct {
	processorBase
	handler RedisProxy
}

func (p *processorSetNXBytes) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := SetNXBytesArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}

	result := SetNXBytesResult{}

	if ret, err2 := p.handler.SetNXBytes(ctx, args.Appid, args.Key, args.Value, args.TTL); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorGet struct {
	processorBase
	handler RedisProxy
}

func (p *processorGet) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := GetArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}

	result := GetResult{}
	if ret, err2 := p.handler.Get(ctx, args.Appid, args.Key); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorGetBytes struct {
	processorBase
	handler RedisProxy
}

func (p *processorGetBytes) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := GetBytesArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}

	result := GetBytesResult{}
	if ret, err2 := p.handler.GetBytes(ctx, args.Appid, args.Key); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorMSet struct {
	processorBase
	handler RedisProxy
}

func (p *processorMSet) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := MSetArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}

	result := MSetResult{}
	if ret, err2 := p.handler.MSet(ctx, args.Appid, args.Reqs); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorMSetBytes struct {
	processorBase
	handler RedisProxy
}

func (p *processorMSetBytes) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := RedisProxyMSetBytesArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}

	result := MSetBytesResult{}
	if ret, err2 := p.handler.MSetBytes(ctx, args.Appid, args.Reqs); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorMGet struct {
	processorBase
	handler RedisProxy
}

func (p *processorMGet) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := MGetArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}

	result := MGetResult{}
	if ret, err2 := p.handler.MGet(ctx, args.Appid, args.Keys); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorMGetBytes struct {
	processorBase
	handler RedisProxy
}

func (p *processorMGetBytes) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := MGetBytesArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}

	result := MGetBytesResult{}
	if ret, err2 := p.handler.MGetBytes(ctx, args.Appid, args.Keys); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorDel struct {
	processorBase
	handler RedisProxy
}

func (p *processorDel) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := DelArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}

	result := DelResult{}
	if ret, err2 := p.handler.Del(ctx, args.Appid, args.Key); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorMDel struct {
	processorBase
	handler RedisProxy
}

func (p *processorMDel) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := MDelArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}

	result := MDelResult{}
	if ret, err2 := p.handler.MDel(ctx, args.Appid, args.Keys); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorKeys struct {
	processorBase
	handler RedisProxy
}

func (p *processorKeys) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := KeysArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}

	result := KeysResult{}
	if ret, err2 := p.handler.Keys(ctx, args.Appid, args.Key); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorExpire struct {
	processorBase
	handler RedisProxy
}

func (p *processorExpire) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := ExpireArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}

	result := ExpireResult{}
	if ret, err2 := p.handler.Expire(ctx, args.Appid, args.Key, args.TTL); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorTTL struct {
	processorBase
	handler RedisProxy
}

func (p *processorTTL) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := TtlArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := TtlResult{}
	if ret, err2 := p.handler.TTL(ctx, args.Appid, args.Key); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorExists struct {
	processorBase
	handler RedisProxy
}

func (p *processorExists) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := ExistsArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := ExistsResult{}
	if ret, err2 := p.handler.Exists(ctx, args.Appid, args.Key); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorHSet struct {
	processorBase
	handler RedisProxy
}

func (p *processorHSet) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := HSetArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := HSetResult{}
	if ret, err2 := p.handler.HSet(ctx, args.Appid, args.Key, args.Field, args.Value); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorHSetBytes struct {
	processorBase
	handler RedisProxy
}

func (p *processorHSetBytes) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := HSetBytesArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := HSetBytesResult{}
	if ret, err2 := p.handler.HSetBytes(ctx, args.Appid, args.Key, args.Field, args.Value); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorHSetNX struct {
	processorBase
	handler RedisProxy
}

func (p *processorHSetNX) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := HSetNXArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := HSetNXResult{}
	if ret, err2 := p.handler.HSetNX(ctx, args.Appid, args.Key, args.Field, args.Value); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorHSetNXBytes struct {
	processorBase
	handler RedisProxy
}

func (p *processorHSetNXBytes) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := HSetNXBytesArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := HSetNXBytesResult{}
	if ret, err2 := p.handler.HSetNXBytes(ctx, args.Appid, args.Key, args.Field, args.Value); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorHGet struct {
	processorBase
	handler RedisProxy
}

func (p *processorHGet) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := ProxyHGetArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}

	result := HGetResult{}
	if ret, err2 := p.handler.HGet(ctx, args.Appid, args.Key, args.Field); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorHGetBytes struct {
	processorBase
	handler RedisProxy
}

func (p *processorHGetBytes) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := RedisProxyHGetBytesArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := HGetBytesResult{}
	if ret, err2 := p.handler.HGetBytes(ctx, args.Appid, args.Key, args.Field); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorHMSet struct {
	processorBase
	handler RedisProxy
}

func (p *processorHMSet) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := HmSetArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := HmSetResult{}
	if ret, err2 := p.handler.HMSet(ctx, args.Appid, args.Key, args.Fields); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorHMSetBytes struct {
	processorBase
	handler RedisProxy
}

func (p *processorHMSetBytes) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := HmSetBytesArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := HmSetBytesResult{}
	if ret, err2 := p.handler.HMSetBytes(ctx, args.Appid, args.Key, args.Fields); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorHMGet struct {
	processorBase
	handler RedisProxy
}

func (p *processorHMGet) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := HmGetArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := HMGetResult{}
	if ret, err2 := p.handler.HMGet(ctx, args.Appid, args.Key, args.Fields); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorHMGetBytes struct {
	processorBase
	handler RedisProxy
}

func (p *processorHMGetBytes) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := HMGetBytesArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := HMGetBytesResult{}
	if ret, err2 := p.handler.HMGetBytes(ctx, args.Appid, args.Key, args.Fields); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorHGetAll struct {
	processorBase
	handler RedisProxy
}

func (p *processorHGetAll) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := HGetAllArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := HGetAllResult{}
	if ret, err2 := p.handler.HGetAll(ctx, args.Appid, args.Key); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorHDel struct {
	processorBase
	handler RedisProxy
}

func (p *processorHDel) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := HDelArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := HDelResult{}
	if ret, err2 := p.handler.HDel(ctx, args.Appid, args.Key, args.Field); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorMHDel struct {
	processorBase
	handler RedisProxy
}

func (p *processorMHDel) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := MhDelArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := MhDelResult{}
	if ret, err2 := p.handler.MHDel(ctx, args.Appid, args.Key, args.Fields); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorHKeys struct {
	processorBase
	handler RedisProxy
}

func (p *processorHKeys) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := HKeysArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := HKeysResult{}
	if ret, err2 := p.handler.HKeys(ctx, args.Appid, args.Key); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorHVals struct {
	processorBase
	handler RedisProxy
}

func (p *processorHVals) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := HValuesArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := HValuesResult{}
	if ret, err2 := p.handler.HVals(ctx, args.Appid, args.Key); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorHIncrBy struct {
	processorBase
	handler RedisProxy
}

func (p *processorHIncrBy) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := HIncrByArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := HIncrByResult{}
	if ret, err2 := p.handler.HIncrBy(ctx, args.Appid, args.Key, args.Field, args.Inrc); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorHIncrByFloat struct {
	processorBase
	handler RedisProxy
}

func (p *processorHIncrByFloat) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := HIncrByFloatArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := HIncrByFloatResult{}
	if ret, err2 := p.handler.HIncrByFloat(ctx, args.Appid, args.Key, args.Field, args.Inrc); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorHExists struct {
	processorBase
	handler RedisProxy
}

func (p *processorHExists) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := HExistsArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := HExistsResult{}
	if ret, err2 := p.handler.HExists(ctx, args.Appid, args.Key, args.Field); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorZAdd struct {
	processorBase
	handler RedisProxy
}

func (p *processorZAdd) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := ZAddArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := ZAddResult{}
	if ret, err2 := p.handler.ZAdd(ctx, args.Appid, args.Key, args.Member); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorMZAdd struct {
	processorBase
	handler RedisProxy
}

func (p *processorMZAdd) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := MzAddArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := MzAddResult{}
	if ret, err2 := p.handler.MZAdd(ctx, args.Appid, args.Key, args.Members); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorZCard struct {
	processorBase
	handler RedisProxy
}

func (p *processorZCard) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := ZCardArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := ZCardResult{}
	if ret, err2 := p.handler.ZCard(ctx, args.Appid, args.Key); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorZCount struct {
	processorBase
	handler RedisProxy
}

func (p *processorZCount) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := ZCountArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := ZCountResult{}
	if ret, err2 := p.handler.ZCount(ctx, args.Appid, args.Key, args.Min, args.Max); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorZRangeWithScores struct {
	processorBase
	handler RedisProxy
}

func (p *processorZRangeWithScores) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := ZRangeWithScoresArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := ZRangeWithScoresResult{}
	if ret, err2 := p.handler.ZRangeWithScores(ctx, args.Appid, args.Key, args.Start, args.Stop); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorZRangeByScoreWithScores struct {
	processorBase
	handler RedisProxy
}

func (p *processorZRangeByScoreWithScores) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := ZRangeByScoreWithScoresArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := ZRangeByScoreWithScoresResult{}
	if ret, err2 := p.handler.ZRangeByScoreWithScores(ctx, args.Appid, args.Key, args.Min, args.Max, args.Offset, args.Count); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorZRem struct {
	processorBase
	handler RedisProxy
}

func (p *processorZRem) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := ZRemArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := ZRemResult{}
	if ret, err2 := p.handler.ZRem(ctx, args.Appid, args.Key, args.Member); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorMZRem struct {
	processorBase
	handler RedisProxy
}

func (p *processorMZRem) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := MzRemArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := MzRemResult{}
	if ret, err2 := p.handler.MZRem(ctx, args.Appid, args.Key, args.Members); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorZRemRangeByRank struct {
	processorBase
	handler RedisProxy
}

func (p *processorZRemRangeByRank) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := ZRemRangeByRankArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := ZRemRangeByRankResult{}
	if ret, err2 := p.handler.ZRemRangeByRank(ctx, args.Appid, args.Key, args.Start, args.Stop); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorZRemRangeByScore struct {
	processorBase
	handler RedisProxy
}

func (p *processorZRemRangeByScore) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := ZRemRangeByScoreArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := ZRemRangeByScoreResult{}
	if ret, err2 := p.handler.ZRemRangeByScore(ctx, args.Appid, args.Key, args.Min, args.Max); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorZIncrBy struct {
	processorBase
	handler RedisProxy
}

func (p *processorZIncrBy) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := ZIncrByArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := ZIncrByResult{}
	if ret, err2 := p.handler.ZIncrBy(ctx, args.Appid, args.Key, args.Increment, args.Member); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorZScore struct {
	processorBase
	handler RedisProxy
}

func (p *processorZScore) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := ZScoreArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := ZScoreResult{}
	if ret, err2 := p.handler.ZScore(ctx, args.Appid, args.Key, args.Member); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorSAdd struct {
	processorBase
	handler RedisProxy
}

func (p *processorSAdd) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := SAddArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := SAddResult{}
	if ret, err2 := p.handler.SAdd(ctx, args.Appid, args.Key, args.Member); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorMSAdd struct {
	processorBase
	handler RedisProxy
}

func (p *processorMSAdd) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {

	args := MsAddArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := MsAddResult{}
	if ret, err2 := p.handler.MSAdd(ctx, args.Appid, args.Key, args.Members); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorSCard struct {
	processorBase
	handler RedisProxy
}

func (p *processorSCard) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := SCardArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := SCardResult{}
	if ret, err2 := p.handler.SCard(ctx, args.Appid, args.Key); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorSIsMember struct {
	processorBase
	handler RedisProxy
}

func (p *processorSIsMember) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := SIsMemberArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := SIsMemberResult{}
	if ret, err2 := p.handler.SIsMember(ctx, args.Appid, args.Key, args.Member); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorSMembers struct {
	processorBase
	handler RedisProxy
}

func (p *processorSMembers) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := SMembersArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := SMembersResult{}
	if ret, err2 := p.handler.SMembers(ctx, args.Appid, args.Key); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorSRandMember struct {
	processorBase
	handler RedisProxy
}

func (p *processorSRandMember) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := SRandMemberArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := SRandMemberResult{}
	if ret, err2 := p.handler.SRandMember(ctx, args.Appid, args.Key); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorSRandMemberN struct {
	processorBase
	handler RedisProxy
}

func (p *processorSRandMemberN) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := SRandMemberNArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := SRandMemberNResult{}
	if ret, err2 := p.handler.SRandMemberN(ctx, args.Appid, args.Key, args.N); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorSRem struct {
	processorBase
	handler RedisProxy
}

func (p *processorSRem) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := SRemArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := SRemResult{}
	if ret, err2 := p.handler.SRem(ctx, args.Appid, args.Key, args.Member); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorMSRem struct {
	processorBase
	handler RedisProxy
}

func (p *processorMSRem) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := MsRemArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := MsRemResult{}
	if ret, err2 := p.handler.MSRem(ctx, args.Appid, args.Key, args.Member); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorLPush struct {
	processorBase
	handler RedisProxy
}

func (p *processorLPush) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := LPushArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := LPushResult{}
	if ret, err2 := p.handler.LPush(ctx, args.Appid, args.Key, args.Value); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorLPushByte struct {
	processorBase
	handler RedisProxy
}

func (p *processorLPushByte) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := LPushByteArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := LPushByteResult{}
	if ret, err2 := p.handler.LPushByte(ctx, args.Appid, args.Key, args.Value); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorRPush struct {
	processorBase
	handler RedisProxy
}

func (p *processorRPush) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := RPushArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := RPushResult{}
	if ret, err2 := p.handler.RPush(ctx, args.Appid, args.Key, args.Value); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorRPushByte struct {
	processorBase
	handler RedisProxy
}

func (p *processorRPushByte) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := RPushByteArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := RPushByteResult{}
	if ret, err2 := p.handler.RPushByte(ctx, args.Appid, args.Key, args.Value); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorMLPush struct {
	processorBase
	handler RedisProxy
}

func (p *processorMLPush) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := MlPushArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := MlPushResult{}
	if ret, err2 := p.handler.MLPush(ctx, args.Appid, args.Key, args.Value); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorMLPushWithTTL struct {
	processorBase
	handler RedisProxy
}

func (p *processorMLPushWithTTL) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := MlPushWithTTLArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := MlPushWithTTLResult{}
	if ret, err2 := p.handler.MLPushWithTTL(ctx, args.Appid, args.Key, args.Value, args.TTL); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorMLPushByteWithTTL struct {
	processorBase
	handler RedisProxy
}

func (p *processorMLPushByteWithTTL) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := MlPushByteWithTTLArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := MlPushByteWithTTLResult{}
	if ret, err2 := p.handler.MLPushByteWithTTL(ctx, args.Appid, args.Key, args.Value, args.TTL); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorMRPushWithTTL struct {
	processorBase
	handler RedisProxy
}

func (p *processorMRPushWithTTL) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := MrPushWithTTLArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := MrPushWithTTLResult{}
	if ret, err2 := p.handler.MRPushWithTTL(ctx, args.Appid, args.Key, args.Value, args.TTL); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorMRPushByteWithTTL struct {
	processorBase
	handler RedisProxy
}

func (p *processorMRPushByteWithTTL) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := MrPushByteWithTTLArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := MrPushByteWithTTLResult{}
	if ret, err2 := p.handler.MRPushByteWithTTL(ctx, args.Appid, args.Key, args.Value, args.TTL); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorLPop struct {
	processorBase
	handler RedisProxy
}

func (p *processorLPop) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := ProxyLPopArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := LPopResult{}
	if ret, err2 := p.handler.LPop(ctx, args.Appid, args.Key); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorLPopByte struct {
	processorBase
	handler RedisProxy
}

func (p *processorLPopByte) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := LPopByteArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := LPopByteResult{}
	if ret, err2 := p.handler.LPopByte(ctx, args.Appid, args.Key); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorRPop struct {
	processorBase
	handler RedisProxy
}

func (p *processorRPop) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := RPopArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := RPopResult{}
	if ret, err2 := p.handler.RPop(ctx, args.Appid, args.Key); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorRPopByte struct {
	processorBase
	handler RedisProxy
}

func (p *processorRPopByte) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := RPopByteArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := RPopByteResult{}
	if ret, err2 := p.handler.RPopByte(ctx, args.Appid, args.Key); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorLInsertBefore struct {
	processorBase
	handler RedisProxy
}

func (p *processorLInsertBefore) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := LInsertBeforeArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := LInsertBeforeResult{}
	if ret, err2 := p.handler.LInsertBefore(ctx, args.Appid, args.Key, args.Pivot, args.Value); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorLInsertAfter struct {
	processorBase
	handler RedisProxy
}

func (p *processorLInsertAfter) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := LInsertAfterArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := LInsertAfterResult{}
	if ret, err2 := p.handler.LInsertAfter(ctx, args.Appid, args.Key, args.Pivot, args.Value); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorLLen struct {
	processorBase
	handler RedisProxy
}

func (p *processorLLen) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := LLenArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := LLenResult{}
	if ret, err2 := p.handler.LLen(ctx, args.Appid, args.Key); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorMLLen struct {
	processorBase
	handler RedisProxy
}

func (p *processorMLLen) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := MlLenArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := MlLenResult{}
	if ret, err2 := p.handler.MLLen(ctx, args.Appid, args.Keys); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorLSet struct {
	processorBase
	handler RedisProxy
}

func (p *processorLSet) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := LSetArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := LSetResult{}
	if ret, err2 := p.handler.LSet(ctx, args.Appid, args.Key, args.Index, args.Value); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorLIndex struct {
	processorBase
	handler RedisProxy
}

func (p *processorLIndex) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := LIndexArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := LIndexResult{}
	if ret, err2 := p.handler.LIndex(ctx, args.Appid, args.Key, args.Index); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorLRange struct {
	processorBase
	handler RedisProxy
}

func (p *processorLRange) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := LRangeArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := LRangeResult{}
	if ret, err2 := p.handler.LRange(ctx, args.Appid, args.Key, args.Start, args.Stop); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorLTrim struct {
	processorBase
	handler RedisProxy
}

func (p *processorLTrim) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := LTrimArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := LTrimResult{}
	if ret, err2 := p.handler.LTrim(ctx, args.Appid, args.Key, args.Start, args.Stop); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorClusterNodes struct {
	processorBase
	handler RedisProxy
}

func (p *processorClusterNodes) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := ClusterNodesArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := ClusterNodesResult{}
	if ret, err2 := p.handler.ClusterNodes(ctx, args.Appid); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorClusterInfo struct {
	processorBase
	handler RedisProxy
}

func (p *processorClusterInfo) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := ClusterInfoArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := ClusterInfoResult{}
	if ret, err2 := p.handler.ClusterInfo(ctx, args.Appid); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorInfo struct {
	processorBase
	handler RedisProxy
}

func (p *processorInfo) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := InfoArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := InfoResult{}
	if ret, err2 := p.handler.Info(ctx, args.Appid, args.Sections); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}

type processorNodeInfo struct {
	processorBase
	handler RedisProxy
}

func (p *processorNodeInfo) Process(ctx context.Context, seqId int32, in, out thrift.TProtocol) (success bool, err thrift.TException) {
	args := NodeInfoArgs{}
	if _, err := p.Read(ctx, in, out, &args, seqId); err != nil {
		return false, err
	}
	result := NodeInfoResult{}
	if ret, err2 := p.handler.NodeInfo(ctx, args.Appid, args.Addr, args.Sections); err2 != nil {
		return p.CmdException(ctx, in, out, seqId, err2)
	} else {
		result.Success = ret
	}
	return p.Cmd(ctx, in, out, &result, seqId)
}
