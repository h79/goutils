package redis_proxy

import (
	"context"
	"github.com/apache/thrift/lib/go/thrift"
)

type RedisProxyClient struct {
	c thrift.TClient
}

func NewRedisProxyClientFactory(t thrift.TTransport, f thrift.TProtocolFactory) *RedisProxyClient {
	return &RedisProxyClient{
		c: thrift.NewTStandardClient(f.GetProtocol(t), f.GetProtocol(t)),
	}
}

func NewRedisProxyClientProtocol(t thrift.TTransport, inTp thrift.TProtocol, outTp thrift.TProtocol) *RedisProxyClient {
	return &RedisProxyClient{
		c: thrift.NewTStandardClient(inTp, outTp),
	}
}

func NewRedisProxyClient(c thrift.TClient) *RedisProxyClient {
	return &RedisProxyClient{
		c: c,
	}
}

func (p *RedisProxyClient) Client_() thrift.TClient {
	return p.c
}

// Parameters:
//  - Echo
func (p *RedisProxyClient) Ping(ctx context.Context, echo int32) (r int32, err error) {
	var _args9 PingArgs
	_args9.Echo = echo
	var _result10 PingResult
	if _, err = p.Client_().Call(ctx, "Ping", &_args9, &_result10); err != nil {
		return
	}
	return _result10.GetSuccess(), nil
}

// Parameters:
//  - Echo
func (p *RedisProxyClient) RedisProxyPing(ctx context.Context, echo int32) (r int32, err error) {
	var _args11 RedisProxyPingArgs
	_args11.Echo = echo
	var _result12 RedisProxyPingResult
	if _, err = p.Client_().Call(ctx, "redis_proxy_ping", &_args11, &_result12); err != nil {
		return
	}
	return _result12.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Value
func (p *RedisProxyClient) GetSet(ctx context.Context, appid string, key string, value string) (r *Response, err error) {
	var _args13 GetSetArgs
	_args13.Appid = appid
	_args13.Key = key
	_args13.Value = value
	var _result14 GetSetResult
	if _, err = p.Client_().Call(ctx, "GetSet", &_args13, &_result14); err != nil {
		return
	}
	return _result14.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Value
func (p *RedisProxyClient) GetSetBytes(ctx context.Context, appid string, key string, value []byte) (r *BytesResponse, err error) {
	var _args15 GetSetBytesArgs
	_args15.Appid = appid
	_args15.Key = key
	_args15.Value = value
	var _result16 GetSetBytesResult
	if _, err = p.Client_().Call(ctx, "GetSetBytes", &_args15, &_result16); err != nil {
		return
	}
	return _result16.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Value
//  - TTL
func (p *RedisProxyClient) Set(ctx context.Context, appid string, key string, value string, ttl int64) (r *Response, err error) {
	var _args17 SetArgs
	_args17.Appid = appid
	_args17.Key = key
	_args17.Value = value
	_args17.TTL = ttl
	var _result18 SetResult
	if _, err = p.Client_().Call(ctx, "Set", &_args17, &_result18); err != nil {
		return
	}
	return _result18.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Value
//  - TTL
func (p *RedisProxyClient) SetBytes(ctx context.Context, appid string, key string, value []byte, ttl int64) (r *Response, err error) {
	var _args19 SetBytesArgs
	_args19.Appid = appid
	_args19.Key = key
	_args19.Value = value
	_args19.TTL = ttl
	var _result20 RedisProxySetBytesResult
	if _, err = p.Client_().Call(ctx, "SetBytes", &_args19, &_result20); err != nil {
		return
	}
	return _result20.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Value
//  - TTL
func (p *RedisProxyClient) SetNX(ctx context.Context, appid string, key string, value string, ttl int64) (r *Response, err error) {
	var _args21 SetNXArgs
	_args21.Appid = appid
	_args21.Key = key
	_args21.Value = value
	_args21.TTL = ttl
	var _result22 SetNXResult
	if _, err = p.Client_().Call(ctx, "SetNX", &_args21, &_result22); err != nil {
		return
	}
	return _result22.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Value
//  - TTL
func (p *RedisProxyClient) SetNXBytes(ctx context.Context, appid string, key string, value []byte, ttl int64) (r *Response, err error) {
	var _args23 SetNXBytesArgs
	_args23.Appid = appid
	_args23.Key = key
	_args23.Value = value
	_args23.TTL = ttl
	var _result24 SetNXBytesResult
	if _, err = p.Client_().Call(ctx, "SetNXBytes", &_args23, &_result24); err != nil {
		return
	}
	return _result24.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
func (p *RedisProxyClient) Get(ctx context.Context, appid string, key string) (r *Response, err error) {
	var _args25 GetArgs
	_args25.Appid = appid
	_args25.Key = key
	var _result26 GetResult
	if _, err = p.Client_().Call(ctx, "Get", &_args25, &_result26); err != nil {
		return
	}
	return _result26.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
func (p *RedisProxyClient) GetBytes(ctx context.Context, appid string, key string) (r *BytesResponse, err error) {
	var _args27 GetBytesArgs
	_args27.Appid = appid
	_args27.Key = key
	var _result28 GetBytesResult
	if _, err = p.Client_().Call(ctx, "GetBytes", &_args27, &_result28); err != nil {
		return
	}
	return _result28.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Reqs
func (p *RedisProxyClient) MSet(ctx context.Context, appid string, reqs map[string]string) (r *Response, err error) {
	var _args29 MSetArgs
	_args29.Appid = appid
	_args29.Reqs = reqs
	var _result30 MSetResult
	if _, err = p.Client_().Call(ctx, "MSet", &_args29, &_result30); err != nil {
		return
	}
	return _result30.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Reqs
func (p *RedisProxyClient) MSetBytes(ctx context.Context, appid string, reqs map[string][]byte) (r *Response, err error) {
	var _args31 RedisProxyMSetBytesArgs
	_args31.Appid = appid
	_args31.Reqs = reqs
	var _result32 MSetBytesResult
	if _, err = p.Client_().Call(ctx, "MSetBytes", &_args31, &_result32); err != nil {
		return
	}
	return _result32.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Keys
func (p *RedisProxyClient) MGet(ctx context.Context, appid string, keys []string) (r *MResponse, err error) {
	var _args33 MGetArgs
	_args33.Appid = appid
	_args33.Keys = keys
	var _result34 MGetResult
	if _, err = p.Client_().Call(ctx, "MGet", &_args33, &_result34); err != nil {
		return
	}
	return _result34.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Keys
func (p *RedisProxyClient) MGetBytes(ctx context.Context, appid string, keys []string) (r *MBytesResponse, err error) {
	var _args35 MGetBytesArgs
	_args35.Appid = appid
	_args35.Keys = keys
	var _result36 MGetBytesResult
	if _, err = p.Client_().Call(ctx, "MGetBytes", &_args35, &_result36); err != nil {
		return
	}
	return _result36.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
func (p *RedisProxyClient) Del(ctx context.Context, appid string, key string) (r *IntResponse, err error) {
	var _args37 DelArgs
	_args37.Appid = appid
	_args37.Key = key
	var _result38 DelResult
	if _, err = p.Client_().Call(ctx, "Del", &_args37, &_result38); err != nil {
		return
	}
	return _result38.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Keys
func (p *RedisProxyClient) MDel(ctx context.Context, appid string, keys []string) (r *IntResponse, err error) {
	var _args39 MDelArgs
	_args39.Appid = appid
	_args39.Keys = keys
	var _result40 MDelResult
	if _, err = p.Client_().Call(ctx, "MDel", &_args39, &_result40); err != nil {
		return
	}
	return _result40.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
func (p *RedisProxyClient) Keys(ctx context.Context, appid string, key string) (r *LResponse, err error) {
	var _args41 KeysArgs
	_args41.Appid = appid
	_args41.Key = key
	var _result42 KeysResult
	if _, err = p.Client_().Call(ctx, "Keys", &_args41, &_result42); err != nil {
		return
	}
	return _result42.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - TTL
func (p *RedisProxyClient) Expire(ctx context.Context, appid string, key string, ttl int64) (r *Response, err error) {
	var _args43 ExpireArgs
	_args43.Appid = appid
	_args43.Key = key
	_args43.TTL = ttl
	var _result44 ExpireResult
	if _, err = p.Client_().Call(ctx, "Expire", &_args43, &_result44); err != nil {
		return
	}
	return _result44.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
func (p *RedisProxyClient) TTL(ctx context.Context, appid string, key string) (r *IntResponse, err error) {
	var _args45 TtlArgs
	_args45.Appid = appid
	_args45.Key = key
	var _result46 TtlResult
	if _, err = p.Client_().Call(ctx, "TTL", &_args45, &_result46); err != nil {
		return
	}
	return _result46.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
func (p *RedisProxyClient) Exists(ctx context.Context, appid string, key string) (r *Response, err error) {
	var _args47 ExistsArgs
	_args47.Appid = appid
	_args47.Key = key
	var _result48 ExistsResult
	if _, err = p.Client_().Call(ctx, "Exists", &_args47, &_result48); err != nil {
		return
	}
	return _result48.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Field
//  - Value
func (p *RedisProxyClient) HSet(ctx context.Context, appid string, key string, field string, value string) (r *Response, err error) {
	var _args49 HSetArgs
	_args49.Appid = appid
	_args49.Key = key
	_args49.Field = field
	_args49.Value = value
	var _result50 HSetResult
	if _, err = p.Client_().Call(ctx, "HSet", &_args49, &_result50); err != nil {
		return
	}
	return _result50.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Field
//  - Value
func (p *RedisProxyClient) HSetBytes(ctx context.Context, appid string, key string, field string, value []byte) (r *Response, err error) {
	var _args51 HSetBytesArgs
	_args51.Appid = appid
	_args51.Key = key
	_args51.Field = field
	_args51.Value = value
	var _result52 HSetBytesResult
	if _, err = p.Client_().Call(ctx, "HSetBytes", &_args51, &_result52); err != nil {
		return
	}
	return _result52.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Field
//  - Value
func (p *RedisProxyClient) HSetNX(ctx context.Context, appid string, key string, field string, value string) (r *Response, err error) {
	var _args53 HSetNXArgs
	_args53.Appid = appid
	_args53.Key = key
	_args53.Field = field
	_args53.Value = value
	var _result54 HSetNXResult
	if _, err = p.Client_().Call(ctx, "HSetNX", &_args53, &_result54); err != nil {
		return
	}
	return _result54.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Field
//  - Value
func (p *RedisProxyClient) HSetNXBytes(ctx context.Context, appid string, key string, field string, value string) (r *Response, err error) {
	var _args55 HSetNXBytesArgs
	_args55.Appid = appid
	_args55.Key = key
	_args55.Field = field
	_args55.Value = value
	var _result56 HSetNXBytesResult
	if _, err = p.Client_().Call(ctx, "HSetNXBytes", &_args55, &_result56); err != nil {
		return
	}
	return _result56.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Field
func (p *RedisProxyClient) HGet(ctx context.Context, appid string, key string, field string) (r *Response, err error) {
	var _args57 ProxyHGetArgs
	_args57.Appid = appid
	_args57.Key = key
	_args57.Field = field
	var _result58 HGetResult
	if _, err = p.Client_().Call(ctx, "HGet", &_args57, &_result58); err != nil {
		return
	}
	return _result58.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Field
func (p *RedisProxyClient) HGetBytes(ctx context.Context, appid string, key string, field string) (r *BytesResponse, err error) {
	var _args59 RedisProxyHGetBytesArgs
	_args59.Appid = appid
	_args59.Key = key
	_args59.Field = field
	var _result60 HGetBytesResult
	if _, err = p.Client_().Call(ctx, "HGetBytes", &_args59, &_result60); err != nil {
		return
	}
	return _result60.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Fields
func (p *RedisProxyClient) HMSet(ctx context.Context, appid string, key string, fields map[string]string) (r *Response, err error) {
	var _args61 HmSetArgs
	_args61.Appid = appid
	_args61.Key = key
	_args61.Fields = fields
	var _result62 HmSetResult
	if _, err = p.Client_().Call(ctx, "HMSet", &_args61, &_result62); err != nil {
		return
	}
	return _result62.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Fields
func (p *RedisProxyClient) HMSetBytes(ctx context.Context, appid string, key string, fields map[string][]byte) (r *Response, err error) {
	var _args63 HmSetBytesArgs
	_args63.Appid = appid
	_args63.Key = key
	_args63.Fields = fields
	var _result64 HmSetBytesResult
	if _, err = p.Client_().Call(ctx, "HMSetBytes", &_args63, &_result64); err != nil {
		return
	}
	return _result64.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Fields
func (p *RedisProxyClient) HMGet(ctx context.Context, appid string, key string, fields []string) (r *MResponse, err error) {
	var _args65 HmGetArgs
	_args65.Appid = appid
	_args65.Key = key
	_args65.Fields = fields
	var _result66 HMGetResult
	if _, err = p.Client_().Call(ctx, "HMGet", &_args65, &_result66); err != nil {
		return
	}
	return _result66.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Fields
func (p *RedisProxyClient) HMGetBytes(ctx context.Context, appid string, key string, fields []string) (r *MBytesResponse, err error) {
	var _args67 HMGetBytesArgs
	_args67.Appid = appid
	_args67.Key = key
	_args67.Fields = fields
	var _result68 HMGetBytesResult
	if _, err = p.Client_().Call(ctx, "HMGetBytes", &_args67, &_result68); err != nil {
		return
	}
	return _result68.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
func (p *RedisProxyClient) HGetAll(ctx context.Context, appid string, key string) (r *MResponse, err error) {
	var _args69 HGetAllArgs
	_args69.Appid = appid
	_args69.Key = key
	var _result70 HGetAllResult
	if _, err = p.Client_().Call(ctx, "HGetAll", &_args69, &_result70); err != nil {
		return
	}
	return _result70.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Field
func (p *RedisProxyClient) HDel(ctx context.Context, appid string, key string, field string) (r *IntResponse, err error) {
	var _args71 HDelArgs
	_args71.Appid = appid
	_args71.Key = key
	_args71.Field = field
	var _result72 HDelResult
	if _, err = p.Client_().Call(ctx, "HDel", &_args71, &_result72); err != nil {
		return
	}
	return _result72.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Fields
func (p *RedisProxyClient) MHDel(ctx context.Context, appid string, key string, fields []string) (r *IntResponse, err error) {
	var _args73 MhDelArgs
	_args73.Appid = appid
	_args73.Key = key
	_args73.Fields = fields
	var _result74 MhDelResult
	if _, err = p.Client_().Call(ctx, "MHDel", &_args73, &_result74); err != nil {
		return
	}
	return _result74.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
func (p *RedisProxyClient) HKeys(ctx context.Context, appid string, key string) (r *LResponse, err error) {
	var _args75 HKeysArgs
	_args75.Appid = appid
	_args75.Key = key
	var _result76 HKeysResult
	if _, err = p.Client_().Call(ctx, "HKeys", &_args75, &_result76); err != nil {
		return
	}
	return _result76.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
func (p *RedisProxyClient) HVals(ctx context.Context, appid string, key string) (r *LResponse, err error) {
	var _args77 HValuesArgs
	_args77.Appid = appid
	_args77.Key = key
	var _result78 HValuesResult
	if _, err = p.Client_().Call(ctx, "HVals", &_args77, &_result78); err != nil {
		return
	}
	return _result78.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Field
//  - Inrc
func (p *RedisProxyClient) HIncrBy(ctx context.Context, appid string, key string, field string, inrc int64) (r *IntResponse, err error) {
	var _args79 HIncrByArgs
	_args79.Appid = appid
	_args79.Key = key
	_args79.Field = field
	_args79.Inrc = inrc
	var _result80 HIncrByResult
	if _, err = p.Client_().Call(ctx, "HIncrBy", &_args79, &_result80); err != nil {
		return
	}
	return _result80.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Field
//  - Inrc
func (p *RedisProxyClient) HIncrByFloat(ctx context.Context, appid string, key string, field string, inrc float64) (r *FloatResponse, err error) {
	var _args81 HIncrByFloatArgs
	_args81.Appid = appid
	_args81.Key = key
	_args81.Field = field
	_args81.Inrc = inrc
	var _result82 HIncrByFloatResult
	if _, err = p.Client_().Call(ctx, "HIncrByFloat", &_args81, &_result82); err != nil {
		return
	}
	return _result82.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Field
func (p *RedisProxyClient) HExists(ctx context.Context, appid string, key string, field string) (r *Response, err error) {
	var _args83 HExistsArgs
	_args83.Appid = appid
	_args83.Key = key
	_args83.Field = field
	var _result84 HExistsResult
	if _, err = p.Client_().Call(ctx, "HExists", &_args83, &_result84); err != nil {
		return
	}
	return _result84.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Member
func (p *RedisProxyClient) ZAdd(ctx context.Context, appid string, key string, member *Z) (r *IntResponse, err error) {
	var _args85 ZAddArgs
	_args85.Appid = appid
	_args85.Key = key
	_args85.Member = member
	var _result86 ZAddResult
	if _, err = p.Client_().Call(ctx, "ZAdd", &_args85, &_result86); err != nil {
		return
	}
	return _result86.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Members
func (p *RedisProxyClient) MZAdd(ctx context.Context, appid string, key string, members []*Z) (r *IntResponse, err error) {
	var _args87 MzAddArgs
	_args87.Appid = appid
	_args87.Key = key
	_args87.Members = members
	var _result88 MzAddResult
	if _, err = p.Client_().Call(ctx, "MZAdd", &_args87, &_result88); err != nil {
		return
	}
	return _result88.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
func (p *RedisProxyClient) ZCard(ctx context.Context, appid string, key string) (r *IntResponse, err error) {
	var _args89 ZCardArgs
	_args89.Appid = appid
	_args89.Key = key
	var _result90 ZCardResult
	if _, err = p.Client_().Call(ctx, "ZCard", &_args89, &_result90); err != nil {
		return
	}
	return _result90.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Min
//  - Max
func (p *RedisProxyClient) ZCount(ctx context.Context, appid string, key string, min string, max string) (r *IntResponse, err error) {
	var _args91 ZCountArgs
	_args91.Appid = appid
	_args91.Key = key
	_args91.Min = min
	_args91.Max = max
	var _result92 ZCountResult
	if _, err = p.Client_().Call(ctx, "ZCount", &_args91, &_result92); err != nil {
		return
	}
	return _result92.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Start
//  - Stop
func (p *RedisProxyClient) ZRangeWithScores(ctx context.Context, appid string, key string, start int64, stop int64) (r *ZResponse, err error) {
	var _args93 ZRangeWithScoresArgs
	_args93.Appid = appid
	_args93.Key = key
	_args93.Start = start
	_args93.Stop = stop
	var _result94 ZRangeWithScoresResult
	if _, err = p.Client_().Call(ctx, "ZRangeWithScores", &_args93, &_result94); err != nil {
		return
	}
	return _result94.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Min
//  - Max
//  - Offset
//  - Count
func (p *RedisProxyClient) ZRangeByScoreWithScores(ctx context.Context, appid string, key string, min string, max string, offset int64, count int64) (r *ZResponse, err error) {
	var _args95 ZRangeByScoreWithScoresArgs
	_args95.Appid = appid
	_args95.Key = key
	_args95.Min = min
	_args95.Max = max
	_args95.Offset = offset
	_args95.Count = count
	var _result96 ZRangeByScoreWithScoresResult
	if _, err = p.Client_().Call(ctx, "ZRangeByScoreWithScores", &_args95, &_result96); err != nil {
		return
	}
	return _result96.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Member
func (p *RedisProxyClient) ZRem(ctx context.Context, appid string, key string, member string) (r *IntResponse, err error) {
	var _args97 ZRemArgs
	_args97.Appid = appid
	_args97.Key = key
	_args97.Member = member
	var _result98 ZRemResult
	if _, err = p.Client_().Call(ctx, "ZRem", &_args97, &_result98); err != nil {
		return
	}
	return _result98.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Members
func (p *RedisProxyClient) MZRem(ctx context.Context, appid string, key string, members []string) (r *IntResponse, err error) {
	var _args99 MzRemArgs
	_args99.Appid = appid
	_args99.Key = key
	_args99.Members = members
	var _result100 MzRemResult
	if _, err = p.Client_().Call(ctx, "MZRem", &_args99, &_result100); err != nil {
		return
	}
	return _result100.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Start
//  - Stop
func (p *RedisProxyClient) ZRemRangeByRank(ctx context.Context, appid string, key string, start int64, stop int64) (r *IntResponse, err error) {
	var _args101 ZRemRangeByRankArgs
	_args101.Appid = appid
	_args101.Key = key
	_args101.Start = start
	_args101.Stop = stop
	var _result102 ZRemRangeByRankResult
	if _, err = p.Client_().Call(ctx, "ZRemRangeByRank", &_args101, &_result102); err != nil {
		return
	}
	return _result102.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Min
//  - Max
func (p *RedisProxyClient) ZRemRangeByScore(ctx context.Context, appid string, key string, min string, max string) (r *IntResponse, err error) {
	var _args103 ZRemRangeByScoreArgs
	_args103.Appid = appid
	_args103.Key = key
	_args103.Min = min
	_args103.Max = max
	var _result104 ZRemRangeByScoreResult
	if _, err = p.Client_().Call(ctx, "ZRemRangeByScore", &_args103, &_result104); err != nil {
		return
	}
	return _result104.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Increment
//  - Member
func (p *RedisProxyClient) ZIncrBy(ctx context.Context, appid string, key string, increment float64, member string) (r *FloatResponse, err error) {
	var _args105 ZIncrByArgs
	_args105.Appid = appid
	_args105.Key = key
	_args105.Increment = increment
	_args105.Member = member
	var _result106 ZIncrByResult
	if _, err = p.Client_().Call(ctx, "ZIncrBy", &_args105, &_result106); err != nil {
		return
	}
	return _result106.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Member
func (p *RedisProxyClient) ZScore(ctx context.Context, appid string, key string, member string) (r *FloatResponse, err error) {
	var _args107 ZScoreArgs
	_args107.Appid = appid
	_args107.Key = key
	_args107.Member = member
	var _result108 ZScoreResult
	if _, err = p.Client_().Call(ctx, "ZScore", &_args107, &_result108); err != nil {
		return
	}
	return _result108.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Member
func (p *RedisProxyClient) SAdd(ctx context.Context, appid string, key string, member string) (r *IntResponse, err error) {
	var _args109 SAddArgs
	_args109.Appid = appid
	_args109.Key = key
	_args109.Member = member
	var _result110 SAddResult
	if _, err = p.Client_().Call(ctx, "SAdd", &_args109, &_result110); err != nil {
		return
	}
	return _result110.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Members
func (p *RedisProxyClient) MSAdd(ctx context.Context, appid string, key string, members []string) (r *IntResponse, err error) {
	var _args111 MsAddArgs
	_args111.Appid = appid
	_args111.Key = key
	_args111.Members = members
	var _result112 MsAddResult
	if _, err = p.Client_().Call(ctx, "MSAdd", &_args111, &_result112); err != nil {
		return
	}
	return _result112.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
func (p *RedisProxyClient) SCard(ctx context.Context, appid string, key string) (r *IntResponse, err error) {
	var _args113 SCardArgs
	_args113.Appid = appid
	_args113.Key = key
	var _result114 SCardResult
	if _, err = p.Client_().Call(ctx, "SCard", &_args113, &_result114); err != nil {
		return
	}
	return _result114.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Member
func (p *RedisProxyClient) SIsMember(ctx context.Context, appid string, key string, member string) (r *Response, err error) {
	var _args115 SIsMemberArgs
	_args115.Appid = appid
	_args115.Key = key
	_args115.Member = member
	var _result116 SIsMemberResult
	if _, err = p.Client_().Call(ctx, "SIsMember", &_args115, &_result116); err != nil {
		return
	}
	return _result116.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
func (p *RedisProxyClient) SMembers(ctx context.Context, appid string, key string) (r *LResponse, err error) {
	var _args117 SMembersArgs
	_args117.Appid = appid
	_args117.Key = key
	var _result118 SMembersResult
	if _, err = p.Client_().Call(ctx, "SMembers", &_args117, &_result118); err != nil {
		return
	}
	return _result118.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
func (p *RedisProxyClient) SRandMember(ctx context.Context, appid string, key string) (r *Response, err error) {
	var _args119 SRandMemberArgs
	_args119.Appid = appid
	_args119.Key = key
	var _result120 SRandMemberResult
	if _, err = p.Client_().Call(ctx, "SRandMember", &_args119, &_result120); err != nil {
		return
	}
	return _result120.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - N
func (p *RedisProxyClient) SRandMemberN(ctx context.Context, appid string, key string, n int64) (r *LResponse, err error) {
	var _args121 SRandMemberNArgs
	_args121.Appid = appid
	_args121.Key = key
	_args121.N = n
	var _result122 SRandMemberNResult
	if _, err = p.Client_().Call(ctx, "SRandMemberN", &_args121, &_result122); err != nil {
		return
	}
	return _result122.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Member
func (p *RedisProxyClient) SRem(ctx context.Context, appid string, key string, member string) (r *IntResponse, err error) {
	var _args123 SRemArgs
	_args123.Appid = appid
	_args123.Key = key
	_args123.Member = member
	var _result124 SRemResult
	if _, err = p.Client_().Call(ctx, "SRem", &_args123, &_result124); err != nil {
		return
	}
	return _result124.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Member
func (p *RedisProxyClient) MSRem(ctx context.Context, appid string, key string, member []string) (r *IntResponse, err error) {
	var _args125 MsRemArgs
	_args125.Appid = appid
	_args125.Key = key
	_args125.Member = member
	var _result126 MsRemResult
	if _, err = p.Client_().Call(ctx, "MSRem", &_args125, &_result126); err != nil {
		return
	}
	return _result126.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Value
func (p *RedisProxyClient) LPush(ctx context.Context, appid string, key string, value string) (r *IntResponse, err error) {
	var _args127 LPushArgs
	_args127.Appid = appid
	_args127.Key = key
	_args127.Value = value
	var _result128 LPushResult
	if _, err = p.Client_().Call(ctx, "LPush", &_args127, &_result128); err != nil {
		return
	}
	return _result128.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Value
func (p *RedisProxyClient) LPushByte(ctx context.Context, appid string, key string, value []byte) (r *IntResponse, err error) {
	var _args129 LPushByteArgs
	_args129.Appid = appid
	_args129.Key = key
	_args129.Value = value
	var _result130 LPushByteResult
	if _, err = p.Client_().Call(ctx, "LPushByte", &_args129, &_result130); err != nil {
		return
	}
	return _result130.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Value
func (p *RedisProxyClient) RPush(ctx context.Context, appid string, key string, value string) (r *IntResponse, err error) {
	var _args131 RPushArgs
	_args131.Appid = appid
	_args131.Key = key
	_args131.Value = value
	var _result132 RPushResult
	if _, err = p.Client_().Call(ctx, "RPush", &_args131, &_result132); err != nil {
		return
	}
	return _result132.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Value
func (p *RedisProxyClient) RPushByte(ctx context.Context, appid string, key string, value []byte) (r *IntResponse, err error) {
	var _args133 RPushByteArgs
	_args133.Appid = appid
	_args133.Key = key
	_args133.Value = value
	var _result134 RPushByteResult
	if _, err = p.Client_().Call(ctx, "RPushByte", &_args133, &_result134); err != nil {
		return
	}
	return _result134.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Value
func (p *RedisProxyClient) MLPush(ctx context.Context, appid string, key string, value []string) (r *IntResponse, err error) {
	var _args135 MlPushArgs
	_args135.Appid = appid
	_args135.Key = key
	_args135.Value = value
	var _result136 MlPushResult
	if _, err = p.Client_().Call(ctx, "MLPush", &_args135, &_result136); err != nil {
		return
	}
	return _result136.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Value
//  - TTL
func (p *RedisProxyClient) MLPushWithTTL(ctx context.Context, appid string, key string, value []string, ttl int64) (r *IntResponse, err error) {
	var _args137 MlPushWithTTLArgs
	_args137.Appid = appid
	_args137.Key = key
	_args137.Value = value
	_args137.TTL = ttl
	var _result138 MlPushWithTTLResult
	if _, err = p.Client_().Call(ctx, "MLPushWithTTL", &_args137, &_result138); err != nil {
		return
	}
	return _result138.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Value
//  - TTL
func (p *RedisProxyClient) MLPushByteWithTTL(ctx context.Context, appid string, key string, value [][]byte, ttl int64) (r *IntResponse, err error) {
	var _args139 MlPushByteWithTTLArgs
	_args139.Appid = appid
	_args139.Key = key
	_args139.Value = value
	_args139.TTL = ttl
	var _result140 MlPushByteWithTTLResult
	if _, err = p.Client_().Call(ctx, "MLPushByteWithTTL", &_args139, &_result140); err != nil {
		return
	}
	return _result140.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Value
//  - TTL
func (p *RedisProxyClient) MRPushWithTTL(ctx context.Context, appid string, key string, value []string, ttl int64) (r *IntResponse, err error) {
	var _args141 MrPushWithTTLArgs
	_args141.Appid = appid
	_args141.Key = key
	_args141.Value = value
	_args141.TTL = ttl
	var _result142 MrPushWithTTLResult
	if _, err = p.Client_().Call(ctx, "MRPushWithTTL", &_args141, &_result142); err != nil {
		return
	}
	return _result142.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Value
//  - TTL
func (p *RedisProxyClient) MRPushByteWithTTL(ctx context.Context, appid string, key string, value [][]byte, ttl int64) (r *IntResponse, err error) {
	var _args143 MrPushByteWithTTLArgs
	_args143.Appid = appid
	_args143.Key = key
	_args143.Value = value
	_args143.TTL = ttl
	var _result144 MrPushByteWithTTLResult
	if _, err = p.Client_().Call(ctx, "MRPushByteWithTTL", &_args143, &_result144); err != nil {
		return
	}
	return _result144.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
func (p *RedisProxyClient) LPop(ctx context.Context, appid string, key string) (r *Response, err error) {
	var _args145 ProxyLPopArgs
	_args145.Appid = appid
	_args145.Key = key
	var _result146 LPopResult
	if _, err = p.Client_().Call(ctx, "LPop", &_args145, &_result146); err != nil {
		return
	}
	return _result146.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
func (p *RedisProxyClient) LPopByte(ctx context.Context, appid string, key string) (r *BytesResponse, err error) {
	var _args147 LPopByteArgs
	_args147.Appid = appid
	_args147.Key = key
	var _result148 LPopByteResult
	if _, err = p.Client_().Call(ctx, "LPopByte", &_args147, &_result148); err != nil {
		return
	}
	return _result148.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
func (p *RedisProxyClient) RPop(ctx context.Context, appid string, key string) (r *Response, err error) {
	var _args149 RPopArgs
	_args149.Appid = appid
	_args149.Key = key
	var _result150 RPopResult
	if _, err = p.Client_().Call(ctx, "RPop", &_args149, &_result150); err != nil {
		return
	}
	return _result150.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
func (p *RedisProxyClient) RPopByte(ctx context.Context, appid string, key string) (r *BytesResponse, err error) {
	var _args151 RPopByteArgs
	_args151.Appid = appid
	_args151.Key = key
	var _result152 RPopByteResult
	if _, err = p.Client_().Call(ctx, "RPopByte", &_args151, &_result152); err != nil {
		return
	}
	return _result152.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Pivot
//  - Value
func (p *RedisProxyClient) LInsertBefore(ctx context.Context, appid string, key string, pivot string, value string) (r *IntResponse, err error) {
	var _args153 LInsertBeforeArgs
	_args153.Appid = appid
	_args153.Key = key
	_args153.Pivot = pivot
	_args153.Value = value
	var _result154 LInsertBeforeResult
	if _, err = p.Client_().Call(ctx, "LInsertBefore", &_args153, &_result154); err != nil {
		return
	}
	return _result154.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Pivot
//  - Value
func (p *RedisProxyClient) LInsertAfter(ctx context.Context, appid string, key string, pivot string, value string) (r *IntResponse, err error) {
	var _args155 LInsertAfterArgs
	_args155.Appid = appid
	_args155.Key = key
	_args155.Pivot = pivot
	_args155.Value = value
	var _result156 LInsertAfterResult
	if _, err = p.Client_().Call(ctx, "LInsertAfter", &_args155, &_result156); err != nil {
		return
	}
	return _result156.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
func (p *RedisProxyClient) LLen(ctx context.Context, appid string, key string) (r *IntResponse, err error) {
	var _args157 LLenArgs
	_args157.Appid = appid
	_args157.Key = key
	var _result158 LLenResult
	if _, err = p.Client_().Call(ctx, "LLen", &_args157, &_result158); err != nil {
		return
	}
	return _result158.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Keys
func (p *RedisProxyClient) MLLen(ctx context.Context, appid string, keys []string) (r *MIntResponse, err error) {
	var _args159 MlLenArgs
	_args159.Appid = appid
	_args159.Keys = keys
	var _result160 MlLenResult
	if _, err = p.Client_().Call(ctx, "MLLen", &_args159, &_result160); err != nil {
		return
	}
	return _result160.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Index
//  - Value
func (p *RedisProxyClient) LSet(ctx context.Context, appid string, key string, index int64, value string) (r *Response, err error) {
	var _args161 LSetArgs
	_args161.Appid = appid
	_args161.Key = key
	_args161.Index = index
	_args161.Value = value
	var _result162 LSetResult
	if _, err = p.Client_().Call(ctx, "LSet", &_args161, &_result162); err != nil {
		return
	}
	return _result162.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Index
func (p *RedisProxyClient) LIndex(ctx context.Context, appid string, key string, index int64) (r *Response, err error) {
	var _args163 LIndexArgs
	_args163.Appid = appid
	_args163.Key = key
	_args163.Index = index
	var _result164 LIndexResult
	if _, err = p.Client_().Call(ctx, "LIndex", &_args163, &_result164); err != nil {
		return
	}
	return _result164.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Start
//  - Stop
func (p *RedisProxyClient) LRange(ctx context.Context, appid string, key string, start int64, stop int64) (r *LResponse, err error) {
	var _args165 LRangeArgs
	_args165.Appid = appid
	_args165.Key = key
	_args165.Start = start
	_args165.Stop = stop
	var _result166 LRangeResult
	if _, err = p.Client_().Call(ctx, "LRange", &_args165, &_result166); err != nil {
		return
	}
	return _result166.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Key
//  - Start
//  - Stop
func (p *RedisProxyClient) LTrim(ctx context.Context, appid string, key string, start int64, stop int64) (r *Response, err error) {
	var _args167 LTrimArgs
	_args167.Appid = appid
	_args167.Key = key
	_args167.Start = start
	_args167.Stop = stop
	var _result168 LTrimResult
	if _, err = p.Client_().Call(ctx, "LTrim", &_args167, &_result168); err != nil {
		return
	}
	return _result168.GetSuccess(), nil
}

// Parameters:
//  - AppId
func (p *RedisProxyClient) ClusterNodes(ctx context.Context, appid string) (r *Response, err error) {
	var _args169 ClusterNodesArgs
	_args169.Appid = appid
	var _result170 ClusterNodesResult
	if _, err = p.Client_().Call(ctx, "ClusterNodes", &_args169, &_result170); err != nil {
		return
	}
	return _result170.GetSuccess(), nil
}

// Parameters:
//  - AppId
func (p *RedisProxyClient) ClusterInfo(ctx context.Context, appid string) (r *Response, err error) {
	var _args171 ClusterInfoArgs
	_args171.Appid = appid
	var _result172 ClusterInfoResult
	if _, err = p.Client_().Call(ctx, "ClusterInfo", &_args171, &_result172); err != nil {
		return
	}
	return _result172.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Sections
func (p *RedisProxyClient) Info(ctx context.Context, appid string, sections []string) (r *MResponse, err error) {
	var _args173 InfoArgs
	_args173.Appid = appid
	_args173.Sections = sections
	var _result174 InfoResult
	if _, err = p.Client_().Call(ctx, "Info", &_args173, &_result174); err != nil {
		return
	}
	return _result174.GetSuccess(), nil
}

// Parameters:
//  - AppId
//  - Addr
//  - Sections
func (p *RedisProxyClient) NodeInfo(ctx context.Context, appid string, addr string, sections []string) (r *Response, err error) {
	var _args175 NodeInfoArgs
	_args175.Appid = appid
	_args175.Addr = addr
	_args175.Sections = sections
	var _result176 NodeInfoResult
	if _, err = p.Client_().Call(ctx, "NodeInfo", &_args175, &_result176); err != nil {
		return
	}
	return _result176.GetSuccess(), nil
}
