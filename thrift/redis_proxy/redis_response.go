package redis_proxy

import (
	"context"
	"fmt"
	"github.com/apache/thrift/lib/go/thrift"
)

// Attributes:
//  - Code
//  - Msg
//  - Res
type Response struct {
	Code int32  `thrift:"code,1" db:"code" json:"code"`
	Msg  string `thrift:"msg,2" db:"msg" json:"msg"`
	Res  string `thrift:"res,3" db:"res" json:"res"`
}

func NewResponse() *Response {
	return &Response{}
}

func (p *Response) GetCode() int32 {
	return p.Code
}

func (p *Response) GetMsg() string {
	return p.Msg
}

func (p *Response) GetRes() string {
	return p.Res
}

func (p *Response) Read(ctx context.Context, tp thrift.TProtocol) error {
	if _, err := tp.ReadStructBegin(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T read error: ", p), err)
	}

	for {
		_, fieldTypeId, fieldId, err := tp.ReadFieldBegin(ctx)
		if err != nil {
			return thrift.PrependError(fmt.Sprintf("%T field %d read error: ", p, fieldId), err)
		}
		if fieldTypeId == thrift.STOP {
			break
		}
		switch fieldId {
		case 1:
			if fieldTypeId == thrift.I32 {
				if err := p.ReadField1(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		case 2:
			if fieldTypeId == thrift.STRING {
				if err := p.ReadField2(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		case 3:
			if fieldTypeId == thrift.STRING {
				if err := p.ReadField3(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		default:
			if err := tp.Skip(ctx, fieldTypeId); err != nil {
				return err
			}
		}
		if err := tp.ReadFieldEnd(ctx); err != nil {
			return err
		}
	}
	if err := tp.ReadStructEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T read struct end error: ", p), err)
	}
	return nil
}

func (p *Response) ReadField1(ctx context.Context, tp thrift.TProtocol) error {
	if v, err := tp.ReadI32(ctx); err != nil {
		return thrift.PrependError("error reading field 1: ", err)
	} else {
		p.Code = v
	}
	return nil
}

func (p *Response) ReadField2(ctx context.Context, tp thrift.TProtocol) error {
	if v, err := tp.ReadString(ctx); err != nil {
		return thrift.PrependError("error reading field 2: ", err)
	} else {
		p.Msg = v
	}
	return nil
}

func (p *Response) ReadField3(ctx context.Context, tp thrift.TProtocol) error {
	if v, err := tp.ReadString(ctx); err != nil {
		return thrift.PrependError("error reading field 3: ", err)
	} else {
		p.Res = v
	}
	return nil
}

func (p *Response) Write(ctx context.Context, tp thrift.TProtocol) error {
	if err := tp.WriteStructBegin(ctx, "response"); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write struct begin error: ", p), err)
	}
	if p != nil {
		if err := p.writeField1(ctx, tp); err != nil {
			return err
		}
		if err := p.writeField2(ctx, tp); err != nil {
			return err
		}
		if err := p.writeField3(ctx, tp); err != nil {
			return err
		}
	}
	if err := tp.WriteFieldStop(ctx); err != nil {
		return thrift.PrependError("write field stop error: ", err)
	}
	if err := tp.WriteStructEnd(ctx); err != nil {
		return thrift.PrependError("write struct stop error: ", err)
	}
	return nil
}

func (p *Response) writeField1(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "code", thrift.I32, 1); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 1:code: ", p), err)
	}
	if err := tp.WriteI32(ctx, int32(p.Code)); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T.code (1) field write error: ", p), err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 1:code: ", p), err)
	}
	return err
}

func (p *Response) writeField2(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "msg", thrift.STRING, 2); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 2:msg: ", p), err)
	}
	if err := tp.WriteString(ctx, string(p.Msg)); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T.msg (2) field write error: ", p), err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 2:msg: ", p), err)
	}
	return err
}

func (p *Response) writeField3(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "res", thrift.STRING, 3); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 3:res: ", p), err)
	}
	if err := tp.WriteString(ctx, string(p.Res)); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T.res (3) field write error: ", p), err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 3:res: ", p), err)
	}
	return err
}

func (p *Response) String() string {
	if p == nil {
		return "<nil>"
	}
	return fmt.Sprintf("Response(%+v)", *p)
}

// Attributes:
//  - Code
//  - Msg
//  - Res
type MResponse struct {
	Code int32             `thrift:"code,1" db:"code" json:"code"`
	Msg  string            `thrift:"msg,2" db:"msg" json:"msg"`
	Res  map[string]string `thrift:"res,3" db:"res" json:"res"`
}

func NewMResponse() *MResponse {
	return &MResponse{}
}

func (p *MResponse) GetCode() int32 {
	return p.Code
}

func (p *MResponse) GetMsg() string {
	return p.Msg
}

func (p *MResponse) GetRes() map[string]string {
	return p.Res
}
func (p *MResponse) Read(ctx context.Context, tp thrift.TProtocol) error {
	if _, err := tp.ReadStructBegin(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T read error: ", p), err)
	}

	for {
		_, fieldTypeId, fieldId, err := tp.ReadFieldBegin(ctx)
		if err != nil {
			return thrift.PrependError(fmt.Sprintf("%T field %d read error: ", p, fieldId), err)
		}
		if fieldTypeId == thrift.STOP {
			break
		}
		switch fieldId {
		case 1:
			if fieldTypeId == thrift.I32 {
				if err := p.ReadField1(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		case 2:
			if fieldTypeId == thrift.STRING {
				if err := p.ReadField2(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		case 3:
			if fieldTypeId == thrift.MAP {
				if err := p.ReadField3(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		default:
			if err := tp.Skip(ctx, fieldTypeId); err != nil {
				return err
			}
		}
		if err := tp.ReadFieldEnd(ctx); err != nil {
			return err
		}
	}
	if err := tp.ReadStructEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T read struct end error: ", p), err)
	}
	return nil
}

func (p *MResponse) ReadField1(ctx context.Context, tp thrift.TProtocol) error {
	if v, err := tp.ReadI32(ctx); err != nil {
		return thrift.PrependError("error reading field 1: ", err)
	} else {
		p.Code = v
	}
	return nil
}

func (p *MResponse) ReadField2(ctx context.Context, tp thrift.TProtocol) error {
	if v, err := tp.ReadString(ctx); err != nil {
		return thrift.PrependError("error reading field 2: ", err)
	} else {
		p.Msg = v
	}
	return nil
}

func (p *MResponse) ReadField3(ctx context.Context, tp thrift.TProtocol) error {
	_, _, size, err := tp.ReadMapBegin(ctx)
	if err != nil {
		return thrift.PrependError("error reading map begin: ", err)
	}
	tMap := make(map[string]string, size)
	p.Res = tMap
	for i := 0; i < size; i++ {
		var _key0 string
		if v, err := tp.ReadString(ctx); err != nil {
			return thrift.PrependError("error reading field 0: ", err)
		} else {
			_key0 = v
		}
		var _val1 string
		if v, err := tp.ReadString(ctx); err != nil {
			return thrift.PrependError("error reading field 0: ", err)
		} else {
			_val1 = v
		}
		p.Res[_key0] = _val1
	}
	if err := tp.ReadMapEnd(ctx); err != nil {
		return thrift.PrependError("error reading map end: ", err)
	}
	return nil
}

func (p *MResponse) Write(ctx context.Context, tp thrift.TProtocol) error {
	if err := tp.WriteStructBegin(ctx, "mResponse"); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write struct begin error: ", p), err)
	}
	if p != nil {
		if err := p.writeField1(ctx, tp); err != nil {
			return err
		}
		if err := p.writeField2(ctx, tp); err != nil {
			return err
		}
		if err := p.writeField3(ctx, tp); err != nil {
			return err
		}
	}
	if err := tp.WriteFieldStop(ctx); err != nil {
		return thrift.PrependError("write field stop error: ", err)
	}
	if err := tp.WriteStructEnd(ctx); err != nil {
		return thrift.PrependError("write struct stop error: ", err)
	}
	return nil
}

func (p *MResponse) writeField1(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "code", thrift.I32, 1); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 1:code: ", p), err)
	}
	if err := tp.WriteI32(ctx, int32(p.Code)); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T.code (1) field write error: ", p), err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 1:code: ", p), err)
	}
	return err
}

func (p *MResponse) writeField2(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "msg", thrift.STRING, 2); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 2:msg: ", p), err)
	}
	if err := tp.WriteString(ctx, string(p.Msg)); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T.msg (2) field write error: ", p), err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 2:msg: ", p), err)
	}
	return err
}

func (p *MResponse) writeField3(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "res", thrift.MAP, 3); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 3:res: ", p), err)
	}
	if err := tp.WriteMapBegin(ctx, thrift.STRING, thrift.STRING, len(p.Res)); err != nil {
		return thrift.PrependError("error writing map begin: ", err)
	}
	for k, v := range p.Res {
		if err := tp.WriteString(ctx, string(k)); err != nil {
			return thrift.PrependError(fmt.Sprintf("%T. (0) field write error: ", p), err)
		}
		if err := tp.WriteString(ctx, string(v)); err != nil {
			return thrift.PrependError(fmt.Sprintf("%T. (0) field write error: ", p), err)
		}
	}
	if err := tp.WriteMapEnd(ctx); err != nil {
		return thrift.PrependError("error writing map end: ", err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 3:res: ", p), err)
	}
	return err
}

func (p *MResponse) String() string {
	if p == nil {
		return "<nil>"
	}
	return fmt.Sprintf("MResponse(%+v)", *p)
}

// Attributes:
//  - Code
//  - Msg
//  - Res
type LResponse struct {
	Code int32    `thrift:"code,1" db:"code" json:"code"`
	Msg  string   `thrift:"msg,2" db:"msg" json:"msg"`
	Res  []string `thrift:"res,3" db:"res" json:"res"`
}

func NewLResponse() *LResponse {
	return &LResponse{}
}

func (p *LResponse) GetCode() int32 {
	return p.Code
}

func (p *LResponse) GetMsg() string {
	return p.Msg
}

func (p *LResponse) GetRes() []string {
	return p.Res
}
func (p *LResponse) Read(ctx context.Context, tp thrift.TProtocol) error {
	if _, err := tp.ReadStructBegin(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T read error: ", p), err)
	}

	for {
		_, fieldTypeId, fieldId, err := tp.ReadFieldBegin(ctx)
		if err != nil {
			return thrift.PrependError(fmt.Sprintf("%T field %d read error: ", p, fieldId), err)
		}
		if fieldTypeId == thrift.STOP {
			break
		}
		switch fieldId {
		case 1:
			if fieldTypeId == thrift.I32 {
				if err := p.ReadField1(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		case 2:
			if fieldTypeId == thrift.STRING {
				if err := p.ReadField2(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		case 3:
			if fieldTypeId == thrift.LIST {
				if err := p.ReadField3(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		default:
			if err := tp.Skip(ctx, fieldTypeId); err != nil {
				return err
			}
		}
		if err := tp.ReadFieldEnd(ctx); err != nil {
			return err
		}
	}
	if err := tp.ReadStructEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T read struct end error: ", p), err)
	}
	return nil
}

func (p *LResponse) ReadField1(ctx context.Context, tp thrift.TProtocol) error {
	if v, err := tp.ReadI32(ctx); err != nil {
		return thrift.PrependError("error reading field 1: ", err)
	} else {
		p.Code = v
	}
	return nil
}

func (p *LResponse) ReadField2(ctx context.Context, tp thrift.TProtocol) error {
	if v, err := tp.ReadString(ctx); err != nil {
		return thrift.PrependError("error reading field 2: ", err)
	} else {
		p.Msg = v
	}
	return nil
}

func (p *LResponse) ReadField3(ctx context.Context, tp thrift.TProtocol) error {
	_, size, err := tp.ReadListBegin(ctx)
	if err != nil {
		return thrift.PrependError("error reading list begin: ", err)
	}
	tSlice := make([]string, 0, size)
	p.Res = tSlice
	for i := 0; i < size; i++ {
		var _elem2 string
		if v, err := tp.ReadString(ctx); err != nil {
			return thrift.PrependError("error reading field 0: ", err)
		} else {
			_elem2 = v
		}
		p.Res = append(p.Res, _elem2)
	}
	if err := tp.ReadListEnd(ctx); err != nil {
		return thrift.PrependError("error reading list end: ", err)
	}
	return nil
}

func (p *LResponse) Write(ctx context.Context, tp thrift.TProtocol) error {
	if err := tp.WriteStructBegin(ctx, "lResponse"); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write struct begin error: ", p), err)
	}
	if p != nil {
		if err := p.writeField1(ctx, tp); err != nil {
			return err
		}
		if err := p.writeField2(ctx, tp); err != nil {
			return err
		}
		if err := p.writeField3(ctx, tp); err != nil {
			return err
		}
	}
	if err := tp.WriteFieldStop(ctx); err != nil {
		return thrift.PrependError("write field stop error: ", err)
	}
	if err := tp.WriteStructEnd(ctx); err != nil {
		return thrift.PrependError("write struct stop error: ", err)
	}
	return nil
}

func (p *LResponse) writeField1(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "code", thrift.I32, 1); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 1:code: ", p), err)
	}
	if err := tp.WriteI32(ctx, int32(p.Code)); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T.code (1) field write error: ", p), err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 1:code: ", p), err)
	}
	return err
}

func (p *LResponse) writeField2(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "msg", thrift.STRING, 2); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 2:msg: ", p), err)
	}
	if err := tp.WriteString(ctx, string(p.Msg)); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T.msg (2) field write error: ", p), err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 2:msg: ", p), err)
	}
	return err
}

func (p *LResponse) writeField3(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "res", thrift.LIST, 3); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 3:res: ", p), err)
	}
	if err := tp.WriteListBegin(ctx, thrift.STRING, len(p.Res)); err != nil {
		return thrift.PrependError("error writing list begin: ", err)
	}
	for _, v := range p.Res {
		if err := tp.WriteString(ctx, string(v)); err != nil {
			return thrift.PrependError(fmt.Sprintf("%T. (0) field write error: ", p), err)
		}
	}
	if err := tp.WriteListEnd(ctx); err != nil {
		return thrift.PrependError("error writing list end: ", err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 3:res: ", p), err)
	}
	return err
}

func (p *LResponse) String() string {
	if p == nil {
		return "<nil>"
	}
	return fmt.Sprintf("LResponse(%+v)", *p)
}

// Attributes:
//  - Code
//  - Msg
//  - Res
type IntResponse struct {
	Code int32  `thrift:"code,1" db:"code" json:"code"`
	Msg  string `thrift:"msg,2" db:"msg" json:"msg"`
	Res  int64  `thrift:"res,3" db:"res" json:"res"`
}

func NewIntResponse() *IntResponse {
	return &IntResponse{}
}

func (p *IntResponse) GetCode() int32 {
	return p.Code
}

func (p *IntResponse) GetMsg() string {
	return p.Msg
}

func (p *IntResponse) GetRes() int64 {
	return p.Res
}
func (p *IntResponse) Read(ctx context.Context, tp thrift.TProtocol) error {
	if _, err := tp.ReadStructBegin(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T read error: ", p), err)
	}

	for {
		_, fieldTypeId, fieldId, err := tp.ReadFieldBegin(ctx)
		if err != nil {
			return thrift.PrependError(fmt.Sprintf("%T field %d read error: ", p, fieldId), err)
		}
		if fieldTypeId == thrift.STOP {
			break
		}
		switch fieldId {
		case 1:
			if fieldTypeId == thrift.I32 {
				if err := p.ReadField1(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		case 2:
			if fieldTypeId == thrift.STRING {
				if err := p.ReadField2(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		case 3:
			if fieldTypeId == thrift.I64 {
				if err := p.ReadField3(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		default:
			if err := tp.Skip(ctx, fieldTypeId); err != nil {
				return err
			}
		}
		if err := tp.ReadFieldEnd(ctx); err != nil {
			return err
		}
	}
	if err := tp.ReadStructEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T read struct end error: ", p), err)
	}
	return nil
}

func (p *IntResponse) ReadField1(ctx context.Context, tp thrift.TProtocol) error {
	if v, err := tp.ReadI32(ctx); err != nil {
		return thrift.PrependError("error reading field 1: ", err)
	} else {
		p.Code = v
	}
	return nil
}

func (p *IntResponse) ReadField2(ctx context.Context, tp thrift.TProtocol) error {
	if v, err := tp.ReadString(ctx); err != nil {
		return thrift.PrependError("error reading field 2: ", err)
	} else {
		p.Msg = v
	}
	return nil
}

func (p *IntResponse) ReadField3(ctx context.Context, tp thrift.TProtocol) error {
	if v, err := tp.ReadI64(ctx); err != nil {
		return thrift.PrependError("error reading field 3: ", err)
	} else {
		p.Res = v
	}
	return nil
}

func (p *IntResponse) Write(ctx context.Context, tp thrift.TProtocol) error {
	if err := tp.WriteStructBegin(ctx, "intResponse"); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write struct begin error: ", p), err)
	}
	if p != nil {
		if err := p.writeField1(ctx, tp); err != nil {
			return err
		}
		if err := p.writeField2(ctx, tp); err != nil {
			return err
		}
		if err := p.writeField3(ctx, tp); err != nil {
			return err
		}
	}
	if err := tp.WriteFieldStop(ctx); err != nil {
		return thrift.PrependError("write field stop error: ", err)
	}
	if err := tp.WriteStructEnd(ctx); err != nil {
		return thrift.PrependError("write struct stop error: ", err)
	}
	return nil
}

func (p *IntResponse) writeField1(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "code", thrift.I32, 1); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 1:code: ", p), err)
	}
	if err := tp.WriteI32(ctx, int32(p.Code)); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T.code (1) field write error: ", p), err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 1:code: ", p), err)
	}
	return err
}

func (p *IntResponse) writeField2(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "msg", thrift.STRING, 2); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 2:msg: ", p), err)
	}
	if err := tp.WriteString(ctx, string(p.Msg)); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T.msg (2) field write error: ", p), err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 2:msg: ", p), err)
	}
	return err
}

func (p *IntResponse) writeField3(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "res", thrift.I64, 3); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 3:res: ", p), err)
	}
	if err := tp.WriteI64(ctx, int64(p.Res)); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T.res (3) field write error: ", p), err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 3:res: ", p), err)
	}
	return err
}

func (p *IntResponse) String() string {
	if p == nil {
		return "<nil>"
	}
	return fmt.Sprintf("IntResponse(%+v)", *p)
}

// Attributes:
//  - Code
//  - Msg
//  - Res
type MIntResponse struct {
	Code int32            `thrift:"code,1" db:"code" json:"code"`
	Msg  string           `thrift:"msg,2" db:"msg" json:"msg"`
	Res  map[string]int64 `thrift:"res,3" db:"res" json:"res"`
}

func NewMIntResponse() *MIntResponse {
	return &MIntResponse{}
}

func (p *MIntResponse) GetCode() int32 {
	return p.Code
}

func (p *MIntResponse) GetMsg() string {
	return p.Msg
}

func (p *MIntResponse) GetRes() map[string]int64 {
	return p.Res
}

func (p *MIntResponse) Read(ctx context.Context, tp thrift.TProtocol) error {
	if _, err := tp.ReadStructBegin(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T read error: ", p), err)
	}

	for {
		_, fieldTypeId, fieldId, err := tp.ReadFieldBegin(ctx)
		if err != nil {
			return thrift.PrependError(fmt.Sprintf("%T field %d read error: ", p, fieldId), err)
		}
		if fieldTypeId == thrift.STOP {
			break
		}
		switch fieldId {
		case 1:
			if fieldTypeId == thrift.I32 {
				if err := p.ReadField1(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		case 2:
			if fieldTypeId == thrift.STRING {
				if err := p.ReadField2(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		case 3:
			if fieldTypeId == thrift.MAP {
				if err := p.ReadField3(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		default:
			if err := tp.Skip(ctx, fieldTypeId); err != nil {
				return err
			}
		}
		if err := tp.ReadFieldEnd(ctx); err != nil {
			return err
		}
	}
	if err := tp.ReadStructEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T read struct end error: ", p), err)
	}
	return nil
}

func (p *MIntResponse) ReadField1(ctx context.Context, tp thrift.TProtocol) error {
	if v, err := tp.ReadI32(ctx); err != nil {
		return thrift.PrependError("error reading field 1: ", err)
	} else {
		p.Code = v
	}
	return nil
}

func (p *MIntResponse) ReadField2(ctx context.Context, tp thrift.TProtocol) error {
	if v, err := tp.ReadString(ctx); err != nil {
		return thrift.PrependError("error reading field 2: ", err)
	} else {
		p.Msg = v
	}
	return nil
}

func (p *MIntResponse) ReadField3(ctx context.Context, tp thrift.TProtocol) error {
	_, _, size, err := tp.ReadMapBegin(ctx)
	if err != nil {
		return thrift.PrependError("error reading map begin: ", err)
	}
	tMap := make(map[string]int64, size)
	p.Res = tMap
	for i := 0; i < size; i++ {
		var _key3 string
		if v, err := tp.ReadString(ctx); err != nil {
			return thrift.PrependError("error reading field 0: ", err)
		} else {
			_key3 = v
		}
		var _val4 int64
		if v, err := tp.ReadI64(ctx); err != nil {
			return thrift.PrependError("error reading field 0: ", err)
		} else {
			_val4 = v
		}
		p.Res[_key3] = _val4
	}
	if err := tp.ReadMapEnd(ctx); err != nil {
		return thrift.PrependError("error reading map end: ", err)
	}
	return nil
}

func (p *MIntResponse) Write(ctx context.Context, tp thrift.TProtocol) error {
	if err := tp.WriteStructBegin(ctx, "mIntResponse"); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write struct begin error: ", p), err)
	}
	if p != nil {
		if err := p.writeField1(ctx, tp); err != nil {
			return err
		}
		if err := p.writeField2(ctx, tp); err != nil {
			return err
		}
		if err := p.writeField3(ctx, tp); err != nil {
			return err
		}
	}
	if err := tp.WriteFieldStop(ctx); err != nil {
		return thrift.PrependError("write field stop error: ", err)
	}
	if err := tp.WriteStructEnd(ctx); err != nil {
		return thrift.PrependError("write struct stop error: ", err)
	}
	return nil
}

func (p *MIntResponse) writeField1(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "code", thrift.I32, 1); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 1:code: ", p), err)
	}
	if err := tp.WriteI32(ctx, int32(p.Code)); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T.code (1) field write error: ", p), err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 1:code: ", p), err)
	}
	return err
}

func (p *MIntResponse) writeField2(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "msg", thrift.STRING, 2); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 2:msg: ", p), err)
	}
	if err := tp.WriteString(ctx, string(p.Msg)); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T.msg (2) field write error: ", p), err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 2:msg: ", p), err)
	}
	return err
}

func (p *MIntResponse) writeField3(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "res", thrift.MAP, 3); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 3:res: ", p), err)
	}
	if err := tp.WriteMapBegin(ctx, thrift.STRING, thrift.I64, len(p.Res)); err != nil {
		return thrift.PrependError("error writing map begin: ", err)
	}
	for k, v := range p.Res {
		if err := tp.WriteString(ctx, string(k)); err != nil {
			return thrift.PrependError(fmt.Sprintf("%T. (0) field write error: ", p), err)
		}
		if err := tp.WriteI64(ctx, int64(v)); err != nil {
			return thrift.PrependError(fmt.Sprintf("%T. (0) field write error: ", p), err)
		}
	}
	if err := tp.WriteMapEnd(ctx); err != nil {
		return thrift.PrependError("error writing map end: ", err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 3:res: ", p), err)
	}
	return err
}

func (p *MIntResponse) String() string {
	if p == nil {
		return "<nil>"
	}
	return fmt.Sprintf("MIntResponse(%+v)", *p)
}

// Attributes:
//  - Code
//  - Msg
//  - Res
type FloatResponse struct {
	Code int32   `thrift:"code,1" db:"code" json:"code"`
	Msg  string  `thrift:"msg,2" db:"msg" json:"msg"`
	Res  float64 `thrift:"res,3" db:"res" json:"res"`
}

func NewFloatResponse() *FloatResponse {
	return &FloatResponse{}
}

func (p *FloatResponse) GetCode() int32 {
	return p.Code
}

func (p *FloatResponse) GetMsg() string {
	return p.Msg
}

func (p *FloatResponse) GetRes() float64 {
	return p.Res
}

func (p *FloatResponse) Read(ctx context.Context, tp thrift.TProtocol) error {
	if _, err := tp.ReadStructBegin(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T read error: ", p), err)
	}

	for {
		_, fieldTypeId, fieldId, err := tp.ReadFieldBegin(ctx)
		if err != nil {
			return thrift.PrependError(fmt.Sprintf("%T field %d read error: ", p, fieldId), err)
		}
		if fieldTypeId == thrift.STOP {
			break
		}
		switch fieldId {
		case 1:
			if fieldTypeId == thrift.I32 {
				if err := p.ReadField1(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		case 2:
			if fieldTypeId == thrift.STRING {
				if err := p.ReadField2(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		case 3:
			if fieldTypeId == thrift.DOUBLE {
				if err := p.ReadField3(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		default:
			if err := tp.Skip(ctx, fieldTypeId); err != nil {
				return err
			}
		}
		if err := tp.ReadFieldEnd(ctx); err != nil {
			return err
		}
	}
	if err := tp.ReadStructEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T read struct end error: ", p), err)
	}
	return nil
}

func (p *FloatResponse) ReadField1(ctx context.Context, tp thrift.TProtocol) error {
	if v, err := tp.ReadI32(ctx); err != nil {
		return thrift.PrependError("error reading field 1: ", err)
	} else {
		p.Code = v
	}
	return nil
}

func (p *FloatResponse) ReadField2(ctx context.Context, tp thrift.TProtocol) error {
	if v, err := tp.ReadString(ctx); err != nil {
		return thrift.PrependError("error reading field 2: ", err)
	} else {
		p.Msg = v
	}
	return nil
}

func (p *FloatResponse) ReadField3(ctx context.Context, tp thrift.TProtocol) error {
	if v, err := tp.ReadDouble(ctx); err != nil {
		return thrift.PrependError("error reading field 3: ", err)
	} else {
		p.Res = v
	}
	return nil
}

func (p *FloatResponse) Write(ctx context.Context, tp thrift.TProtocol) error {
	if err := tp.WriteStructBegin(ctx, "floatResponse"); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write struct begin error: ", p), err)
	}
	if p != nil {
		if err := p.writeField1(ctx, tp); err != nil {
			return err
		}
		if err := p.writeField2(ctx, tp); err != nil {
			return err
		}
		if err := p.writeField3(ctx, tp); err != nil {
			return err
		}
	}
	if err := tp.WriteFieldStop(ctx); err != nil {
		return thrift.PrependError("write field stop error: ", err)
	}
	if err := tp.WriteStructEnd(ctx); err != nil {
		return thrift.PrependError("write struct stop error: ", err)
	}
	return nil
}

func (p *FloatResponse) writeField1(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "code", thrift.I32, 1); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 1:code: ", p), err)
	}
	if err := tp.WriteI32(ctx, int32(p.Code)); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T.code (1) field write error: ", p), err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 1:code: ", p), err)
	}
	return err
}

func (p *FloatResponse) writeField2(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "msg", thrift.STRING, 2); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 2:msg: ", p), err)
	}
	if err := tp.WriteString(ctx, string(p.Msg)); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T.msg (2) field write error: ", p), err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 2:msg: ", p), err)
	}
	return err
}

func (p *FloatResponse) writeField3(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "res", thrift.DOUBLE, 3); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 3:res: ", p), err)
	}
	if err := tp.WriteDouble(ctx, float64(p.Res)); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T.res (3) field write error: ", p), err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 3:res: ", p), err)
	}
	return err
}

func (p *FloatResponse) String() string {
	if p == nil {
		return "<nil>"
	}
	return fmt.Sprintf("FloatResponse(%+v)", *p)
}

// Attributes:
//  - Code
//  - Msg
//  - Res
type BytesResponse struct {
	Code int32  `thrift:"code,1" db:"code" json:"code"`
	Msg  string `thrift:"msg,2" db:"msg" json:"msg"`
	Res  []byte `thrift:"res,3" db:"res" json:"res"`
}

func NewBytesResponse() *BytesResponse {
	return &BytesResponse{}
}

func (p *BytesResponse) GetCode() int32 {
	return p.Code
}

func (p *BytesResponse) GetMsg() string {
	return p.Msg
}

func (p *BytesResponse) GetRes() []byte {
	return p.Res
}

func (p *BytesResponse) Read(ctx context.Context, tp thrift.TProtocol) error {
	if _, err := tp.ReadStructBegin(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T read error: ", p), err)
	}

	for {
		_, fieldTypeId, fieldId, err := tp.ReadFieldBegin(ctx)
		if err != nil {
			return thrift.PrependError(fmt.Sprintf("%T field %d read error: ", p, fieldId), err)
		}
		if fieldTypeId == thrift.STOP {
			break
		}
		switch fieldId {
		case 1:
			if fieldTypeId == thrift.I32 {
				if err := p.ReadField1(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		case 2:
			if fieldTypeId == thrift.STRING {
				if err := p.ReadField2(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		case 3:
			if fieldTypeId == thrift.STRING {
				if err := p.ReadField3(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		default:
			if err := tp.Skip(ctx, fieldTypeId); err != nil {
				return err
			}
		}
		if err := tp.ReadFieldEnd(ctx); err != nil {
			return err
		}
	}
	if err := tp.ReadStructEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T read struct end error: ", p), err)
	}
	return nil
}

func (p *BytesResponse) ReadField1(ctx context.Context, tp thrift.TProtocol) error {
	if v, err := tp.ReadI32(ctx); err != nil {
		return thrift.PrependError("error reading field 1: ", err)
	} else {
		p.Code = v
	}
	return nil
}

func (p *BytesResponse) ReadField2(ctx context.Context, tp thrift.TProtocol) error {
	if v, err := tp.ReadString(ctx); err != nil {
		return thrift.PrependError("error reading field 2: ", err)
	} else {
		p.Msg = v
	}
	return nil
}

func (p *BytesResponse) ReadField3(ctx context.Context, tp thrift.TProtocol) error {
	if v, err := tp.ReadBinary(ctx); err != nil {
		return thrift.PrependError("error reading field 3: ", err)
	} else {
		p.Res = v
	}
	return nil
}

func (p *BytesResponse) Write(ctx context.Context, tp thrift.TProtocol) error {
	if err := tp.WriteStructBegin(ctx, "bytesResponse"); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write struct begin error: ", p), err)
	}
	if p != nil {
		if err := p.writeField1(ctx, tp); err != nil {
			return err
		}
		if err := p.writeField2(ctx, tp); err != nil {
			return err
		}
		if err := p.writeField3(ctx, tp); err != nil {
			return err
		}
	}
	if err := tp.WriteFieldStop(ctx); err != nil {
		return thrift.PrependError("write field stop error: ", err)
	}
	if err := tp.WriteStructEnd(ctx); err != nil {
		return thrift.PrependError("write struct stop error: ", err)
	}
	return nil
}

func (p *BytesResponse) writeField1(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "code", thrift.I32, 1); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 1:code: ", p), err)
	}
	if err := tp.WriteI32(ctx, int32(p.Code)); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T.code (1) field write error: ", p), err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 1:code: ", p), err)
	}
	return err
}

func (p *BytesResponse) writeField2(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "msg", thrift.STRING, 2); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 2:msg: ", p), err)
	}
	if err := tp.WriteString(ctx, string(p.Msg)); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T.msg (2) field write error: ", p), err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 2:msg: ", p), err)
	}
	return err
}

func (p *BytesResponse) writeField3(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "res", thrift.STRING, 3); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 3:res: ", p), err)
	}
	if err := tp.WriteBinary(ctx, p.Res); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T.res (3) field write error: ", p), err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 3:res: ", p), err)
	}
	return err
}

func (p *BytesResponse) String() string {
	if p == nil {
		return "<nil>"
	}
	return fmt.Sprintf("BytesResponse(%+v)", *p)
}

// Attributes:
//  - Code
//  - Msg
//  - Res
type MBytesResponse struct {
	Code int32             `thrift:"code,1" db:"code" json:"code"`
	Msg  string            `thrift:"msg,2" db:"msg" json:"msg"`
	Res  map[string][]byte `thrift:"res,3" db:"res" json:"res"`
}

func NewMBytesResponse() *MBytesResponse {
	return &MBytesResponse{}
}

func (p *MBytesResponse) GetCode() int32 {
	return p.Code
}

func (p *MBytesResponse) GetMsg() string {
	return p.Msg
}

func (p *MBytesResponse) GetRes() map[string][]byte {
	return p.Res
}

func (p *MBytesResponse) Read(ctx context.Context, tp thrift.TProtocol) error {
	if _, err := tp.ReadStructBegin(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T read error: ", p), err)
	}

	for {
		_, fieldTypeId, fieldId, err := tp.ReadFieldBegin(ctx)
		if err != nil {
			return thrift.PrependError(fmt.Sprintf("%T field %d read error: ", p, fieldId), err)
		}
		if fieldTypeId == thrift.STOP {
			break
		}
		switch fieldId {
		case 1:
			if fieldTypeId == thrift.I32 {
				if err := p.ReadField1(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		case 2:
			if fieldTypeId == thrift.STRING {
				if err := p.ReadField2(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		case 3:
			if fieldTypeId == thrift.MAP {
				if err := p.ReadField3(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		default:
			if err := tp.Skip(ctx, fieldTypeId); err != nil {
				return err
			}
		}
		if err := tp.ReadFieldEnd(ctx); err != nil {
			return err
		}
	}
	if err := tp.ReadStructEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T read struct end error: ", p), err)
	}
	return nil
}

func (p *MBytesResponse) ReadField1(ctx context.Context, tp thrift.TProtocol) error {
	if v, err := tp.ReadI32(ctx); err != nil {
		return thrift.PrependError("error reading field 1: ", err)
	} else {
		p.Code = v
	}
	return nil
}

func (p *MBytesResponse) ReadField2(ctx context.Context, tp thrift.TProtocol) error {
	if v, err := tp.ReadString(ctx); err != nil {
		return thrift.PrependError("error reading field 2: ", err)
	} else {
		p.Msg = v
	}
	return nil
}

func (p *MBytesResponse) ReadField3(ctx context.Context, tp thrift.TProtocol) error {
	_, _, size, err := tp.ReadMapBegin(ctx)
	if err != nil {
		return thrift.PrependError("error reading map begin: ", err)
	}
	tMap := make(map[string][]byte, size)
	p.Res = tMap
	for i := 0; i < size; i++ {
		var _key5 string
		if v, err := tp.ReadString(ctx); err != nil {
			return thrift.PrependError("error reading field 0: ", err)
		} else {
			_key5 = v
		}
		var _val6 []byte
		if v, err := tp.ReadBinary(ctx); err != nil {
			return thrift.PrependError("error reading field 0: ", err)
		} else {
			_val6 = v
		}
		p.Res[_key5] = _val6
	}
	if err := tp.ReadMapEnd(ctx); err != nil {
		return thrift.PrependError("error reading map end: ", err)
	}
	return nil
}

func (p *MBytesResponse) Write(ctx context.Context, tp thrift.TProtocol) error {
	if err := tp.WriteStructBegin(ctx, "mBytesResponse"); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write struct begin error: ", p), err)
	}
	if p != nil {
		if err := p.writeField1(ctx, tp); err != nil {
			return err
		}
		if err := p.writeField2(ctx, tp); err != nil {
			return err
		}
		if err := p.writeField3(ctx, tp); err != nil {
			return err
		}
	}
	if err := tp.WriteFieldStop(ctx); err != nil {
		return thrift.PrependError("write field stop error: ", err)
	}
	if err := tp.WriteStructEnd(ctx); err != nil {
		return thrift.PrependError("write struct stop error: ", err)
	}
	return nil
}

func (p *MBytesResponse) writeField1(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "code", thrift.I32, 1); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 1:code: ", p), err)
	}
	if err := tp.WriteI32(ctx, int32(p.Code)); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T.code (1) field write error: ", p), err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 1:code: ", p), err)
	}
	return err
}

func (p *MBytesResponse) writeField2(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "msg", thrift.STRING, 2); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 2:msg: ", p), err)
	}
	if err := tp.WriteString(ctx, string(p.Msg)); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T.msg (2) field write error: ", p), err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 2:msg: ", p), err)
	}
	return err
}

func (p *MBytesResponse) writeField3(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "res", thrift.MAP, 3); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 3:res: ", p), err)
	}
	if err := tp.WriteMapBegin(ctx, thrift.STRING, thrift.STRING, len(p.Res)); err != nil {
		return thrift.PrependError("error writing map begin: ", err)
	}
	for k, v := range p.Res {
		if err := tp.WriteString(ctx, string(k)); err != nil {
			return thrift.PrependError(fmt.Sprintf("%T. (0) field write error: ", p), err)
		}
		if err := tp.WriteBinary(ctx, v); err != nil {
			return thrift.PrependError(fmt.Sprintf("%T. (0) field write error: ", p), err)
		}
	}
	if err := tp.WriteMapEnd(ctx); err != nil {
		return thrift.PrependError("error writing map end: ", err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 3:res: ", p), err)
	}
	return err
}

func (p *MBytesResponse) String() string {
	if p == nil {
		return "<nil>"
	}
	return fmt.Sprintf("MBytesResponse(%+v)", *p)
}

// Attributes:
//  - Code
//  - Msg
//  - Res
type LBytesResponse struct {
	Code int32    `thrift:"code,1" db:"code" json:"code"`
	Msg  string   `thrift:"msg,2" db:"msg" json:"msg"`
	Res  [][]byte `thrift:"res,3" db:"res" json:"res"`
}

func NewLBytesResponse() *LBytesResponse {
	return &LBytesResponse{}
}

func (p *LBytesResponse) GetCode() int32 {
	return p.Code
}

func (p *LBytesResponse) GetMsg() string {
	return p.Msg
}

func (p *LBytesResponse) GetRes() [][]byte {
	return p.Res
}
func (p *LBytesResponse) Read(ctx context.Context, tp thrift.TProtocol) error {
	if _, err := tp.ReadStructBegin(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T read error: ", p), err)
	}

	for {
		_, fieldTypeId, fieldId, err := tp.ReadFieldBegin(ctx)
		if err != nil {
			return thrift.PrependError(fmt.Sprintf("%T field %d read error: ", p, fieldId), err)
		}
		if fieldTypeId == thrift.STOP {
			break
		}
		switch fieldId {
		case 1:
			if fieldTypeId == thrift.I32 {
				if err := p.ReadField1(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		case 2:
			if fieldTypeId == thrift.STRING {
				if err := p.ReadField2(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		case 3:
			if fieldTypeId == thrift.LIST {
				if err := p.ReadField3(ctx, tp); err != nil {
					return err
				}
			} else {
				if err := tp.Skip(ctx, fieldTypeId); err != nil {
					return err
				}
			}
		default:
			if err := tp.Skip(ctx, fieldTypeId); err != nil {
				return err
			}
		}
		if err := tp.ReadFieldEnd(ctx); err != nil {
			return err
		}
	}
	if err := tp.ReadStructEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T read struct end error: ", p), err)
	}
	return nil
}

func (p *LBytesResponse) ReadField1(ctx context.Context, tp thrift.TProtocol) error {
	if v, err := tp.ReadI32(ctx); err != nil {
		return thrift.PrependError("error reading field 1: ", err)
	} else {
		p.Code = v
	}
	return nil
}

func (p *LBytesResponse) ReadField2(ctx context.Context, tp thrift.TProtocol) error {
	if v, err := tp.ReadString(ctx); err != nil {
		return thrift.PrependError("error reading field 2: ", err)
	} else {
		p.Msg = v
	}
	return nil
}

func (p *LBytesResponse) ReadField3(ctx context.Context, tp thrift.TProtocol) error {
	_, size, err := tp.ReadListBegin(ctx)
	if err != nil {
		return thrift.PrependError("error reading list begin: ", err)
	}
	tSlice := make([][]byte, 0, size)
	p.Res = tSlice
	for i := 0; i < size; i++ {
		var _elem7 []byte
		if v, err := tp.ReadBinary(ctx); err != nil {
			return thrift.PrependError("error reading field 0: ", err)
		} else {
			_elem7 = v
		}
		p.Res = append(p.Res, _elem7)
	}
	if err := tp.ReadListEnd(ctx); err != nil {
		return thrift.PrependError("error reading list end: ", err)
	}
	return nil
}

func (p *LBytesResponse) Write(ctx context.Context, tp thrift.TProtocol) error {
	if err := tp.WriteStructBegin(ctx, "lBytesResponse"); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write struct begin error: ", p), err)
	}
	if p != nil {
		if err := p.writeField1(ctx, tp); err != nil {
			return err
		}
		if err := p.writeField2(ctx, tp); err != nil {
			return err
		}
		if err := p.writeField3(ctx, tp); err != nil {
			return err
		}
	}
	if err := tp.WriteFieldStop(ctx); err != nil {
		return thrift.PrependError("write field stop error: ", err)
	}
	if err := tp.WriteStructEnd(ctx); err != nil {
		return thrift.PrependError("write struct stop error: ", err)
	}
	return nil
}

func (p *LBytesResponse) writeField1(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "code", thrift.I32, 1); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 1:code: ", p), err)
	}
	if err := tp.WriteI32(ctx, int32(p.Code)); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T.code (1) field write error: ", p), err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 1:code: ", p), err)
	}
	return err
}

func (p *LBytesResponse) writeField2(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "msg", thrift.STRING, 2); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 2:msg: ", p), err)
	}
	if err := tp.WriteString(ctx, string(p.Msg)); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T.msg (2) field write error: ", p), err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 2:msg: ", p), err)
	}
	return err
}

func (p *LBytesResponse) writeField3(ctx context.Context, tp thrift.TProtocol) (err error) {
	if err := tp.WriteFieldBegin(ctx, "res", thrift.LIST, 3); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field begin error 3:res: ", p), err)
	}
	if err := tp.WriteListBegin(ctx, thrift.STRING, len(p.Res)); err != nil {
		return thrift.PrependError("error writing list begin: ", err)
	}
	for _, v := range p.Res {
		if err := tp.WriteBinary(ctx, v); err != nil {
			return thrift.PrependError(fmt.Sprintf("%T. (0) field write error: ", p), err)
		}
	}
	if err := tp.WriteListEnd(ctx); err != nil {
		return thrift.PrependError("error writing list end: ", err)
	}
	if err := tp.WriteFieldEnd(ctx); err != nil {
		return thrift.PrependError(fmt.Sprintf("%T write field end error 3:res: ", p), err)
	}
	return err
}

func (p *LBytesResponse) String() string {
	if p == nil {
		return "<nil>"
	}
	return fmt.Sprintf("LBytesResponse(%+v)", *p)
}
