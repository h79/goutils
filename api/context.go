package api

import (
	"gitee.com/h79/goutils/auth/token"
	"gitee.com/h79/goutils/common/app"
	"gitee.com/h79/goutils/common/http"
	"gitee.com/h79/goutils/common/stringutil"
	"strings"
)

type HeaderContext interface {
	Set(key, value string)
	Get(key string) string
}

type CacheContext interface {
	MustGet(key string) interface{}
	Get(key string) (interface{}, bool)
	Set(key string, value interface{})
}

type HttpContext interface {
	Send(httpCode int, data interface{})
	Abort()
	Next()
	Param(key string) string
	Query(key string) string
	ClientIP() string
}

type Context interface {
	HttpContext() HttpContext
	HeaderContext() HeaderContext
	CacheContext() CacheContext
	AuthContext() token.Engine
}

func Get[T any](ctx Context, key string) T {
	var a T
	if v, exist := ctx.CacheContext().Get(key); exist {
		a = v.(T)
	}
	return a
}

func MustGet[T any](ctx Context, key string) T {
	var v = ctx.CacheContext().MustGet(key)
	return v.(T)
}

func SetUid(ctx Context, uid string) {
	ctx.CacheContext().Set(Uid, uid)
}

func GetUid(ctx Context) string {
	return Get[string](ctx, Uid)
}

func SetInt64Uid(ctx Context, uid int64) {
	SetUid(ctx, stringutil.Int64ToString(uid))
}

func GetInt64Uid(ctx Context) int64 {
	return stringutil.StringToInt64(GetUid(ctx))
}

func GetHead(ctx Context) *Header {
	return Get[*Header](ctx, Head)
}

func GetBase(ctx Context) Base {
	header := GetHead(ctx)
	return header.Base
}

func GetReqHead(ctx Context) ReqHead {
	header := GetHead(ctx)
	return header.ReqHead
}

func GetCustom(ctx Context) interface{} {
	header := GetHead(ctx)
	return header.Custom
}

func GetSystem(ctx Context) app.System {
	return GetBase(ctx).System
}

func GetApp(ctx Context) app.Info {
	return GetBase(ctx).App
}

func GetClientIP(ctx Context) string {
	return GetBase(ctx).ClientIP
}

func QueryInt64(ctx Context, key string) int64 {
	return stringutil.StringToInt64(ctx.HttpContext().Query(key))
}

func HeadInt64(ctx Context, key string) int64 {
	return stringutil.StringToInt64(ctx.HeaderContext().Get(key))
}

func ParamInt64(ctx Context, key string) int64 {
	return stringutil.StringToInt64(ctx.HttpContext().Param(key))
}

const (
	UNK         = "unk"
	XML         = "xml"
	JSON        = "json"
	PLAIN       = "plain"
	ContentType = "Content-Type"
)

func GetContentType(ctx Context) string {
	return ContentTypeTo(ctx.HeaderContext().Get(ContentType))
}

func ContentTypeTo(ct string) string {
	ct = strings.ToLower(ct)
	if strings.Contains(ct, http.MimeJSON) {
		return JSON
	}
	if strings.Contains(ct, http.MimeXML2) ||
		strings.Contains(ct, http.MimeXML) {
		return XML
	}
	if strings.Contains(ct, http.MimePlain) {
		return PLAIN
	}
	return UNK
}
