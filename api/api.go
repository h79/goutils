package api

import (
	"gitee.com/h79/goutils/common/option"
	"gitee.com/h79/goutils/common/result"
	"gitee.com/h79/goutils/request"
)

type ResponseBuilder interface {
	BuildError(head request.HeadV2, err error, data interface{}, opts ...option.Option) interface{}
	Build(head request.HeadV2, result result.Result, data interface{}, opts ...option.Option) interface{}
	Succeed() result.Result
}

var defResponseBuilder = &responseBuilder{}

type responseBuilder struct {
}

func (*responseBuilder) BuildError(head request.HeadV2, err error, data interface{}, opts ...option.Option) interface{} {
	return NewResponseWithError(head, err, data, opts...)
}

func (*responseBuilder) Build(head request.HeadV2, result result.Result, data interface{}, opts ...option.Option) interface{} {
	return NewResponse(head, result, data, opts...)
}

func (*responseBuilder) Succeed() result.Result {
	return result.Succeed()
}

type Api struct {
	Builder ResponseBuilder
}

func (a *Api) GetBuilder() ResponseBuilder {
	if a.Builder == nil {
		return defResponseBuilder
	}
	return a.Builder
}

func (a *Api) SendEx(ctx Context, httpCode int, result result.Result, data interface{}, opts ...option.Option) {
	req := GetReqHead(ctx)
	ctx.HttpContext().Send(httpCode, a.GetBuilder().Build(&req, result, data, opts...))
}

func (a *Api) SendHead(ctx Context, httpCode int, req request.HeadV2, result result.Result, data interface{}, opts ...option.Option) {
	ctx.HttpContext().Send(httpCode, a.GetBuilder().Build(req, result, data, opts...))
}

func (a *Api) SendNHead(ctx Context, httpCode int, result result.Result, data interface{}, opts ...option.Option) {
	req := GetReqHead(ctx)
	ctx.HttpContext().Send(httpCode, a.GetBuilder().Build(&req, result, data, opts...))
}

func (a *Api) SendMaybeError(ctx Context, httpCode int, data interface{}, err error, opts ...option.Option) {
	req := GetReqHead(ctx)
	if err != nil {
		ctx.HttpContext().Send(httpCode, a.GetBuilder().BuildError(&req, err, data, opts...))
	} else {
		ctx.HttpContext().Send(200, a.GetBuilder().Build(&req, a.GetBuilder().Succeed(), data, opts...))
	}
}

func (a *Api) SendException(ctx Context, httpCode int, req request.HeadV2, opts ...option.Option) {
	ctx.HttpContext().Send(httpCode, a.GetBuilder().BuildError(req, result.RErrException, nil, opts...))
}
