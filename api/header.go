package api

import (
	"gitee.com/h79/goutils/common/stringutil"
	"gitee.com/h79/goutils/common/timer"
)

const (
	CSysVersion = "X-H79-SysVersion"
	CDeviceId   = "X-H79-DeviceId"
	CModel      = "X-H79-Model"
	CWifi       = "X-H79-Wifi"
	CChannel    = "X-H79-Channel"
	CExtInfo    = "X-H79-ExtInfo"
	CTerminal   = "terminal"
	CAppId      = "appId"
	CVersion    = "version"
	CSeqId      = "seqId"
	CSource     = "source"
	CTimeAt     = "timeAt"
	CToken      = "Authorization"
	CSign       = "sign"
	CTimeStamp  = "timestamp"
	XReqTime    = "X-ReqTime"
	XSeqId      = "X-SeqNo"
	XSign       = "X-Sign"
	XTimeStamp  = "X-Timestamp"
)

type Header struct {
	Base
	ReqHead
	Token string
	// 自定义属性
	Custom interface{}
}

func NewHeader(req ReqHead) *Header {
	h := &Header{
		ReqHead: req,
	}
	h.RecvAt = timer.CurrentMS()
	return h
}

func (h *Header) WriteTo(ctx Context) {
	headerCtx := ctx.HeaderContext()

	headerCtx.Set(CVersion, h.Version)
	headerCtx.Set(CSource, h.Source)
	headerCtx.Set(CSeqId, h.SeqId)

	headerCtx.Set(CDeviceId, h.System.DeviceId)
	headerCtx.Set(CSysVersion, h.System.SysVer)
	headerCtx.Set(CModel, h.System.Model)
	headerCtx.Set(CWifi, h.System.Wifi)

	headerCtx.Set(CAppId, h.App.AppId)
	headerCtx.Set(CTerminal, h.App.Terminal)
	headerCtx.Set(CVersion, h.App.Version)
	headerCtx.Set(CChannel, h.App.Channel)
	headerCtx.Set(CExtInfo, h.App.ExtInfo)
	headerCtx.Set(CToken, Bearer(h.Token))
}

func (h *Header) ReadFrom(ctx Context) {
	headerCtx := ctx.HeaderContext()

	time := headerCtx.Get(CTimeAt)
	if time == "" {
		time = headerCtx.Get(XReqTime)
	}
	h.ReqAt = stringutil.StringToInt64(time)

	h.Version = headerCtx.Get(CVersion)
	h.Source = headerCtx.Get(CSource)
	h.SeqId = headerCtx.Get(CSeqId)
	if h.SeqId == "" {
		h.SeqId = headerCtx.Get(XSeqId)
	}

	h.System.DeviceId = headerCtx.Get(CDeviceId)
	h.System.SysVer = headerCtx.Get(CSysVersion)
	h.System.Model = headerCtx.Get(CModel)
	h.System.Wifi = headerCtx.Get(CWifi)

	h.App.Source = h.Source
	h.App.AppId = headerCtx.Get(CAppId)
	h.App.Terminal = headerCtx.Get(CTerminal)
	h.App.Version = headerCtx.Get(CVersion)
	h.App.Channel = headerCtx.Get(CChannel)
	h.App.ExtInfo = headerCtx.Get(CExtInfo)

	if tk, er := TokenReq(ctx); er == nil {
		h.Token = tk
	}
}

func (h *Header) HasApp() bool {
	return h.App.HasValid()
}

func (h *Header) HasSystem() bool {
	return h.System.HasValid()
}

func (h *Header) HasDeviceId() bool {
	return len(h.System.DeviceId) > 0
}

func (h *Header) HasToken() bool {
	return len(h.Token) > 0
}
