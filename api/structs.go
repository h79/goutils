package api

import (
	"gitee.com/h79/goutils/common/app"
	"gitee.com/h79/goutils/common/config"
	"gitee.com/h79/goutils/common/option"
	"gitee.com/h79/goutils/common/result"
	"gitee.com/h79/goutils/common/stringutil"
	"gitee.com/h79/goutils/common/timer"
	"gitee.com/h79/goutils/request"
	"time"
)

type Base struct {
	ClientIP string
	System   app.System
	App      app.Info
}

var _ request.Head = (*ReqHead)(nil)
var _ request.HeadV2 = (*ReqHead)(nil)

type ReqHead struct {
	Version string        `form:"version" binding:"-" json:"version"`
	Source  string        `form:"source" binding:"-" json:"source"`
	SeqId   string        `form:"seqId" binding:"-" json:"seqId"`
	ReqAt   int64         `form:"timeAt" binding:"-" json:"timeAt"` //请求者的时间(客户端时间）
	RecvAt  int64         `json:"-"`                                //收到请求时间(服务端时间)
	TimeOut time.Duration `json:"-"`
}

// GetSeqId request.Head interface
func (h *ReqHead) GetSeqId() string {
	return h.SeqId
}

// GetSource request.Head interface
func (h *ReqHead) GetSource() string {
	return h.Source
}

// GetVersion request.Head interface
func (h *ReqHead) GetVersion() string {
	return h.Version
}

// SetSource request.HeadV2 interface
func (h *ReqHead) SetSource(source string) {
	h.Source = source
}

// GetTimeOut request.HeadV2 interface
func (h *ReqHead) GetTimeOut() time.Duration {
	return h.TimeOut
}

// SetTimeOut request.HeadV2 interface
func (h *ReqHead) SetTimeOut(duration time.Duration) {
	h.TimeOut = duration
}

// GetRecvAt request.HeadV2 interface
func (h *ReqHead) GetRecvAt() int64 {
	return h.RecvAt
}

// GetReqAt request.HeadV2 interface
func (h *ReqHead) GetReqAt() int64 {
	return h.ReqAt
}

type Request struct {
	ReqHead
	Data string `json:"data,omitempty"` //json format string
}

func NewRequest() Request {
	config.SeqId++
	return Request{
		ReqHead: ReqHead{
			Version: config.Version,
			Source:  config.Source,
			SeqId:   stringutil.Int64ToString(config.SeqId),
		},
	}
}

type ResHead struct {
	result.Result
	Version  string `json:"version"`            //版本，请求者版本
	Source   string `json:"source,omitempty"`   //请求源，可以请求者填写
	SeqId    string `json:"seqId,omitempty"`    //请求序号，由请求者定义，服务器原路返回
	TimeAt   int64  `json:"timeAt,omitempty"`   //服务收到请求的时间(ms)
	DiffAt   int64  `json:"diffAt,omitempty"`   //客户端与服务端时间差值（ms)
	SpendAt  int64  `json:"spendAt,omitempty"`  //从服务收到请求到响应完成，所花的时长(ms)
	ServerAt int64  `json:"serverAt,omitempty"` //服务器时间，豪秒，用于检验对时(ms)
	LogId    string `json:"logId,omitempty"`
}

func NewResHead(res result.Result) ResHead {
	return ResHead{
		Result:   res,
		Version:  config.Version,
		Source:   config.Source,
		ServerAt: timer.CurrentMS(),
	}
}

type Response struct {
	ResHead
	EventType int32       `json:"eventType,omitempty"` //回应事件类型（业务自己定义,比如tcp，websocket)
	Total     int64       `json:"total,omitempty"`
	Page      int         `json:"page,omitempty"`
	Size      int         `json:"size,omitempty"`
	Data      interface{} `json:"data,omitempty"` //业务具体数据
}

func NewResponse(head request.HeadV2, result result.Result, data interface{}, opts ...option.Option) Response {
	resHead := NewResHead(result)
	resHead.Source = head.GetSource()
	resHead.SeqId = head.GetSeqId()
	resHead.TimeAt = head.GetRecvAt()
	resHead.DiffAt = resHead.TimeAt - head.GetReqAt()
	resHead.SpendAt = timer.CurrentMS() - head.GetRecvAt()
	res := Response{ResHead: resHead, Data: data}
	if page, ok := PageExist(opts...); ok {
		res.Total = page.Total
		res.Page = page.Page
		res.Size = page.Size
	}
	if et, ok := EventTypeExist(opts...); ok {
		res.EventType = et
	}
	return res
}

func NewResponseWithError(head request.HeadV2, err error, data interface{}, opts ...option.Option) Response {
	return NewResponse(head, result.WithErr(err), data)
}

const (
	TypePage = iota + 1000
	TypeEventType
)

type PageOption struct {
	Total int64
	Page  int
	Size  int
}

func WithPageOption(total int64) PageOption {
	return PageOption{Total: total}
}

func (t PageOption) String() string {
	return "page:response"
}
func (t PageOption) Type() int          { return TypePage }
func (t PageOption) Value() interface{} { return t }

func PageExist(opts ...option.Option) (PageOption, bool) {
	if r, ok := option.Exist(TypePage, opts...); ok {
		return r.Value().(PageOption), true
	}
	return PageOption{}, false
}

type EventType int32

func WithEventType(e int32) EventType {
	return EventType(e)
}

func (t EventType) String() string {
	return "eventType:response"
}
func (t EventType) Type() int          { return TypeEventType }
func (t EventType) Value() interface{} { return t }

func EventTypeExist(opts ...option.Option) (int32, bool) {
	if r, ok := option.Exist(TypeEventType, opts...); ok {
		return int32(r.Value().(EventType)), true
	}
	return 0, false
}
