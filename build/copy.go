package build

import (
	"fmt"
	"gitee.com/h79/goutils/common/archive"
	fileconfig "gitee.com/h79/goutils/common/file/config"
	"io"
	"os"
	"path/filepath"
)

type copyBuilder struct {
}

func (t *copyBuilder) Ext() string {
	return ""
}

func (*copyBuilder) New(w io.WriteCloser, conf archive.Config) archive.Archive {
	return &copyArchive{Path: conf.Path}
}

func (*copyBuilder) Coping(r *os.File, w io.Writer, conf archive.Config) (archive.Archive, error) {
	return nil, fmt.Errorf("not support")
}

type copyArchive struct {
	Path string
}

func (c *copyArchive) Close() error {
	return nil
}

func (c *copyArchive) Add(f fileconfig.File, stream ...fileconfig.ReaderStream) error {
	info, err := os.Lstat(f.Source) // #nosec
	if err != nil {
		return fmt.Errorf("%s: %w", f.Source, err)
	}
	filename := filepath.Join(c.Path, f.Destination)
	if info.IsDir() {
		return os.Mkdir(filename, f.Info.Mode)
	}
	_ = os.MkdirAll(filepath.Dir(filename), f.Info.Mode)
	// Open the file and create a new one
	r, err := os.Open(f.Source)
	if err != nil {
		return err
	}
	defer func(r *os.File) {
		err = r.Close()
		if err != nil {
		}
	}(r)

	w, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer func(w *os.File) {
		err = w.Close()
		if err != nil {
		}
	}(w)

	// Copy the content
	_, err = io.Copy(w, r)
	if err != nil {
		return err
	}
	if err != nil {
		return err
	}
	for i := range stream {
		stream[i].OnReader(r)
	}
	return nil
}
