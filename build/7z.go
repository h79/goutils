package build

import (
	"context"
	"fmt"
	"gitee.com/h79/goutils/common/archive"
	"gitee.com/h79/goutils/common/archive/e7z"
	fileconfig "gitee.com/h79/goutils/common/file/config"
	"io"
	"os"
	"path/filepath"
	"runtime"
)

type e7zBuilder struct {
}

func (t *e7zBuilder) Ext() string {
	return ".7z"
}

func (*e7zBuilder) New(target io.WriteCloser, conf archive.Config) archive.Archive {
	return &e7zArchive{Archive: e7z.New(target, conf.Path, conf.File), closed: false}
}

func (*e7zBuilder) Coping(r *os.File, w io.Writer, conf archive.Config) (archive.Archive, error) {
	return nil, fmt.Errorf("not support")
}

type e7zArchive struct {
	e7z.Archive
	closed bool
}

func (c *e7zArchive) Close() error {
	if c.closed {
		return nil
	}
	c.closed = true //防止重复close
	cmd := "7z"
	switch runtime.GOOS {
	case "windows":
		cmd = "7z.exe"
	}
	// for 7z 命令
	src := filepath.Join(c.Path, "*")
	if len(src) >= 2 && filepath.VolumeName(src) == "" && !os.IsPathSeparator(src[0]) {
		if src[0] != '.' && !os.IsPathSeparator(src[1]) {
			src = "." + string(filepath.Separator) + src
		}
	}
	return run(context.Background(), []string{cmd, "a", c.Filename, src}, nil, "")
}

func (c *e7zArchive) Add(f fileconfig.File, stream ...fileconfig.ReaderStream) error {
	err := c.Stream(f, stream...)
	if err != nil {
		c.closed = true
	}
	return err
}
