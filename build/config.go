package build

import (
	"regexp"
	"strconv"
	"strings"
)

// Build contains the build configuration section.
type Build struct {
	ID          string                          `yaml:"id,omitempty" json:"id,omitempty"`
	Goos        []string                        `yaml:"goos,omitempty" json:"goos,omitempty"`
	GoArch      []string                        `yaml:"goarch,omitempty" json:"goarch,omitempty"`
	GoArm       []string                        `yaml:"goarm,omitempty" json:"goarm,omitempty"`
	GoMips      []string                        `yaml:"gomips,omitempty" json:"gomips,omitempty"`
	GoAmd64     []string                        `yaml:"goamd64,omitempty" json:"goamd64,omitempty"`
	Executor    map[string]string               `yaml:"executor" json:"executor"` //生成的执行文件名,不同的操作系统对应不同的执行文件，"drawin":"projectName-mac",“linux":"projectName-linux","windows":"projectName-win.exe"
	ModulePath  string                          `yaml:"module_path" json:"module_path"`
	ProductId   string                          `yaml:"product_id" json:"product_id"`
	ProjectName string                          `yaml:"project_name" json:"project_name"`
	Target      string                          `yaml:"target,omitempty" json:"target,omitempty"` //生成目标目录
	Dir         string                          `yaml:"dir,omitempty" json:"dir,omitempty"`
	Main        string                          `yaml:"main,omitempty" json:"main,omitempty"`
	GoBinary    string                          `yaml:"go_binary,omitempty" json:"go_binary,omitempty"`
	Command     string                          `yaml:"command,omitempty" json:"command,omitempty"`
	Package     Package                         `yaml:"package" json:"package"`
	Details     `yaml:",inline" json:",inline"` // nolint: tagliatelle
}

type Details struct {
	Buildmode string      `yaml:"buildmode,omitempty" json:"buildmode,omitempty"`
	Ldflags   StringArray `yaml:"ldflags,omitempty" json:"ldflags,omitempty"`
	Tags      FlagArray   `yaml:"tags,omitempty" json:"tags,omitempty"`
	Flags     FlagArray   `yaml:"flags,omitempty" json:"flags,omitempty"`
	AsmFlags  StringArray `yaml:"asm_flags,omitempty" json:"asm_flags,omitempty"`
	GcFlags   StringArray `yaml:"gc_flags,omitempty" json:"gc_flags,omitempty"`
	Env       []string    `yaml:"env,omitempty" json:"env,omitempty"`
}

type PackCheck func(path, name string, isDir bool) bool
type PackCompleted func(out *PackOut, pf *PackFile, completed bool)

type PackOut struct {
	Version     string      `json:"version"`
	Path        string      `json:"path"` // ./build
	PackagePath string      `json:"packagePath"`
	PublishPath string      `json:"publishPath"`
	Name        string      `json:"name"` // file name
	Ext         string      `json:"ext"`  // file ext
	Target      string      `json:"target"`
	Url         string      `json:"url"`
	Hash        string      `json:"hash"` // updates.json 文件名(包括路径)
	Data        interface{} `json:"-"`    //自定义
}

const (
	OutHashFile       = 0x00001 // 输出updates.json(hash文件)
	HashNeedProject   = 0x00002 // hash文件需要用程序名
	HashNeedVersion   = 0x00004 // hash文件需要带版本号
	HashNeedModel     = 0x00008 // hash输出带model
	HashNeedOs        = 0x00010 // hash输出带os
	TargetNeedVersion = 0x00020
	TargetNeedModel   = 0x00040
	TargetNeedOs      = 0x00080
	TargetNeedDate    = 0x00100
	NeedArchiveHash   = 0x02000 // hashFile需要添加到压缩文件中
	OnlyPackOut       = 0x04000
	NotArchive        = 0x08000 //不需要压缩
	ArchiveVersion    = 0x10000 //压缩目录带版本
	ArchiveProject    = 0x20000 //压缩目录带产品名（ProjectName）
	NotCheckTpl       = 0x40000 //不需要检测模板 *.tpl
	NotCheckPackFile  = 0x80000 //不对 exclude, include 检测
)

var flagMap = map[string]int{
	"OutHashFile":       OutHashFile,
	"HashNeedProject":   HashNeedProject, // hash文件需要用程序名
	"HashNeedVersion":   HashNeedVersion, // hash文件需要带版本号
	"HashNeedModel":     HashNeedModel,   // hash输出带model
	"HashNeedOs":        HashNeedOs,      // hash输出带os
	"TargetNeedVersion": TargetNeedVersion,
	"TargetNeedModel":   TargetNeedModel,
	"TargetNeedOs":      TargetNeedOs,
	"TargetNeedDate":    TargetNeedDate,
	"NeedArchiveHash":   NeedArchiveHash, // hashFile需要添加到压缩文件中
	"OnlyPackOut":       OnlyPackOut,
	"NotArchive":        NotArchive,     //不需要压缩
	"ArchiveVersion":    ArchiveVersion, //压缩目录带版本
	"ArchiveProject":    ArchiveProject, //压缩目录带产品名（ProjectName）
	"NotCheckTpl":       NotCheckTpl,    //不需要检测模板 *.tpl
	"NotCheckPackFile":  NotCheckPackFile,
}

func AddFlag(f string, bit int) {
	flagMap[f] = bit
}

func GetFlag(f string) int {
	ret, ok := flagMap[f]
	if ok {
		return ret
	}
	return 0
}

type Package struct {
	ProductCode   string   `yaml:"productCode" json:"productCode"` //产品编号
	Model         string   `yaml:"model" json:"model"`             //prod, test, dev
	Os            string   `yaml:"os" json:"os"`                   //操作系统 windows,linux,darwin
	Format        string   `yaml:"format" json:"format"`           //zip, tar.gz, tgz, tar, cab(windows), setup
	Dist          string   `yaml:"dist" json:"dist"`               //输出目录
	PackagePath   string   `yaml:"packagePath" json:"packagePath"` // 包目录 dist/package_path， 默认 dist/packages
	PublishPath   string   `yaml:"publishPath" json:"publishPath"` // 发布目录 dist/publish_path 默认 dist/publishes
	Target        string   `yaml:"target" json:"target"`           //输出文件名，可以为空
	Hash          string   `yaml:"hash" json:"hash"`               //输出pack 总的包描述文件,如: updates.json
	BaseUrl       string   `yaml:"baseUrl" json:"baseUrl"`         //文件存储基本地址（oss，如阿里云)
	MgrHost       string   `yaml:"mgrHost" json:"mgrHost"`         //版本管理后台host,需要把打包的信息保存到后台进行管理，比如: http://xxx.com:port
	Flag          string   `yaml:"flag" json:"flag"`               //标识 @see OutHashFile
	Exclude       []string `yaml:"exclude" json:"exclude"`         //打包不包括的选项
	Include       []string `yaml:"include" json:"include"`         //打包包括的选项
	flag          Bit
	excludeR      []Regex
	includeR      []Regex
	excludeCheck  PackCheck
	includeCheck  PackCheck
	packCompleted PackCompleted
}

type Bit int32

func ToBit(b string) Bit {
	var ret int
	bits := strings.Split(b, "|")
	for i := range bits {
		bit, err := strconv.Atoi(bits[i])
		if err != nil {
			bit = GetFlag(bits[i])
		}
		ret |= bit
	}
	return Bit(ret)
}

func (a Bit) Bit(bit int32) bool {
	return int32(a)&bit == bit
}

type Regex struct {
	Path bool
	Exp  *regexp.Regexp
}

func (p *Package) Parse() {
	p.flag = ToBit(p.Flag)
}

func (p *Package) IsFlag(o int32) bool {
	return p.flag.Bit(o)
}

func (p *Package) WithExcludeCheck(check PackCheck) {
	p.excludeCheck = check
}

func (p *Package) WithIncludeCheck(check PackCheck) {
	p.includeCheck = check
}

func (p *Package) WithCompleted(check PackCompleted) {
	p.packCompleted = check
}

func (p *Package) buildCheck() {
	if len(p.Exclude) > 0 && len(p.excludeR) == 0 {
		p.excludeR = make([]Regex, len(p.Exclude))
	}
	if len(p.Include) > 0 && len(p.includeR) == 0 {
		p.includeR = make([]Regex, len(p.Include))
	}
}

const (
	RegexPrefix = "regex|"
	PathPrefix  = "path|"
)

func (p *Package) IncludeCheck(path, name string, isDir bool) bool {
	p.buildCheck()
	if p.includeCheck != nil {
		if p.includeCheck(path, name, isDir) {
			return true
		}
	}
	for i := range p.Include {
		if strings.EqualFold(p.Include[i], name) {
			return true
		}
		if p.includeR[i].Exp == nil {
			if strings.HasPrefix(p.Include[i], RegexPrefix) {
				p.includeR[i].Exp, _ = regexp.Compile(strings.TrimPrefix(p.Include[i], RegexPrefix))
			} else if strings.HasPrefix(p.Include[i], PathPrefix) {
				p.includeR[i].Exp, _ = regexp.Compile(strings.TrimPrefix(p.Include[i], PathPrefix))
				p.includeR[i].Path = true
			}
			if p.includeR[i].Exp == nil {
				continue
			}
		}
		if p.includeR[i].Path && isDir {
			if path != "" && p.includeR[i].Exp.MatchString(path) {
				return true
			}
		} else if p.includeR[i].Exp.MatchString(name) {
			return true
		}
	}
	return false
}

func (p *Package) ExcludeCheck(path, name string, isDir bool) bool {
	p.buildCheck()
	if p.excludeCheck != nil {
		if p.excludeCheck(path, name, isDir) {
			return true
		}
	}
	for i := range p.Exclude {
		if strings.EqualFold(p.Exclude[i], name) {
			return true
		}
		if p.excludeR[i].Exp == nil {
			if strings.HasPrefix(p.Exclude[i], RegexPrefix) {
				p.excludeR[i].Exp, _ = regexp.Compile(strings.TrimPrefix(p.Exclude[i], RegexPrefix))
			} else if strings.HasPrefix(p.Exclude[i], PathPrefix) {
				p.excludeR[i].Exp, _ = regexp.Compile(strings.TrimPrefix(p.Exclude[i], PathPrefix))
				p.excludeR[i].Path = true
			}
			if p.excludeR[i].Exp == nil {
				continue
			}
		}
		if p.excludeR[i].Path {
			if isDir && path != "" && p.excludeR[i].Exp.MatchString(path) {
				return true
			}
		} else if p.excludeR[i].Exp.MatchString(name) {
			return true
		}
	}
	return false
}

func (p *Package) Ignore(path, name string, isDir bool) bool {
	if p.IsFlag(NotCheckPackFile) {
		return false
	}
	ss := strings.ToLower(name)
	if p.CheckOs(ss) {
		return true
	}
	if p.IncludeCheck(path, ss, isDir) {
		return false
	}
	if p.ExcludeCheck(path, ss, isDir) {
		return true
	}
	return false
}

func (p *Package) CheckOs(name string) bool {
	if p.Model != "" {
		if strings.HasPrefix(name, "dev") && p.Model != "dev" {
			return true
		}
		if strings.HasPrefix(name, "prod") && p.Model != "prod" {
			return true
		}
		if strings.HasPrefix(name, "test") && p.Model != "test" {
			return true
		}
	}
	if p.Os != "" {
		if p.Os == "windows" {
			if strings.HasSuffix(name, ".sh") ||
				strings.HasSuffix(name, "-mac") ||
				strings.HasSuffix(name, "-linux") ||
				strings.HasSuffix(name, ".so") {
				return true
			}
		} else {
			if p.Os == "darwin" {
				if strings.HasSuffix(name, "-linux") {
					return true
				}
			} else if p.Os == "linux" {
				if strings.HasSuffix(name, "-mac") {
					return true
				}
			}
			if strings.HasSuffix(name, ".bat") ||
				strings.HasSuffix(name, ".exe") ||
				strings.HasSuffix(name, ".dll") {
				return true
			}
		}
	}
	return strings.HasSuffix(name, ".lib") ||
		strings.HasSuffix(name, ".go") ||
		strings.HasSuffix(name, ".c") ||
		strings.HasSuffix(name, ".h")
}

type PackFile struct {
	Name string `json:"name"`
	MD5  string `json:"md5"`
	Size int64  `json:"size"`
	Url  string `json:"url,omitempty"`
}

type HashFile struct {
	Os          string     `json:"os"`
	Version     string     `json:"version"`
	Model       string     `json:"model"`
	ProductId   string     `json:"product_id"`
	ProductCode string     `json:"product_code"`
	ProjectName string     `json:"project_name"`
	Packs       []PackFile `json:"files"`
}

// StringArray is a wrapper for an array of strings.
type StringArray []string

type FlagArray []string
