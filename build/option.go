package build

// Options to be passed down to a builder.
type Options struct {
	Name   string
	Path   string
	Ext    string
	Target string
	Goos   string
	Goarch string
}
