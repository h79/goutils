package build

import (
	"context"
	"fmt"
	"gitee.com/h79/goutils/common/archive"
	"gitee.com/h79/goutils/common/file"
	fileconfig "gitee.com/h79/goutils/common/file/config"
	"gitee.com/h79/goutils/common/json"
	"gitee.com/h79/goutils/common/logger"
	"go.uber.org/zap"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

// Default builder instance.
var Default = &Builder{}

func init() {
	_ = Default
	archive.Register("copy", &copyBuilder{})
	archive.Register("zip", archive.NewMultiBuilder(".zip", &copyBuilder{}, archive.NewBuilder("zip")))
	archive.Register("tgz", archive.NewMultiBuilder(".tgz", &copyBuilder{}, archive.NewBuilder("tgz")))
	archive.Register("tar.gz", archive.NewMultiBuilder(".tar.gz", &copyBuilder{}, archive.NewBuilder("tar.gz")))
	archive.Register("tar", archive.NewMultiBuilder(".tar", &copyBuilder{}, archive.NewBuilder("tar")))
	archive.Register("gz", archive.NewMultiBuilder(".gz", &copyBuilder{}, archive.NewBuilder("gz")))
	archive.Register("txz", archive.NewMultiBuilder(".txz", &copyBuilder{}, archive.NewBuilder("txz")))
	archive.Register("tar.xz", archive.NewMultiBuilder(".tar.xz", &copyBuilder{}, archive.NewBuilder("tar.xz")))
	archive.Register("7z", archive.NewMultiBuilder(".7z", &copyBuilder{}, &e7zBuilder{}))
}

// Builder is golang builder.
type Builder struct{}

// WithDefaults sets the defaults for a golang build and returns it.
func (*Builder) WithDefaults(build Build) (Build, error) {
	if build.GoBinary == "" {
		build.GoBinary = "go"
	}
	if build.Command == "" {
		build.Command = "build"
	}
	if build.Dir == "" {
		build.Dir = "."
	}
	if build.Main == "" {
		build.Main = "."
	}
	if len(build.Ldflags) == 0 {
		build.Ldflags = []string{"-s -w -X main.version={{.Version}} -X main.CommitV={{.Commit}} -X main.DateV={{.BuildDate}} -X main.BranchV={{.Branch}} -X main.TagV={{.Tag}} -X main.appName={{.ProjectName}}"}
	}

	if len(build.Goos) == 0 {
		build.Goos = []string{"linux", "darwin", "windows"}
	}
	if len(build.GoArch) == 0 {
		build.GoArch = []string{"amd64", "arm64", "386"}
	}
	return build, nil
}

// Build builds a golang build.
func (*Builder) Build(ctx *Context, build Build, opts Options) error {
	var venv []string
	venv = append(venv, ctx.Env.Strings()...)
	for _, e := range build.Details.Env {
		ee, err := NewTemplate(ctx).WithEnvS(venv).Apply(e)
		if err != nil {
			return err
		}
		logger.I("builder", "env '%v' evaluated to '%v'", e, ee)
		if ee != "" {
			venv = append(venv, ee)
		}
	}
	venv = append(
		venv,
		"GOOS="+opts.Goos,
		"GOARCH="+opts.Goarch,
	)

	cmd, err := buildGoBuildLine(ctx, build, build.Details, &opts, venv)
	if err != nil {
		return err
	}

	logger.I("builder", "build '%s' running", opts.Target)
	logger.I("builder", "build cmd= '%v'", strings.Join(cmd, " "))
	if err = run(ctx, cmd, venv, build.Dir); err != nil {
		logger.E("builder", "build '%s' failure, err= %s", opts.Target, err)
		return fmt.Errorf("failed to build for %s: %w", opts.Target, err)
	}
	logger.I("builder", "build '%s' completed", opts.Target)
	return nil
}

func (b *Builder) BuildHashFileName(ctx *Context, bit Bit, filename string) string {
	temp := NewTemplate(ctx)
	if filename == "" {
		if !bit.Bit(HashNeedProject) || ctx.Config.ProjectName == "" {
			temp.AddField(projectName, "")
		} else {
			temp.AddField(projectName, "_"+ctx.Config.ProjectName)
		}
		if !bit.Bit(HashNeedModel) || ctx.Config.Model == "" {
			temp.AddField(model, "")
		} else {
			temp.AddField(model, "_"+ctx.Config.Model)
		}
		if !bit.Bit(HashNeedOs) || ctx.Config.Os == "" {
			temp.AddField(osKey, "")
		} else {
			temp.AddField(osKey, "_"+ctx.Config.Os)
		}
		if !bit.Bit(HashNeedVersion) {
			temp.AddField(version, "")
		} else {
			temp.AddField(version, "-"+ctx.Config.Version)
		}
		filename = "updates{{.ProjectName}}{{.Model}}{{.Os}}{{.Version}}.json"
	}
	filename, err := temp.Apply(filename)
	if err != nil {
		return "updates.json"
	}
	return filename
}

func (b *Builder) BuildTargetFileName(ctx *Context, bit Bit, filename string) string {
	temp := NewTemplate(ctx)
	if filename == "" {
		filename = ctx.Config.ProjectName
		if !bit.Bit(TargetNeedDate) {
			temp.AddField(date, "")
		} else {
			filename += ".{{.BuildDate}}"
		}
		if !bit.Bit(TargetNeedModel) || ctx.Config.Model == "" {
			temp.AddField(model, "")
		} else {
			filename += "_{{.Model}}"
		}
		if !bit.Bit(TargetNeedOs) || ctx.Config.Os == "" {
			temp.AddField(osKey, "")
		} else {
			filename += "_{{.Os}}"
		}
		if !bit.Bit(TargetNeedVersion) {
			temp.AddField(version, "")
		} else {
			filename += "-{{.Version}}"
		}
	}
	filename, err := temp.Apply(filename)
	if err != nil {
		return ctx.Config.ProjectName
	}
	return filename
}

// Package 打包
func (b *Builder) Package(ctx *Context, build Build, out *PackOut) error {
	logger.I("builder", "package '%s' running use '%s' format for '%s'", build.Target, build.Package.Format, build.Package.Model)
	if build.Package.Format == "" {
		build.Package.Format = "zip"
	}
	if build.Package.Dist == "" {
		build.Package.Dist = "./build"
	}
	if build.Package.PackagePath == "" {
		build.Package.PackagePath = "packages"
	}
	if build.Package.PublishPath == "" {
		build.Package.PublishPath = "publishes"
	}
	out.Version = ctx.Config.Version
	out.Path = strings.TrimSuffix(build.Package.Dist, "/")

	build.Package.Dist = filepath.Join(out.Path, "dist")
	build.Package.Parse()

	if build.Package.packCompleted == nil {
		build.Package.packCompleted = func(out *PackOut, pf *PackFile, completed bool) {}
	}
	if build.Package.BaseUrl != "" {
		build.Package.BaseUrl = strings.TrimSuffix(build.Package.BaseUrl, "/")
		build.Package.BaseUrl += "/"
	}
	out.PackagePath = filepath.Join(out.Path, build.Package.PackagePath)
	out.PublishPath = filepath.Join(out.Path, build.Package.PublishPath)
	out.Ext = archive.Ext(build.Package.Format)
	if out.Ext != "" {
		out.Name = b.BuildTargetFileName(ctx, build.Package.flag, build.Package.Target)
		out.Target = filepath.Join(out.PackagePath, out.Name+out.Ext)
	} else {
		out.Name = ""
		out.Target = ""
	}
	if build.Package.IsFlag(OutHashFile) {
		build.Package.Hash = b.BuildHashFileName(ctx, build.Package.flag, build.Package.Hash)
		out.Hash = filepath.Join(build.Target, build.Package.Hash)
	}
	logger.I("builder", "package info{dist: '%s', Path: '%s', target: '%s'}", build.Package.Dist, out.Path, out.Target)
	if build.Package.IsFlag(OnlyPackOut) {
		return nil
	}
	err := os.RemoveAll(build.Package.Dist)
	err = os.MkdirAll(build.Package.Dist, 0777)
	if err != nil {
		logger.W("builder", "mkdir dist failure, dir= %s, err= %s", build.Package.Dist, err)
	}
	err = os.Mkdir(out.PackagePath, 0777)
	if err != nil {
		logger.W("builder", "mkdir packages failure, dir= %s, err= %s", out.PackagePath, err)
	}
	err = os.Mkdir(out.PublishPath, 0777)
	if err != nil {
		logger.W("builder", "mkdir publishes failure, dir= %s, err= %s", out.PublishPath, err)
	}
	err = b.buildPackage(ctx, &build, out)
	if err != nil {
		logger.E("builder", "package failure, err= %s", err)
		_ = os.RemoveAll(build.Package.Dist)
		return err
	}
	logger.I("builder", "package build completed")
	return nil
}

func (b *Builder) buildPackage(ctx *Context, build *Build, out *PackOut) error {
	var err error
	var f *os.File = nil
	if out.Target != "" {
		_ = os.Remove(out.Target)
		f, err = os.Create(out.Target)
		if err != nil {
			return err
		}
	}
	defer func() {
		if f != nil {
			_ = f.Close() // nolint: errcheck
		}
	}()
	if build.Package.IsFlag(NotArchive) {
		//不需要压缩,只创建目录
		return nil
	}
	archiveConfig := archive.Config{
		Format: build.Package.Format,
		Path:   build.Package.Dist,
		File:   out.Target,
	}
	ar, err := archive.New(f, archiveConfig)
	if err != nil {
		logger.E("builder", "create archive file failure, config= %#v, err:  %s", archiveConfig, err)
		return err
	}
	defer func(ar archive.Archive) {
		err = ar.Close()
		if err != nil {
			logger.E("builder", "close archive err= %s", err)
		}
	}(ar) // nolint: errcheck
	if build.Package.IsFlag(OutHashFile) {
		err = os.Remove(out.Hash)
	}
	root := filepath.Join(build.Target, "/")
	root = filepath.ToSlash(root)
	var relativeName = func(name string) string {
		return strings.TrimPrefix(filepath.ToSlash(name), root)
	}
	var destName = func(dest string) string {
		var ot = "./"
		if build.Package.IsFlag(ArchiveProject) {
			ot = filepath.Join(ot, build.ProjectName)
		}
		if build.Package.IsFlag(ArchiveVersion) {
			ot = filepath.Join(ot, ctx.Config.Version)
		}
		if ot != "./" {
			dest = filepath.Join(ot, dest)
		}
		return dest
	}
	var packUrl = func(name string) string {
		if build.Package.BaseUrl == "" {
			return ""
		}
		return build.Package.BaseUrl + name
	}
	var pack = HashFile{
		Os:          build.Package.Os,
		Version:     ctx.Config.Version,
		Model:       build.Package.Model,
		ProductCode: build.Package.ProductCode,
		ProductId:   build.ProductId, ProjectName: build.ProjectName}
	logger.I("builder", "archive config= %+v, target= '%s', root= '%s'", archiveConfig, build.Target, root)
	depth := file.WithMaxDepth()
	err = file.ReadDir(build.Target, &depth, func(name string, isDir bool, entry os.DirEntry) int {
		baseName := entry.Name()
		relative := relativeName(name)
		if build.Package.Ignore(relative, baseName, isDir) {
			//logger.W("builder", "ignore the file: %s, %s", relative, baseName)
			return 1 //ignore
		}
		dest := destName(relative)
		err = ar.Add(fileconfig.File{
			Info:        fileconfig.FileInfo{Mode: 0777},
			Source:      name,
			Destination: dest,
			Dir:         isDir,
		}, fileconfig.ReaderStreamFunc(func(r io.ReadSeeker) {
			if !build.Package.IsFlag(OutHashFile) {
				return
			}
			if _, err = r.Seek(0, io.SeekStart); err != nil {
				return
			}

			pf := PackFile{Name: dest, Url: packUrl(baseName)}
			pf.MD5, pf.Size = file.MD5Size(r)
			build.Package.packCompleted(out, &pf, false)

			pack.Packs = append(pack.Packs, pf)
		}))
		if err != nil {
			logger.E("builder", "add the file to archive failure(%s=>%s), err= %s", name, dest, err)
			return -1
		}
		return 0
	})
	if err == nil {
		if !build.Package.IsFlag(OutHashFile) {
			return nil
		}
		uf := filepath.Join(build.Target, build.Package.Hash)
		err = json.Write(uf, &pack, 0777)
		if err != nil {
			return fmt.Errorf("create hash file,err= %s", err)
		}
		if !build.Package.IsFlag(NeedArchiveHash) {
			return nil
		}
		err = ar.Add(fileconfig.File{
			Info:        fileconfig.FileInfo{Mode: 0777},
			Source:      uf,
			Destination: destName(build.Package.Hash),
		}, fileconfig.ReaderStreamFunc(func(r io.ReadSeeker) {
			if _, err = r.Seek(0, io.SeekStart); err != nil {
				return
			}
			pf := PackFile{Name: destName(relativeName(uf)), Url: packUrl(build.Package.Hash)}
			pf.MD5, pf.Size = file.MD5Size(r)
			build.Package.packCompleted(out, &pf, true)

			logger.I("builder", "hash file= '%+v'", pf)
		}))
		if err != nil {
			return fmt.Errorf("archive hash file= '%s',err= %s", uf, err)
		}
	}
	return err
}

func (*Builder) Clean(ctx *Context, build Build, options Options) error {
	logger.I("builder", "clean running")

	_ = os.Remove(filepath.Join(build.Target, "logs"))

	if err := os.Remove(options.Path); err != nil {
		logger.W("builder", "clean '%s' failure,err= %s", options.Path, err)
	}
	if build.Package.Dist == "" {
		build.Package.Dist = "./build"
	}
	dir := strings.TrimSuffix(build.Package.Dist, "/")
	if err := os.RemoveAll(dir); err != nil {
		logger.W("builder", "clean '%s' failure, err= %s", dir, err)
		return nil
	}
	logger.I("builder", "clean completed")
	return nil
}

func buildGoBuildLine(ctx *Context, build Build, details Details, options *Options, env []string) ([]string, error) {
	cmd := []string{build.GoBinary, build.Command}

	// tags, ldflags, and buildmode, should only appear once, warning only to avoid a breaking change
	validateUniqueFlags(details)

	flags, err := processFlags(ctx, env, details.Flags, "")
	if err != nil {
		return cmd, err
	}
	cmd = append(cmd, flags...)

	asmFlags, err := processFlags(ctx, env, details.AsmFlags, "-asmflags=")
	if err != nil {
		return cmd, err
	}
	cmd = append(cmd, asmFlags...)

	gcFlags, err := processFlags(ctx, env, details.GcFlags, "-gcflags=")
	if err != nil {
		return cmd, err
	}
	cmd = append(cmd, gcFlags...)

	// tags is not a repeatable flag
	if len(details.Tags) > 0 {
		tags, err := processFlags(ctx, env, details.Tags, "")
		if err != nil {
			return cmd, err
		}
		cmd = append(cmd, "-tags="+strings.Join(tags, ","))
	}

	// ldflags is not a repeatable flag
	if len(details.Ldflags) > 0 {
		// flag prefix is skipped because ldflags need to output a single string
		ldflags, err := processFlags(ctx, env, details.Ldflags, "")
		if err != nil {
			return cmd, err
		}
		// ldflags need to be single string in order to apply correctly
		cmd = append(cmd, "-ldflags="+strings.Join(ldflags, " "))
	}

	if details.Buildmode != "" {
		cmd = append(cmd, "-buildmode="+details.Buildmode)
	}

	cmd = append(cmd, "-o", options.Path, build.Main)
	return cmd, nil
}

func validateUniqueFlags(details Details) {
	for _, flag := range details.Flags {
		if strings.HasPrefix(flag, "-tags") && len(details.Tags) > 0 {
			zap.L().Warn("builder", zap.String("flag", flag), zap.Any("tags", details.Tags))
		}
		if strings.HasPrefix(flag, "-ldflags") && len(details.Ldflags) > 0 {
			zap.L().Warn("builder", zap.String("flag", flag), zap.Any("ldflags", details.Ldflags))
		}
		if strings.HasPrefix(flag, "-buildmode") && details.Buildmode != "" {
			zap.L().Warn("builder", zap.String("flag", flag), zap.Any("buildmode", details.Buildmode))
		}
	}
}

func processFlags(ctx *Context, env, flags []string, flagPrefix string) ([]string, error) {
	processed := make([]string, 0, len(flags))
	for _, rawFlag := range flags {
		flag, err := processFlag(ctx, env, rawFlag)
		if err != nil {
			return nil, err
		}
		processed = append(processed, flagPrefix+flag)
	}
	return processed, nil
}

func processFlag(ctx *Context, env []string, rawFlag string) (string, error) {
	return NewTemplate(ctx).WithEnvS(env).Apply(rawFlag)
}

func run(ctx context.Context, command, env []string, dir string) error {
	cmd := exec.CommandContext(ctx, command[0], command[1:]...)
	cmd.Env = env
	cmd.Dir = dir
	out, err := cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf("%w: %s", err, string(out))
	}
	return nil
}
