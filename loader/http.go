package loader

import (
	commonhttp "gitee.com/h79/goutils/common/http"
	"net/http"
)

type Http struct {
	Loader
	headFunc commonhttp.HeaderFunc
	url      string
	da       interface{}
}

func NewHttp(url string, data interface{}, headFunc commonhttp.HeaderFunc) *Loader {
	hp := &Http{
		Loader:   CreateLoader(),
		headFunc: headFunc,
		url:      url,
		da:       data,
	}
	return hp.WithLoadFunc(hp.load)
}

func (hp *Http) load() (interface{}, error) {
	chp := commonhttp.Http{}

	body, err := chp.Do("GET", hp.url, nil, func(h *http.Header) {
		h.Set("Content-Type", "application/json;charset=utf-8")
		if hp.headFunc != nil {
			hp.headFunc(h)
		}
	})
	if err != nil {
		return nil, err
	}
	if err = hp.unmarshal.Unmarshal(body, hp.da); err != nil {
		return nil, err
	}
	return hp.da, err
}
