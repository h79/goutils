package loader

import (
	"context"
	"gitee.com/h79/goutils/dao"
)

type Cache struct {
	Loader
	cacheLoader CacheLoadFunc
	dao         *dao.Dao
	key         string
	da          interface{}
}

type CacheLoadFunc func(dao *dao.Dao, opt *CacheOption) error

type CacheOption struct {
	Key  string
	Data interface{}
}

func NewCache(key string, dao *dao.Dao, data interface{}) *Loader {
	cache := &Cache{
		Loader: CreateLoader(),
		dao:    dao,
		key:    key,
		da:     data,
	}
	return cache.WithLoadFunc(cache.load)
}

func (ca *Cache) WithCacheLoadFunc(fn CacheLoadFunc) *Cache {
	ca.cacheLoader = fn
	return ca
}

func (ca *Cache) load() (interface{}, error) {
	if ca.cacheLoader != nil {
		opt := CacheOption{
			Key:  ca.key,
			Data: ca.da,
		}
		err := ca.cacheLoader(ca.dao, &opt)
		if err != nil {
			return nil, err
		}
		return opt.Data, nil
	}
	return ca.defaultLoad()
}

func (ca *Cache) defaultLoad() (interface{}, error) {
	client, err := ca.dao.RedisClient()
	if err != nil {
		return ca.da, err
	}
	ret, err := client.Get(context.Background(), ca.key).Result()
	if err != nil {
		return ca.da, err
	}
	if err = ca.unmarshal.Unmarshal([]byte(ret), ca.da); err != nil {
		return ca.da, err
	}
	return ca.da, nil
}
