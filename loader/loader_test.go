package loader

import (
	"testing"
	"time"
)

func TestLoader_Watch(t *testing.T) {

	item := map[string]interface{}{}
	ll := NewFiler("./load.json", "json", &item)

	if err, _ := ll.Read(); err != nil {
		t.Error(err)
		return
	}
	jn, _ := ll.JSON()
	t.Logf("JSON: %+v", jn)

	ch := ll.WatchDo()

	//ch := make(bus.EventChan)
	//bs.Subscribe("load.data", ch)

	go func() {
		for {
			select {
			case <-ch:
				t.Logf("file changed, %+v", ll.Data().(*map[string]interface{}))

			default:
			}
		}
	}()

	//ll.WithLoadFunc(func(data Data) (error, bool) {
	//	return nil, false
	//})
	time.Sleep(time.Second * 50)
}
