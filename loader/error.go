package loader

import "errors"

var ErrNotDefined = errors.New("not defined")
