package loader

import (
	"gitee.com/h79/goutils/common/file"
)

const (
	JsonFormat = "json"
	XmlFormat  = "xml"
	YamlFormat = "yaml"
)

type Filer struct {
	Loader
	path           string
	lastModifyTime int64
	da             interface{}
}

func NewFiler(path string, format string, data interface{}) *Loader {
	l := &Filer{
		Loader: CreateLoader(),
		path:   path,
		da:     data,
	}
	return l.WithFormat(format).WithLoadFunc(l.load)
}

func (f *Filer) load() (interface{}, error) {
	lastModify, data, err := file.DecodeFileModifyTime(f.path, f.lastModifyTime, func(v []byte) (interface{}, error) {
		er := f.unmarshal.Unmarshal(v, f.da)
		return f.da, er
	})
	f.lastModifyTime = lastModify
	return data, err
}

func JSON(filename string, data interface{}) error {
	ll := NewFiler(filename, JsonFormat, data)
	if err, _ := ll.Read(); err != nil {
		return err
	}
	return nil
}

func XML(filename string, data interface{}) error {
	ll := NewFiler(filename, XmlFormat, data)
	if err, _ := ll.Read(); err != nil {
		return err
	}
	return nil
}

func YAML(filename string, data interface{}) error {
	ll := NewFiler(filename, YamlFormat, data)
	if err, _ := ll.Read(); err != nil {
		return err
	}
	return nil
}
