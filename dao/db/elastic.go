/**
 * @author huqiuyun
 * 数据Elasticsearch
 */
package db

import (
	"context"
	"fmt"
	commonconfig "gitee.com/h79/goutils/common/config"
	"gitee.com/h79/goutils/common/logger"
	"gitee.com/h79/goutils/common/result"
	"gitee.com/h79/goutils/dao/config"
	"gitee.com/h79/goutils/dao/util"
	"github.com/olivere/elastic/v7"
	"go.uber.org/zap"
	"time"
)

type esClient struct {
	Client *elastic.Client
}

var _ EsDatabase = (*elasticGroup)(nil)

type elasticGroup struct {
	selectName string
	esMap      map[string]*esClient
}

func NewES(conf []config.Elastic) (EsDatabase, error) {
	logger.L().Info("Elastic", zap.Any("config", conf))
	ess := &elasticGroup{esMap: map[string]*esClient{}}
	for _, cfg := range conf {
		if len(cfg.Host) <= 0 {
			continue
		}
		interval := cfg.Interval
		if interval <= 0 {
			interval = 10
		} else if interval > 3600 {
			interval = 3600
		}
		interval = interval * time.Second

		url := cfg.Host[0]
		errLog := &esLogger{
			LogLevel: logger.ErrorLevel,
			Level:    cfg.Logger.LogLevel,
		}
		infoLog := &esLogger{
			LogLevel: logger.InfoLevel,
			Level:    cfg.Logger.LogLevel,
		}
		traceLog := &esLogger{
			LogLevel: logger.DebugLevel,
			Level:    cfg.Logger.LogLevel,
		}
		var client, err = elastic.NewClient(
			elastic.SetURL(cfg.Host...),
			elastic.SetBasicAuth(cfg.User, cfg.Pwd),
			elastic.SetHealthcheckInterval(interval),
			elastic.SetSniff(cfg.Sniff),
			elastic.SetErrorLog(errLog),
			elastic.SetInfoLog(infoLog),
			elastic.SetTraceLog(traceLog))

		if err != nil {
			util.Alarm(result.ErrEsClientInternal, "", fmt.Sprintf("New Elastic(%s)", cfg.Name), err)
			return nil, err
		}
		_, _, err = client.Ping(url).Do(context.Background())
		if err != nil {
			util.Alarm(result.ErrEsPingInternal, "", fmt.Sprintf("Ping Elastic(%s)", cfg.Name), err)
			return nil, err
		}
		ver, err := client.ElasticsearchVersion(url)
		logger.L().Info("Elastic", zap.String("Name", cfg.Name), zap.String("Version", ver))
		ess.esMap[cfg.Name] = &esClient{client}
		if commonconfig.RegisterConfig != nil {
			commonconfig.RegisterConfig("ES:Err|"+cfg.Name, errLog.handlerConfig)
			commonconfig.RegisterConfig("ES:Info|"+cfg.Name, infoLog.handlerConfig)
			commonconfig.RegisterConfig("ES:Trace|"+cfg.Name, traceLog.handlerConfig)
		}
	}
	return ess, nil
}
func (conns *elasticGroup) Get(name string) (*elastic.Client, string, error) {
	if len(name) == 0 {
		name = conns.selectName
	}
	es, exists := conns.esMap[name]
	if exists == true {
		return es.Client, name, nil
	}
	return nil, "", result.Error(result.ErrNotFound, "Not found")
}

func (conns *elasticGroup) Do(name string, call func(es *elastic.Client) error) error {
	if len(name) == 0 {
		name = conns.selectName
	}
	if es, exists := conns.esMap[name]; exists == true {
		return call(es.Client)
	}
	return result.Error(result.ErrNotFound, "Not found")
}

func (conns *elasticGroup) Close(name string) error {
	if len(name) == 0 {
		name = conns.selectName
	}
	es, exists := conns.esMap[name]
	if exists == true {
		es.Client.Stop()
		delete(conns.esMap, name)
	}
	return nil
}

func (conns *elasticGroup) CloseAll() {
	for _, es := range conns.esMap {
		es.Client.Stop()
	}
	conns.esMap = nil
}

func (conns *elasticGroup) Select(name string) {
	conns.selectName = name
}

func (conns *elasticGroup) GetSelector() string {
	return conns.selectName
}
