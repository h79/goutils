/**
 * @author huqiuyun
 * 数据服务
 */
package db

import (
	"fmt"
	"gitee.com/h79/goutils/common/logger"
	commonoption "gitee.com/h79/goutils/common/option"
	"gitee.com/h79/goutils/common/result"
	"gitee.com/h79/goutils/dao/config"
	"gitee.com/h79/goutils/dao/util"
	"go.uber.org/zap"
)

var _ SqlDatabase = (*sqlGroup)(nil)

type sqlGroup struct {
	selectName string
	dbMap      map[string]*Adapter
}

func NewDB(cfgs []config.Sql, opts ...commonoption.Option) (SqlDatabase, error) {
	logger.L().Info("DB", zap.Any("Config", cfgs))
	gr := &sqlGroup{dbMap: map[string]*Adapter{}}
	for _, cfg := range cfgs {
		ad, err := NewAdapter(&cfg, opts...)
		if err != nil {
			util.Alarm(result.ErrDbOpenInternal, "", fmt.Sprintf("Open %s DB failure server", cfg.Master.Name), err)
			return nil, err
		}
		gr.dbMap[cfg.Name] = ad
	}
	return gr, nil
}

func (gr *sqlGroup) Do(dbname string, call func(db Sql) error) error {
	if len(dbname) == 0 {
		dbname = gr.selectName
	}
	db, exists := gr.dbMap[dbname]
	if exists == true {
		return call(db)
	}
	return result.Errorf(result.ErrDbExistInternal, "'%s' database not exist,please check config!", dbname)
}

func (gr *sqlGroup) Get(dbname string) (Sql, error) {
	if len(dbname) == 0 {
		dbname = gr.selectName
	}
	if db, exists := gr.dbMap[dbname]; exists == true {
		return db, nil
	}
	return nil, result.Error(result.ErrNotFound, "Not found")
}

func (gr *sqlGroup) Close(dbname string) {
	if len(dbname) == 0 {
		dbname = gr.selectName
	}
	db, exists := gr.dbMap[dbname]
	if exists == true {
		db.Close()
	}
}

func (gr *sqlGroup) CloseAll() {
	for _, db := range gr.dbMap {
		db.Close()
	}
	gr.dbMap = nil
}

func (gr *sqlGroup) Select(name string) {
	gr.selectName = name
}

func (gr *sqlGroup) GetSelector() string {
	return gr.selectName
}
