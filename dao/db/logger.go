package db

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/h79/goutils/common/logger"
	"gitee.com/h79/goutils/common/result"
	"gitee.com/h79/goutils/dao/config"
	"gitee.com/h79/goutils/dao/util"
	"go.uber.org/zap"
	"gorm.io/gorm"
	gormLogger "gorm.io/gorm/logger"
	"strconv"
	"strings"
	"time"
)

type Logger struct {
	config.SqlLogger
}

// LogMode log mode
func (l *Logger) LogMode(level gormLogger.LogLevel) gormLogger.Interface {
	newLogger := *l
	newLogger.LogLevel = int(level)
	return &newLogger
}

// Info print info
func (l *Logger) Info(ctx context.Context, msg string, data ...interface{}) {
	if l.check(gormLogger.Info) {
		logger.Info(msg, data...)
	}
}

// Warn print warn messages
func (l *Logger) Warn(ctx context.Context, msg string, data ...interface{}) {
	if l.check(gormLogger.Warn) {
		logger.Warn(msg, data...)
	}
}

// Error print error messages
func (l *Logger) Error(ctx context.Context, msg string, data ...interface{}) {
	if l.check(gormLogger.Error) {
		logger.Error(msg, data...)
	}
}

// Trace print sql message
func (l *Logger) Trace(ctx context.Context, begin time.Time, fc func() (string, int64), err error) {

	elapsed := time.Now().Sub(begin)
	switch {
	case err != nil:

		recordNotFound := errors.Is(err, gorm.ErrRecordNotFound)
		logEnabled := l.check(gormLogger.Error) && (!recordNotFound || !l.IgnoreRecordNotFoundError)
		if !logEnabled && !l.AlarmEnabled {
			return
		}

		sql, rows := fc()
		if logEnabled {
			zap.L().Info("DB",
				zap.Error(err),
				zap.Int64("rows", rows),
				zap.String("sql", sql),
				zap.String("speed", elapsed.String()))
		}

		if l.AlarmEnabled && !recordNotFound {
			var b = strings.Builder{}
			b.WriteString("sql: '")
			b.WriteString(sql)
			b.WriteString("',rows: '")
			b.WriteString(strconv.Itoa(int(rows)))
			b.WriteString("',speed: '")
			b.WriteString(elapsed.String())
			b.WriteString("'")
			util.Alarm(result.ErrDbInternal, "Sql", b.String(), err)
		}

	case elapsed > l.SlowThreshold && l.SlowThreshold != 0:

		if !l.check(gormLogger.Warn) && !l.AlarmEnabled {
			return
		}
		sql, rows := fc()
		if l.check(gormLogger.Warn) {
			zap.L().Warn("DB",
				zap.String("slow", fmt.Sprintf("SLOW SQL >= %v", l.SlowThreshold)),
				zap.Int64("rows", rows),
				zap.String("sql", sql),
				zap.String("speed", elapsed.String()))
		}

		if l.AlarmEnabled {
			var b = strings.Builder{}
			b.WriteString("sql: '")
			b.WriteString(sql)
			b.WriteString("',rows: '")
			b.WriteString(strconv.Itoa(int(rows)))
			b.WriteString("',speed: '")
			b.WriteString(elapsed.String())
			b.WriteString("'")
			util.Alarm(result.ErrDbInternal, "Sql", b.String(), nil)
		}

	case l.check(gormLogger.Info):
		sql, rows := fc()
		zap.L().Info("DB",
			zap.Int64("rows", rows),
			zap.String("sql", sql),
			zap.String("speed", elapsed.String()))
	}
}

func (l *Logger) check(level gormLogger.LogLevel) bool {
	return gormLogger.LogLevel(l.LogLevel) >= level
}

func (l *Logger) handlerConfig(ctx context.Context, cmd int, configType, conf string) (any, error) {
	switch cmd {
	case 1:
		var c = config.SqlLogger{}
		var err = json.Unmarshal([]byte(conf), &c)
		if err != nil {
			return "", err
		}
		l.LogLevel = c.LogLevel
		l.IgnoreRecordNotFoundError = c.IgnoreRecordNotFoundError
		l.AlarmEnabled = c.AlarmEnabled
		l.SlowThreshold = c.SlowThreshold
		return "", nil
	case 2: //获取
		return config.SqlLogger{}, nil
	}
	return "", result.RErrNotSupport
}

type esLogger struct {
	Level    int  `json:"level"`
	LogLevel int8 `json:"logLevel"`
}

func (l *esLogger) Printf(format string, v ...interface{}) {
	if l.LogLevel < int8(l.Level) {
		return
	}
	switch l.LogLevel {
	case logger.ErrorLevel:
		logger.E("Elastic", format, v...)
	case logger.DebugLevel:
		logger.D("Elastic", format, v...)
	case logger.InfoLevel:
		logger.I("Elastic", format, v...)
	}
}

func (l *esLogger) handlerConfig(ctx context.Context, cmd int, configType, conf string) (any, error) {
	switch cmd {
	case 1:
		var c = esLogger{}
		var err = json.Unmarshal([]byte(conf), &c)
		if err != nil {
			return "", err
		}
		l.Level = c.Level
		l.LogLevel = c.LogLevel
		return "", nil
	case 2: //获取
		return esLogger{}, nil
	}
	return "", result.RErrNotSupport
}
