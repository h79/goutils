package db

import (
	"context"
	"github.com/olivere/elastic/v7"
	"gorm.io/gorm"
)

type Sql interface {
	Db() *gorm.DB
	Name() string
}

type SqlDatabase interface {
	Get(name string) (Sql, error)
	Do(name string, c func(db Sql) error) error
	Close(name string)
	CloseAll()
	Select(name string)
	GetSelector() string
}

type EsDatabase interface {
	Get(name string) (*elastic.Client, string, error)
	Do(name string, call func(es *elastic.Client) error) error
	Close(name string) error
	CloseAll()
	Select(name string)
	GetSelector() string
}

type ConditionCallback func(ctx context.Context, data interface{}, db *gorm.DB) *gorm.DB
