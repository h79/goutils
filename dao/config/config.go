package config

import (
	"gitee.com/h79/goutils/common/server"
	"gitee.com/h79/goutils/common/tls"
	"time"
)

type Redis struct {
	Name     string      `json:"name" yaml:"name" xml:"name"` //redis名
	Master   RedisConfig `json:"master" yaml:"master" xml:"master"`
	Cluster  RedisConfig `json:"cluster" yaml:"cluster" xml:"cluster"`
	Sentinel RedisConfig `json:"sentinel" yaml:"sentinel" xml:"sentinel"`
	Logger   RedisLogger `json:"logger" yaml:"logger" xml:"logger"`
}

type RedisConfig struct {
	Host               []string      `json:"host" yaml:"host" xml:"host"` //ip:port
	User               string        `json:"user" yaml:"user" xml:"user"`
	Pwd                string        `json:"pwd" yaml:"pwd" xml:"pwd"`
	DB                 int           `json:"db" yaml:"db" xml:"db"`
	PoolSize           int           `json:"poolSize" yaml:"poolSize" xml:"poolSize"`
	MinIdleConns       int           `json:"minIdleConns" yaml:"minIdleConns" xml:"minIdleConns"`
	MaxRetries         int           `json:"maxRetries" yaml:"maxRetries" xml:"maxRetries"`
	DialTimeout        time.Duration `json:"dialTimeout" yaml:"dialTimeout" xml:"dialTimeout"`
	ReadTimeout        time.Duration `json:"readTimeout" yaml:"readTimeout" xml:"readTimeout"`
	WriteTimeout       time.Duration `json:"writeTimeout" yaml:"writeTimeout" xml:"writeTimeout"`
	IdleTimeout        time.Duration `json:"idleTimeout" yaml:"idleTimeout" xml:"idleTimeout"`
	MinRetryBackoff    time.Duration `json:"MinRetryBackoff" yaml:"MinRetryBackoff" xml:"MinRetryBackoff"`
	MaxRetryBackoff    time.Duration `json:"MaxRetryBackoff" yaml:"MaxRetryBackoff" xml:"MaxRetryBackoff"`
	MaxConnAge         time.Duration `json:"MaxConnAge" yaml:"MaxConnAge" xml:"MaxConnAge"`
	PoolTimeout        time.Duration `json:"PoolTimeout" yaml:"PoolTimeout" xml:"PoolTimeout"`
	IdleCheckFrequency time.Duration `json:"IdleCheckFrequency" yaml:"IdleCheckFrequency" xml:"IdleCheckFrequency"`
	Tls                tls.Tls       `json:"tls" yaml:"tls" xml:"tls"`
}

type RedisLogger struct {
	LogLevel int `json:"level" yaml:"level" xml:"level"`
}

type Sql struct {
	Name         string        `json:"name" yaml:"name" xml:"name"` //数据名（不是数据库名）
	Master       Database      `json:"master" yaml:"master" xml:"master"`
	Sources      []Database    `json:"sources" yaml:"sources" xml:"sources"`
	Replicas     []Database    `json:"replicas" yaml:"replicas" xml:"replicas"`
	MaxOpenConns int           `json:"max_open_conns" yaml:"max_open_conns" xml:"max_open_conns"`
	MaxIdleConns int           `json:"max_idle_conns" yaml:"max_idle_conns" xml:"max_idle_conns"`
	MaxIdleTime  time.Duration `json:"max_idle_time" yaml:"max_idle_time" xml:"max_idle_time"`
	MaxLifetime  time.Duration `json:"max_life_time" yaml:"max_life_time" xml:"max_life_time"`
	Logger       SqlLogger     `json:"logger" yaml:"logger" xml:"logger"`
}

type SqlLogger struct {
	LogLevel                  int           `json:"level" yaml:"level" xml:"level"`
	SlowThreshold             time.Duration `json:"slowThreshold" yaml:"slowThreshold" xml:"slowThreshold"`
	IgnoreRecordNotFoundError bool          `json:"ignoreRecordNotFoundError" yaml:"ignoreRecordNotFoundError" xml:"ignoreRecordNotFoundError"`
	AlarmEnabled              bool          `json:"alarmEnabled" yaml:"alarmEnabled" xml:"alarmEnabled"`
}

type Database struct {
	Host         string           `json:"host" yaml:"host" xml:"host"`
	Port         int              `json:"port" yaml:"port" xml:"port"`
	User         string           `json:"user" yaml:"user" xml:"user"`
	Pwd          string           `json:"pwd" yaml:"pwd" xml:"pwd"`
	Charset      string           `json:"charset" yaml:"charset" xml:"charset"`                // charset字符值
	DriverType   string           `json:"type" yaml:"type" xml:"type"`                         //mysql, sqlite
	Name         string           `json:"name" yaml:"name" xml:"name"`                         //数据库名
	Tls          tls.Tls          `json:"tls" yaml:"tls" xml:"tls"`                            //for key
	ServerPubKey tls.ServerPubKey `json:"serverPubKey" yaml:"serverPubKey" xml:"serverPubKey"` //for serverPubKey
}

type Elastic struct {
	Host              []string      `json:"hosts" yaml:"hosts" xml:"hosts"` //机群
	Name              string        `json:"name" yaml:"name" xml:"name"`    //数据名（不是数据库名）
	User              string        `json:"user" yaml:"user" xml:"user"`
	Pwd               string        `json:"pwd" yaml:"pwd" xml:"pwd"`
	XPackSecurityUser string        `json:"XPackSecurityUser" yaml:"XPackSecurityUser" xml:"XPackSecurityUser"`
	Interval          time.Duration `json:"interval" yaml:"interval" xml:"interval"`
	Sniff             bool          `json:"sniff" yaml:"sniff" xml:"sniff"`
	Logger            EsLogger      `json:"logger" yaml:"logger" xml:"logger"`
}

type EsLogger struct {
	LogLevel int `json:"level" yaml:"level" xml:"level"`
}

// Cluster 集群配置
type Cluster struct {
	Nodes        []server.Address `json:"nodes" yaml:"nodes" xml:"nodes"`
	Password     string           `json:"password" yaml:"password" xml:"password"`
	PoolSize     int              `json:"pool_size" yaml:"pool_size" xml:"pool_size"`
	ReadOnly     bool             `json:"read_only" yaml:"read_only" xml:"read_only"`
	DialTimeout  time.Duration    `json:"dial_timeout" yaml:"dial_timeout" xml:"dial_timeout"`
	ReadTimeout  time.Duration    `json:"read_timeout" yaml:"read_timeout" xml:"read_timeout"`
	WriteTimeout time.Duration    `json:"write_timeout" yaml:"write_timeout" xml:"write_timeout"`
}
