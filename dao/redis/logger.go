package redis

import (
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/h79/goutils/common/result"
	"gitee.com/h79/goutils/dao/config"
	"go.uber.org/zap"
)

type Logger struct {
	config.RedisLogger
}

func (l *Logger) Printf(ctx context.Context, format string, v ...interface{}) {
	if l.LogLevel <= 0 {
		return
	}
	zap.L().Info("Redis", zap.String("info", fmt.Sprintf(format, v...)))
}

func (l *Logger) handlerConfig(ctx context.Context, cmd int, configType, conf string) (any, error) {
	switch cmd {
	case 1:
		var c = config.RedisLogger{}
		var err = json.Unmarshal([]byte(conf), &c)
		if err != nil {
			return "", err
		}
		l.LogLevel = c.LogLevel
		return "", nil
	case 2: //获取
		return config.RedisLogger{}, nil
	}
	return "", result.RErrNotSupport
}
