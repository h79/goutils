package pool

import (
	"context"
	"time"

	"github.com/go-redis/redis/v8"
)

//LPush 把value 插到表头
func (c *Client) LPush(ctx context.Context, expiration time.Duration, key string, value ...interface{}) (res int64, err error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		pipe.LPush(ctx, key, value...)
		if expiration != 0 {
			pipe.Expire(ctx, key, expiration)
		}
		return nil
	})

	// res, err := client.LPush(key, value...).Result()

	if err != nil {
		return 0, err
	}

	err = nil

	for _, cmd := range cmds {
		if cmd.Name() == "lpush" {
			res, err = cmd.(*redis.IntCmd).Result()
		} else {
			_, err = cmd.(*redis.BoolCmd).Result()
		}
	}
	return res, err
}

//RPush 把value 插到表头
func (c *Client) RPush(ctx context.Context, expiration time.Duration, key string, value ...interface{}) (res int64, err error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		pipe.RPush(ctx, key, value...)
		if expiration != 0 {
			pipe.Expire(ctx, key, expiration)
		}
		return nil
	})

	// res, err := client.LPush(key, value...).Result()

	if err != nil {
		return 0, err
	}

	err = nil

	for _, cmd := range cmds {
		if cmd.Name() == "rpush" {
			res, err = cmd.(*redis.IntCmd).Result()
		} else {
			_, err = cmd.(*redis.BoolCmd).Result()
		}
	}
	return res, err
}

//MLPushReq 批量 LPush 参数
type MLPushReq struct {
	Key        string
	Value      []interface{}
	Expiration time.Duration
}

type MRPushReq MLPushReq

//MLPush 批量 LPush
func (c *Client) MLPush(ctx context.Context, reqs []*MLPushReq) (res int64, err error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, r := range reqs {
			pipe.LPush(ctx, r.Key, r.Value...)
			if r.Expiration != 0 {
				pipe.Expire(ctx, r.Key, r.Expiration)
			}
		}
		return nil
	})

	if err != nil {
		return 0, err
	}

	err = nil
	for _, cmd := range cmds {
		if cmd.Name() == "lpush" {
			res, err = cmd.(*redis.IntCmd).Result()
		} else {
			_, err = cmd.(*redis.BoolCmd).Result()
		}

		if err != nil {
			break
		}
	}

	return res, err
}

//MRPush 批量 RPush
func (c *Client) MRPush(ctx context.Context, reqs []*MRPushReq) (res int64, err error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, r := range reqs {
			pipe.RPush(ctx, r.Key, r.Value...)
			if r.Expiration != 0 {
				pipe.Expire(ctx, r.Key, r.Expiration)
			}
		}
		return nil
	})

	if err != nil {
		return 0, err
	}

	err = nil
	for _, cmd := range cmds {
		if cmd.Name() == "rpush" {
			res, err = cmd.(*redis.IntCmd).Result()
		} else {
			_, err = cmd.(*redis.BoolCmd).Result()
		}

		if err != nil {
			break
		}
	}

	return res, err
}

//LPop 弹出表头
func (c *Client) LPop(ctx context.Context, key string) (string, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return "", cErr
	}

	res, err := client.LPop(ctx, key).Result()

	return res, err
}

//MLPop 批量弹出表头
func (c *Client) MLPop(ctx context.Context, keys ...string) (res string, err error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return "", cErr
	}

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, k := range keys {
			pipe.LPop(ctx, k)
		}
		return nil
	})

	if err != nil {
		return "", err
	}

	err = nil
	for _, cmd := range cmds {
		res, err = cmd.(*redis.StringCmd).Result()
		if err != nil {
			break
		}
	}

	return res, err
}

//RPop 弹出表尾
func (c *Client) RPop(ctx context.Context, key string) (string, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return "", cErr
	}

	res, err := client.RPop(ctx, key).Result()

	return res, err
}

//MRPop 批量弹出表尾
func (c *Client) MRPop(ctx context.Context, keys ...string) (res string, err error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return "", cErr
	}

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, k := range keys {
			pipe.RPop(ctx, k)
		}
		return nil
	})

	if err != nil {
		return "", err
	}

	err = nil
	for _, cmd := range cmds {
		res, err = cmd.(*redis.StringCmd).Result()
		if err != nil {
			break
		}
	}

	return res, err
}

//LInsert 将值 value 插入到列表 key 当中，位于值 pivot 之前或之后。
func (c *Client) LInsert(ctx context.Context, key, op string, pivot, value interface{}) (int64, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	res, err := client.LInsert(ctx, key, op, pivot, value).Result()

	return res, err
}

//MLInsertReq 批量 LInsert参数
type MLInsertReq struct {
	Key   string
	Op    string
	Pivot interface{}
	Value interface{}
}

//MLInsert 批量 LInsert
func (c *Client) MLInsert(ctx context.Context, reqs []*MLInsertReq) (res int64, err error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, r := range reqs {
			pipe.LInsert(ctx, r.Key, r.Op, r.Pivot, r.Value)
		}
		return nil
	})

	if err != nil {
		return 0, err
	}

	err = nil
	for _, cmd := range cmds {
		res, err = cmd.(*redis.IntCmd).Result()
		if err != nil {
			break
		}
	}

	return res, err
}

//LLen 返回表长度
func (c *Client) LLen(ctx context.Context, key string) (int64, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	res, err := client.LLen(ctx, key).Result()

	return res, err
}

//MLLen 返回表长度
func (c *Client) MLLen(ctx context.Context, keys ...string) (map[string]int64, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return nil, cErr
	}

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, k := range keys {
			pipe.LLen(ctx, k)
		}
		return nil
	})

	resMap := make(map[string]int64, 0)

	for i, cmd := range cmds {
		r, _ := cmd.(*redis.IntCmd).Result()
		resMap[keys[i]] = r
	}

	// res, err := client.LLen(key).Result()

	return resMap, err
}

//LSet 设置表中index 的值为 value
func (c *Client) LSet(ctx context.Context, key string, index int64, value interface{}) (string, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return "", cErr
	}

	res, err := client.LSet(ctx, key, index, value).Result()

	return res, err
}

//MLSetReq 批量 LSet 参数
type MLSetReq struct {
	Key   string
	Index int64
	Value interface{}
}

//MLSet 批量 LSet
func (c *Client) MLSet(ctx context.Context, reqs []*MLSetReq) (res string, err error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return "", cErr
	}

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, r := range reqs {
			pipe.LSet(ctx, r.Key, r.Index, r.Value)
		}
		return nil
	})

	if err != nil {
		return "", err
	}

	err = nil
	for _, cmd := range cmds {
		res, err = cmd.(*redis.StringCmd).Result()
		if err != nil {
			break
		}
	}

	return res, err
}

//LIndex 返回表中index的值
func (c *Client) LIndex(ctx context.Context, key string, index int64) (string, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return "", cErr
	}

	res, err := client.LIndex(ctx, key, index).Result()

	return res, err
}

//LRange 随机返回index 在start和stop 一个value
func (c *Client) LRange(ctx context.Context, key string, start, stop int64) ([]string, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return nil, cErr
	}

	res, err := client.LRange(ctx, key, start, stop).Result()

	return res, err
}

func (c *Client) LTrim(ctx context.Context, key string, start, stop int64) (string, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return "", cErr
	}

	res, err := client.LTrim(ctx, key, start, stop).Result()

	return res, err
}
