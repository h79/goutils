package pool

import (
	"context"
	"fmt"

	"github.com/go-redis/redis/v8"
)

// Hash

//HSetWithBatch 批量插入多个key
func (c *Client) HSetWithBatch(ctx context.Context, keys []string, field string, value interface{}) (int64, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, k := range keys {
			pipe.HSet(ctx, k, field, value)
		}
		return nil
	})
	if err != nil {
		return 0, err
	}
	for _, cmd := range cmds {
		res, err := cmd.(*redis.IntCmd).Result()
		if err != nil {
			return res, err
		}
	}
	return 0, nil
}

//HSetWithBatch 批量插入多个key
func (c *Client) HSetNXWithBatch(ctx context.Context, keys []string, field string, value interface{}) (bool, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return false, cErr
	}

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, k := range keys {
			pipe.HSetNX(ctx, k, field, value)
		}
		return nil
	})

	res := true
	err = nil
	for _, cmd := range cmds {
		res, err = cmd.(*redis.BoolCmd).Result()
		if err != nil {
			break
		}
	}
	return res, err
}

//HMSet 批量HSet
func (c *Client) HMSet(ctx context.Context, key string, fields map[string]interface{}) (bool, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return false, cErr
	}

	return client.HMSet(ctx, key, fields).Result()
}

//HMSetReq 批量HMSet参数
type HMSetReq struct {
	Key   string
	Filed map[string]interface{}
}

//HMSetWithBatch 批量HMSet
func (c *Client) HMSetWithBatch(ctx context.Context, reqs []*HMSetReq) (res string, err error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return "", cErr
	}

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, r := range reqs {
			pipe.HMSet(ctx, r.Key, r.Filed)
		}
		return nil
	})

	if err != nil {
		return "", err
	}

	res = ""
	err = nil
	for _, cmd := range cmds {
		res, err = cmd.(*redis.StatusCmd).Result()
		if err != nil {
			break
		}
	}

	return res, err
}

//HGet Hash table 查询
func (c *Client) HGet(ctx context.Context, key, field string) (string, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return "", cErr
	}

	return client.HGet(ctx, key, field).Result()
}

//HMGet 批量获取Hash table
func (c *Client) HMGet(ctx context.Context, key string, fields ...string) (map[string]interface{}, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return nil, cErr
	}

	res, err := client.HMGet(ctx, key, fields...).Result()
	fmt.Println(res)
	if err != nil {
		return nil, err
	}

	resMap := make(map[string]interface{}, 0)

	for i, f := range fields {

		resMap[f] = res[i]
	}
	return resMap, err
}

//HGetAll 获取整个Hash table
func (c *Client) HGetAll(ctx context.Context, key string) (map[string]string, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return nil, cErr
	}

	res, err := client.HGetAll(ctx, key).Result()
	return res, err
}

//HDel 批量 Del
func (c *Client) HDel(ctx context.Context, key string, fields ...string) (int64, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	res, err := client.HDel(ctx, key, fields...).Result()
	return res, err
}

//MHDelreq 批量HDel参数
type MHDelreq struct {
	Key   string
	Field []string
}

//HDelWithBatch 批量HDel
func (c *Client) HDelWithBatch(ctx context.Context, reqs []*MHDelreq) (res int64, err error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, r := range reqs {
			pipe.HDel(ctx, r.Key, r.Field...)
		}
		return nil
	})

	if err != nil {
		return 0, err
	}

	res = 0
	err = nil
	for _, cmd := range cmds {
		res, err = cmd.(*redis.IntCmd).Result()
		if err != nil {
			break
		}
	}

	return res, err
}

//HKeys 获取hash table中的fields
func (c *Client) HKeys(ctx context.Context, key string) (res []string, err error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return nil, cErr
	}

	res, err = client.HKeys(ctx, key).Result()
	return res, err
}

//HVals 获取hash vals
func (c *Client) HVals(ctx context.Context, key string) (res []string, err error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return nil, cErr
	}

	res, err = client.HVals(ctx, key).Result()
	return res, err
}

//HIncrBy field中的值增加inrc
func (c *Client) HIncrBy(ctx context.Context, key string, field string, inrc int64) (res int64, err error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	res, err = client.HIncrBy(ctx, key, field, inrc).Result()
	return res, err
}

//MHIncrByReq 批量 HIncrBy 参数
type MHIncrByReq struct {
	Key   string
	Field string
	Inrc  int64
}

//HIncrByWithBatch 批量 HIncrBy
func (c *Client) HIncrByWithBatch(ctx context.Context, reqs []*MHIncrByReq) (res int64, err error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, r := range reqs {
			pipe.HIncrBy(ctx, r.Key, r.Field, r.Inrc)
		}
		return nil
	})

	if err != nil {
		return 0, err
	}

	res = 0
	err = nil
	for _, cmd := range cmds {
		res, err = cmd.(*redis.IntCmd).Result()
		if err != nil {
			break
		}
	}

	return res, err
}

//HIncrByFloat HIncrBy 的float版
func (c *Client) HIncrByFloat(ctx context.Context, key string, field string, inrc float64) (res float64, err error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	res, err = client.HIncrByFloat(ctx, key, field, inrc).Result()
	return res, err
}

//MHIncrByFloatReq MHIncrByReq float版
type MHIncrByFloatReq struct {
	Key   string
	Field string
	Inrc  float64
}

//MHIncrByFloatWithBatch HIncrByWithBatch float版
func (c *Client) MHIncrByFloatWithBatch(ctx context.Context, reqs []*MHIncrByFloatReq) (res float64, err error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, r := range reqs {
			pipe.HIncrByFloat(ctx, r.Key, r.Field, r.Inrc)
		}
		return nil
	})

	if err != nil {
		return 0, err
	}

	res = 0
	err = nil
	for _, cmd := range cmds {
		res, err = cmd.(*redis.FloatCmd).Result()
		if err != nil {
			break
		}
	}

	return res, err
}

//HExists 判断field是否存在
func (c *Client) HExists(ctx context.Context, key string, field string) (res bool, err error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return false, cErr
	}

	res, err = client.HExists(ctx, key, field).Result()
	return res, err
}
