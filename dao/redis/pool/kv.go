package pool

import (
	"context"
	"fmt"
	"time"

	"github.com/go-redis/redis/v8"
)

// GetSet 的包装
func (c *Client) GetSet(ctx context.Context, key string, value interface{}) (string, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return "", cErr
	}

	res := ""
	var err error

	res, err = client.GetSet(ctx, key, value).Result()

	return res, err
}

// Set 对于Set
func (c *Client) Set(ctx context.Context, key string, value interface{}, expiration time.Duration) (string, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return "", cErr
	}
	return client.Set(ctx, key, value, expiration).Result()

}

//  对于SetNX 的包装
func (c *Client) SetNX(ctx context.Context, key string, value interface{}, expiration time.Duration) (bool, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return false, cErr
	}

	return client.SetNX(ctx, key, value, expiration).Result()

}

func (c *Client) SetWithKeys(ctx context.Context, keys []string, value interface{}, expiration time.Duration) (string, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return "", cErr
	}

	res := ""
	var err error

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, key := range keys {
			pipe.Set(ctx, key, value, expiration)
		}
		return nil
	})

	for _, cmd := range cmds {
		if value, ok := cmd.(*redis.StatusCmd); ok {
			res, err = value.Result()
			if err != nil {
				return res, err
			}
		}
	}

	return res, err
}

// Sets 对于Set和SetNX 的包装
func (c *Client) SetNXWithKeys(ctx context.Context, keys []string, value interface{}, expiration time.Duration) (bool, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return false, cErr
	}

	res := true
	var err error

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, key := range keys {
			pipe.SetNX(ctx, key, value, expiration)
		}
		return nil
	})

	for _, cmd := range cmds {
		if value, ok := cmd.(*redis.BoolCmd); ok {
			res, err = value.Result()
			if err != nil {
				return res, err
			}
		}
		//switch value := cmd.(type) {
		//case *redis.StatusCmd:
		//	_, err = value.Result()
		//case *redis.BoolCmd:
		//	res, err = value.Result()
		//
		//}
		//
		//if err != nil {
		//	return res, err
		//}
	}

	return res, err
}

//Get 获取字段
func (c *Client) Get(ctx context.Context, key string) (string, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return "", cErr
	}

	return client.Get(ctx, key).Result()
}

//MSetReq MSets的参数
type MSetReq struct {
	Key   string
	Value interface{}
}

//MSet 批量设置
func (c *Client) MSet(ctx context.Context, req ...*MSetReq) (string, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return "", cErr
	}

	slotsMap := make(map[int][]*MSetReq)
	for _, r := range req {
		slot := Slot(r.Key)
		if _, ok := slotsMap[slot]; !ok {
			slotsMap[slot] = make([]*MSetReq, 0)
		}
		slotsMap[slot] = append(slotsMap[slot], r)
	}

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, r := range slotsMap {
			pairs := make([]interface{}, 0)
			for _, p := range r {
				pairs = append(pairs, p.Key, p.Value)
			}
			err := pipe.MSet(ctx, pairs...).Err()
			if err != nil {
				return err
			}
		}
		return nil
	})

	var res string
	for _, cmd := range cmds {
		res, err = cmd.(*redis.StatusCmd).Result()
		if err != nil {
			return res, err
		}
	}

	return res, err
}

//KeyValue key-value数对
type KeyValue struct {
	Key   string
	Value string
	Err   error
}

//MGet 批量设置
func (c *Client) MGet(ctx context.Context, keys ...string) ([]*KeyValue, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return nil, cErr
	}

	slotsMap := make(map[int][]string)
	for _, k := range keys {
		slot := Slot(k)
		if _, ok := slotsMap[slot]; !ok {
			slotsMap[slot] = make([]string, 0)
		}
		slotsMap[slot] = append(slotsMap[slot], k)
	}

	slotsSlice := make([][]string, 0)

	for _, s := range slotsMap {
		slotsSlice = append(slotsSlice, s)
	}

	res := make([]*KeyValue, 0)
	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, k := range slotsSlice {
			pipe.MGet(ctx, k...)
		}
		return nil
	})

	if err != nil {
		return nil, err
	}

	if len(cmds) != len(slotsSlice) {
		return nil, fmt.Errorf("MGet fail, keys = %v", keys)
	}

	i := 0
	for _, slot := range slotsSlice {
		// fmt.Println(i)
		r, err := cmds[i].(*redis.SliceCmd).Result()
		if len(slot) != len(r) {
			return nil, fmt.Errorf("MGet fail, keys = %v", keys)
		}

		for j, k := range slot {
			v := ""
			if r[j] != nil {
				v = r[j].(string)

				res = append(res, &KeyValue{
					Key:   k,
					Value: v,
					Err:   err,
				})
			}
		}
		i++
	}

	return res, nil
}

//HSet Hash table 插入
func (c *Client) HSet(ctx context.Context, key, field string, value interface{}) (int64, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}
	return client.HSet(ctx, key, field, value).Result()
}

//HSet Hash table 插入
func (c *Client) HSetNX(ctx context.Context, key, field string, value interface{}) (bool, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return false, cErr
	}

	return client.HSetNX(ctx, key, field, value).Result()

}
