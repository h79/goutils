package pool

import (
	"context"
	"github.com/go-redis/redis/v8"
)

//ClusterNodes 获取
func (c *Client) ClusterNodes(ctx context.Context) (string, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return "", cErr
	}

	res, err := client.ClusterNodes(ctx).Result()

	return res, err
}

//ClusterInfo 获取集群信息
func (c *Client) ClusterInfo(ctx context.Context) (string, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return "", cErr
	}

	res, err := client.ClusterInfo(ctx).Result()

	return res, err
}

func (c *Client) Forget(ctx context.Context, nodeID string) (string, error) {
	cl, cErr := getClusterClient(c)
	if cErr != nil {
		return "", cErr
	}
	err := cl.ForEachShard(ctx, func(ctx context.Context, client *redis.Client) error {
		_, e := client.ClusterForget(ctx, nodeID).Result()
		return e
	})

	if err != nil {
		return "", err
	}
	return "", err
}
