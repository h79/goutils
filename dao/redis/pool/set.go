package pool

import (
	"context"
	"github.com/go-redis/redis/v8"
)

//SAdd set中添加 members
func (c *Client) SAdd(ctx context.Context, key string, members ...interface{}) (int64, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	res, err := client.SAdd(ctx, key, members...).Result()

	return res, err
}

//MSAddReq 批量 SAdd 参数
type MSAddReq struct {
	Key     string
	Members []interface{}
}

//MSAdd 批量 SAdd
func (c *Client) MSAdd(ctx context.Context, reqs []*MSAddReq) (int64, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, r := range reqs {
			pipe.SAdd(ctx, r.Key, r.Members...)
		}
		return nil
	})

	if err != nil {
		return 0, err
	}

	var res int64
	err = nil
	for _, cmd := range cmds {
		res, err = cmd.(*redis.IntCmd).Result()
		if err != nil {
			break
		}
	}

	return res, err
}

//SCard 返回set中 key 的基数
func (c *Client) SCard(ctx context.Context, key string) (int64, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	res, err := client.SCard(ctx, key).Result()

	return res, err
}

//SIsMember 判断member是否为 set中的成员
func (c *Client) SIsMember(ctx context.Context, key string, member interface{}) (bool, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return false, cErr
	}

	res, err := client.SIsMember(ctx, key, member).Result()

	return res, err
}

//SMembers 返回set中的多有成员
func (c *Client) SMembers(ctx context.Context, key string) ([]string, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return nil, cErr
	}

	res, err := client.SMembers(ctx, key).Result()

	return res, err
}

//SPop 随机弹出 set中的成员
func (c *Client) SPop(ctx context.Context, key string) (string, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return "", cErr
	}

	res, err := client.SPop(ctx, key).Result()

	return res, err
}

//SRandMember 随机返回set中一个元素
func (c *Client) SRandMember(ctx context.Context, key string) (string, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return "", cErr
	}

	res, err := client.SRandMember(ctx, key).Result()

	return res, err
}

//SRandMemberN 随机返回set中N个元素
func (c *Client) SRandMemberN(ctx context.Context, key string, n int64) ([]string, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return nil, cErr
	}

	res, err := client.SRandMemberN(ctx, key, n).Result()

	return res, err
}

//SRem 移除集合 key 中的一个或多个 member 元素
func (c *Client) SRem(ctx context.Context, key string, member ...interface{}) (int64, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	res, err := client.SRem(ctx, key, member...).Result()

	return res, err
}

//MSRemReq 批量 SRem 参数
type MSRemReq struct {
	Key     string
	Members []interface{}
}

//MSRem 批量 SRem
func (c *Client) MSRem(ctx context.Context, reqs []*MSRemReq) (int64, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, r := range reqs {
			pipe.SRem(ctx, r.Key, r.Members...)
		}
		return nil
	})

	if err != nil {
		return 0, err
	}

	var res int64
	err = nil
	for _, cmd := range cmds {
		res, err = cmd.(*redis.IntCmd).Result()
		if err != nil {
			break
		}
	}

	return res, err
}
