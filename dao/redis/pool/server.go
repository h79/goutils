package pool

import (
	"context"
	"sync"

	"github.com/go-redis/redis/v8"
)

//Info 获取所有就节点信息
func (c *Client) Info(ctx context.Context, section ...string) (res map[string]string, err error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return nil, cErr
	}

	res = make(map[string]string, 0)
	locker := sync.RWMutex{}
	err = client.ForEachShard(ctx, func(ctx context.Context, c *redis.Client) error {
		defer locker.Unlock()

		addr := c.Options().Addr
		r, e := c.Info(ctx, section...).Result()
		locker.Lock()
		if e == nil {
			res[addr] = r
		} else {
			res[addr] = e.Error()
		}

		return e
	})

	return res, err
}

//NodeInfo 获取单个节点的info
func (c *Client) NodeInfo(ctx context.Context, addr string, section ...string) (res string, err error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return "", cErr
	}

	err = client.ForEachShard(ctx, func(ctx context.Context, c *redis.Client) (err error) {
		if addr == c.Options().Addr {
			res, err = c.Info(ctx, section...).Result()
		}
		return
	})

	return res, err
}
