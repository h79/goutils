package pool

import (
	"context"
	"github.com/go-redis/redis/v8"
)

//Z sorted set Strut
type Z struct {
	Score  float64
	Member interface{}
}

//ZAdd 添加sorted set
func (c *Client) ZAdd(ctx context.Context, key string, member ...Z) (int64, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	ms := make([]*redis.Z, 0)
	for _, m := range member {
		ms = append(ms, &redis.Z{
			Score:  m.Score,
			Member: m.Member,
		})
	}
	res, err := client.ZAdd(ctx, key, ms...).Result()
	return res, err
}

//MZAddReq 批量插入有序集 参数
type MZAddReq struct {
	Key     string
	Members []*Z
}

//MZAdd 添加sorted set
func (c *Client) MZAdd(ctx context.Context, reqs []*MZAddReq) (int64, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, r := range reqs {
			ms := make([]*redis.Z, 0)
			for _, m := range r.Members {
				ms = append(ms, &redis.Z{
					Score:  m.Score,
					Member: m.Member,
				})
			}
			pipe.ZAdd(ctx, r.Key, ms...)
		}
		return nil
	})

	if err != nil {
		return 0, err
	}

	var res int64
	err = nil
	for _, cmd := range cmds {
		res, err = cmd.(*redis.IntCmd).Result()
		if err != nil {
			break
		}
	}

	return res, err
}

//ZCard 返回有序集 key 的基数。
func (c *Client) ZCard(ctx context.Context, key string) (int64, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	res, err := client.ZCard(ctx, key).Result()
	return res, err
}

//ZCount 返回score在min, max 中的数量 ，默认闭区间开一在min和max前添加 ( 来使用开区间，min 和 max 可以是 -inf 和 +inf
func (c *Client) ZCount(ctx context.Context, key, min, max string) (int64, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	res, err := client.ZCount(ctx, key, min, max).Result()

	return res, err
}

//ZRange  返回有序集 key 中，指定区间内的成员,从小到大,
//下标参数 start 和 stop 都以 0 为底，也就是说，以 0 表示有序集第一个成员，以 1 表示有序集第二个成员，以此类推。
//你也可以使用负数下标，以 -1 表示最后一个成员， -2 表示倒数第二个成员，以此类推。
func (c *Client) ZRange(ctx context.Context, key string, start, stop int64) ([]string, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return nil, cErr
	}

	res, err := client.ZRange(ctx, key, start, stop).Result()

	return res, err
}

//ZRevRange  返回有序集 key 中，指定区间内的成员,从大到小
//下标参数 start 和 stop 都以 0 为底，也就是说，以 0 表示有序集第一个成员，以 1 表示有序集第二个成员，以此类推。
//你也可以使用负数下标，以 -1 表示最后一个成员， -2 表示倒数第二个成员，以此类推。
func (c *Client) ZRevRange(ctx context.Context, key string, start, stop int64) ([]string, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return nil, cErr
	}

	res, err := client.ZRevRange(ctx, key, start, stop).Result()

	return res, err
}

//ZRangeWithScores 返回有序集 key 中，指定区间内的成员(带score), 从小到大
func (c *Client) ZRangeWithScores(ctx context.Context, key string, start, stop int64) ([]*Z, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return nil, cErr
	}

	tmp, err := client.ZRangeWithScores(ctx, key, start, stop).Result()

	res := make([]*Z, 0)
	for _, t := range tmp {
		res = append(res, &Z{
			Score:  t.Score,
			Member: t.Member,
		})
	}

	return res, err
}

//ZRevRangeWithScores 返回有序集 key 中，指定区间内的成员(带score), 从大到小
func (c *Client) ZRevRangeWithScores(ctx context.Context, key string, start, stop int64) ([]*Z, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return nil, cErr
	}

	tmp, err := client.ZRevRangeWithScores(ctx, key, start, stop).Result()

	res := make([]*Z, 0)
	for _, t := range tmp {
		res = append(res, &Z{
			Score:  t.Score,
			Member: t.Member,
		})
	}

	return res, err
}

//ZRangeByScore 返回有序集 key 中，所有 score 值介于 min 和 max 之间(包括等于 min 或 max )的成员。有序集成员按 score 值递增(从小到大)次序排列。
func (c *Client) ZRangeByScore(ctx context.Context, key, min, max string, offset, count int64) ([]string, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return nil, cErr
	}

	res, err := client.ZRangeByScore(ctx, key, &redis.ZRangeBy{
		Max:    max,
		Min:    min,
		Offset: offset,
		Count:  count,
	}).Result()

	return res, err
}

//ZRevRangeByScore 返回有序集 key 中，所有 score 值介于 min 和 max 之间(包括等于 min 或 max )的成员。有序集成员按 score 值递增(从小到大)次序排列。
func (c *Client) ZRevRangeByScore(ctx context.Context, key, min, max string, offset, count int64) ([]string, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return nil, cErr
	}

	res, err := client.ZRevRangeByScore(ctx, key, &redis.ZRangeBy{
		Max:    max,
		Min:    min,
		Offset: offset,
		Count:  count,
	}).Result()

	return res, err
}

//ZRangeByScoreWithScores 返回有序集 key 中，所有 score 值介于 min 和 max 之间(包括等于 min 或 max )的成员。有序集成员按 score 值递增(从小到大)次序排列(带score)。
func (c *Client) ZRangeByScoreWithScores(ctx context.Context, key, min, max string, offset, count int64) ([]*Z, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return nil, cErr
	}

	tmp, err := client.ZRangeByScoreWithScores(ctx, key, &redis.ZRangeBy{
		Max:    max,
		Min:    min,
		Offset: offset,
		Count:  count,
	}).Result()

	res := make([]*Z, 0)
	for _, t := range tmp {
		res = append(res, &Z{
			Score:  t.Score,
			Member: t.Member,
		})
	}

	return res, err
}

//ZRevRangeByScoreWithScores 返回有序集 key 中，所有 score 值介于 min 和 max 之间(包括等于 min 或 max )的成员。有序集成员按 score 值递增(从小到大)次序排列(带score)。
func (c *Client) ZRevRangeByScoreWithScores(ctx context.Context, key, min, max string, offset, count int64) ([]*Z, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return nil, cErr
	}

	tmp, err := client.ZRevRangeByScoreWithScores(ctx, key, &redis.ZRangeBy{
		Max:    max,
		Min:    min,
		Offset: offset,
		Count:  count,
	}).Result()

	res := make([]*Z, 0)
	for _, t := range tmp {
		res = append(res, &Z{
			Score:  t.Score,
			Member: t.Member,
		})
	}

	return res, err
}

//ZRem 移除有序集 key 中的一个或多个成员，不存在的成员将被忽略
func (c *Client) ZRem(ctx context.Context, key string, members ...interface{}) (int64, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	res, err := client.ZRem(ctx, key, members...).Result()

	return res, err
}

//MZRemReq 批量移除有序集参数
type MZRemReq struct {
	Key     string
	Members []interface{}
}

//MZRem 移除多个序集 key 中的一个或多个成员，不存在的成员将被忽略
func (c *Client) MZRem(ctx context.Context, reqs []*MZRemReq) (int64, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, r := range reqs {
			pipe.ZRem(ctx, r.Key, r.Members...)
		}
		return nil
	})

	if err != nil {
		return 0, err
	}

	var res int64
	err = nil
	for _, cmd := range cmds {
		res, err = cmd.(*redis.IntCmd).Result()
		if err != nil {
			break
		}
	}

	return res, err
}

//ZRemRangeByRank 移除有序集 key 中，指定排名(rank)区间内的所有成员
func (c *Client) ZRemRangeByRank(ctx context.Context, key string, start, stop int64) (int64, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	res, err := client.ZRemRangeByRank(ctx, key, start, stop).Result()

	return res, err
}

//MZRemRangeByRankReq 按照排序批量移除多个有序集成员的参数
type MZRemRangeByRankReq struct {
	Key         string
	Start, Stop int64
}

//MZRemRangeByRank 按照排序批量移除多个有序集成员
func (c *Client) MZRemRangeByRank(ctx context.Context, reqs []*MZRemRangeByRankReq) (int64, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, r := range reqs {
			pipe.ZRemRangeByRank(ctx, r.Key, r.Start, r.Stop)
		}
		return nil
	})

	if err != nil {
		return 0, err
	}

	var res int64
	err = nil
	for _, cmd := range cmds {
		res, err = cmd.(*redis.IntCmd).Result()
		if err != nil {
			break
		}
	}

	return res, err
}

//ZRemRangeByScore 移除有序集 key 中，所有 score 值介于 min 和 max 之间(包括等于 min 或 max )的成员。
func (c *Client) ZRemRangeByScore(ctx context.Context, key string, min, max string) (int64, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	res, err := client.ZRemRangeByScore(ctx, key, min, max).Result()

	return res, err
}

//MZRemRangeByScoreReq 按照score，批量移除多个有序集成员参数，
type MZRemRangeByScoreReq struct {
	Key      string
	Min, Max string
}

//MZRemRangeByScore 按照score，批量移除多个有序集成员
func (c *Client) MZRemRangeByScore(ctx context.Context, reqs []*MZRemRangeByScoreReq) (int64, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, r := range reqs {
			pipe.ZRemRangeByScore(ctx, r.Key, r.Min, r.Max)
		}
		return nil
	})

	if err != nil {
		return 0, err
	}

	var res int64
	err = nil
	for _, cmd := range cmds {
		res, err = cmd.(*redis.IntCmd).Result()
		if err != nil {
			break
		}
	}

	return res, err
}

//ZIncrBy 为有序集 key 的成员 member 的 score 值加上增量 increment
func (c *Client) ZIncrBy(ctx context.Context, key string, increment float64, member string) (float64, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	return client.ZIncrBy(ctx, key, increment, member).Result()
}

//MZIncrByReq 批量为有序集 key 的成员 member 的 score 值加上增量 increment 参数
type MZIncrByReq struct {
	Key       string
	Increment float64
	Member    string
}

//MZIncrBy 批量为有序集 key 的成员 member 的 score 值加上增量 increment
func (c *Client) MZIncrBy(ctx context.Context, reqs []*MZIncrByReq) (float64, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}

	cmds, err := client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, r := range reqs {
			pipe.ZIncrBy(ctx, r.Key, r.Increment, r.Member)
		}
		return nil
	})

	if err != nil {
		return 0, err
	}

	var res float64
	err = nil
	for _, cmd := range cmds {
		res, err = cmd.(*redis.FloatCmd).Result()
		if err != nil {
			break
		}
	}

	return res, err
}

//ZScore 返回有序集 key 中，成员 member 的 score 值。
func (c *Client) ZScore(ctx context.Context, key, member string) (float64, error) {
	client, cErr := getClusterClient(c)

	if cErr != nil {
		return 0, cErr
	}
	return client.ZScore(ctx, key, member).Result()
}
