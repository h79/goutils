package pool

import (
	"fmt"
	"gitee.com/h79/goutils/dao/config"
	"time"

	"github.com/go-redis/redis/v8"
)

// Pool redis机群单例
var pool *Client

// Client 线程池类型
type Client struct {
	Clusters     []*redis.ClusterClient //集群
	W            int                    //写成功数
	R            int                    //读成功数
	WriteTimeout time.Duration          //总写时延
	ReadTimeout  time.Duration          //总读时延
}

func NewPool(clusterConf []*config.Cluster) {
	pool = newClient(clusterConf)
}

// newClusterClient new a ClusterClient
func newClusterClient(clusterConf *config.Cluster) *redis.ClusterClient {

	adders := make([]string, 0)
	for _, node := range clusterConf.Nodes {
		adders = append(adders, node.To())
	}

	options := redis.ClusterOptions{
		Addrs:              adders,
		DialTimeout:        clusterConf.DialTimeout * time.Millisecond,
		ReadTimeout:        clusterConf.ReadTimeout * time.Millisecond,
		WriteTimeout:       clusterConf.WriteTimeout * time.Millisecond,
		Password:           clusterConf.Password,
		PoolSize:           clusterConf.PoolSize,
		ReadOnly:           clusterConf.ReadOnly,
		PoolTimeout:        30 * time.Second,
		IdleTimeout:        10 * time.Second,
		IdleCheckFrequency: 1 * time.Second,
	}

	return redis.NewClusterClient(&options)
}

// newClient 新建连接池
func newClient(clusterConf []*config.Cluster) *Client {
	client := Client{}
	for _, clusterConfig := range clusterConf {
		cluster := newClusterClient(clusterConfig)
		client.Clusters = append(client.Clusters, cluster)
	}

	client.R = 1
	client.W = 1
	client.ReadTimeout = time.Millisecond * 200000
	client.WriteTimeout = time.Millisecond * 200000

	if client.W > len(client.Clusters) {
		client.W = len(client.Clusters)
	}

	if client.R > len(client.Clusters) {
		client.R = len(client.Clusters)
	}

	return &client
}

// Get 获取线程池
func Get() *Client {
	return pool
}

func getClusterClient(c *Client) (*redis.ClusterClient, error) {
	if len(c.Clusters) == 0 {
		return nil, fmt.Errorf("Clusters is 0")
	}
	return c.Clusters[0], nil
}
