package redis

import (
	"github.com/go-redis/redis/v8"
)

type Redis interface {
	Rds() *redis.Client
	Sentinel() *redis.SentinelClient
	Cluster() *redis.ClusterClient
	Name() string
}

type Client interface {
	Get(name string) (Redis, error)
	Close(name string) error
	CloseAll()
	Select(name string) //选哪个redis
	GetSelector() string
	SelectDB(name string, index int) error //选哪个redis db
}
