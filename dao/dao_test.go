package dao

import (
	commonoption "gitee.com/h79/goutils/common/option"
	"gitee.com/h79/goutils/dao/config"
	"gitee.com/h79/goutils/dao/db"
	"gitee.com/h79/goutils/dao/option"
	"gitee.com/h79/goutils/dao/redis"
	"gitee.com/h79/goutils/dao/wrapper"
	"strings"
	"testing"
	"time"
)

// Storage [...]
type Storage struct {
	StorageID    int       `gorm:"primaryKey;column:STORAGE_ID" json:"-"`
	StorageName  string    `gorm:"column:STORAGE_NAME" json:"sTORAGENAME"`
	Remark       string    `gorm:"column:REMARK" json:"rEMARK"`
	GroupLimit   string    `gorm:"column:GROUP_LIMIT" json:"gROUPLIMIT"`
	CreateUserID int       `gorm:"column:CREATE_USER_ID" json:"cREATEUSERId"`
	CreateDate   time.Time `gorm:"column:CREATE_DATE" json:"cREATEDATE"`
}

// TableName get sql table name.获取数据库表名
func (m *Storage) TableName() string {
	return "storage"
}

func TestCondition(t *testing.T) {

	var builder = strings.Builder{}

	wrapper.SqlColumn(&builder, "`bbbb`.`xxx`")
	wrapper.AddQuoted(&builder, "`xxx")
	t.Log(builder.String())
	/*	conds := make([]interface{}, 0)
		conds = append(conds, "active = ?")
		conds = append(conds, 1)
		conds = append(conds, " and ")
		conds = append(conds, "(name,alb,lddd) IN")
		conds = append(conds, "email LIKE ?")
		conds = append(conds, "%jinzhu%")

		t.Log(conds)*/

	Con := wrapper.Condition{}
	Con.And("name", wrapper.In, []interface{}{1, 3, 4})
	Con.Or("name", wrapper.Like, "\"3333%")
	Con.Or("name", wrapper.Like, "'3333")
	Con.Or("name", wrapper.Like, "\\3333")
	Con.Between("create_at", 111, 222)

	t.Logf("In: %v\n%v", Con.Cond(), Con.Value())

	t.Logf("Sql= %s", Con.Build(option.WithFullSQL()))
}

func TestSelect(t *testing.T) {
	sel := wrapper.Select{}
	sel.Add("xxxx")
	csel := sel.Clone()
	csel.Add("yyyy")
	csel.Add("zzzz")
	t.Logf("sel: %#v", sel)
	t.Logf("clone: %#v", csel)

	csel.Delete("zzzz")
	t.Logf("clone1: %#v", csel)
}

func TestName(t *testing.T) {
}
func TestSQL(t *testing.T) {

	col := wrapper.Column{
		Column: "xxxx",
	}
	col.WithFunc(wrapper.KSQLMax).AS("bbbb")
	t.Logf("col: %s", col.Build(nil))

	sel := wrapper.Select{}
	sel.Add("xxxx.yyy")
	sel.Add("bbb")
	sel.Add("max(xxx)")
	//
	SQL := wrapper.NewSQL(&wrapper.From{From: "tb_xxx"}, option.WithFullSQL(), option.WithSubQuerySQL(""))
	SQL.WithSelector(&sel)
	//
	Con := wrapper.Condition{}
	Con.And("name", wrapper.In, []interface{}{1, 3, 4})
	Con.Or("name", wrapper.Like, "\"3333")
	Con.Or("name", wrapper.Like, "%'3333")
	Con.Between("create_at", 111, 222)

	by := wrapper.OrderBy{
		{Column: "create_at", Desc: true},
		{Column: "update_at", Desc: true},
	}
	SQL.WithOrderBy(&by)
	SQL.WithLimit(&wrapper.Limit{Limit: 50, Offset: 20})
	SQL.WithWhere(&Con)

	group := wrapper.GroupBy{{Column: "create_at"}}
	SQL.WithGroupBy(&group)

	SQL.WithJoin(&wrapper.Join{Type: "", Table: "TB_BBBB", ON: "XXX=YYYY"})
	//
	sql := SQL.Build(option.SqlColumnConvert(func(col string) string {
		return col
	}))
	t.Logf("sql1: %#v", sql)
	//
	//SQL1 := wrapper.NewSQL(SQL.AsChild("A"))
	//SQL1.WithSelector(&sel)
	//groupBy := wrapper.GroupBy{
	//	{Column: "create_at"},
	//	{Column: "update_at"},
	//}
	//SQL1.WithGroupBy(&groupBy)

	//t.Logf("sql2: %#v", SQL1.Build())

	SQL2 := wrapper.NewSQL(&wrapper.From{From: "tb_xxx"})
	SQL2.WithSelector(&sel)
	Con2 := wrapper.Condition{}
	Con2.Or("name", wrapper.Like, "\\3333")
	wrapper.AND(&Con2, "XXX", 2, []int{11, 12, 13})
	Con2.Or("name", wrapper.NotIn, SQL.AsSubQuery(""))
	Con2.Or("", wrapper.Exists, SQL)
	SQL2.WithOrderBy(&by)
	SQL2.WithWhere(&Con2)
	t.Logf("sql3: %#v", SQL2.Build())
}

func TestConnectDB(t *testing.T) {

	cfg := config.Sql{
		Master: config.Database{
			Host:       "192.168.2.233",
			Port:       3306,
			User:       "5",
			Pwd:        "5",
			DriverType: "mysql",
			Name:       "dm",
		},
	}

	adapter, err := db.NewAdapter(&cfg)
	if err != nil {
		t.Error(err)
		return
	}
	t.Log("连接数据库OK")

	page := wrapper.NewPage(adapter.Db(), 500).
		WithSize(10)

	ss := make([]Storage, 0)
	page.WithResult(&ss)
	err = page.FindPage(Storage{}, nil, nil)
	if err != nil {
		t.Error(err)
		return
	}
	t.Logf("Total: %v", page.Total)
	t.Logf("Result 1: %v", page.Results)

	/*base := &wrapper.Base{
		DB: adapter.Db(),
	}*/
	Cond0 := wrapper.Condition{}
	Cond0.Eq("STORAGE_ID", 13)

	Sel0 := wrapper.Select{}
	Sel0.Add("REMARK").Add("STORAGE_NAME")
	st := Storage{}
	page.WithResult(&st)
	err = page.Find(Storage{}, &Cond0, &Sel0)

	t.Logf("Result 2: %v", page.Results)

	Cond := wrapper.Condition{}
	Cond.Like("STORAGE_NAME", "%仓%")
	Cond1 := wrapper.Condition{}
	Cond1.Eq("REMARK", "备注")

	t.Logf("Cond: %s", Cond.Cond())
	t.Logf("Values: %v", Cond.Value())
	st2 := make([]Storage, 0)
	page.DB.Model(Storage{}).Where(Cond.Cond(), Cond.Value()...).Or(Cond1.Cond(), Cond1.Value()...).Find(&st2)
	t.Logf("Result 2-1: %v", st2)

	//
	cs := Storage{}
	er := page.Create(&cs, func(q *option.Query) {
		q.Q["STORAGE_NAME"] = "huqiuyun"
		q.Q["REMARK"] = "huqiuyun"
	})
	t.Logf("创建：+%v", er)

	Cond2 := wrapper.Condition{}
	Cond2.Eq("STORAGE_ID", 16)
	err = page.Delete(&Storage{}, &Cond2)
	t.Logf("Delete :%v", err)

	Cond3 := &wrapper.Condition{}
	sss := make([]Storage, 0)
	page.WithResult(&sss)
	_ = page.Find(Storage{}, Cond3.Eq("CREATE_USER_ID", 63), nil)

	t.Logf("Result 3: %v", page.Results)

}

func TestConnectRedis(t *testing.T) {
	cfg := []config.Redis{{
		Name: "dm",
		Master: config.RedisConfig{
			Host: []string{"127.0.0.1:5882"},
			Pwd:  "Dmzg2017",
			DB:   1,
		},
	}}
	_, err := redis.NewRedis(cfg)
	if err != nil {
		t.Error(err)
		return
	}
	t.Log("连接REDIS成功")
}

func TestSwap(t *testing.T) {
	cateId := []int32{2, 23}
	cond := wrapper.Condition{}
	cond.AndLt("ss", 1)
	if len(cateId) == 1 {
		cond.AndEq("ss", cateId[0])
	} else if len(cateId) > 1 {
		in := wrapper.Condition{}
		in.In("ss", cateId)
		cond.From(&in, false, wrapper.And)
	}
	t.Log(cond.Query())
}

func TestInSubQuery(t *testing.T) {
	cateId := []int32{2, 23}
	cond := wrapper.Condition{}
	cond.AndLt("ss", 1)
	if len(cateId) == 1 {
		cond.AndEq("ss", cateId[0])
	} else if len(cateId) > 1 {
		in := wrapper.Condition{}
		in.In("ss", &inSubQuery{List: cateId})
		cond.From(&in, false, wrapper.And)
	}
	t.Log(cond.Build())
}

var _ wrapper.IBuilder = (*inSubQuery)(nil)

type inSubQuery struct {
	List []int32
}

func (i inSubQuery) Is() bool {
	return true
}

func (i inSubQuery) Build(opts ...commonoption.Option) string {
	return "select xxxxxddfr"
}
