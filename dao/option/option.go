package option

import "gitee.com/h79/goutils/common/option"

const (
	TypeMaxColumn = iota
	TypeFullSql
	TypeSqlColumnConvert
	TypeSubQuery
	TypeQuery
	TypeNameKey //子查询
	TypeTimeOut
	TypeAlarm
	TypeSqlDns          = 1000
	TypeSqlTls          = 1001
	TypeRedisTls        = 1002
	TypeSqlServerPubKey = 1003
)

type Query struct {
	Q map[string]interface{}
}

func WithQuery() option.Option {
	return &Query{Q: map[string]interface{}{}}
}

func (t Query) String() string {
	return "query"
}
func (t Query) Type() int          { return TypeQuery }
func (t Query) Value() interface{} { return t.Q }

func QueryExist(opts ...option.Option) *Query {
	if r, ok := option.Exist(TypeQuery, opts...); ok {
		return r.(*Query)
	}
	return nil
}

type QueryFunc func(q *Query)

func (f QueryFunc) Apply(q *Query) {
	f(q)
}

func WithMaxColumn(c int) option.Option {
	return option.WithOpt(c, "max:column", TypeMaxColumn)
}

func MaxColumnExist(opts ...option.Option) int {
	return option.Select[int](TypeMaxColumn, 6, opts...)
}

func WithFullSQL() option.Option {
	return option.WithOpt(true, "full:sql", TypeFullSql)
}

func FullSqlExist(opts ...option.Option) bool {
	return option.Select[bool](TypeFullSql, false, opts...)
}

func SqlColumnConvert(f func(col string) string) option.Option {
	return ColumnConvertFunc(f)
}

type ColumnConvertFunc func(col string) string

func (t ColumnConvertFunc) String() string {
	return "sql:column:convert"
}
func (t ColumnConvertFunc) Type() int          { return TypeSqlColumnConvert }
func (t ColumnConvertFunc) Value() interface{} { return t }

func SqlColumnConvertExist(opts ...option.Option) ColumnConvertFunc {
	if r, ok := option.Exist(TypeSqlColumnConvert, opts...); ok {
		return r.Value().(ColumnConvertFunc)
	}
	return nil
}

func WithSubQuerySQL(as string) option.Option {
	return option.WithOpt(as, "subquery:sql", TypeSubQuery)
}

func SubQueryExist(opts ...option.Option) (string, bool) {
	return option.SelectV2[string](TypeSubQuery, "", opts...)
}

func WithNameKey(name string) option.Option {
	return option.WithOpt(name, "db.name.key", TypeNameKey)
}

func NameKeyExist(opts ...option.Option) (string, bool) {
	return option.SelectV2[string](TypeNameKey, "", opts...)
}

func WithTimeOutKey(timeout int64) option.Option {
	return option.WithOpt(timeout, "db.timeout.key", TypeTimeOut)
}

func TimeOutExist(opts ...option.Option) (int64, bool) {
	return option.SelectV2[int64](TypeTimeOut, 3000, opts...)
}

func WithAlarmKey() option.Option {
	return option.WithOpt(true, "db.alarm.key", TypeAlarm)
}

func AlarmExist(opts ...option.Option) bool {
	return option.Select(TypeAlarm, false, opts...)
}
