package util

import (
	"context"
	"gitee.com/h79/goutils/alarm"
)

func Alarm(code int32, title string, detail string, err error) {
	alarm.Fatal(context.Background(), code, "dao", detail, title, err)
}
