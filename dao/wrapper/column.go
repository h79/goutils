package wrapper

import (
	"fmt"
	commonoption "gitee.com/h79/goutils/common/option"
	"gitee.com/h79/goutils/dao/option"
	"strings"
)

func SqlColumn(builder *strings.Builder, column string) bool {
	before, after, ok := strings.Cut(column, ".")
	if ok && IsQuoted(before) && IsQuoted(after) {
		builder.WriteString(column)
		return true
	}
	if !AddQuoted(builder, before) {
		return false
	}
	if ok && after != "" {
		builder.WriteByte('.')
		AddQuoted(builder, after)
	}
	return true
}

func ParseColumn(column string) TableColumn {
	var t = strings.Split(column, ".")
	switch len(t) {
	case 1:
		return TableColumn{Col: t[0]}
	case 2:
		return TableColumn{Name: t[0], Col: t[1]}
	}
	return TableColumn{}
}

type TableColumn struct {
	Name string `form:"name" json:"name"` //表名
	Col  string `form:"col" json:"col"`   //项
}

func (t *TableColumn) Column() string {
	var builder = strings.Builder{}
	if AddQuoted(&builder, t.Name) {
		builder.WriteByte('.')
	}
	AddQuoted(&builder, t.Col)
	return builder.String()
}

type FField string

func (ff FField) Equal(des FField) bool {
	return ff == des
}

type Flag struct {
	FInt int `form:"flag" json:"flag" yaml:"flag"`
}

type Column struct {
	Flag   //是否函数
	Table  string
	Column string `form:"col" json:"col" yaml:"col"` //项
	As     string `form:"as" json:"as" yaml:"as"`    // 别名
	Expr   string `form:"expr" json:"expr" yaml:"expr"`
	Desc   bool   `form:"desc" json:"desc" yaml:"desc"` //（降序（大=>小）desc, 升序（小=>大）asc
}

func WithTable(table string) *Column {
	return &Column{
		Table: table,
	}
}

func WithColumn(column string) *Column {
	return &Column{
		Column: column,
	}
}

func WithColumns(cols []string) *Column {
	c := &Column{}
	c.SetColumn(cols)
	return c
}

func (col *Column) GetColumn(opts ...commonoption.Option) []string {
	return strings.SplitN(col.Column, ",", option.MaxColumnExist(opts...))
}

func (col *Column) SetColumn(cols []string) {
	if len(cols) <= 0 {
		col.Column = ""
		return
	}
	col.Column = strings.Join(cols, ",")
}

func (col *Column) WithTable(table string) *Column {
	col.Table = table
	return col
}

func (col *Column) With(column string) *Column {
	col.Column = column
	return col
}

func (col *Column) AS(as string) *Column {
	col.As = as
	return col
}

func (col *Column) WithFunc(fg int) *Column {
	col.FInt = fg
	return col
}

func (col *Column) Check(filters []FField) *Column {
	var (
		filter = func(col string) bool {
			for i := range filters {
				if filters[i].Equal(FField(col)) {
					return true
				}
			}
			return false
		}
		newCol []string
	)
	cols := col.GetColumn()
	for i := range cols {
		if filter(cols[i]) {
			newCol = append(newCol, cols[i])
		}
	}
	col.SetColumn(newCol)
	return col
}

func (col *Column) Build(convert option.ColumnConvertFunc) string {

	builder := strings.Builder{}
	if col.Expr != "" {
		builder.WriteString(col.Expr)
	} else {
		fn := SQLFunc(col.FInt)
		if len(fn) > 0 {
			builder.WriteString(fn)
			builder.WriteByte('(')
			if convert != nil {
				AddTable(&builder, col.Table)
				AddQuoted(&builder, convert(col.Column))
			} else {
				AddTable(&builder, col.Table)
				AddQuoted(&builder, col.Column)
			}
			builder.WriteByte(')')
		} else if convert != nil {
			AddTable(&builder, col.Table)
			AddQuoted(&builder, convert(col.Column))
		} else {
			AddTable(&builder, col.Table)
			AddQuoted(&builder, col.Column)
		}
	}
	AddAlias(&builder, col.As)
	return builder.String()
}

func ColumnName[T ~string](column T) string {
	return string(column)
}

func TColumnName[T ~string](table string, column T) string {
	return fmt.Sprintf("`%s`.`%s`", table, column)
}

func ColumnAs[T ~string](column T, as string) string {
	if as != "" {
		return fmt.Sprintf("`%s` as %s", column, as)
	}
	return fmt.Sprintf("`%s`", column)
}

func TColumnAs[T ~string](table string, column T, as string) string {
	if as != "" {
		return fmt.Sprintf("`%s`.`%s` as %s", table, column, as)
	}
	return fmt.Sprintf("`%s`.`%s`", table, column)
}
