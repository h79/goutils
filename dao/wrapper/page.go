package wrapper

import (
	"context"
	"gitee.com/h79/goutils/common"
	"gorm.io/gorm"
	"time"
)

type Page struct {
	Base
	Total  int64
	size   int
	offset int
}

func NewPage(db *gorm.DB, timeout time.Duration) *Page {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	return &Page{
		Base: Base{
			DB:     db,
			ctx:    ctx,
			cancel: cancel,
		},
	}
}

func (page *Page) WithSize(size int) *Page {
	page.size = size
	return page
}

func (page *Page) WithOffset(offset int) *Page {
	page.offset = offset
	return page
}

// FindPage
// param results := make([]object, 0)
func (page *Page) FindPage(model interface{}, where IQuery, sel ISelect) error {
	db := page.DB.Model(model)
	if where != nil && where.Is() {
		db = db.Where(where.Query(), where.Value()...)
	}
	if sel != nil && sel.Is() {
		db = db.Select(sel.Query(), sel.Value()...)
	}
	db.Count(&page.Total)
	if !common.IsNil(page.order) {
		db = db.Order(page.order)
	}
	if page.size > 0 {
		db = db.Limit(page.size)
		db = db.Offset(page.offset)
	}
	return db.Find(page.Results).Error
}
