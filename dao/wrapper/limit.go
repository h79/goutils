package wrapper

import (
	commonoption "gitee.com/h79/goutils/common/option"
	"strings"
)

var _ ILimit = (*Limit)(nil)

type Limit struct {
	Offset int
	Limit  int
}

func (l *Limit) Is() bool {
	return l.Limit > 0
}

func (l *Limit) GetLimit() int {
	return l.Limit
}

func (l *Limit) GetOffset() int {
	return l.Offset
}

func (l *Limit) Build(opts ...commonoption.Option) string {
	builder := strings.Builder{}
	if l.Limit > 0 {
		builder.WriteString(" LIMIT ")
		AddVar(&builder, l.Limit)
	}
	if l.Offset > 0 {
		builder.WriteString(" OFFSET ")
		AddVar(&builder, l.Offset)
	}
	return builder.String()
}
