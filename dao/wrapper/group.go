package wrapper

import (
	commonoption "gitee.com/h79/goutils/common/option"
	"gitee.com/h79/goutils/dao/option"
	"strings"
)

type GroupBy []Column

var _ IBuilder = (*GroupBy)(nil)

func (g *GroupBy) Is() bool {
	return len(*g) > 0
}

func (g *GroupBy) Build(opts ...commonoption.Option) string {
	var ii = 0
	var m = 6
	var full = true
	var convert option.ColumnConvertFunc
	commonoption.Filter(func(opt commonoption.Option) bool {
		if opt.Type() == option.TypeMaxColumn {
			m = opt.Value().(int)
			ii++
		} else if opt.Type() == option.TypeFullSql {
			full = opt.Value().(bool)
			ii++
		} else if opt.Type() == option.TypeSqlColumnConvert {
			convert = opt.Value().(option.ColumnConvertFunc)
			ii++
		}
		return ii == 3
	}, opts...)
	return g.buildOpt(full, convert, m)
}

func (g *GroupBy) Check(filters []FField, opts ...commonoption.Option) {
	length := len(*g)
	m := option.MaxColumnExist(opts...)
	if length > m { //防止多
		length = m
	}
	cols := (*g)[:length]
	for i := range cols {
		cols[i].Check(filters)
	}
}

func (g *GroupBy) buildOpt(full bool, convert option.ColumnConvertFunc, max int) string {
	count := 0
	builder := strings.Builder{}
	expr := false
	cc := false
	for i := range *g {
		if count >= max {
			//后面不要接入
			break
		}
		if count == 0 {
			if full {
				builder.WriteString(" GROUP BY ")
			}
		} else if count > 0 {
			builder.WriteByte(',')
		}
		expr = (*g)[i].Expr != ""
		if (*g)[i].Column != "" {
			if convert != nil {
				cc = SqlColumn(&builder, convert((*g)[i].Column))
			} else {
				cc = SqlColumn(&builder, (*g)[i].Column)
			}
			if expr && cc {
				builder.WriteByte(' ')
			}
		}
		if expr {
			builder.WriteString((*g)[i].Expr)
		}
		count++
	}
	return builder.String()
}
