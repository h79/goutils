package wrapper

import (
	commonoption "gitee.com/h79/goutils/common/option"
	"strings"
)

// 内连接(inner join…on…)
// 全外连接（full join…on…)
// 左连接（left join…on…)
// 右连接（right join…on…)
// 交叉连接(cross join …on…)
const (
	JoinLeft  = "LEFT"
	JoinRight = "RIGHT"
	JoinInner = "INNER"
	JoinFull  = "FULL"
	JonnCross = "CROSS"
)

var _ IBuilder = (*Join)(nil)

type Join struct {
	Type  string   `json:"type" yaml:"type"`
	Table string   `json:"table" yaml:"table"`
	Alias string   `json:"as" yaml:"as"`
	ON    string   `json:"expr" yaml:"expr"`
	Using []string `json:"using" yaml:"using"`
}

func (jn *Join) Is() bool {
	if jn.Type == "" {
		return false
	}
	if jn.Table == "" {
		if jn.ON == "" {
			return false
		}
	} else if jn.ON == "" && len(jn.Using) == 0 {
		return false
	}
	return true
}

func (jn *Join) JoinType() (string, bool) {
	jt := strings.ToUpper(jn.Type)
	return jt, (jt == JoinLeft ||
		jt == JoinRight ||
		jt == JoinFull ||
		jt == JoinInner ||
		jt == JonnCross) && jn.Is()
}

func (jn *Join) Build(opts ...commonoption.Option) string {
	joinType, ok := jn.JoinType()
	if !ok {
		return ""
	}
	var builder = strings.Builder{}
	builder.WriteByte(' ')
	builder.WriteString(joinType)
	builder.WriteString(" JOIN ")
	if jn.Table != "" {
		AddQuoted(&builder, jn.Table)
		AddAlias(&builder, jn.Alias)
		if len(jn.ON) > 0 {
			builder.WriteString(" ON ")
			builder.WriteString(jn.ON)
		} else if len(jn.Using) > 0 {
			builder.WriteString(" USING (")
			for idx, c := range jn.Using {
				if idx > 0 {
					builder.WriteByte(',')
				}
				builder.WriteByte('\'')
				builder.WriteString(c)
				builder.WriteByte('\'')
			}
			builder.WriteByte(')')
		}
	} else {
		// 直接表达式
		builder.WriteString(jn.ON)
	}
	return builder.String()
}

var _ IBuilder = (*MultiJoin)(nil)

type MultiJoin struct {
	Joins []Join
}

func (j *MultiJoin) Add(join Join) {
	j.Joins = append(j.Joins, join)
}

func (j *MultiJoin) Is() bool {
	return len(j.Joins) > 0
}

func (j *MultiJoin) Query() string {
	return ""
}

func (j *MultiJoin) Value() []interface{} {
	return nil
}

func (j *MultiJoin) Build(opts ...commonoption.Option) string {
	var b = strings.Builder{}
	for i := range j.Joins {
		b.WriteString(" ")
		b.WriteString(j.Joins[i].Build(opts...))
	}
	b.WriteString(" ")
	return b.String()
}
