package wrapper

import (
	"strings"
	"time"
)

// Time 时间范围
type Time struct {
	Column   string `form:"col" json:"col"`
	Operator string `form:"operator" json:"operator"`
	Min      int64  `form:"begin" json:"begin"`
	Max      int64  `form:"end" json:"end"`
}

func (t *Time) Adjust() {
	if t.Min > t.Max {
		t.Min, t.Max = t.Max, t.Min
	}
}

func (t *Time) Legal(legal int64) {
	t.Adjust()
	if t.Max-t.Min <= legal {
		return
	}
	t.Min = t.Max - legal
}

func (t *Time) Month(month time.Duration, unit int) int64 {
	return t.At(time.Hour*24*30*month, unit)
}

func (t *Time) Day(day time.Duration, unit int) int64 {
	return t.At(time.Hour*24*day, unit)
}

func (t *Time) At(at time.Duration, unit int) int64 {
	end := at.Milliseconds()
	if unit == 1 {
		end = int64(at.Seconds())
	} else if unit == 2 {
		end = int64(at.Minutes())
	} else if unit == 3 {
		end = int64(at.Hours())
	}
	return end
}

func (t *Time) HasValid() bool {
	// 时间有效，操作符长度不能太长(wrapper.Gt,wrapper.Between...），太长就是无效，在进行ToUpper时，造成时间消耗
	return len(t.Operator) > 0 && len(t.Operator) < 8 && len(t.Column) > 0
}

func (t *Time) Cond(cond *Condition) {
	if !t.HasValid() {
		return
	}
	operator := strings.ToUpper(t.Operator)
	if operator == ">" {
		cond.Gt(t.Column, t.Min)
	} else if operator == ">=" {
		cond.Gte(t.Column, t.Min)
	} else if operator == "<" {
		cond.Lt(t.Column, t.Max)
	} else if operator == "<=" {
		cond.Lte(t.Column, t.Max)
	} else if operator == "BETWEEN" {
		cond.Between(t.Column, t.Min, t.Max)
	} else if operator == "EQ" {
		cond.Eq(t.Column, t.Min)
	}
}

func (t *Time) And(cond *Condition) {
	if !t.HasValid() {
		return
	}
	op := strings.ToUpper(t.Operator)
	if op == ">" {
		cond.AndGt(t.Column, t.Min)
	} else if op == ">=" {
		cond.AndGte(t.Column, t.Min)
	} else if op == "<" {
		cond.AndLt(t.Column, t.Max)
	} else if op == "<=" {
		cond.AndLte(t.Column, t.Max)
	} else if op == "BETWEEN" {
		cond.AndBetween(t.Column, t.Min, t.Max)
	} else if op == "=" {
		cond.AndEq(t.Column, t.Min)
	}
}

func (t *Time) Or(cond *Condition) {
	if !t.HasValid() {
		return
	}
	op := strings.ToUpper(t.Operator)
	if op == ">" {
		cond.OrGt(t.Column, t.Min)
	} else if op == ">=" {
		cond.OrGte(t.Column, t.Min)
	} else if op == "<" {
		cond.OrLt(t.Column, t.Max)
	} else if op == "<=" {
		cond.OrLte(t.Column, t.Max)
	} else if op == "BETWEEN" {
		cond.OrBetween(t.Column, t.Min, t.Max)
	} else if op == "=" {
		cond.OrEq(t.Column, t.Min)
	}
}

type TimeV2 struct {
	TimCol string `form:"time_col" json:"time_col"`
	TimOp  string `form:"time_op" json:"time_op"`
	Start  int64  `form:"start" json:"start"`
	End    int64  `form:"end" json:"end"`
}

func (t *TimeV2) HasValid() bool {
	// 时间有效，操作符长度不能太长,太长就是无效，在进行ToUpper时，造成时间消耗
	return len(t.TimOp) > 0 && len(t.TimOp) < 5 && len(t.TimCol) > 0
}

func (t *TimeV2) BuildCond(cond *Condition) {
	if !t.HasValid() {
		return
	}
	op := strings.ToUpper(t.TimOp)
	if op == "GT" {
		cond.Gt(t.TimCol, t.Start)
	} else if op == "GTE" {
		cond.Gte(t.TimCol, t.Start)
	} else if op == "LT" {
		cond.Lt(t.TimCol, t.End)
	} else if op == "LTE" {
		cond.Lte(t.TimCol, t.End)
	} else if op == "BET" {
		cond.Between(t.TimCol, t.Start, t.End)
	} else if op == "EQ" {
		cond.Eq(t.TimCol, t.Start)
	}
}

func (t *TimeV2) And(cond *Condition) {
	if !t.HasValid() {
		return
	}
	op := strings.ToUpper(t.TimOp)
	if op == "GT" {
		cond.AndGt(t.TimCol, t.Start)
	} else if op == "GTE" {
		cond.AndGte(t.TimCol, t.Start)
	} else if op == "LT" {
		cond.AndLt(t.TimCol, t.End)
	} else if op == "LTE" {
		cond.AndLte(t.TimCol, t.End)
	} else if op == "BET" {
		cond.AndBetween(t.TimCol, t.Start, t.End)
	} else if op == "EQ" {
		cond.AndEq(t.TimCol, t.Start)
	}
}

func (t *TimeV2) Or(cond *Condition) {
	if !t.HasValid() {
		return
	}
	op := strings.ToUpper(t.TimOp)
	if op == "GT" {
		cond.OrGt(t.TimCol, t.Start)
	} else if op == "GTE" {
		cond.OrGte(t.TimCol, t.Start)
	} else if op == "LT" {
		cond.OrLt(t.TimCol, t.End)
	} else if op == "LTE" {
		cond.OrLte(t.TimCol, t.End)
	} else if op == "BET" {
		cond.OrBetween(t.TimCol, t.Start, t.End)
	} else if op == "EQ" {
		cond.OrEq(t.TimCol, t.Start)
	}
}

func (t *TimeV2) Adjust() {
	if t.Start > t.End {
		t.Start, t.End = t.End, t.Start
	}
}

func (t *TimeV2) Legal(legal int64) {
	t.Adjust()
	if t.End-t.Start <= legal {
		return
	}
	t.Start = t.End - legal
}

func (t *TimeV2) Month(month time.Duration, unit int) int64 {
	return t.At(time.Hour*24*30*month, unit)
}

func (t *TimeV2) Day(day time.Duration, unit int) int64 {
	return t.At(time.Hour*24*day, unit)
}

func (t *TimeV2) At(at time.Duration, unit int) int64 {
	end := at.Milliseconds()
	if unit == 1 {
		end = int64(at.Seconds())
	} else if unit == 2 {
		end = int64(at.Minutes())
	} else if unit == 3 {
		end = int64(at.Hours())
	}
	return end
}
