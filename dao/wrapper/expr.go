package wrapper

import (
	commonoption "gitee.com/h79/goutils/common/option"
	"reflect"
	"strconv"
	"strings"
)

func AddTable(builder *strings.Builder, table string) {
	if table == "" {
		return
	}
	AddQuoted(builder, table)
	builder.WriteByte('.')
}

func IsQuoted(v string) bool {
	if v == "" {
		return false
	}
	first := v[0]
	end := v[len(v)-1]
	return first == '`' && end == '`'
}

func AddQuoted(builder *strings.Builder, v string) bool {
	if v == "" {
		return false
	}
	first := v[0]
	end := v[len(v)-1]
	if first != '`' { //引号
		builder.WriteByte('`')
	}
	builder.WriteString(v)
	if end != '`' {
		builder.WriteByte('`')
	}
	return true
}

func AddAlias(builder *strings.Builder, v string) bool {
	if v == "" {
		return false
	}
	builder.WriteString(" AS ")
	builder.WriteString(v)
	return true
}

func AddVar(builder *strings.Builder, val interface{}, opts ...commonoption.Option) {
	r := reflect.ValueOf(val)
	ref := reflect.Indirect(r)
	switch ref.Kind() {

	case reflect.String:
		s := ref.String()
		s = strings.ReplaceAll(s, "\"", "\\\"")
		s = strings.ReplaceAll(s, "'", "\\'")
		builder.WriteByte('\'')
		builder.WriteString(s)
		builder.WriteByte('\'')

	case reflect.Int:
		fallthrough
	case reflect.Int8:
		fallthrough
	case reflect.Int16:
		fallthrough
	case reflect.Int32:
		fallthrough
	case reflect.Int64:
		builder.WriteString(strconv.FormatInt(ref.Int(), 10))

	case reflect.Uint:
		fallthrough
	case reflect.Uint8:
		fallthrough
	case reflect.Uint16:
		fallthrough
	case reflect.Uint32:
		fallthrough
	case reflect.Uint64:
		builder.WriteString(strconv.FormatUint(ref.Uint(), 10))

	case reflect.Float32:
		fallthrough
	case reflect.Float64:
		builder.WriteString(strconv.FormatFloat(ref.Float(), 'f', 5, 64))

	case reflect.Bool:
		if ref.Bool() {
			builder.WriteString("true")
		} else {
			builder.WriteString("false")
		}

	case reflect.Struct:
		if r.CanInterface() && r.Type().Implements(BuilderType) {
			bui := r.Interface().(IBuilder)
			builder.WriteString(bui.Build(opts...))
		}
	default:
	}
}
