package wrapper

import (
	commonoption "gitee.com/h79/goutils/common/option"
	"reflect"
)

var BuilderType = reflect.TypeOf((*IBuilder)(nil)).Elem()

type IBuilder interface {
	Is() bool
	Build(opts ...commonoption.Option) string
}

var FromType = reflect.TypeOf((*IFrom)(nil)).Elem()

type IFrom interface {
	SetAs(as string)
	IBuilder
}

var LimitType = reflect.TypeOf((*ILimit)(nil)).Elem()

type ILimit interface {
	GetLimit() int
	GetOffset() int
	IBuilder
}

var QueryType = reflect.TypeOf((*IQuery)(nil)).Elem()

type IQuery interface {
	Query() string
	Value() []interface{}
	IBuilder
}

var SelectType = reflect.TypeOf((*ISelect)(nil)).Elem()

type ISelect interface {
	Query() []string
	Value() []interface{}
	IBuilder
}

var CondType = reflect.TypeOf((*ICond)(nil)).Elem()

type ICond interface {
	HasValid() error
	Decode(cond string)
	BuildCond(cond *Condition)
	Build(key string, cb func(obj interface{})) error
}

var ModelType = reflect.TypeOf((*IModel)(nil)).Elem()

type IModel interface {
	Clone() IModel
	Model() interface{}
	Set(key string, v interface{})
	Get(key string) interface{}
	ForEach(cb func(key string, v interface{}))
}

type NewCondFunc func() ICond
type NewModelFunc func() IModel
