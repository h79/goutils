package wrapper

import (
	commonoption "gitee.com/h79/goutils/common/option"
	"gitee.com/h79/goutils/dao/option"
	"strings"
)

var _ ISelect = (*Select)(nil)

type Select struct {
	query []string
}

func (sel *Select) Clone() *Select {
	return &Select{query: sel.query}
}

func (sel *Select) Delete(column string) *Select {
	for i := range sel.query {
		if sel.query[i] == column {
			sel.query = append(sel.query[:i], sel.query[i+1:]...)
			break
		}
	}
	return sel
}

func (sel *Select) Add(column string) *Select {
	sel.query = append(sel.query, column)
	return sel
}

func (sel *Select) Query() []string {
	return sel.query
}

func (sel *Select) Is() bool {
	if sel == nil {
		return false
	}
	return len(sel.query) > 0
}

func (sel *Select) Value() []interface{} {
	return nil
}

func (sel *Select) Build(opts ...commonoption.Option) string {
	return sel.build(option.FullSqlExist(opts...))
}

func (sel *Select) build(full bool) string {
	count := 0
	builder := strings.Builder{}
	for i := range sel.query {
		if len(sel.query[i]) > 0 {
			if count == 0 {
				if full {
					builder.WriteString("SELECT ")
				}
			} else if count > 0 {
				builder.WriteByte(',')
			}
			builder.WriteString(sel.query[i])
			count++
		}
	}
	return builder.String()
}
