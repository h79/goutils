package wrapper

import (
	commonoption "gitee.com/h79/goutils/common/option"
	"gitee.com/h79/goutils/dao/option"
	"reflect"
	"strings"
)

var _ IQuery = (*Condition)(nil)

// Operate
const (
	And       = "AND "
	Or        = "OR "
	Eq        = "= ?"
	Neq       = "<> ?"
	Lt        = "< ?"
	Lte       = "<= ?"
	Gt        = "> ?"
	Gte       = ">= ?"
	NotIn     = "NOT IN ?"
	In        = "IN ?"
	Between   = "BETWEEN ? AND ?"
	Like      = "LIKE ?"
	Exists    = "EXISTS ?"
	NotExists = "NOT EXISTS ?"
)

func Compose[T ComposeType](op []string, cond *Condition, column string, size int, values []T) {
	if len(values) <= 0 {
		return
	}
	if len(values) == 1 {
		cond.And(column, op[0], values[0])
	} else if len(values) >= size {
		cond.Compose(column, op[1], values)
	} else {
		for i := range values {
			cond.Or(column, op[0], values[i])
		}
	}
}

func OrCompose[T ComposeType](op []string, cond *Condition, column string, size int, values []T) {
	if len(values) <= 0 {
		return
	}
	if len(values) == 1 {
		cond.Or(column, op[0], values[0])
	} else if len(values) >= size {
		cond.Or(column, op[1], values)
	} else {
		for i := range values {
			cond.Or(column, op[0], values[i])
		}
	}
}

func AndCompose[T ComposeType](op []string, cond *Condition, column string, size int, values []T) {
	if len(values) <= 0 {
		return
	}
	if len(values) == 1 {
		cond.And(column, op[0], values[0])
	} else {
		cs := Condition{}
		if len(values) >= size {
			cs.Compose(column, op[1], values)
		} else {
			for i := range values {
				cs.Or(column, op[0], values[i])
			}
		}
		cond.From(&cs, true, And)
	}
}

var InOp = []string{Eq, In}

func IN[T ComposeType](cond *Condition, column string, size int, values []T) {
	Compose(InOp, cond, column, size, values)
}

var NotInOp = []string{Neq, NotIn}

func NIN[T ComposeType](cond *Condition, column string, size int, values []T) {
	Compose(NotInOp, cond, column, size, values)
}

func OR[T ComposeType](cond *Condition, column string, size int, values []T) {
	OrCompose(InOp, cond, column, size, values)
}

func AND[T ComposeType](cond *Condition, column string, size int, values []T) {
	AndCompose(InOp, cond, column, size, values)
}

type ComposeType interface {
	~string | bool | ~int8 | ~int16 | ~int | ~int32 | ~int64 | ~uint8 | ~uint16 | ~uint | ~uint32 | ~uint64 | ~float32 | ~float64
}

type Condition struct {
	sql   strings.Builder
	vars  []any
	hasOp bool
}

// And
/*  @param column
 *  @param operator "=、<小于、>大于、>=大于等于、<=小于等于、<>以及不等于"
 */
func (sq *Condition) And(column, operator string, value any) *Condition {
	if sq.hasOp {
		sq.Operate(And)
	}
	return sq.Compose(column, operator, value)
}

func (sq *Condition) AndEq(column string, value any) *Condition {
	return sq.And(column, Eq, value)
}

func (sq *Condition) AndNEq(column string, value any) *Condition {
	return sq.And(column, Neq, value)
}

func (sq *Condition) AndLt(column string, value any) *Condition {
	return sq.And(column, Lt, value)
}

func (sq *Condition) AndLte(column string, value any) *Condition {
	return sq.And(column, Lte, value)
}

func (sq *Condition) AndGt(column string, value any) *Condition {
	return sq.And(column, Gt, value)
}

func (sq *Condition) AndGte(column string, value any) *Condition {
	return sq.And(column, Gte, value)
}

func (sq *Condition) Or(column, operator string, value any) *Condition {
	if sq.hasOp {
		sq.Operate(Or)
	}
	return sq.Compose(column, operator, value)
}

func (sq *Condition) OrEq(column string, value any) *Condition {
	return sq.Or(column, Eq, value)
}

func (sq *Condition) OrNEq(column string, value any) *Condition {
	return sq.Or(column, Neq, value)
}

func (sq *Condition) OrLt(column string, value any) *Condition {
	return sq.Or(column, Lt, value)
}

func (sq *Condition) OrLte(column string, value any) *Condition {
	return sq.Or(column, Lte, value)
}

func (sq *Condition) OrGt(column string, value any) *Condition {
	return sq.Or(column, Gt, value)
}

func (sq *Condition) OrGte(column string, value any) *Condition {
	return sq.Or(column, Gte, value)
}

func (sq *Condition) In(column string, value any) *Condition {
	return sq.Compose(column, In, value)
}

func (sq *Condition) NotIn(column string, value any) *Condition {
	return sq.Compose(column, NotIn, value)
}

// Like
/* @example db.Where("name LIKE ?", "%jin%").Find(&users)
 * Condition.Like("name","jin")
 */
func (sq *Condition) Like(column string, value any) *Condition {
	return sq.Compose(column, Like, value)
}

// PLike %?
func PLike(value string) string {
	return "%" + value
}
func (sq *Condition) PLike(column string, value string) *Condition {
	return sq.Compose(column, Like, "%"+value)
}

// SLike ?%
func SLike(value string) string {
	return value + "%"
}
func (sq *Condition) SLike(column string, value string) *Condition {
	return sq.Compose(column, Like, value+"%")
}

// MLike %?%
func MLike(value string) string {
	return "%" + value + "%"
}
func (sq *Condition) MLike(column string, value string) *Condition {
	return sq.Compose(column, Like, "%"+value+"%")
}

func (sq *Condition) Between(column string, value1, value2 any) *Condition {
	return sq.AndBetween(column, value1, value2)
}

func (sq *Condition) AndBetween(column string, value1, value2 any) *Condition {
	if sq.hasOp {
		sq.Operate(And)
	}
	return sq.Compose(column, Between, value1, value2)
}

func (sq *Condition) OrBetween(column string, value1, value2 any) *Condition {
	if sq.hasOp {
		sq.Operate(Or)
	}
	return sq.Compose(column, Between, value1, value2)
}

func (sq *Condition) Eq(column string, value any) *Condition {
	return sq.Compose(column, Eq, value)
}

func (sq *Condition) NEq(column string, value any) *Condition {
	return sq.Compose(column, Neq, value)
}
func (sq *Condition) Neq(column string, value any) *Condition {
	return sq.Compose(column, Neq, value)
}

func (sq *Condition) Lt(column string, value any) *Condition {
	return sq.Compose(column, Lt, value)
}

func (sq *Condition) LtEq(column string, value any) *Condition {
	return sq.Compose(column, Lte, value)
}
func (sq *Condition) Lte(column string, value any) *Condition {
	return sq.Compose(column, Lte, value)
}

func (sq *Condition) Gt(column string, value any) *Condition {
	return sq.Compose(column, Gt, value)
}

func (sq *Condition) GtEq(column string, value any) *Condition {
	return sq.Compose(column, Gte, value)
}
func (sq *Condition) Gte(column string, value any) *Condition {
	return sq.Compose(column, Gte, value)
}

// Column
/*
 * example db.Where("created_at BETWEEN ? AND ?", lastWeek, today).Find(&users)
 */
func (sq *Condition) Column(column string) *Condition {
	SqlColumn(&sq.sql, column)
	return sq
}

// Operate
/* @param operator
 * @example created_at BETWEEN ? AND ?
 * Condition.Column("created_at").Operate("BETWEEN").Operate("AND").Values(11333,34444)
 */
func (sq *Condition) Operate(operator string) *Condition {
	if !sq.hasOp {
		sq.hasOp = true
	}
	sq.sql.WriteByte(' ')
	sq.sql.WriteString(operator)
	return sq
}

func (sq *Condition) Values(value ...interface{}) *Condition {
	sq.vars = append(sq.vars, value...)
	return sq
}

func (sq *Condition) Compose(column string, operator string, value ...any) *Condition {
	sq.Column(column)
	sq.Operate(operator)
	sq.vars = append(sq.vars, value...)
	return sq
}

// Modifier 修饰符，比如 ()
func (sq *Condition) Modifier(m string) *Condition {
	sq.sql.WriteString(m)
	return sq
}

// ComposeFrom 组合
// Deprecated: please use Condition.From
func (sq *Condition) ComposeFrom(src *Condition, operator string) *Condition {
	return sq.From(src, true, operator)
}

// From 组合 组合
// brackets 是否需要用 () 把语句
func (sq *Condition) From(src *Condition, brackets bool, operator string) *Condition {
	if !sq.hasOp {
		sq.hasOp = src.hasOp
	} else {
		sq.Operate(operator)
	}
	if brackets {
		sq.sql.WriteByte('(')
		sq.sql.WriteString(strings.Trim(src.Query(), " "))
		sq.sql.WriteByte(')')
	} else {
		sq.sql.WriteString(src.Query())
	}
	sq.Values(src.Value()...)
	return sq
}

func (sq *Condition) IsFirst() bool {
	return sq.hasOp
}

func (sq *Condition) Is() bool {
	if sq == nil {
		return false
	}
	return sq.sql.Len() > 0 && len(sq.vars) > 0
}

func (sq *Condition) Cond() string {
	return sq.sql.String()
}

func (sq *Condition) Query() string {
	return sq.sql.String()
}

func (sq *Condition) Value() []interface{} {
	return sq.vars
}

func (sq *Condition) Reset() *Condition {
	sq.hasOp = false
	sq.sql.Reset()
	sq.vars = nil
	return sq
}

func (sq *Condition) Build(opts ...commonoption.Option) string {
	idx := 0
	full := option.FullSqlExist(opts...)
	sql := sq.Query()
	builder := strings.Builder{}
	for i, v := range []byte(sql) {
		if i == 0 && full {
			builder.WriteString(" WHERE ")
		}
		if v == '?' && len(sq.vars) > idx {
			switch rv := reflect.ValueOf(sq.vars[idx]); rv.Kind() {
			case reflect.Slice, reflect.Array:
				if rv.Len() == 0 {
					builder.WriteString("(NULL)")
					AddVar(&builder, nil, opts...)
				} else {
					builder.WriteByte('(')
					for j := 0; j < rv.Len(); j++ {
						if j > 0 {
							builder.WriteByte(',')
						}
						AddVar(&builder, rv.Index(j).Interface(), opts...)
					}
					builder.WriteByte(')')
				}
			default:
				AddVar(&builder, sq.vars[idx], opts...)
			}
			idx++
		} else {
			builder.WriteByte(v)
		}
	}
	return builder.String()
}
