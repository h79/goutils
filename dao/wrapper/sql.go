package wrapper

import (
	commonoption "gitee.com/h79/goutils/common/option"
	"gitee.com/h79/goutils/dao/option"
	"strings"
)

var _ IFrom = (*SQL)(nil)

type SQL struct {
	From     IFrom
	Selector ISelect
	Where    IQuery
	Join     IBuilder
	GroupBy  IBuilder
	OrderBy  IBuilder
	Limit    ILimit
	Opts     []commonoption.Option
}

func (s *SQL) SetAs(as string) {
	if s.From == nil {
		panic("from is empty")
	}
	s.From.SetAs(as)
}

func (s *SQL) Is() bool {
	return s.From != nil
}

// NewSQL From 表名
func NewSQL(from IFrom, opts ...commonoption.Option) *SQL {
	return &SQL{
		From: from,
		Opts: opts,
	}
}

func (s *SQL) WithFrom(from IFrom) *SQL {
	s.From = from
	return s
}

func (s *SQL) WithSelector(sel ISelect) *SQL {
	s.Selector = sel
	return s
}

func (s *SQL) WithJoin(join IBuilder) *SQL {
	s.Join = join
	return s
}

func (s *SQL) WithWhere(where IQuery) *SQL {
	s.Where = where
	return s
}

func (s *SQL) WithGroupBy(by IBuilder) *SQL {
	s.GroupBy = by
	return s
}

func (s *SQL) WithLimit(limit ILimit) *SQL {
	s.Limit = limit
	return s
}

func (s *SQL) WithOrderBy(by IBuilder) *SQL {
	s.OrderBy = by
	return s
}

// AsSubQuery 作为子查询使用
func (s *SQL) AsSubQuery(as string) *SQL {
	s.Opts = append(s.Opts, option.WithSubQuerySQL(as))
	return s
}

// AsChild 作为子源来使用
func (s *SQL) AsChild(as string, opts ...commonoption.Option) From {
	return From{From: s.Build(opts...), As: as}
}

func (s *SQL) Build(opts ...commonoption.Option) string {
	opts = append(opts, option.WithFullSQL())
	return s.BuildOpt(opts...)
}

func (s *SQL) BuildOpt(opts ...commonoption.Option) string {
	opts = append(opts, s.Opts...)
	as, sq := option.SubQueryExist(opts...)
	builder := strings.Builder{}

	if sq {
		builder.WriteByte('(')
	}
	if s.Selector != nil {
		builder.WriteString(s.Selector.Build(opts...))
	}
	if s.From != nil {
		builder.WriteString(s.From.Build(opts...))
	}
	if s.Join != nil {
		builder.WriteString(s.Join.Build(opts...))
	}
	if s.Where != nil {
		builder.WriteString(s.Where.Build(opts...))
	}
	if s.GroupBy != nil {
		builder.WriteString(s.GroupBy.Build(opts...))
	}
	// order by ...limit...
	if s.OrderBy != nil {
		builder.WriteString(s.OrderBy.Build(opts...))
	}
	if s.Limit != nil {
		builder.WriteString(s.Limit.Build(opts...))
	}
	if sq {
		builder.WriteByte(')')
		if as != "" {
			builder.WriteString(" AS ")
			builder.WriteString(as)
		}
	}
	return builder.String()
}

func SQLFunc(flag int) string {
	if flag <= KSQLFunc {
		return ""
	}
	if fn, ok := sqlFuncMap[flag]; ok {
		return fn
	}
	return ""
}

const (
	KSQLFunc = iota
	KSQLCount
	KSQLMax
	KSQLMin
	KSQLWeek
	KSQLYear
	KSQLMonth
	KSQLSum
	KSQLAbs
	KSQLFloor
	KSQLGreatest
	KSQLLeast
	KSQLUpper
	KSQLLower
	KSQLTrim
	KSQLRTrim
	KSQLLTrim
	KSQLCharLength
	KSQLConcat
	KSQLReverse
)

var (
	sqlFuncMap = map[int]string{
		KSQLCount:      "COUNT",
		KSQLMax:        "MAX",
		KSQLMin:        "MIN",
		KSQLWeek:       "WEEK",
		KSQLYear:       "YEAR",
		KSQLMonth:      "MONTH",
		KSQLSum:        "SUM",
		KSQLAbs:        "ABS",
		KSQLFloor:      "FLOOR", //返回小于或等于 x 的最大整数
		KSQLGreatest:   "GREATEST",
		KSQLLeast:      "LEAST",
		KSQLUpper:      "UPPER", //字符串大写
		KSQLLower:      "LOWER",
		KSQLTrim:       "TRIM",
		KSQLRTrim:      "RTRIM",
		KSQLLTrim:      "LTRIM",
		KSQLCharLength: "CHAR_LENGTH",
		KSQLConcat:     "CONCAT",
		KSQLReverse:    "REVERSE", //将字符串s的顺序反过来
	}
)

func AddSQLFunc(flag int, fn string) {
	if _, ok := sqlFuncMap[flag]; ok {
		return
	}
	sqlFuncMap[flag] = fn
}
