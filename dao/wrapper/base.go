package wrapper

import (
	"context"
	"errors"
	"gitee.com/h79/goutils/common"
	"gitee.com/h79/goutils/dao/option"
	"gorm.io/gorm"
	"reflect"
)

type Base struct {
	DB      *gorm.DB
	ctx     context.Context
	cancel  context.CancelFunc
	order   interface{}
	Results interface{}
}

func (b *Base) NewDB() *gorm.DB {
	return b.DB.Session(&gorm.Session{NewDB: true, Context: b.ctx})
}

func (b *Base) WithResult(result interface{}) *Base {
	if !IsValid(result) {
		panic("the result is need ptr or slice")
	}
	b.Results = result
	return b
}

func (b *Base) WithOrder(order interface{}) *Base {
	b.order = order
	return b
}

func (b *Base) Cancel() {
	b.cancel()
}

// Get 获取
func (b *Base) Get(model interface{}) (result interface{}, err error) {
	err = b.DB.Model(model).First(&result).Error
	return
}

// Gets 获取批量结果
func (b *Base) Gets(model interface{}) (results []*interface{}, err error) {
	err = b.DB.Model(model).Find(&results).Error
	return
}

func (b *Base) Count(model interface{}, count *int64) error {
	return b.DB.Model(model).Count(count).Error
}

func (b *Base) Create(model interface{}, opts ...option.QueryFunc) error {
	// 根据 `map` 更新属性
	// db.Model(&user).Updates(map[string]interface{}{"name": "hello", "age": 18, "active": false})
	q := option.Query{
		Q: make(map[string]interface{}),
	}
	for _, o := range opts {
		o.Apply(&q)
	}
	db := b.DB.Model(model)
	return db.Create(q.Q).Error
}

func (b *Base) Delete(model interface{}, cond IQuery) error {
	db := b.DB
	if cond.Is() {
		db = db.Where(cond.Query(), cond.Value()...)
	}
	return db.Delete(model).Error
}

func (b *Base) UpdateColumn(model interface{}, where IQuery, column string, value interface{}) error {
	return b.Updates(model, where, option.QueryFunc(func(q *option.Query) {
		q.Q[column] = value
	}))
}

func (b *Base) Updates(model interface{}, where IQuery, opts ...option.QueryFunc) error {
	// 根据 `map` 更新属性
	// db.Model(&user).Updates(map[string]interface{}{"name": "hello", "age": 18, "active": false})
	q := option.Query{
		Q: make(map[string]interface{}),
	}
	for _, o := range opts {
		o.Apply(&q)
	}
	db := b.DB.Model(model)
	if where != nil && where.Is() {
		db = db.Where(where.Query(), where.Value()...)
	}
	return db.Updates(q.Q).Error
}

func (b *Base) First(model interface{}, where IQuery, sel ISelect) error {
	//db.Model(User{ID: 10}).First(&result)
	// SELECT * FROM users WHERE id = 10;
	db := b.DB.Model(model)
	if where != nil && where.Is() {
		db = db.Where(where.Query(), where.Value()...)
	}
	if sel != nil && sel.Is() {
		db = db.Select(sel.Query(), sel.Value()...)
	}
	if !common.IsNil(b.order) {
		db = db.Order(b.order)
	}
	return db.First(b.Results).Error
}

func (b *Base) Find(model interface{}, where IQuery, sel ISelect) error {

	db := b.DB.Model(model)

	if where != nil && where.Is() {
		db = db.Where(where.Query(), where.Value()...)
	}
	if sel != nil && sel.Is() {
		db = db.Select(sel.Query(), sel.Value()...)
	}
	if !common.IsNil(b.order) {
		db = db.Order(b.order)
	}
	return db.Find(b.Results).Error
}

func IsNotFound(db *gorm.DB) int {
	if errors.Is(db.Error, gorm.ErrRecordNotFound) {
		return 1
	}
	if db.Error != nil {
		return -1
	}
	return 0
}

func IsValid(r interface{}) bool {
	if r == nil {
		return false
	}
	val := reflect.Indirect(reflect.ValueOf(r))
	k := val.Kind()
	switch k {
	case reflect.Struct:
		fallthrough
	case reflect.Slice:
		return true
	}
	return false
}
