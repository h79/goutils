package dao

import (
	"context"
	"errors"
	"fmt"
	commonoption "gitee.com/h79/goutils/common/option"
	"gitee.com/h79/goutils/common/result"
	"gitee.com/h79/goutils/common/stringutil"
	"gitee.com/h79/goutils/dao/db"
	"gitee.com/h79/goutils/dao/option"
	"gitee.com/h79/goutils/dao/redis"
	"gitee.com/h79/goutils/dao/util"
	"gitee.com/h79/goutils/dao/wrapper"
	"gitee.com/h79/goutils/perf"
	goredis "github.com/go-redis/redis/v8"
	"github.com/olivere/elastic/v7"
	"gorm.io/gorm"
	"time"
)

// Dao https://gorm.io/docs/query.html
type Dao struct {
	Sql     db.SqlDatabase
	Redis   redis.Client
	Elastic db.EsDatabase
}

var errZero = fmt.Errorf("length is zero")

func (d *Dao) DBClient(opts ...commonoption.Option) (*gorm.DB, error) {
	r, _, err := d.DBClientV2(opts...)
	return r, err
}

func (d *Dao) DBClientV2(opts ...commonoption.Option) (*gorm.DB, string, error) {
	if d.Sql == nil {
		return nil, "", result.RErrNotSupport
	}
	var name, _ = option.NameKeyExist(opts...)
	sql, err := d.Sql.Get(name)
	if err != nil {
		return nil, name, result.RErrNil
	}
	return sql.Db(), sql.Name(), nil
}

func (d *Dao) RedisClient(opts ...commonoption.Option) (*goredis.Client, error) {
	r, _, err := d.RedisClientV2(opts...)
	return r, err
}

func (d *Dao) RedisClientV2(opts ...commonoption.Option) (*goredis.Client, string, error) {
	if d.Redis == nil {
		return nil, "", result.RErrNotSupport
	}
	var name, _ = option.NameKeyExist(opts...)
	rds, err := d.Redis.Get(name)
	if err != nil {
		return nil, name, err
	}
	return rds.Rds(), rds.Name(), nil
}

func (d *Dao) ClusterRedisClient(opts ...commonoption.Option) (*goredis.ClusterClient, error) {
	r, _, err := d.ClusterRedisClientV2(opts...)
	return r, err
}

func (d *Dao) ClusterRedisClientV2(opts ...commonoption.Option) (*goredis.ClusterClient, string, error) {
	if d.Redis == nil {
		return nil, "", result.RErrNotSupport
	}
	var name, _ = option.NameKeyExist(opts...)
	rds, err := d.Redis.Get(name)
	if err != nil {
		return nil, name, err
	}
	return rds.Cluster(), rds.Name(), nil
}

func (d *Dao) SentinelRedisClient(opts ...commonoption.Option) (*goredis.SentinelClient, error) {
	r, _, err := d.SentinelRedisClientV2(opts...)
	return r, err
}

func (d *Dao) SentinelRedisClientV2(opts ...commonoption.Option) (*goredis.SentinelClient, string, error) {
	if d.Redis == nil {
		return nil, "", result.RErrNotSupport
	}
	var name, _ = option.NameKeyExist(opts...)
	rds, err := d.Redis.Get(name)
	if err != nil {
		return nil, name, err
	}
	return rds.Sentinel(), rds.Name(), nil
}

func (d *Dao) ElasticClient(opts ...commonoption.Option) (*elastic.Client, error) {
	r, _, err := d.ElasticClientV2(opts...)
	return r, err
}

func (d *Dao) ElasticClientV2(opts ...commonoption.Option) (*elastic.Client, string, error) {
	if d.Elastic == nil {
		return nil, "", result.RErrNotSupport
	}
	var name, _ = option.NameKeyExist(opts...)
	es, na, err := d.Elastic.Get(name)
	return es, na, err
}

func (d *Dao) UseRedis(opts ...commonoption.Option) CurrentRedis {
	client, name, _ := d.RedisClientV2(opts...)
	t, _ := option.TimeOutExist(opts...)
	w := option.AlarmExist(opts...)
	return CurrentRedis{Client: client, Name: name, timeout: t, warn: w}
}

func (d *Dao) UseDB(opts ...commonoption.Option) CurrentDb {
	client, name, _ := d.DBClientV2(opts...)
	return CurrentDb{DB: client, Name: name}
}

func (d *Dao) UseElastic(opts ...commonoption.Option) CurrentElastic {
	client, name, _ := d.ElasticClientV2(opts...)
	return CurrentElastic{Client: client, Name: name}
}

func (d *Dao) Condition(ctx context.Context, data interface{}, call db.ConditionCallback, opts ...commonoption.Option) result.Result {

	sql, err := d.DBClient(opts...)
	if err != nil {
		return result.Error(result.ErrDbInternal, fmt.Sprintf("the db non existed"))
	}

	tx := call(ctx, data, sql)

	if errors.Is(tx.Error, gorm.ErrRecordNotFound) {
		return result.Error(result.ErrNotFound, tx.Error.Error())
	}
	if tx.Error != nil {
		return result.Error(result.ErrDbInternal, tx.Error.Error())
	}
	return result.Succeed()
}

func (d *Dao) IsNotFound(db *gorm.DB) int {
	return wrapper.IsNotFound(db)
}

type CurrentElastic struct {
	*elastic.Client
	Name string
}

func (d *CurrentElastic) OK() bool {
	return d.Client != nil
}

type CurrentDb struct {
	*gorm.DB
	Name string
}

func (d *CurrentDb) OK() bool {
	return d.DB != nil
}

type CurrentRedis struct {
	*goredis.Client
	Name    string
	timeout int64
	warn    bool
}

func (d *CurrentRedis) Warning(r *perf.RunTime, err error) {
	dif := r.End()
	if dif == 0 || !d.warn {
		return
	}
	util.Alarm(result.ErrDbInternal, "Redis", fmt.Sprintf("run too time long=> %d,start=> %d", dif, r.Start()), err)
}

func (d *CurrentRedis) OK() bool {
	return d.Client != nil
}

func (d *CurrentRedis) HSet(ctx context.Context, key, field string, value interface{}) (int64, error) {
	if d.Client == nil {
		return 0, result.RErrNil
	}
	if len(field) <= 0 {
		return 0, errZero
	}
	var err error
	var rt = perf.Begin(d.timeout)
	defer d.Warning(rt, nil)
	ret, err := d.Client.HSet(ctx, key, field, value).Result()
	return ret, err
}

func (d *CurrentRedis) HSetNX(ctx context.Context, key, field string, value interface{}) (bool, error) {
	if d.Client == nil {
		return false, result.RErrNil
	}
	if len(field) <= 0 {
		return false, errZero
	}
	var err error
	var rt = perf.Begin(d.timeout)
	defer d.Warning(rt, err)
	ret, err := d.Client.HSetNX(ctx, key, field, value).Result()
	return ret, err
}

func (d *CurrentRedis) HGet(ctx context.Context, key, field string) (string, error) {
	if d.Client == nil {
		return "", result.RErrNil
	}
	var err error
	var rt = perf.Begin(d.timeout)
	defer d.Warning(rt, err)
	ret, err := d.Client.HGet(ctx, key, field).Result()
	return ret, err
}

func (d *CurrentRedis) HGetAll(ctx context.Context, key string) (map[string]string, error) {
	if d.Client == nil {
		return nil, result.RErrNil
	}
	var err error
	var rt = perf.Begin(d.timeout)
	defer d.Warning(rt, err)
	ret, err := d.Client.HGetAll(ctx, key).Result()
	return ret, err
}

func (d *CurrentRedis) HGetInt(ctx context.Context, key, field string) (int, error) {
	if d.Client == nil {
		return 0, result.RErrNil
	}
	var err error
	var rt = perf.Begin(d.timeout)
	defer d.Warning(rt, err)
	ret, err := d.Client.HGet(ctx, key, field).Result()
	if err != nil {
		return 0, err
	}
	return stringutil.StringToInt(ret), nil
}

func (d *CurrentRedis) HDel(ctx context.Context, key, field string) (int64, error) {
	if d.Client == nil {
		return 0, result.RErrNil
	}
	var err error
	var rt = perf.Begin(d.timeout)
	defer d.Warning(rt, err)
	ret, err := d.Client.HDel(ctx, key, field).Result()
	return ret, err
}

func (d *CurrentRedis) HInc(ctx context.Context, key, field string, value int64) (int64, error) {
	if d.Client == nil {
		return 0, result.RErrNil
	}
	if len(field) <= 0 {
		return 0, errZero
	}
	var err error
	var rt = perf.Begin(d.timeout)
	defer d.Warning(rt, err)
	ret, err := d.Client.HIncrBy(ctx, key, field, value).Result()
	return ret, err
}

func (d *CurrentRedis) Set(ctx context.Context, key string, value interface{}, expiration time.Duration) (string, error) {
	if d.Client == nil {
		return "", result.RErrNil
	}
	if len(key) <= 0 {
		return "", errZero
	}
	var err error
	var rt = perf.Begin(d.timeout)
	defer d.Warning(rt, err)
	ret, err := d.Client.Set(ctx, key, value, expiration).Result()
	return ret, err
}

func (d *CurrentRedis) SetNX(ctx context.Context, key string, value interface{}, expiration time.Duration) (bool, error) {
	if d.Client == nil {
		return false, result.RErrNil
	}
	if len(key) <= 0 {
		return false, errZero
	}
	var err error
	var rt = perf.Begin(d.timeout)
	defer d.Warning(rt, err)
	ret, err := d.Client.SetNX(ctx, key, value, expiration).Result()
	return ret, err
}

func (d *CurrentRedis) SetEX(ctx context.Context, key string, value interface{}, expiration time.Duration) (string, error) {
	if d.Client == nil {
		return "", result.RErrNil
	}
	if len(key) <= 0 {
		return "", errZero
	}
	var err error
	var rt = perf.Begin(d.timeout)
	defer d.Warning(rt, err)
	ret, err := d.Client.SetEX(ctx, key, value, expiration).Result()
	return ret, err
}

func (d *CurrentRedis) Get(ctx context.Context, key string) (string, error) {
	if d.Client == nil {
		return "", result.RErrNil
	}
	var err error
	var rt = perf.Begin(d.timeout)
	defer d.Warning(rt, err)
	ret, err := d.Client.Get(ctx, key).Result()
	return ret, err
}

func (d *CurrentRedis) Inc(ctx context.Context, key string, value int64) (int64, error) {
	if d.Client == nil {
		return 0, result.RErrNil
	}
	if len(key) <= 0 {
		return 0, errZero
	}
	var err error
	var rt = perf.Begin(d.timeout)
	defer d.Warning(rt, err)
	ret, err := d.Client.IncrBy(ctx, key, value).Result()
	return ret, err
}

func (d *CurrentRedis) GetInt(ctx context.Context, key string) (int, error) {
	if d.Client == nil {
		return 0, result.RErrNil
	}
	var err error
	var rt = perf.Begin(d.timeout)
	defer d.Warning(rt, err)
	ret, err := d.Client.Get(ctx, key).Result()
	if err != nil {
		return 0, err
	}
	return stringutil.StringToInt(ret), nil
}

// ExpireAt 在某个时间点过期
func (d *CurrentRedis) ExpireAt(ctx context.Context, key string, time time.Time) {
	if d.Client == nil {
		return
	}
	var err error
	var rt = perf.Begin(d.timeout)
	defer d.Warning(rt, err)
	_, err = d.Client.ExpireAt(ctx, key, time).Result()
}

// Expire 过多久失效
func (d *CurrentRedis) Expire(ctx context.Context, key string, time time.Duration) {
	if d.Client == nil {
		return
	}
	var err error
	var rt = perf.Begin(d.timeout)
	defer d.Warning(rt, err)
	_, err = d.Client.Expire(ctx, key, time).Result()
}
