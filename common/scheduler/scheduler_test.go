package scheduler

import (
	"fmt"
	"gitee.com/h79/goutils/common/option"
	"gitee.com/h79/goutils/common/system"
	"gitee.com/h79/goutils/common/timer"
	"testing"
	"time"
)

func TestScheduler(t *testing.T) {
	sched := NewScheduler(4)
	sched.Run()

	sched.AddTask(NewShellJob("ls -ltf").WithMustFlag(QuitExecMustFlag))
	for i := 0; i < 4; i++ {
		_ = sched.AddTriggerTask(NewShellJob(fmt.Sprintf("cp job.go xxx-%d.txt", i)).WithHandlerFunc(func(job *Job, opts ...option.Option) (interface{}, error) {
			t.Logf("jobId= '%s',quited= %v, at: %s", job.GetId(), IsQuited(opts...), timer.CurrentTime())
			return nil, nil
		}).WithMustFlag(QuitExecMustFlag), NewSimpleTrigger(time.Second*10))
	}
	idx := 0
	cron, _ := NewCronTrigger("*/5 * * * * ?")
	_ = sched.AddTriggerTask(NewJob("test", "1", 0).WithHandlerFunc(func(job *Job, opts ...option.Option) (interface{}, error) {
		idx++
		t.Logf("jobId= %s,quited= %v", job.GetId(), IsQuited(opts...))
		var s = system.Shell{}
		res := s.Run(fmt.Sprintf("cp job.go xxx-w-%d.txt", idx))
		if res.Err != nil {
			job.State = ErrorState
			return res, res.Err
		}
		job.State = CompletedState
		return res, nil
	}), cron)

	_ = sched.AddTriggerTask(NewJob("test", "2", 0).WithHandlerFunc(func(job *Job, opts ...option.Option) (interface{}, error) {
		idx++
		t.Logf("jobId= %s,quited= %v,at: %s", job.GetId(), IsQuited(opts...), timer.CurrentTime())
		return nil, nil
	}).WithMustFlag(QuitExecMustFlag), NewSimpleTrigger(time.Second*2))

	system.ChildRunning(func() {
		time.Sleep(time.Second * 10)

		sched.Foreach(func(group int, task Task, tk time.Duration, now, trigger int64) bool {
			t.Logf("jobId= '%s' in group=%d,time= %v,trigger= %v", task.GetId(), group, tk, time.Duration(trigger))
			return false
		})
		//system.Close()
	})

	system.MainRunning(func() {
		for {
			select {
			case <-system.Closed():
				return
			}
		}
	})
	system.Go().Wait()
}

func TestNewShellJob(t *testing.T) {
	job := NewShellJob("cp job.go xxx.txt")
	res, err := job.Execute()
	if err != nil {
		return
	}

	t.Log(res)

	job = NewShellJob("ls -ltf")
	res, err = job.Execute()
	if err != nil {
		return
	}
	t.Log(res)
}

func TestNewPool(t *testing.T) {
	pool := NewPool(1, 10)

	pool.AddJob(
		NewEmptyJob(func(job *Job, opts ...option.Option) (interface{}, error) {
			t.Log("run 0")
			return nil, nil
		}).WithDelay(time.Second * 3))

	pool.AddJob(
		NewEmptyJob(func(job *Job, opts ...option.Option) (interface{}, error) {
			t.Logf("run 1, quited= %v", IsQuited(opts...))
			return nil, nil
		}).WithDelay(time.Second * 15))

	pool.AddJob(
		NewEmptyJob(func(job *Job, opts ...option.Option) (interface{}, error) {
			t.Logf("run 2, quited= %v", IsQuited(opts...))
			return nil, nil
		}).WithDelay(time.Second * 0))

	system.ChildRunning(func() {
		time.Sleep(time.Second * 5)
		for i := 0; i < 5; i++ {
			pool.AddJob(
				NewJob("", fmt.Sprintf("%d", i+10), 0).
					WithHandlerFunc(func(job *Job, opts ...option.Option) (interface{}, error) {
						t.Logf("run %s, quited= %v", job.GetId(), IsQuited(opts...))
						return nil, nil
					}).
					WithDelay(time.Second * 0).WithMustFlag(QuitExecMustFlag))
		}
		system.Close()
	})
	system.MainRunning(func() {
		for {
			select {
			case <-system.Closed():
				return
			}
		}
	})
	system.Go().Wait()
}
