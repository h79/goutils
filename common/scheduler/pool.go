package scheduler

import (
	"fmt"
	"gitee.com/h79/goutils/common/logger"
	"gitee.com/h79/goutils/common/option"
	"gitee.com/h79/goutils/common/system"
	"sync/atomic"
	"time"
)

type IdleHandler interface {
	Idle(num int32)
}

// IdleFunc idle working number
type IdleFunc func(num int32)

func (fn IdleFunc) Idle(num int32) {
	fn(num)
}

type Pool struct {
	jobs        chan *Job
	jobWorkerCh chan *jobWorker
	stop        chan bool
	delay       *Delay
	tm          *time.Ticker
	workers     map[int]*jobWorker
	name        string
	numWorkers  int32 //总数
	runWorkers  int32 //正常运行
	idleWorking int32 //空闲worker数
	idleHandler IdleHandler
	running     system.RunningCheck
}

func NewPool(numWorkers, jobQueueLen int, opts ...option.Option) *Pool {
	if numWorkers <= 0 || jobQueueLen <= 0 {
		panic("length is zero")
	}
	num := int32(numWorkers)
	name := option.Select(NameOpt, "", opts...)
	pool := &Pool{
		jobs:        make(chan *Job, jobQueueLen),
		jobWorkerCh: make(chan *jobWorker, num),
		stop:        make(chan bool),
		workers:     make(map[int]*jobWorker, num),
		delay:       NewDelay(),
		tm:          time.NewTicker(time.Millisecond * 10),
		name:        name,
		numWorkers:  num,
		runWorkers:  num,
		idleWorking: num,
	}
	pool.createWorkers(numWorkers)
	pool.Run()
	return pool
}

func (p *Pool) SetIdleHandler(fn IdleHandler) *Pool {
	p.idleHandler = fn
	return p
}

func (p *Pool) WithIdleFunc(fn func(num int32)) *Pool {
	return p.SetIdleHandler(IdleFunc(fn))
}

func (p *Pool) AddJob(job *Job) {
	if job == nil {
		return
	}
	if system.IsQuit() {
		logger.W("Task", "application is quited,not can add for jobId= %s", job.GetId())
		return
	}
	ww := atomic.LoadInt32(&p.runWorkers)
	if ww <= p.numWorkers/2 {
		// 中间有原因，线程池中的线程，由于某些原因，退出了，需要再启动
		p.createWorkers(int(p.numWorkers - ww))
	}
	p.Run()
	p.jobs <- job
}

func (p *Pool) Run() {
	p.running.GoRunning(p.dispatch, func() {
		logger.N("Pool", "quit the pool,name= %s", p.name)
	})
}

func (p *Pool) HasWorking() bool {
	return atomic.LoadInt32(&p.idleWorking) != p.numWorkers
}

func (p *Pool) Release() {
	system.Stop(time.Second, p.stop)
	p.idleHandle(p.numWorkers)
}

func (p *Pool) createWorkers(num int) {
	logger.N("Pool", "create worker number= %d", num)
	for i := 0; i < num; i++ {
		worker := newWorker(p, i)
		worker.start()
		p.workers[i] = worker
	}
}

func (p *Pool) idleHandle(idleWork int32) {
	if p.idleHandler != nil {
		p.idleHandler.Idle(idleWork)
	}
}

func (p *Pool) workerRunning() {
	n := atomic.AddInt32(&p.runWorkers, 1)
	fmt.Printf("worker running in pool, num: %d,name= %s\n", n, p.name)
}

func (p *Pool) workerQuit() {
	n := atomic.AddInt32(&p.runWorkers, -1)
	fmt.Printf("worker quit in pool, num: %d,name= %s\n", n, p.name)
}

func (p *Pool) workerIdle() {
	p.idleHandle(atomic.AddInt32(&p.idleWorking, 1))
}

func (p *Pool) dispatch() {
	defer p.tm.Stop()
	defer p.idleHandle(p.numWorkers)
	for {
		select {
		case <-p.stop:
			fmt.Printf("worker stop in pool, name= %s...\n", p.name)
			p.stopJob(1)
			p.stop <- true
			fmt.Printf("worker stoped in pool, name= %s...\n", p.name)
			return

		case <-system.Closed():
			fmt.Printf("worker quit in pool, name= %s...\n", p.name)
			p.stopJob(2)
			fmt.Printf("worker quited in pool, name= %s...\n", p.name)
			return

		case job := <-p.jobs:
			if job.Delay > 0 {
				p.delayJob(job)
			} else {
				p.executeJob(job)
			}
		case <-p.tm.C:
			p.checkDelayJob()
		}
	}
}

func (p *Pool) stopJob(s int) {
	for _, w := range p.workers {
		w.stop <- s
		<-w.stop
	}
	atomic.StoreInt32(&p.idleWorking, p.numWorkers)

	// delay
	opts := With(s)
	p.delay.Exec(opts...)

	for i := 0; i < len(p.jobs); i++ {
		job, ok := <-p.jobs
		if ok {
			_, _ = job.Execute(opts...)
		}
	}
}

func (p *Pool) idleWork(w *jobWorker) {
	if system.IsQuit() {
		return
	}
	p.jobWorkerCh <- w
}

func (p *Pool) executeJob(job *Job) {
	if system.IsQuit() {
		_, _ = job.Execute(WithQuited())
		return
	}
	worker := <-p.jobWorkerCh
	p.idleHandle(atomic.AddInt32(&p.idleWorking, -1))
	worker.job <- job
}

func (p *Pool) delayJob(job *Job) {
	p.delay.Add(job)
}

func (p *Pool) checkDelayJob() {
	p.delay.Check(p)
}
