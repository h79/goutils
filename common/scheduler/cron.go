package scheduler

import (
	"fmt"
	"github.com/robfig/cron/v3"
	"time"
)

// CronTrigger implements
// Examples:
// http://www.quartz-scheduler.org/documentation/quartz-2.3.0/tutorials/tutorial-lesson-06.html
// {秒数} {分钟} {小时} {日期} {月份} {星期}
// 1 Seconds (0-59)
// 2 Minutes (0-59)
// 3 Hours (0-23)
// 4 Day-of-Month (1-31)
// 5 Month (0-11)
// 6 Day-of-Week(1-7)
// 7 Year (optional field)
//
// 每个符号的意义
// * 表示所有值；
// ? 表示未说明的值，即不关心它为何值；
// - 表示一个指定的范围；
// , 表示附加一个可能值；
// / 符号前表示开始时间，符号后表示每次递增的值
//
// 一些cron表达式案例
// */5 * * * * ? 每隔5秒执行一次
// 0 */1 * * * ? 每隔1分钟执行一次
// 0 0 5-15 * * ? 每天5-15点整点触发
// 0 0/3 * * * ? 每三分钟触发一次
// 0 0-5 14 * * ? 在每天下午2点到下午2:05期间的每1分钟触发
// 0 0/5 14 * * ? 在每天下午2点到下午2:55期间的每5分钟触发
// 0 0/5 14,18 * * ? 在每天下午2点到2:55期间和下午6点到6:55期间的每5分钟触发
// 0 0/30 9-17 * * ? 朝九晚五工作时间内每半小时
// 0 0 10,14,16 * * ? 每天上午10点，下午2点，4点
// 0 0 12 ? * WED 表示每个星期三中午12点
// 0 0 17 ? * TUES,THUR,SAT 每周二、四、六下午五点
// 0 10,44 14 ? 3 WED 每年三月的星期三的下午2:10和2:44触发
// 0 15 10 ? * MON-FRI 周一至周五的上午10:15触发
// 0 0 23 L * ? 每月最后一天23点执行一次
// 0 15 10 L * ? 每月最后一日的上午10:15触发
// 0 15 10 ? * 6L 每月的最后一个星期五上午10:15触发
// 0 15 10 * * ? 2005 2005年的每天上午10:15触发
// 0 15 10 ? * 6L 2002-2005 2002年至2005年的每月的最后一个星期五上午10:15触发
// 0 15 10 ? * 6#3 每月的第三个星期五上午10:15触发
//
// "30 * * * * ?" 每半分钟触发任务
// "30 10 * * * ?" 每小时的10分30秒触发任务
// "30 10 1 * * ?" 每天1点10分30秒触发任务
// "30 10 1 20 * ?" 每月20号1点10分30秒触发任务
// "30 10 1 20 10 ? *" 每年10月20号1点10分30秒触发任务
// "30 10 1 20 10 ? 2011" 2011年10月20号1点10分30秒触发任务
// "30 10 1 ? 10 * 2011" 2011年10月每天1点10分30秒触发任务
// "30 10 1 ? 10 SUN 2011" 2011年10月每周日1点10分30秒触发任务
// "15,30,45 * * * * ?" 每15秒，30秒，45秒时触发任务
// "15-45 * * * * ?" 15到45秒内，每秒都触发任务
// "15/5 * * * * ?" 每分钟的每15秒开始触发，每隔5秒触发一次
// "15-30/5 * * * * ?" 每分钟的15秒到30秒之间开始触发，每隔5秒触发一次
// "0 0/3 * * * ?" 每小时的第0分0秒开始，每三分钟触发一次
// "0 15 10 ? * MON-FRI" 星期一到星期五的10点15分0秒触发任务
// "0 15 10 L * ?" 每个月最后一天的10点15分0秒触发任务
// "0 15 10 LW * ?" 每个月最后一个工作日的10点15分0秒触发任务
// "0 15 10 ? * 5L" 每个月最后一个星期四的10点15分0秒触发任务
// "0 15 10 ? * 5#3" 每个月第三周的星期四的10点15分0秒触发任务
// 五：表达式生成器
// 有很多的cron表达式在线生成器，这里给大家推荐几款
// http://www.pdtools.net/tools/becron.jsp
// http://cron.qqe2.com/
type CronTrigger struct {
	expr     string
	trigger  cron.Schedule
	location *time.Location
}

// Verify CronTrigger satisfies the Trigger interface.
var _ Trigger = (*CronTrigger)(nil)

// NewCronTrigger returns a new CronTrigger using the UTC location.
func NewCronTrigger(expr string) (*CronTrigger, error) {
	parser := cron.NewParser(cron.Second | cron.Minute | cron.Hour | cron.Dom | cron.Month | cron.Dow | cron.Descriptor)
	trigger, err := parser.Parse(expr)
	if err != nil {
		return nil, err
	}
	return &CronTrigger{trigger: trigger, expr: expr, location: time.Local}, nil
}

func (ct *CronTrigger) Type() string {
	return "cron"
}

func (ct *CronTrigger) WithLocation(lc *time.Location) *CronTrigger {
	ct.location = lc
	return ct
}

// NextFireTime returns the next time at which the CronTrigger is scheduled to fire.
func (ct *CronTrigger) NextFireTime(prev int64) (int64, error) {
	t := ct.trigger.Next(time.Unix(0, prev).In(ct.location))
	return t.UnixNano(), nil
}

// Description returns the description of the trigger.
func (ct *CronTrigger) Description() string {
	return fmt.Sprintf("CronTrigger %s", ct.expr)
}
