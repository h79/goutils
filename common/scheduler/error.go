package scheduler

import "gitee.com/h79/goutils/common/result"

var ErrJobFuncNil = result.Error(result.ErrParam, "job func is nil")
var ErrJobQuited = result.Error(result.ErrParam, "job quited")
var ErrJobStop = result.Error(result.ErrParam, "job stop")
var ErrJobExist = result.Error(result.ErrParam, "record exist")
