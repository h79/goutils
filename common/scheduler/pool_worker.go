package scheduler

import (
	"fmt"
	"gitee.com/h79/goutils/common/logger"
	"gitee.com/h79/goutils/common/system"
)

type jobWorker struct {
	running system.RunningCheck
	pool    *Pool
	index   int
	job     chan *Job
	stop    chan int
}

func (w *jobWorker) start() {
	w.running.GoRunning(w.run, func() {
		logger.N("Pool", "quit the jobWorker,name= %s,index= %d", w.pool.name, w.index)
	})
}

func (w *jobWorker) run() {
	w.pool.workerRunning()
	defer w.pool.workerQuit()
	for {
		w.pool.idleWork(w) //把idle work放进去
		select {
		case job := <-w.job:
			_, _ = job.Execute()
			w.pool.workerIdle()

		case s := <-w.stop:
			fmt.Printf("job worker stop,name= %s,index= %d\n", w.pool.name, w.index)
			w.stopJob(s)
			w.stop <- 1
			fmt.Printf("job worker stoped,name= %s,index= %d\n", w.pool.name, w.index)
			return
		}
	}
}

func newWorker(pool *Pool, index int) *jobWorker {
	return &jobWorker{
		pool:  pool,
		index: index,
		job:   make(chan *Job),
		stop:  make(chan int),
	}
}

func (w *jobWorker) stopJob(s int) {
	opts := With(s)
	for i := 0; i < len(w.job); i++ {
		job, ok := <-w.job
		if ok {
			_, _ = job.Execute(opts...)
		}
	}
}
