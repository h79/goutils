package scheduler

import (
	"errors"
	"fmt"
	"time"
)

// Trigger represents the mechanism by which Jobs are scheduled.
type Trigger interface {
	Type() string

	// NextFireTime returns the next time at which the Trigger is scheduled to fire.
	NextFireTime(prev int64) (int64, error)

	// Description returns the description of the Trigger.
	Description() string
}

// SimpleTrigger implements the quartz.Trigger interface; uses a fixed interval.
type SimpleTrigger struct {
	Interval time.Duration
	delay    time.Duration
	isDelay  int
}

// Verify SimpleTrigger satisfies the Trigger interface.
var _ Trigger = (*SimpleTrigger)(nil)

// NewSimpleTrigger returns a new SimpleTrigger using the given interval.
func NewSimpleTrigger(interval time.Duration) *SimpleTrigger {
	return &SimpleTrigger{
		Interval: interval,
		delay:    0,
		isDelay:  0,
	}
}

func NewDelaySimpleTrigger(interval time.Duration, delay time.Duration) *SimpleTrigger {
	return &SimpleTrigger{
		Interval: interval,
		delay:    delay,
		isDelay:  1,
	}
}

func (st *SimpleTrigger) Type() string {
	return "simple"
}

// NextFireTime returns the next time at which the SimpleTrigger is scheduled to fire.
func (st *SimpleTrigger) NextFireTime(prev int64) (int64, error) {
	if st.isDelay == 1 {
		st.isDelay = 0
		return prev + st.delay.Nanoseconds(), nil
	}
	return prev + st.Interval.Nanoseconds(), nil
}

// Description returns the description of the trigger.
func (st *SimpleTrigger) Description() string {
	return fmt.Sprintf("SimpleTrigger with interval: %d", st.Interval)
}

// OnceTrigger implements the quartz.Trigger interface.
// This type of Trigger can only be fired once and will expire immediately.
type OnceTrigger struct {
	Delay   time.Duration
	expired bool
}

// Verify OnceTrigger satisfies the Trigger interface.
var _ Trigger = (*OnceTrigger)(nil)

// NewOnceTrigger returns a new OnceTrigger with the given delay time.
func NewOnceTrigger(delay time.Duration) *OnceTrigger {
	return &OnceTrigger{
		Delay:   delay,
		expired: false,
	}
}

func (tr *OnceTrigger) Type() string {
	return "once"
}

// NextFireTime returns the next time at which the OnceTrigger is scheduled to fire.
// Sets expired to true afterwards.
func (tr *OnceTrigger) NextFireTime(prev int64) (int64, error) {
	if !tr.expired {
		next := prev + tr.Delay.Nanoseconds()
		tr.expired = true
		return next, nil
	}

	return 0, errors.New("RunOnce trigger is expired")
}

// Description returns the description of the trigger.
func (tr *OnceTrigger) Description() string {
	status := "valid"
	if tr.expired {
		status = "expired"
	}

	return fmt.Sprintf("OnceTrigger (%s).", status)
}
