package scheduler

import (
	"fmt"
	"gitee.com/h79/goutils/common/option"
	"gitee.com/h79/goutils/common/system"
	"time"
)

const (
	QuitExecMustFlag = 1 //退出时，也执行一下
)

type ProcessHandler interface {
	ProcessJob(job *Job, opts ...option.Option) (interface{}, error)
}

var _ ProcessHandler = (HandlerFunc)(nil)

type HandlerFunc func(job *Job, opts ...option.Option) (interface{}, error)

func (fn HandlerFunc) ProcessJob(job *Job, opts ...option.Option) (interface{}, error) {
	return fn(job, opts...)
}

type ResultHandler interface {
	ResultJob(res *Result, opts ...option.Option)
}

var _ ResultHandler = (ResultFunc)(nil)

type ResultFunc func(res *Result, opts ...option.Option)

func (fn ResultFunc) ResultJob(res *Result, opts ...option.Option) {
	fn(res, opts...)
}

var _ Task = (*Job)(nil)

type Job struct {
	Id             string
	Type           string
	TraceId        string
	State          State
	MustFlag       int32         // QuitExecMustFlag quit时，要不要被执行，默认不执行
	Delay          time.Duration // 延后执行，卡住
	Timeout        time.Duration // 超时
	execAt         int64
	startAt        int64
	count          int64 // execute count
	payload        any   // 自定认数据
	processHandler ProcessHandler
	resultHandler  ResultHandler
}

type Result struct {
	TraceId string `json:"traceId"`
	Type    string `json:"type"`
	Id      string `json:"id"`
	Err     error  `json:"err,omitempty"`
	Data    any    `json:"data,omitempty"` //处理结果
}

type shellData struct {
	Cmd string
}

func NewShellJob(cmd string) *Job {
	job := NewJob("shell", fmt.Sprintf("ShellJob:Cmd:%s", cmd), 0)
	job.WithPayload(&shellData{Cmd: cmd})
	job.WithHandlerFunc(func(j *Job, opts ...option.Option) (interface{}, error) {
		var payload = j.GetPayload()
		shell, ok := payload.(*shellData)
		if !ok {
			return nil, fmt.Errorf("payload not shelldata")
		}
		var res = system.SyncExec(shell.Cmd, job.Timeout)
		return res, nil
	})
	return job
}

func BuildJob(jobType, jobId string, timeout time.Duration) Job {
	return Job{
		Type:    jobType,
		Id:      jobId,
		State:   InitState,
		startAt: time.Now().Local().UnixMilli(),
		Timeout: timeout,
	}
}

func NewJob(jobType, jobId string, timeout time.Duration) *Job {
	return &Job{
		Type:    jobType,
		Id:      jobId,
		State:   InitState,
		startAt: time.Now().Local().UnixMilli(),
		Timeout: timeout,
	}
}

func NewEmptyJob(fn HandlerFunc) *Job {
	job := &Job{
		Type:    "",
		Id:      "",
		State:   InitState,
		startAt: time.Now().Local().UnixMilli(),
		Timeout: 0,
	}
	return job.SetHandler(fn)
}

func (job *Job) SetHandler(fn ProcessHandler) *Job {
	job.processHandler = fn
	return job
}

func (job *Job) WithHandlerFunc(fn func(job *Job, opts ...option.Option) (interface{}, error)) *Job {
	return job.SetHandler(HandlerFunc(fn))
}

func (job *Job) SetResultHandler(fn ResultHandler) *Job {
	job.resultHandler = fn
	return job
}

func (job *Job) WithResultFunc(fn func(res *Result, opts ...option.Option)) *Job {
	return job.SetResultHandler(ResultFunc(fn))
}

func (job *Job) WithMustFlag(m int32) *Job {
	job.MustFlag = m
	return job
}

func (job *Job) WithTimeOut(timeout time.Duration) *Job {
	job.Timeout = timeout
	return job
}

func (job *Job) WithTraceId(traceId string) *Job {
	job.TraceId = traceId
	return job
}

func (job *Job) WithDelay(delay time.Duration) *Job {
	job.Delay = delay
	return job
}

func (job *Job) WithPayload(payload interface{}) *Job {
	job.payload = payload
	return job
}

func (job *Job) GetType() string {
	return job.Type
}

func (job *Job) GetId() string {
	return job.Id
}

func (job *Job) GetState() State {
	return job.State
}

func (job *Job) GetPayload() any {
	return job.payload
}
func (job *Job) SetPayload(payload any) {
	job.payload = payload
}

func (job *Job) GetExecAt() int64 {
	return job.execAt
}

func (job *Job) GetCount() int64 {
	return job.count
}

func (job *Job) GetStartAt() int64 {
	return job.startAt
}

func (job *Job) Execute(opts ...option.Option) (interface{}, error) {
	if job.State.IsQuit() {
		return nil, ErrJobQuited
	}
	if job.processHandler == nil {
		job.State = CompletedState
		return nil, ErrJobFuncNil
	}
	quited := option.Select(QuitedOpt, false, opts...)
	stop := option.Select(StopOpt, false, opts...)
	if job.MustFlag&QuitExecMustFlag == 0 {
		if quited {
			return nil, ErrJobQuited
		}
		if stop {
			return nil, ErrJobStop
		}
	}
	timeout := job.Timeout
	delay := job.Delay
	if quited || stop {
		timeout = 0
		delay = 0
	}
	if job.State == InitState {
		job.State = RunningState
		if delay > 0 {
			system.Wait(delay)
		}
	}
	job.count++
	return system.RunAfter(timeout, func(opts ...option.Option) (any, error) {
		res, err := job.processHandler.ProcessJob(job, opts...)
		if job.resultHandler != nil {
			job.resultHandler.ResultJob(job.NewResult(res, err))
		}
		return res, err
	}, opts...)
}

func (job *Job) Cancel() {
	job.State = CancelState
}

func (job *Job) Pause() {
	job.State = PauseState
}

func (job *Job) Start() {
	job.State = RunningState
}

func (job *Job) IsQuit() bool {
	return job.State.IsQuit()
}

func (job *Job) CheckTimeout() bool {
	if job.Timeout <= 0 {
		return false
	}
	now := time.Now().Local().UnixMilli()
	if job.startAt <= 0 {
		job.startAt = now
	}
	return now-job.startAt > job.Timeout.Milliseconds()
}

func (job *Job) NewResult(data any, err error) *Result {
	return &Result{TraceId: job.TraceId, Type: job.Type, Id: job.Id, Data: data, Err: err}
}
