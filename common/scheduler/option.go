package scheduler

import (
	"gitee.com/h79/goutils/common/option"
	"time"
)

const (
	DurationOpt = iota + 1
	GroupNumOpt
	CapacityOpt
	LoggEnableOpt
	StopOpt
	QuitedOpt
	TimeLocationOpt
	UniqueOpt           //唯一检测添加，存在就不添加了
	GenerateOrInitFnOpt //更新或初始化函数，在更新时，有 NotExistNewOpt 时生成函数
	NotExistNewOpt      //更新时，不存在就生成
	MinDurationOpt
	NameOpt
)

type GenerateOrInitFunc func(group int, tk Task) (Task, Trigger, bool)

func WithUnique() option.Option {
	return option.WithOpt(true, "unique", UniqueOpt)
}
func WithGenerateOrInitFunc(fn GenerateOrInitFunc) option.Option {
	return option.WithOpt[GenerateOrInitFunc](fn, "generate func", GenerateOrInitFnOpt)
}

func WithNotExistNew() option.Option {
	return option.WithOpt(true, "not exist to new", NotExistNewOpt)
}
func WithTaskMinDuration(t time.Duration) option.Option {
	return option.WithOpt(t, "task min duration", MinDurationOpt)
}
func WithName(name string) option.Option {
	return option.WithOpt(name, "name", NameOpt)
}
func WithTimeLocation(lc *time.Location) option.Option {
	return option.WithOpt(lc, "time location", TimeLocationOpt)
}
func WithTaskDuration(t time.Duration) option.Option {
	return option.WithOpt(t, "task duration", DurationOpt)
}
func WithGroupNum(num int) option.Option {
	return option.WithOpt(num, "group number", GroupNumOpt)
}
func WithCapacity(capacity int) option.Option {
	return option.WithOpt(capacity, "logger enable", CapacityOpt)
}
func WithLoggerEnabled() option.Option {
	return option.WithOpt(true, "", LoggEnableOpt)
}
func WithStop() option.Option {
	return option.WithOpt(true, "", StopOpt)
}
func IsStop(opts ...option.Option) bool {
	return option.Select(StopOpt, false, opts...)
}
func WithQuited() option.Option {
	return option.WithOpt(true, "", QuitedOpt)
}
func IsQuited(opts ...option.Option) bool {
	return option.Select(QuitedOpt, false, opts...)
}
func With(s int) []option.Option {
	var opts []option.Option
	if s == 1 {
		opts = append(opts, WithStop())
	} else if s == 2 {
		opts = append(opts, WithQuited())
	}
	return opts
}
