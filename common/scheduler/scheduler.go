package scheduler

import (
	"gitee.com/h79/goutils/common/algorithm"
	"gitee.com/h79/goutils/common/option"
	"sync"
	"time"
)

type Scheduler struct {
	rm            sync.Mutex
	groups        []*groupScheduled
	groupMod      uint32
	uniqueEnabled bool
	generateFn    GenerateOrInitFunc
}

var (
	inst *Scheduler
	once sync.Once
)

func Instance(opts ...option.Option) *Scheduler {
	once.Do(func() {
		inst = newScheduler(20, 2000, opts...)
	})
	return inst
}

func NewSchedulerCap(groupNum uint32, capacity int, opts ...option.Option) *Scheduler {
	return newScheduler(groupNum, capacity, opts...)
}

func NewScheduler(groupNum uint32, opts ...option.Option) *Scheduler {
	return newScheduler(groupNum, 5000, opts...)
}

func newScheduler(groupNum uint32, capacity int, opts ...option.Option) *Scheduler {
	location := option.Select(TimeLocationOpt, time.Local, opts...)
	duration := option.Select(DurationOpt, time.Minute*2, opts...)
	minDuration := option.Select(MinDurationOpt, defTaskDuration, opts...)
	groupNum = option.Select(GroupNumOpt, groupNum, opts...)
	capacity = option.Select(CapacityOpt, capacity, opts...)
	loggerEnabled := option.Select(LoggEnableOpt, false, opts...)
	uniqueEnabled := option.Select(UniqueOpt, false, opts...)
	name := option.Select(NameOpt, "", opts...)
	generateFn := option.Select[GenerateOrInitFunc](GenerateOrInitFnOpt, defaultUpdateFn, opts...)
	if groupNum <= 0 {
		groupNum = 20
	}
	var groups = make([]*groupScheduled, groupNum)
	for i := 0; i < int(groupNum); i++ {
		groups[i] = newGroupScheduled(name, i, capacity, location, minDuration, duration, loggerEnabled)
	}
	return &Scheduler{
		groupMod:      groupNum,
		groups:        groups,
		uniqueEnabled: uniqueEnabled,
		generateFn:    generateFn,
	}
}

func (s *Scheduler) groupScheduled(group uint32) *groupScheduled {
	s.rm.Lock()
	defer s.rm.Unlock()
	return s.groups[group]
}

func (s *Scheduler) GroupIndex(key string) uint32 {
	return algorithm.HashCode(key) % s.groupMod
}

func (s *Scheduler) Index(id uint32) uint32 {
	return id % s.groupMod
}

func (s *Scheduler) AddTask(task Task, opts ...option.Option) error {
	return s.AddGTask(s.GroupIndex(task.GetId()), task, opts...)
}

func (s *Scheduler) AddGTask(group uint32, task Task, opts ...option.Option) error {
	fn := option.Select[GenerateOrInitFunc](GenerateOrInitFnOpt, s.generateFn, opts...)
	uniqueEnabled := option.Select(UniqueOpt, s.uniqueEnabled, opts...)
	groups := s.groupScheduled(group)
	return groups.AddTask(task, uniqueEnabled, fn)
}

func (s *Scheduler) AddTriggerTask(task Task, trigger Trigger, opts ...option.Option) error {
	return s.AddTriggerGTask(s.GroupIndex(task.GetId()), task, trigger, opts...)
}

func (s *Scheduler) AddTriggerGTask(group uint32, task Task, trigger Trigger, opts ...option.Option) error {
	fn := option.Select[GenerateOrInitFunc](GenerateOrInitFnOpt, s.generateFn, opts...)
	uniqueEnabled := option.Select(UniqueOpt, s.uniqueEnabled, opts...)
	groups := s.groupScheduled(group)
	return groups.AddTriggerTask(task, trigger, uniqueEnabled, fn)
}

func (s *Scheduler) UpdateTask(taskId string, opts ...option.Option) bool {
	return s.UpdateGTask(s.GroupIndex(taskId), taskId, opts...)
}

func (s *Scheduler) UpdateGTask(group uint32, taskId string, opts ...option.Option) bool {
	fn := option.Select[GenerateOrInitFunc](GenerateOrInitFnOpt, s.generateFn, opts...)
	uniqueEnabled := option.Select(UniqueOpt, s.uniqueEnabled, opts...)
	notExistNewFlag := option.Select(NotExistNewOpt, false, opts...)
	groups := s.groupScheduled(group)
	return groups.UpdateTask(taskId, uniqueEnabled, notExistNewFlag, fn)
}

func (s *Scheduler) ForeachTask(group uint32, update func(group int, task Task, time time.Duration, now, trigger int64) bool) {
	groups := s.groupScheduled(group)
	groups.ForeachTask(update)
}

func (s *Scheduler) Foreach(update func(group int, task Task, time time.Duration, now, trigger int64) bool) {
	for i := 0; i < len(s.groups); i++ {
		s.groups[i].ForeachTask(update)
	}
}

func (s *Scheduler) StopTask(taskId string) {
	s.StopGTask(s.GroupIndex(taskId), taskId)
}

func (s *Scheduler) StopGTask(group uint32, taskId string) {
	groups := s.groupScheduled(group)
	groups.StopTask(taskId)
}

func (s *Scheduler) RemoveTask(taskId string) {
	s.RemoveGTask(s.GroupIndex(taskId), taskId)
}

func (s *Scheduler) RemoveGTask(group uint32, taskId string) {
	groups := s.groupScheduled(group)
	groups.RemoveTask(taskId)
}

func (s *Scheduler) HasTask(taskId string) bool {
	return s.HasGTask(s.GroupIndex(taskId), taskId)
}

func (s *Scheduler) HasGTask(group uint32, taskId string) bool {
	groups := s.groupScheduled(group)
	return groups.HasTask(taskId)
}

func (s *Scheduler) Run() {
	s.rm.Lock()
	defer s.rm.Unlock()
	for i := range s.groups {
		s.groups[i].Run()
	}
}
