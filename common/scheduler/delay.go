package scheduler

import (
	"gitee.com/h79/goutils/common/cache"
	"gitee.com/h79/goutils/common/option"
	"time"
)

type Delay struct {
	items *cache.Cache[int64, bool, int64, *Job]
}

func NewDelay() *Delay {
	return &Delay{
		items: cache.New[int64, bool, int64, *Job](true),
	}
}

func (d *Delay) Add(job *Job) {
	now := time.Now().Local().UnixMilli() + job.Delay.Milliseconds()
	part, _, _ := d.items.Add(now, false, true)
	if part != nil {
		part.AddChild(now, job, true)
	}
}

func (d *Delay) Check(pool *Pool) {
	var now = time.Now().Local().UnixMilli()
	d.items.Delete(func(key int64, c *cache.Part[bool, int64, *Job]) bool {
		if c.Get() {
			return true
		}
		if now < key {
			return false
		}
		// 需要执行
		c.Set(true)
		c.ForeachChild(func(key int64, c *Job) {
			c.Delay = 0
			pool.executeJob(c)
		})
		return true
	})
}

func (d *Delay) Exec(opts ...option.Option) {
	d.items.Delete(func(key int64, c *cache.Part[bool, int64, *Job]) bool {
		// 需要执行
		c.Set(true)
		c.ForeachChild(func(key int64, c *Job) {
			c.Delay = 0
			_, _ = c.Execute(opts...)
		})
		return true
	})
}
