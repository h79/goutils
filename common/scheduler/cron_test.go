package scheduler

import (
	"fmt"
	"reflect"
	"testing"
	"time"
)

func assertEqual(t *testing.T, a interface{}, b interface{}) {
	if !reflect.DeepEqual(a, b) {
		t.Fatalf("%v != %v", a, b)
	}
}

func TestCronExpression1(t *testing.T) {
	prev := int64(1555351200000000000)
	result := ""
	cronTrigger, err := NewCronTrigger("10/20 15 14 5-10 ? *")
	cronTrigger.WithLocation(time.UTC).Description()
	if err != nil {
		t.Fatal(err)
	} else {
		result, _ = iterate(prev, cronTrigger, 1000)
	}
	assertEqual(t, result, "Fri Dec 8 14:15:10 2023")
}

func TestCronExpression2(t *testing.T) {
	prev := int64(1555351200000000000)
	result := ""
	cronTrigger, err := NewCronTrigger("* 5,7,9 14-16 * * ?")
	if err != nil {
		t.Fatal(err)
	} else {
		result, _ = iterate(prev, cronTrigger.WithLocation(time.UTC), 1000)
	}
	assertEqual(t, result, "Mon Aug 5 14:05:00 2019")
}

func TestCronExpression3(t *testing.T) {
	prev := int64(1555351200000000000)
	result := ""
	cronTrigger, err := NewCronTrigger("* 5,7,9 14/2 * * Wed,Sat")
	if err != nil {
		t.Fatal(err)
	} else {
		result, _ = iterate(prev, cronTrigger.WithLocation(time.UTC), 1000)
	}
	t.Log(result)
}

func TestCronExpression4(t *testing.T) {
	expression := "0 5,7 14 1 * Sun"
	_, err := NewCronTrigger(expression)
	if err != nil {
		t.Fatalf("%s should fail", expression)
	}
}

func TestCronExpression5(t *testing.T) {
	prev := int64(1555351200000000000)
	result := ""
	cronTrigger, err := NewCronTrigger("* * * * * ?")
	if err != nil {
		t.Fatal(err)
	} else {
		result, _ = iterate(prev, cronTrigger.WithLocation(time.UTC), 1000)
	}
	assertEqual(t, result, "Mon Apr 15 18:16:40 2019")
}

func TestCronExpression6(t *testing.T) {
	prev := int64(1555351200000000000)
	result := ""
	cronTrigger, err := NewCronTrigger("* * 14/2 * * mon/3")
	if err != nil {
		t.Fatal(err)
	} else {
		result, _ = iterate(prev, cronTrigger.WithLocation(time.UTC), 1000)
	}
	assertEqual(t, result, "Mon Mar 15 18:00:00 2021")
}

func TestCronExpression7(t *testing.T) {
	prev := int64(1555351200000000000)
	result := ""
	cronTrigger, err := NewCronTrigger("* 5-9 14/2 * * 1-3")
	if err != nil {
		t.Fatal(err)
	} else {
		result, _ = iterate(prev, cronTrigger.WithLocation(time.UTC), 1000)
	}
	assertEqual(t, result, "Tue Jul 16 16:09:00 2019")
}

func TestCronExpression8(t *testing.T) {
	prev := int64(1555351200000000000)
	t.Log(time.Unix(0, prev).In(time.UTC))
	result := ""
	cronTrigger, err := NewCronTrigger("*/3 */51 */12 */2 */4 ?")
	if err != nil {
		t.Fatal(err)
	} else {
		result, _ = iterate(prev, cronTrigger.WithLocation(time.UTC), 1000)
	}
	assertEqual(t, result, "Sat Sep 7 12:00:00 2019")
}

func TestCronYearly(t *testing.T) {
	prev := int64(1555351200000000000)
	result := ""
	cronTrigger, err := NewCronTrigger("@yearly")
	if err != nil {
		t.Fatal(err)
	} else {
		result, _ = iterate(prev, cronTrigger.WithLocation(time.UTC), 100)
	}
	assertEqual(t, result, "Sun Jan 1 00:00:00 2119")
}

func TestCronMonthly(t *testing.T) {
	prev := int64(1555351200000000000)
	result := ""
	cronTrigger, err := NewCronTrigger("@monthly")
	if err != nil {
		t.Fatal(err)
	} else {
		result, _ = iterate(prev, cronTrigger.WithLocation(time.UTC), 100)
	}
	assertEqual(t, result, "Sun Aug 1 00:00:00 2027")
}

func TestCronWeekly(t *testing.T) {
	prev := int64(1555351200000000000)
	result := ""
	cronTrigger, err := NewCronTrigger("@weekly")
	if err != nil {
		t.Fatal(err)
	} else {
		result, _ = iterate(prev, cronTrigger.WithLocation(time.UTC), 100)
	}
	assertEqual(t, result, "Sun Mar 14 00:00:00 2021")
}

func TestCronDaily(t *testing.T) {
	prev := int64(1555351200000000000)
	result := ""
	cronTrigger, err := NewCronTrigger("@daily")
	if err != nil {
		t.Fatal(err)
	} else {
		result, _ = iterate(prev, cronTrigger.WithLocation(time.UTC), 1000)
	}
	assertEqual(t, result, "Sun Jan 9 00:00:00 2022")
}

func TestCronHourly(t *testing.T) {
	prev := int64(1555351200000000000)
	result := ""
	cronTrigger, err := NewCronTrigger("@hourly")
	if err != nil {
		t.Fatal(err)
	} else {
		result, _ = iterate(prev, cronTrigger.WithLocation(time.UTC), 1000)
	}
	assertEqual(t, result, "Mon May 27 10:00:00 2019")
}

var readDateLayout = "Mon Jan 2 15:04:05 2006"

func iterate(prev int64, cronTrigger *CronTrigger, iterations int) (string, error) {
	var err error
	for i := 0; i < iterations; i++ {
		prev, err = cronTrigger.NextFireTime(prev)
		if err != nil {
			fmt.Println(err)
			return "", err
		}
	}
	return time.Unix(prev/int64(time.Second), 0).In(time.UTC).Format(readDateLayout), nil
}
