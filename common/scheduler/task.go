package scheduler

import "gitee.com/h79/goutils/common/option"

type Task interface {
	GetId() string
	GetType() string
	GetState() State
	GetStartAt() int64 // new object time
	GetExecAt() int64  // 最近一次执行时间
	GetCount() int64   // 执行次数
	GetPayload() any
	Execute(opts ...option.Option) (any, error)
	Cancel()
	Pause()
	Start()
}

type State int

func (s State) Int() int {
	return int(s)
}

func (s State) Int32() int32 {
	return int32(s)
}

func (s State) Equal(ss State) bool {
	return s == ss
}

func (s State) String() string {
	switch s {
	case InitState:
		return "Init"
	case RunningState:
		return "Running"
	case PauseState:
		return "Pause"
	case CancelState:
		return "Cancel"
	case CompletedState:
		return "Completed"
	case TimeoutState:
		return "Timeout"
	case ErrorState:
		return "Error"
	}
	return ""
}

func (s State) IsQuit() bool {
	return s != RunningState && s != InitState && s != PauseState
}

const (
	InitState      = State(0)
	RunningState   = State(1)
	PauseState     = State(2)
	CancelState    = State(3)
	CompletedState = State(4)
	TimeoutState   = State(5)
	ErrorState     = State(6)
)
