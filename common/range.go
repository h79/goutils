package common

import "time"

// Range 范围
type Range[T IntegerType] struct {
	Op  string // "GT","GTE","LT","LTE","BET"
	Min T
	Max T
}

func (rg *Range[T]) HasValid() bool {
	// 时间有效，操作符长度不能太长,太长就是无效，在进行ToUpper时，造成时间消耗
	return len(rg.Op) > 0 && len(rg.Op) < 5
}

func (rg *Range[T]) Adjust() {
	if rg.Max < rg.Min {
		rg.Min, rg.Max = rg.Max, rg.Min
	}
}

func (rg *Range[T]) Legal(legal T) {
	rg.Adjust()
	if rg.Max-rg.Min <= legal {
		return
	}
	rg.Min = rg.Max - legal
}

func (rg *Range[T]) Month(month time.Duration, unit int) T {
	return rg.At(time.Hour*24*30*month, unit)
}

func (rg *Range[T]) Day(day time.Duration, unit int) T {
	return rg.At(time.Hour*24*day, unit)
}

func (rg *Range[T]) At(at time.Duration, unit int) T {
	end := T(at.Milliseconds())
	if unit == 1 {
		end = T(at.Seconds())
	} else if unit == 2 {
		end = T(at.Minutes())
	} else if unit == 3 {
		end = T(at.Hours())
	}
	return end
}
