package server

import "testing"

func TestAddress(t *testing.T) {

	addr, er := Parse("127.0.0.1:9000")
	if er != nil {
		t.Error("dddd", er)
		return
	}
	t.Logf("addr:%+v, String: %s", addr, addr.To())

	addr, er = Parse("https://172.0.0.1:9000/test")
	t.Logf("addr:%+v, String: %s", addr, addr.To())

}

func TestNewArray(t *testing.T) {
	arr := Addresses{}

	arr.Add("https://172.0.0.1:9000/test")
	arr.Add("https://172.0.0.1:9001/test")
	arr.Add("https://172.0.0.1:9002/test")
	arr.Add("https://172.0.0.1:9003/test")

	t.Logf("addr:%+v", arr)

	arr.ToWeight()

	t.Logf("addr:%+v", arr)
}
