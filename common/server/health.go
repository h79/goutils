package server

import (
	"gitee.com/h79/goutils/common/config"
	"gitee.com/h79/goutils/common/json"
	"time"
)

const (
	GHealthTtl    = 10
	KGRpcProtocol = "grpc"
	KHttpProtocol = "http"
	KTCPProtocol  = "tcp"
)

type Health struct {
	URL           Address       `json:"url"`
	Method        string        `json:"method"`
	Protocol      string        `json:"protocol"`
	Ttl           bool          `json:"ttl"`
	Interval      time.Duration `json:"interval"` //(second)
	SuccessStatus int           `json:"successStatus"`
}

func ReadHealth(filename string) (Health, error) {
	if len(filename) == 0 {
		filename = config.FileNameInEnvPath("healthcheck.json")
	}
	health := Health{
		Ttl:      true,
		Interval: GHealthTtl}
	err := json.Read(filename, &health)
	return health, err
}
