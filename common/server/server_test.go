package server

import "testing"

func TestParse(t *testing.T) {

	s := ParseWithSeparate("name|scheme://absolute_path")

	t.Logf("1 server config: %v", s)

	s2 := ParseWithSeparate("name|192.168.0.1:8080|80|10")

	t.Logf("2 server config: %v", s2)
}
