package server

type Domain struct {
	Domain string `json:"-"`
	Raw    string `json:"domain" yaml:"domain" xml:"domain"`
}

func (c Domain) DomainTo() string {
	if c.Domain == "" && c.Raw != "" {
		if ad, er := Parse(c.Raw); er == nil {
			c.Domain = ad.To()
		}
	}
	return c.Domain
}
