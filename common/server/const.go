package server

type (
	// AuthType 认证 1=用户名, 2=手机 3=邮箱 4=微信 5=微博 6=QQ 7=小程序
	AuthType int8

	//AuthMethod 认证方法
	AuthMethod int8

	//AuthStatus 认证状态
	AuthStatus int8

	//ValidatingType 验证类型
	ValidatingType int32
)

const (
	AtDefault = AuthType(0)
	AtName    = AuthType(1)  //用户名
	AtPhone   = AuthType(2)  //手机
	AtEmail   = AuthType(3)  //邮箱
	AtWeiXin  = AuthType(4)  //微信（第三方）
	AtWeibo   = AuthType(5)  //微博（第三方）
	AtQq      = AuthType(6)  //QQ （第三方）
	AtWxMini  = AuthType(7)  //微信小程序（第三方）
	ATWxSer   = AuthType(8)  //微信服务号（第三方）
	AtToday   = AuthType(9)  //今日头条（第三方）
	AtBaidu   = AuthType(10) //百度（第三方）
	AtToken   = AuthType(99) //业务TOKEN验证
	AtVisitor = AuthType(100)
	AtInner   = AuthType(101)
	AtEnd     = AuthType(102)
)

func (a AuthType) Int() int8 {
	return int8(a)
}

const (
	AsDefault    = AuthStatus(0) //0=未激活 1=正在.. 2=已激活 3=删除
	AsActivating = AuthStatus(1)
	AsActivated  = AuthStatus(2)
	AsDeleted    = AuthStatus(3)
)

func (a AuthStatus) Int() int8 {
	return int8(a)
}

const (
	AmDefault = AuthMethod(0)
	AmPass    = AuthMethod(1) //密码
	AmSms     = AuthMethod(2) //短信
	AmEnd     = AuthMethod(3)
)

func (a AuthMethod) Int() int8 {
	return int8(a)
}

const (
	VtDefault   = ValidatingType(0)
	VtRegSms    = ValidatingType(1) //注册短信
	VtLogSms    = ValidatingType(2) //登录短信
	VtRegInvite = ValidatingType(3) //注册邀请
	VtActEmail  = ValidatingType(4)
	VtEnd       = ValidatingType(255)
)

func (a ValidatingType) Int() int32 {
	return int32(a)
}

const (
	PlatPc     = "pc"
	PlatMac    = "mac"
	PlatIPhone = "iphone"
	PlatIPad   = "ipad"
	PlatAPhone = "aphone"
	PlatAPad   = "apad"
	PlatTv     = "tv"
)
