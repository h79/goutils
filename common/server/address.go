package server

import (
	"fmt"
	"gitee.com/h79/goutils/common/random"
	"gitee.com/h79/goutils/common/stringutil"
	"gitee.com/h79/goutils/common/system"
	"net"
	"net/url"
	"regexp"
	"sort"
	"strings"
)

type Address struct {
	Scheme  string `json:"scheme" yaml:"scheme" xml:"scheme"`
	Host    string `json:"host" yaml:"host" xml:"host"`
	Port    int    `json:"port" yaml:"port" xml:"port"`
	Path    string `json:"path,omitempty" yaml:"path,omitempty" xml:"path,omitempty"`
	Name    string `json:"name,omitempty" yaml:"name,omitempty" xml:"name,omitempty"`
	Weight  int    `json:"weight,omitempty" yaml:"weight,omitempty" xml:"weight,omitempty"`
	Timeout int    `json:"timeout,omitempty" yaml:"timeout,omitempty" xml:"timeout,omitempty"`
}

func (s Address) IsHttp() bool {
	return s.Scheme == "http" || s.Scheme == "https"
}

func (s Address) IsWs() bool {
	return s.Scheme == "ws" || s.Scheme == "wss"
}

func (s Address) IsScheme(scheme string) bool {
	return s.Scheme == scheme
}

// To
// scheme://host:port
func (s Address) To() string {
	var buf strings.Builder
	if len(s.Scheme) > 0 {
		buf.WriteString(s.Scheme)
		buf.WriteString("://")
	}
	if len(s.Host) > 0 {
		buf.WriteString(s.Host)
	}
	if s.Port > 0 {
		buf.WriteByte(':')
		buf.WriteString(stringutil.IntToString(s.Port))
	}
	if len(s.Path) > 0 {
		if s.Path[0] != '/' {
			buf.WriteByte('/')
		}
		buf.WriteString(s.Path)
	}
	return buf.String()
}

// Adjust 如果IP是自己，调整为 127.0.0.1
func (s Address) Adjust() Address {
	if !s.HasIP() {
		return s
	}
	s.Host = system.IsSelfIP(s.Host)
	return s
}

func (s Address) HasScheme() bool {
	return s.Scheme != ""
}

func (s Address) HasPort() bool {
	return s.Port > 0
}

func (s Address) HasIP() bool {
	return net.ParseIP(s.Host) != nil
}

var reg = regexp.MustCompile(".com|.cn|.net|.org")

// HasDomain 简单的域名 .com,.cn
func (s Address) HasDomain() bool {
	return reg.MatchString(s.Host)
}

func (s Address) SetHost(host string) {
	s.Host = host
}

func (s Address) IsEqual(addr Address) bool {
	return addr.Host == s.Host && addr.Port == s.Port
}

// IsSelf 服务就是自己
func (s Address) IsSelf(addr string) bool {
	self, er := Parse(addr)
	if er != nil {
		return false
	}
	return self.Adjust().IsEqual(s.Clone().Adjust())
}

func (s Address) IsValid() bool {
	return len(s.Host) > 0
}

func (s Address) Clone() Address {
	return Address{
		Scheme: s.Scheme,
		Host:   s.Host,
		Port:   s.Port,
		Path:   s.Path,
		Name:   s.Name,
	}
}

type Addresses []Address

// Add @param s = host:port
func (ay *Addresses) Add(addr string) {
	ser, err := Parse(addr)
	if err != nil {
		return
	}
	*ay = append(*ay, ser)
}

func (ay Addresses) Array() []string {

	var points []string
	for i := range ay {
		points = append(points, ay[i].To())
	}
	return points
}

func (ay Addresses) ToWeight() {
	r := random.Random()
	for index := range ay {
		ay[index].Weight = r.Intn(1000)
	}
	sort.Sort(ay)
}

func (ay Addresses) Len() int {
	return len(ay)
}

func (ay Addresses) Swap(i, j int) {
	temp := ay[i]
	ay[i] = ay[j]
	ay[j] = temp
}

// Less 权重越大，在前面
func (ay Addresses) Less(i, j int) bool {
	return ay[i].Weight >= ay[j].Weight
}

func ParseV2(addr net.Addr, pubIp string) Address {
	host := addr.(*net.TCPAddr).IP.String()
	port := addr.(*net.TCPAddr).Port
	if host == "" || host == "::" || host == ":::" {
		p := strings.SplitN(pubIp, ":", 2)
		host = p[0]
	}
	return Address{Host: host, Port: port}
}

func Parse(addr string) (Address, error) {
	u, err := url.Parse(addr)
	if err != nil {
		host := strings.Split(addr, ":")
		if len(host) < 2 {
			return Address{}, err
		}
		if net.ParseIP(host[0]) == nil {
			return Address{}, fmt.Errorf("addr='%v' is not ip", addr)
		}
		return Address{
			Host: host[0],
			Port: stringutil.StringToInt(host[1]),
		}, nil
	}
	return Address{
		Scheme: u.Scheme,
		Host:   u.Hostname(),
		Port:   stringutil.StringToInt(u.Port()),
		Path:   u.Path,
	}, nil
}

// ParseWithSeparate
// info = name|scheme://host:post|timeout|weight
func ParseWithSeparate(info string) Address {
	var name string
	var timeout int
	var weight int
	s := strings.Split(info, "|")
	l := len(s)
	if l >= 2 {
		name = s[0]
		info = s[1]

		if l >= 3 {
			timeout = stringutil.StringToInt(s[2])
		}

		if l >= 4 {
			weight = stringutil.StringToInt(s[3])
		}
	}
	addr, er := Parse(info)
	if er != nil {
		panic(er)
	}
	addr.Weight = weight
	addr.Timeout = timeout
	addr.Name = name
	return addr
}
