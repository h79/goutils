package git

import (
	"bytes"
	"context"
	"errors"
	"os/exec"
	"strings"
)

var (
	ArgsCommitLog = []string{"log", "--pretty=format:\"%h\"", "-1"}
	ArgsCommit    = []string{"rev-parse", "--short", "HEAD"}
	ArgsDirty     = []string{"status", "--porcelain"}
	ArgsBranch    = []string{"rev-parse", "--abbrev-ref", "HEAD"}
	ArgsDescribe  = []string{"describe", "--tags", "--always"}
	ArgsTag       = []string{"describe", "--abbrev=0", "--tags"}
)

type Cmd struct {
}

func (c *Cmd) IsRepo(ctx context.Context, env []string) bool {
	out, err := c.Do(ctx, env, []string{"rev-parse", "--is-inside-work-tree"}...)
	return err == nil && strings.TrimSpace(out) == "true"
}

// VersionNumber sample 1.0.1
func (c *Cmd) VersionNumber(ctx context.Context, env []string) string {
	ver := c.Version(ctx, env)
	if ver != "" && ver[0] == 'v' {
		return ver[1:]
	}
	return ver
}

// Version sample v1.0.1
func (c *Cmd) Version(ctx context.Context, env []string) string {
	ver, err := c.Do(ctx, env, ArgsTag...)
	if err != nil {
		return ""
	}
	return ver
}

func (c *Cmd) Do(ctx context.Context, env []string, args ...string) (string, error) {
	a := append([]string{"-c", "log.showSignature=false"}, args...)
	return c.DoCmd(ctx, env, a...)
}

func (c *Cmd) DoCmd(ctx context.Context, env []string, args ...string) (string, error) {

	cmd := exec.CommandContext(ctx, "git", args...)

	stdout := bytes.Buffer{}
	stderr := bytes.Buffer{}

	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	cmd.Env = append(cmd.Env, env...)

	err := cmd.Run()

	if err != nil {
		return "", errors.New(stderr.String())
	}
	return stdout.String(), nil
}
