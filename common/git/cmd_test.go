package git

import (
	"context"
	"testing"
)

func TestCmd(t *testing.T) {
	c := Cmd{}
	res, err := c.Do(context.Background(), nil, ArgsDescribe...)
	if err != nil {
		t.Error(err)
		return
	}
	t.Log(res)
}

func TestVersionNo(t *testing.T) {
	c := Cmd{}
	ver := c.VersionNumber(context.Background(), nil)
	t.Log(ver)
}
