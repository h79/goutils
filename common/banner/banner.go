package banner

import (
	"fmt"
	"os"
)

const Startup = 1

func init() {
	ban, err := os.ReadFile("./env/banner.txt")
	if err != nil {
		ban, err = os.ReadFile("./bin/env/banner.txt")
	}
	fmt.Println(string(ban))
}
