package trie

import (
	"fmt"
	"testing"
)

func TestNewTrie(t *testing.T) {
	trie := NewTrie()
	trie.Add("广州")
	trie.Add("花都")
	trie.Add("番禺")
	trie.Add("广东")
	trie.Add("广州市")

	const text = "AAA?广州市广东花都区广州XXX街"
	find(trie, text)

	const text1 = "AAA?番禺区广州亚运城运动员村广州市石楼镇xdfdsfd&Ee?@#dd#$广东省"
	find(trie, text1)
}

func find(cy *Trie, text string) {
	ok, name, no := cy.FindInReturnNo(text)
	fmt.Println(fmt.Sprintf("从'%s',找到市县(区）name= %s, ok= %v, no= %s", text, name, ok, no))

	gr := cy.FindAll(text)

	for i := range gr {
		fmt.Println(fmt.Sprintf("从'%s',找到= %+v", text, gr[i]))
	}
	fmt.Println("-----------")
}
