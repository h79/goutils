package flow

type Data interface {
	GetId() string
	GetType() string
	GetValue() any
}

type DataChan chan Data

type Port struct {
	addr string
	data DataChan
}

func (p *Port) SetAddr(addr string) {
	p.addr = addr
}

func (p *Port) GetAddr() string {
	return p.addr
}

func (p *Port) Write(d Data) {
	p.data <- d
}

func (p *Port) Read() DataChan {
	return p.data
}
