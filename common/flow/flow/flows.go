package flow

import (
	"sync"
)

type Flows struct {
	locker sync.Mutex
	flows  map[string]*Flow
}

func NewFlows() *Flows {
	return &Flows{
		flows: make(map[string]*Flow),
	}
}

func (f *Flows) AddFlow(key string, process *Flow) error {
	f.locker.Lock()
	defer f.locker.Unlock()
	if _, ok := f.flows[key]; ok {
		return CExisted
	}
	f.flows[key] = process
	return nil
}

func (f *Flows) Remove(key string) {
	f.locker.Lock()
	defer f.locker.Unlock()
	if _, ok := f.flows[key]; ok {
		delete(f.flows, key)
	}
}

func (f *Flows) Start() {
	f.locker.Lock()
	defer f.locker.Unlock()
	for _, v := range f.flows {
		f.run(v)
	}
}

func (f *Flows) run(proc *Flow) {
	go proc.Run()
}
