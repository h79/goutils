package flow

type Result struct {
	Code int32
	Msg  string
	Data Data
}

// error interface
func (r Result) Error() string {
	return r.Msg
}

func (r Result) Ok() bool {
	return r.Code == CSuccess.Code
}

func (r Result) IsIgnore() bool {
	return r.Code == CIgnore.Code
}

var (
	CExit         = Result{Code: -4, Msg: "exit"}
	CExisted      = Result{Code: -3, Msg: "existed"}
	CNotImplement = Result{Code: -2, Msg: "Not implement"}
	CDeadline     = Result{Code: -1, Msg: "deadline"}
	CSuccess      = Result{Msg: "Success"}
	CIgnore       = Result{Code: 1, Msg: "Continue"}
	CEnd          = Result{Code: 2, Msg: "End"}
)
