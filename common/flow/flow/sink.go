package flow

import "context"

type Sink interface {
	Process(ctx context.Context, in *Port) (*Port, Result)
}

const (
	SinkIn = "sink:in"
)

type DefaultSink struct {
}

func (pt *DefaultSink) Process(ctx context.Context, in *Port) (*Port, Result) {
	return nil, CNotImplement
}
