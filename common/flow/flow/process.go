package flow

import "context"

// Handler 请内部使用 go runtime
type Handler interface {
	Process(ctx context.Context, in *Port, out *Port) Result
}

var _ Sink = (*Processor)(nil)

type Processor struct {
	port    Port
	handler Handler
}

func NewProcessor(chanSize int, handler Handler) *Processor {
	return &Processor{
		port:    Port{data: make(DataChan, chanSize)},
		handler: handler,
	}
}

func (proc *Processor) Process(ctx context.Context, in *Port) (*Port, Result) {
	result := proc.handler.Process(ctx, in, &proc.port)
	return &proc.port, result
}
