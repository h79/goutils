package http

import (
	"os"
)

func Download(url string, filename string, perm os.FileMode) error {
	hp := Http{DisableLog: true}
	res, err := hp.Do("GET", url, nil, nil)
	if err != nil {
		return err
	}
	if err = os.WriteFile(filename, res, perm); err != nil {
		return err
	}
	return nil
}
