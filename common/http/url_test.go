package http

import (
	"testing"
)

type Obj struct {
	XXX  string `url:"xxx"`
	XXX1 int    `url:"xxx1"`
	XXX2 string `url:"xxx2,omitempty"`
}

func TestParse(t *testing.T) {
	obj := Obj{
		XXX:  "X1",
		XXX1: 1,
		XXX2: "222",
	}
	u := NewUrl()
	u.Parse(obj)
	t.Log(u.Encode())

	xx := u.Generate("https://127.0.0:5015/sfsfsf", "?")
	t.Log(xx)
}
