package http

import (
	"bytes"
	"mime/multipart"
	"reflect"
	"testing"
)

type DDDStruct struct {
}

func (d *DDDStruct) CreateForm(w *multipart.Writer) error {
	return nil
}

type DDD struct {
	D1 string
	D2 int     `json:"d2,omitempty"`
	D3 float32 `url:"d3,omitempty"`
	D4 string  `url:"d4"`
}

func TestHttp_ConvertTo(t *testing.T) {
	v := ConvertTo(&DDD{})
	t.Log(v.Encode())
}

func TestForm(t *testing.T) {
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	defer w.Close()

	d := DDDStruct{}
	val := reflect.ValueOf(&d)
	if val.CanInterface() && val.Type().Implements(FormType) {
		fm := val.Interface().(Form)
		fm.CreateForm(w)
	}
}
