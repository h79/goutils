package http

const (
	MimeJSON          = "application/json"
	MimeHTML          = "text/html"
	MimeXML           = "application/xml"
	MimeXML2          = "text/xml"
	MimePlain         = "text/plain"
	MimeForm          = "application/x-www-form-urlencoded"
	MimeMultipartForm = "multipart/form-data"
	MimePROTOBUF      = "application/x-protobuf"
	MimeMSGPACK       = "application/x-msgpack"
	MimeMSGPACK2      = "application/msgpack"
	MimeYAML          = "application/x-yaml"
	MimeJSONUtf8      = "application/json;charset=UTF-8"
)
