package http

import (
	"gitee.com/h79/goutils/common/result"
	"io"
	"mime/multipart"
	"os"
	"path/filepath"
	"reflect"
)

var (
	FormType = reflect.TypeOf((*Form)(nil)).Elem()
)

type Form interface {
	CreateForm(w *multipart.Writer) error
}

type FileForm struct {
	Field string
	Value string
}

func (df *FileForm) CreateForm(w *multipart.Writer) error {
	fh, err := os.Open(df.Value)
	if err != nil {
		return result.Error(-1, err.Error())
	}
	defer fh.Close()

	fw, err := w.CreateFormFile(df.Field, filepath.Base(df.Value))
	if err != nil {
		return result.Error(result.ErrException, err.Error())
	}

	_, err = io.Copy(fw, fh)

	return err
}

type ReadForm struct {
	Field string
	Value string
	R     io.Reader
}

func (df *ReadForm) CreateForm(w *multipart.Writer) error {

	fw, err := w.CreateFormFile(df.Field, df.Value)
	if err != nil {
		return result.Error(result.ErrException, err.Error())
	}

	_, err = io.Copy(fw, df.R)

	return err
}
