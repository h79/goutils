package http

import (
	"gitee.com/h79/goutils/common/stringutil"
	"gitee.com/h79/goutils/common/tag"
	"net/url"
	"reflect"
	"strings"
)

const URLTagName = "url"

type Url struct {
	url.Values
}

func NewUrl() Url {
	return Url{Values: url.Values{}}
}

func WithParams(data interface{}) Url {
	u := Url{Values: url.Values{}}
	u.Parse(data)
	return u
}

func (u Url) Parse(data interface{}) {
	value := tag.StructVal(data)

	switch value.Kind() {
	case reflect.Map:
		u.parseMap(value)

	case reflect.Struct:
		u.parseStruct(value)

	default:
		panic("no support")
	}
}

func (u Url) parseMap(value reflect.Value) {
	iter := value.MapRange()
	for iter.Next() {
		u.Add(iter.Key().String(), stringutil.ToStringV2(iter.Value()))
	}
}

func (u Url) parseStruct(value reflect.Value) {
	fields := tag.StructFields(value, URLTagName)
	for _, field := range fields {
		name := field.Name
		val := value.FieldByName(name)
		tagName, tagOpts := tag.Parse(field.Tag.Get(URLTagName))
		if tagName != "" {
			name = tagName
		}
		emp := tagOpts.Has("omitempty")
		if emp && val.IsZero() {
			continue
		}
		u.Add(name, stringutil.ToStringV2(val))
	}
}

func (u Url) Generate(url, suffix string) string {
	if !strings.HasSuffix(url, suffix) {
		url += suffix
	}
	return url + u.Encode()
}

func ConvertTo(data interface{}) url.Values {
	u := Url{Values: url.Values{}}
	u.Parse(data)
	return u.Values
}

func URLWithPath(rawUrl string, path string, params map[string]interface{}) (u *url.URL) {
	u, err := url.Parse(rawUrl)
	if err != nil {
		return
	}
	if len(path) > 0 {
		u, err = u.Parse(path)
		if err != nil {
			return
		}
	}
	ul := NewUrl()
	ul.Parse(params)
	u.RawQuery = ul.Encode()
	return
}

func URLWithParams(rawUrl string, params map[string]interface{}) (u *url.URL) {
	return URLWithPath(rawUrl, "", params)
}
