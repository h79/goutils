package http

import (
	"bytes"
	"mime/multipart"
	"net/http"
)

func Upload(url string, filename string, headerFunc HeaderFunc) ([]byte, error) {
	return UploadV2(url, &FileForm{Value: filename, Field: "file"}, headerFunc)
}

func UploadV2(url string, form Form, headerFunc HeaderFunc) ([]byte, error) {

	var b bytes.Buffer
	w := multipart.NewWriter(&b)

	err := form.CreateForm(w)
	if err != nil {
		w.Close()
		return nil, err
	}
	// need to close
	w.Close()

	hp := &Http{}
	return hp.Do("POST", url, &b, func(h *http.Header) {
		// Don't forget to set the content type, this will contain the boundary.
		if headerFunc != nil {
			headerFunc(h)
		}
		h.Set(KContentType, w.FormDataContentType())
	})
}
