package compress

import (
	"fmt"
	"github.com/hungys/go-lz4"
)

type LZ4 struct{}

// Name returns name of the algorithm LZ4
func (l LZ4) Name() string { return "LZ4" }

// CompressBound max size of compressed data
func (l LZ4) CompressBound(size int) int { return lz4.CompressBound(size) }

// Compress using LZ4 algorithm
func (l LZ4) Compress(dst, src []byte) (int, error) {
	return lz4.CompressDefault(src, dst)
}

// Decompress using LZ4 algorithm
func (l LZ4) Decompress(dst, src []byte) (int, error) {
	if len(src) == 0 {
		return 0, fmt.Errorf("decompress an empty input")
	}
	return lz4.DecompressSafe(src, dst)
}
