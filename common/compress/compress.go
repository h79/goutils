package compress

import "strings"

type Compressor interface {
	Name() string
	CompressBound(int) int
	Compress(dst, src []byte) (int, error)
	Decompress(dst, src []byte) (int, error)
}

func NewCompressor(level string) Compressor {
	level = strings.ToLower(level)
	switch level {
	case "zstd":
		return ZStandard{ZstdLevel}
	case "lz4":
		return LZ4{}
	case "":
		return noOp{}
	}
	return nil
}
