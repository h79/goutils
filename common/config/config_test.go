package config

import (
	"path/filepath"
	"testing"
)

func TestConfFileName(t *testing.T) {
	pa := filepath.Dir("./data/run/conf/xxx.json")
	t.Log(pa)

	pa, fi := filepath.Split("./bin//")
	t.Log(pa)
	t.Log(fi)

	t.Log(filepath.Clean("./bin//"))

	conf := Config{}
	conf.Init()

	t.Log(FileNameInAppDataPathV2("name"))

	t.Log(FileNameInDataPath("name"))

	t.Log(FileNameInConfPath("name"))
	t.Log(FileNameInConfAppPath("NAME"))
	t.Log(FileNameInEnvPath("NAME"))
}
