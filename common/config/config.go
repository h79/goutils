package config

import (
	"context"
	"flag"
	"fmt"
	"gitee.com/h79/goutils/common/file"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
)

const (
	EProd = "prod"
	ETest = "test"
	EDev  = "dev"
)

const (
	KForever  = "forever"
	KDaemon   = "daemon"
	KPprof    = "pprof"
	KPath     = "path"
	KEnv      = "env"
	KConf     = "conf"
	KDataPath = "dataPath"
	KAppId    = "appid"
	KServer   = "server"
	KAgent    = "agent"
)

var (
	Version  = "1.0.0"
	AppName  = "unknown"
	AppAlias = "" //别名，example 某某公司的某某应用
	AppId    string
	Source   string
	Env      = EDev
	WorkDir  string
	MainConf string
	DataPath string //程序存储数据
	Server   string
	Agent    string
	SeqId    int64 = 0
	Daemon   bool
	Forever  bool
	Pprof    bool
)

var (
	appExecPath     string
	envPath         string
	confPath        string
	appConfPath     string
	appDataPath     string //程序运行数据目录,如views,docs
	appDataPathV2   string
	defaultDataPath string
	appDataVersion  int //数据版本
)

type Config struct {
	appDataPath    string //程序运行数据目录,如views,docs
	appDataPathV2  string
	appConfPath    string
	workDir        string
	dataPath       string
	envPath        string
	Mode           string `json:"mode" yaml:"mode" xml:"mode"`
	Server         string `json:"server" yaml:"server" xml:"server"`
	PubIP          string `json:"publicIp" yaml:"publicIp" xml:"publicIp"`
	AppDataVersion int    `json:"appDataVersion" yaml:"appDataVersion" xml:"appDataVersion"`
	EnabledRedis   bool   `json:"enabledRedis" yaml:"enabledRedis" xml:"enabledRedis"`
	EnabledDb      bool   `json:"enabledDb" yaml:"enabledDb" xml:"enabledDb"`
	EnabledEs      bool   `json:"enabledEs" yaml:"enabledEs" xml:"enabledEs"`
	EnabledMQ      bool   `json:"enabledMQ" yaml:"enabledMQ" xml:"enabledMQ"`
}

type Option func(c *Config)

func WithEnvPath(path string) Option {
	return func(c *Config) {
		c.envPath = path
	}
}

func WithWorkDir(dir string) Option {
	return func(c *Config) {
		c.workDir = dir
	}
}

func WithAppConfPath(p string) Option {
	return func(c *Config) {
		c.appConfPath = p
	}
}

func WithAppDataPath(p string) Option {
	return func(c *Config) {
		c.appDataPath = p
	}
}

func WithAppDataPathV2(p string) Option {
	return func(c *Config) {
		c.appDataPathV2 = p
	}
}

func WithDataPath(p string) Option {
	return func(c *Config) {
		c.dataPath = p
	}
}

func (c *Config) Init(opts ...Option) {
	if len(Env) <= 0 {
		Env = EDev
	}
	for i := range opts {
		opts[i](c)
	}
	if c.workDir != "" {
		WorkDir = c.workDir
	}
	if WorkDir == "." || WorkDir == "" {
		if app := GetExecPath(); app != "" {
			WorkDir = app
		}
	}
	if !strings.HasSuffix(WorkDir, "/") {
		WorkDir += "/"
	}
	WorkDir = filepath.Dir(WorkDir)

	envPath = c.envPath
	if envPath == "" || !file.IsExist(envPath) {
		envPath = fmt.Sprintf("%s/env", WorkDir)
	}
	confPath = fmt.Sprintf("%s/conf", WorkDir)

	if c.appConfPath == "" {
		appConfPath = fmt.Sprintf("%s/%s", confPath, AppName)
	} else {
		appConfPath = c.appConfPath
	}

	if c.appDataPath == "" {
		appDataPath = WorkDir
	} else {
		appDataPath = c.appDataPath
	}

	if c.appDataPathV2 == "" {
		appDataPathV2 = fmt.Sprintf("%s/app", WorkDir)
	} else {
		appDataPathV2 = c.appDataPathV2
	}

	if MainConf == "" {
		MainConf = fmt.Sprintf("%s/%s.main.json", appConfPath, Env)
	}

	defaultDataPath = fmt.Sprintf("%s/data/%s", WorkDir, AppName)
	if DataPath == "" {
		if c.dataPath != "" {
			DataPath = c.dataPath
		} else {
			DataPath = defaultDataPath
		}
	}

	Source = AppName
}

func init() {
	var _ = AppAlias

	flag.StringVar(&Env, KEnv, EDev, "dev,test,prod")
	flag.StringVar(&WorkDir, KPath, ".", "path of work")
	flag.StringVar(&MainConf, KConf, "", "config path of main.json")
	flag.StringVar(&AppId, KAppId, "", "appid")
	flag.StringVar(&DataPath, KDataPath, "", "data path")
	flag.StringVar(&Server, KServer, "", "server address")
	flag.StringVar(&Agent, KAgent, "", "server agent name")

	flag.BoolVar(&Forever, KForever, false, "forever run")
	flag.BoolVar(&Daemon, KDaemon, false, "daemon run")
	flag.BoolVar(&Pprof, KPprof, false, "pprof switch")

	flag.Usage = func() {
		flag.PrintDefaults()
	}
}

func IsProdEnvironment() bool {
	return Env == EProd
}

func IsDevEnvironment() bool {
	return Env == EDev
}

func IsTestEnvironment() bool {
	return Env == ETest
}

// FileNameInEnvPath
// env/{Env}/name
func FileNameInEnvPath(name string) string {
	return filepath.Join(envPath, Env, name)
}

// FileNameWithoutEnvInEnvPath
// env/name
func FileNameWithoutEnvInEnvPath(name string) string {
	return filepath.Join(envPath, name)
}

// FileNameWithEnvInConfAppPath
// conf/appName 带env "conf/{appName}/{Env}.name"
func FileNameWithEnvInConfAppPath(name string) string {
	return filepath.Join(appConfPath, WithEnv(name))
}

// FileNameWithoutEnvInConfAppPath
// Deprecated: this function simply calls [FileNameInConfAppPath].
func FileNameWithoutEnvInConfAppPath(name string) string {
	return filepath.Join(appConfPath, name)
}

// FileNameInConfAppPath
// conf/appName 不带env "conf/{appName}/name"
func FileNameInConfAppPath(name string) string {
	return filepath.Join(appConfPath, name)
}

// FileNameInConfPath
// conf/ "confPath/name"
func FileNameInConfPath(name string) string {
	return filepath.Join(confPath, name)
}

// FileNameInAppDataPath
// conf/ "{workdir}/name" 或 {workdir}/app/name
func FileNameInAppDataPath(name string) string {
	return filepath.Join(GetAppDataPath(), name)
}

// FileNameInAppDataPathV2
// conf/ "{workdir}/app/name"
func FileNameInAppDataPathV2(name string) string {
	return filepath.Join(GetAppDataPathV2(), name)
}

// FileNameInDataPath
// conf/ 目录下 "{DataPath}/name"
func FileNameInDataPath(name string) string {
	return filepath.Join(DataPath, name)
}

// FileNameWithEnvInDataPath
// conf/ 目录下 "{DataPath}/{Env}.name"
func FileNameWithEnvInDataPath(name string) string {
	return filepath.Join(DataPath, WithEnv(name))
}

// FileNameInEnvDataPath
// conf/ 目录下 "{DataPath}/{Env}/name"
func FileNameInEnvDataPath(name string) string {
	return filepath.Join(DataPath, Env, name)
}

// FileNameInWork
// 工作目录下 "{WorkDir}/"
func FileNameInWork(name string) string {
	return filepath.Join(WorkDir, name)
}

// FileNameWithEnvInWork
// 工作目录下 "{WorkDir}/"
func FileNameWithEnvInWork(name string) string {
	return filepath.Join(WorkDir, WithEnv(name))
}

// WithEnv
// 与env组合
func WithEnv(name string) string {
	return fmt.Sprintf("%s.%s", Env, name)
}

// MainFileName env.main.json 全路径文件名
func MainFileName() string {
	return MainConf
}

// GetConfPath ./conf
func GetConfPath() string {
	return confPath
}

// GetConfAppPath ./conf/appName
func GetConfAppPath() string {
	return appConfPath
}

// GetDataPath 业务定义存储路径
func GetDataPath() string {
	return DataPath
}

func IsDefaultDataPath() bool {
	return strings.EqualFold(DataPath, defaultDataPath)
}

// GetAppDataPath 程序运行数据目录,如views,docs
func GetAppDataPath() string {
	if appDataVersion >= 1 {
		return GetAppDataPathV2()
	}
	return appDataPath
}

func GetAppDataPathV2() string {
	return appDataPathV2
}

func GetEnvPath() string {
	return envPath
}

func UseAppDataPathV2() {
	UseAppDataVersion(1)
	// to adjust
	GetAppDataPath()
}

func UseAppDataVersion(ver int) {
	appDataVersion = ver
}

func GetExecPath() string {
	if len(appExecPath) > 0 {
		return appExecPath
	}
	path, err := exec.LookPath(os.Args[0])
	if err != nil {
		return ""
	}
	appExecPath = filepath.Dir(path)
	return appExecPath
}

func GetAppPath() (string, error) {
	prog := os.Args[0]
	p, err := filepath.Abs(prog)
	if err != nil {
		return "", err
	}
	fi, err := os.Stat(p)
	if err == nil {
		if !fi.Mode().IsDir() {
			return p, nil
		}
		err = fmt.Errorf("GetAppPath: %s is directory", p)
	}
	if filepath.Ext(p) == "" && runtime.GOOS == "windows" {
		p += ".exe"
		fi, err = os.Stat(p)
		if err == nil {
			if !fi.Mode().IsDir() {
				return p, nil
			}
			err = fmt.Errorf("GetAppPath: %s is directory", p)
		}
	}
	return "", err
}

type HandlerFunc func(ctx context.Context, cmd int, configType, config string) (any, error)

var RegisterConfig func(ty string, handler HandlerFunc)
var UnRegisterConfig func(ty string)
