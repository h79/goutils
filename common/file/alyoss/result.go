package alyoss

type Error struct {
	RequestId string
	HostId    string
	Code      string
	Message   string
}

// SubmitResult 提交转码返回结果
type SubmitResult struct {
	RequestId string
	JobResult JobResultList `json:"JobResultList"`
}

type JobResultList struct {
	JobResults []JobResult `json:"result"`
}

type JobResult struct {
	Success string
	Code    string
	Message string
	Job     Job
}

// QueryJobResult 查询转码状态结果
type QueryJobResult struct {
	RequestId string
	JobList   JobList
}

type JobList struct {
	Jobs []Job `json:"Job"`
}

// MediaInfoResult 媒体信息返回结果
type MediaInfoResult struct {
	RequestId string
	MediaInfo []MediaInfo `json:"MediaInfoJob"`
}

// SnapshotResult 截图返回结果
type SnapshotResult struct {
	RequestId string
	Snapshot  Snapshot `json:"SnapshotJob"`
}
