package alyoss

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base64"
	"encoding/json"
	"gitee.com/h79/goutils/common/file/config"
	"hash"
	"io"
	"time"
)

func getGmtIso8601(expireEnd int64) string {
	var tokenExpire = time.Unix(expireEnd, 0).Format("2006-01-02T15:04:05Z")
	return tokenExpire
}

type ConfigStruct struct {
	Expiration string     `json:"expiration"`
	Conditions [][]string `json:"conditions"`
}

type PolicyToken struct {
	AccessKeyId string `json:"accessId"`
	Host        string `json:"host"`
	Expire      int64  `json:"expire"`
	Signature   string `json:"signature"`
	Policy      string `json:"policy"`
	Directory   string `json:"dir"`
	Callback    string `json:"callback"`
}

type CallbackParam struct {
	CallbackUrl      string `json:"callbackUrl"`
	CallbackBody     string `json:"callbackBody"`
	CallbackBodyType string `json:"callbackBodyType"`
}

const DefaultCallbackBody = "filename=${object}&size=${size}&mimeType=${mimeType}&height=${imageInfo.height}&width=${imageInfo.width}"
const DefaultCallbackBodyType = "application/x-www-form-urlencoded"

func GetCallbackParam(callback string) CallbackParam {
	return CallbackParam{
		CallbackUrl:      callback,
		CallbackBody:     DefaultCallbackBody,
		CallbackBodyType: DefaultCallbackBodyType,
	}
}

// GetPolicyToken 产生一个临时的上传token
func (of *OssFile) GetPolicyToken(objBucket config.Bucket, callbackParam CallbackParam, uploadDir string) (PolicyToken, error) {

	if len(uploadDir) == 0 {
		uploadDir = objBucket.DefaultUploadDir
	}

	if len(callbackParam.CallbackUrl) <= 0 {
		callbackParam.CallbackUrl = of.config.TokenCallbackUrl
	}

	now := time.Now().Unix()
	expireEnd := now + of.config.Expires
	var tokenExpire = getGmtIso8601(expireEnd)

	// create post policy json
	var objConf ConfigStruct
	var condition []string
	condition = append(condition, "starts-with")
	condition = append(condition, "$key")
	condition = append(condition, uploadDir)
	objConf.Conditions = append(objConf.Conditions, condition)
	objConf.Expiration = tokenExpire

	// calculate signature
	bytes, err := json.Marshal(objConf)
	if err != nil {
		return PolicyToken{}, err
	}
	deByte := base64.StdEncoding.EncodeToString(bytes)
	h := hmac.New(func() hash.Hash { return sha1.New() }, []byte(of.config.SecretKey))
	_, err = io.WriteString(h, deByte)
	if err != nil {
		return PolicyToken{}, err
	}
	signed := base64.StdEncoding.EncodeToString(h.Sum(nil))

	callbackStr, err := json.Marshal(callbackParam)
	if err != nil {
		return PolicyToken{}, err
	}
	callbackBase64 := base64.StdEncoding.EncodeToString(callbackStr)

	return PolicyToken{
		AccessKeyId: of.config.AccessKey,
		Host:        objBucket.Host,
		Expire:      expireEnd,
		Signature:   signed,
		Directory:   uploadDir,
		Policy:      deByte,
		Callback:    callbackBase64,
	}, nil
}
