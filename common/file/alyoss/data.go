package alyoss

type Job struct {
	JobId        string
	Input        Object `json:"Input"`
	Output       Output `json:"Output"`
	State        string
	Code         string
	Message      string
	Percent      int
	PipelineId   string
	CreationTime string
}

type Output struct {
	OutputFile    Object        `json:"OutputFile,omitempty"`
	TemplateId    string        `json:"TemplateId,omitempty"`
	UserData      string        `json:"UserData,omitempty"`
	Properties    Properties    `json:"Properties,omitempty"`
	WaterMarkList WaterMarkList `json:"WaterMarkList,omitempty"`
}

type WaterMarkList struct {
	WaterMarks []WaterMark `json:"WaterMark,omitempty"`
}

type WaterMark struct {
	InputFile           Object `json:"InputFile,omitempty"`
	WaterMarkTemplateId string `json:"WaterMarkTemplateId,omitempty"`
}

type Object struct {
	Bucket   string
	Location string
	Object   string
}

// JobQuery 转码状态
type JobQuery struct {
	Job
}

// MediaInfo 媒体信息
type MediaInfo struct {
	JobId      string
	Input      Object
	Properties Properties
}

type Properties struct {
	Streams Streams
}

type Streams struct {
	Format             Format
	State              string
	Code               string
	Message            string
	PipelineId         string
	CreationTime       string
	UserData           string
	VideoStreamList    VideoStreamList    `json:"VideoStreamList,omitempty"`
	AudioStreamList    AudioStreamList    `json:"AudioStreamList,omitempty"`
	SubtitleStreamList SubtitleStreamList `json:"SubtitleStreamList,omitempty"`
}

type Format struct {
	NumStreams     string
	NumPrograms    string
	FormatName     string
	FormatLongName string
	StartTime      string
	Duration       string
	Size           string
	Bitrate        string
}

type VideoStreamList struct {
	VideoStreams []VideoStream `json:"VideoStream,omitempty"`
}

type VideoStream struct {
	Index          string
	CodecName      string
	CodecLongName  string
	Profile        string
	CodecTimeBase  string
	CodecTagString string
	CodecTag       string
	Width          string
	Height         string
	HasBFrames     string
	Sar            string
	Dar            string
	PixFmt         string
	Level          string
	Fps            string
	AvgFPS         string
	Timebase       string
	StartTime      string
	Duration       string
	Bitrate        string
	NumFrames      string
	Lang           string
	NetworkCost    NetworkCost
}

type AudioStreamList struct {
	AudioStreams []AudioStream `json:"AudioStream,omitempty"`
}

type AudioStream struct {
	Index          string
	CodecName      string
	CodecLongName  string
	Profile        string
	CodecTimeBase  string
	CodecTagString string
	CodecTag       string
	SampleFmt      string
	Samplerate     string
	Channels       string
	ChannelLayout  string
	Timebase       string
	StartTime      string
	Duration       string
	Bitrate        string
	NumFrames      string
	Lang           string
}

type SubtitleStreamList struct {
	SubtitleStreams []SubtitleStream `json:"SubtitleStream,omitempty"`
}

type SubtitleStream struct {
	Index string
	Lang  string
}

type NetworkCost struct {
	PreloadTime   string
	CostBandwidth string
	AvgBitrate    string
}

type Snapshot struct {
	Id           string
	State        string
	Code         string
	Message      string
	Config       SnapshotConfig
	PipelineId   string
	UserData     string
	CreationTime string
}

type SnapshotConfig struct {
	OutputFile Object
	Time       string
}
