package alyoss

type HeadReq struct {
	Format           string //  返回值的类型，支持JSON与XML，默认为XML。
	version          string //	API版本号，为日期形式：YYYY-MM-DD，本版本对应为2014-06-18。
	AccessKeyId      string //  阿里云颁发给用户的访问服务所用的密钥ID。
	Signature        string //	签名结果串，关于签名的计算方法，参见 签名机制。
	SignatureMethod  string //	签名方式，目前支持HMAC-SHA1。
	Timestamp        string //	请求的时间戳。日期格式按照ISO8601标准表示，并需要使用UTC时间。格式为：YYYY-MM-DDThh:mm:ssZ例如，2014-7-29T12:00:00Z(为北京时间2014年7月29日的20点0分0秒。
	SignatureVersion string //  签名算法版本，目前版本是1.0。
	SignatureNonce   string //	唯一随机数，用于防止网络重放攻击。用户在不同请求间要使用不同的随机数值。
}

// SubmitReq Action = SubmitJobs
type SubmitReq struct {
	Action         string
	Input          Object `json:"Input"`
	OutputBucket   string
	OutputLocation string
	Outputs        []OutputObject
	PipelineId     string
}

type OutputObject struct {
	OutputObject string
	TemplateId   string
	WaterMarks   []WaterMark
	UserData     string `json:"UserData"`
}

// QueryReq Action = QueryJobList
type QueryReq struct {
	Action string
	JobIds string //是转码作业ID列表，逗号分隔，一次最多10个
}

// SubmitMediaInfoReq Action = SubmitMediaInfo
type SubmitMediaInfoReq struct {
	Action   string
	Input    Object `json:"Input"`
	UserData string
}

// SubmitSnapshotReq Action = SubmitSnapshotJob
type SubmitSnapshotReq struct {
	Action     string
	Input      Object            `json:"Input"`
	Config     SnapshotConfigReq `json:"SnapshotConfig"`
	PipelineId string
	UserData   string
}

// SnapshotConfigReq
/*
BlackLevel 多帧截图针对首帧过滤黑屏图片判定阀值。
	默认值：空
	取值范围：[30,100]
	值越小，图片黑色像素的占比越小。

	Time>0 ，参数设置无效，放弃过滤黑屏功能；
	Time =0 ，Num>1，参数有效，且只作用于第一张图，只检测开始5秒数据，如果没有非黑屏图片，依旧返回第一帧黑屏图片；
	Time =0 ，Num=1，强制过滤黑屏，参数无效。
	如果需要首帧图片过滤纯黑屏，建议参数值设置成100。

	使用示例：Time=0&Num=10 需要对首图做纯黑屏过滤，则设置BlackLevel= 100。

**/
type SnapshotConfigReq struct {
	Format         string
	BlackLevel     string
	OutputFile     Object
	TileOutputFile Object
	Interval       string
	Num            string
	Width          string
	Height         string
	FrameType      string
	TileOut        TileOut
	SubOut         SubOut
}

type TileOut struct {
	Lines         string // 图片拼合行数, 取值范围：(0，10000]， 默认：10
	Columns       string // 图片拼合列数, 取值范围：(0，10000]， 默认：10
	CellWidth     string // 单图片宽度。默认截图输出分辨率宽度。
	CellHeight    string // 单图片高度。 默认截图输出分辨率高度。
	Margin        string // 外框宽度。
	Padding       string // 图片间距，默认0，单位px
	Color         string // 背景颜色。 默认：black， 其中颜色关键字支持三种格式，比如黑色，支持Black，black，#000000
	IsKeepCellPic string // 是否保留单图片。 取值范围：true、false， 默认：true
}

type SubOut struct {
	IsSptFrag string // 图片是否拼合输出。 true表示拼合输出， 默认：false
}
