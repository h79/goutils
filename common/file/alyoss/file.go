package alyoss

import (
	fileconfig "gitee.com/h79/goutils/common/file/config"
	"gitee.com/h79/goutils/common/file/tools"
	"gitee.com/h79/goutils/common/logger"
	"gitee.com/h79/goutils/common/result"
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"io"
	"os"
)

// FileUpload 文件上传
func (of *OssFile) FileUpload(fileResult *fileconfig.Result, data *fileconfig.Base, bucket *oss.Bucket) error {

	fd, err := os.Open(data.FileName)
	defer fd.Close()

	if err != nil {
		return result.Errorf(result.ErrUpload, "[File]open file size : %s", err)
	}

	f, err := fd.Stat()
	if err != nil {
		return result.Errorf(result.ErrUpload, "[File]file stat %s", err)
	}

	if f.Size() <= of.config.MinChunkSize {
		//直接文件上传
		err = bucket.PutObjectFromFile(data.Key, data.FileName)
		if err != nil {
			return result.Errorf(result.ErrUpload, "[File]Bucket put file %s", err)
		}
		return nil
	}

	//分片处理
	imur, err := bucket.InitiateMultipartUpload(data.Key)
	if err != nil {
		return result.Errorf(result.ErrUpload, "[File]Bucket InitiateMultipartUpload %s", err)
	}

	chunks := tools.Chunk(f.Size(), tools.ChunkNum(f.Size(), of.config.MinChunkSize))

	// 对每个分片调用UploadPart方法上传。
	var parts []oss.UploadPart
	for _, chunk := range chunks {
		_, err := fd.Seek(chunk.Offset, io.SeekStart)
		if err != nil {
			return result.Errorf(result.ErrUpload, "[File]Bucket part file seek %s", err)
		}
		part, err := bucket.UploadPart(imur, fd, chunk.Size, chunk.Number)
		if err != nil {
			return result.Errorf(result.ErrUpload, "[File]Bucket part file %s", err)
		}
		parts = append(parts, part)
	}
	// 完成分片上传
	cmur, err := bucket.CompleteMultipartUpload(imur, parts)
	if err != nil {
		return result.Errorf(result.ErrUpload, "[File]Bucket complete part %s", err)
	}
	logger.Info("Upload: Complete part %v", cmur)
	return nil
}
