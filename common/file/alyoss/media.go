/*
*
  - 阿里云，媒体服务工具
    *
  - https://help.aliyun.com/product/29194.html?spm=a2c4g.11186623.6.540.7c1a4f3edFJizJ

//获取媒体信息
http://mts.cn-hangzhou.aliyuncs.com/?Action=SubmitMediaInfoJob&Input=%7B%22Bucket%22%3A%22example-bucket%22%2C%22Location%22%3A%22oss-cn-hangzhou%22%2C%0A%22Object%22%3A%22example.flv%22%7D%0A&PipelineId=88c6ca184c0e47098a5b665e2a126797<公共参数>

//转码
http://mts.cn-hangzhou.aliyuncs.com/?PipelineId=88c6ca184c0e47098a5b665e2a126799&Action=SubmitJobs&Input=%7b%22Bucket%22%3a%22example-bucket%22%2c%22Location%22%3a%22oss-cn-hangzhou%22%2c%22Object%22%3a%22example.flv%22%7d&Outputs=%5b%7b%22OutputObject%22%3a%22example-output.flv%22%2c%22TemplateId%22%3a%22S00000000-000010%22%2c%22WaterMarks%22%3a%5b%7b%22InputFile%22%3a%7b%22Bucket%22%3a%22example-bucket%22%2c%22Location%22%3a%22oss-cn-hangzhou%22%2c%22Object%22%3a%22example-logo.png%22%7d%2c%22WaterMarkTemplateId%22%3a%2288c6ca184c0e47098a5b665e2a126797%22%7d%5d%7d%5d&OutputBucket=example-bucket&公共参数>

//转码状态
http://mts.cn-hangzhou.aliyuncs.com/?Action=QueryJobList&JobIds=88c6ca184c0e47098a5b665e2a126797&<公共参数>

//截图
http://mts.cn-hangzhou.aliyuncs.com?Action=SubmitSnapshotJob&Input=%7b%22Bucket%22%3a%22example-bucket%22%2c%22Location%22%3a%22oss-cn-hangzhou%22%2c%22Object%22%3a%22example.flv%22%7d&SnapshotConfig=%7B%22OutputFile%22%3A%7B%22Bucket%22%3A%22example-001%22%2C%22Location%22%3A%22oss-cn-hangzhou%22%2C%22Object%22%3A%22example.jpg%22%7D%2C%22Time%22%3A%225%22%7D&PipelineId=88c6ca184c0e47098a5b665e2a126797<Public parameter>
*/
package alyoss
