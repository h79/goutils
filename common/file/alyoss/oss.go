package alyoss

import (
	fileconfig "gitee.com/h79/goutils/common/file/config"
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
)

type OssFile struct {
	config fileconfig.Config
}

func NewOssFile(config fileconfig.Config) *OssFile {
	return &OssFile{config: config}
}

func (of *OssFile) UploadFile(fileResult *fileconfig.Result, data *fileconfig.Base) error {
	// 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
	client, err := oss.New(of.config.EndPoint, of.config.AccessKey, of.config.SecretKey)
	if err != nil {
		return err
	}

	bucket, err := client.Bucket(data.Bucket)
	if err != nil {
		return err
	}

	return of.FileUpload(fileResult, data, bucket)
}

// IsObjectExist 文件是否存在
func (of *OssFile) IsObjectExist(data *fileconfig.Base) (bool, error) {

	// 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
	client, err := oss.New(of.config.EndPoint, of.config.AccessKey, of.config.SecretKey)
	if err != nil {
		return false, err
	}

	bucket, err := client.Bucket(data.Bucket)
	if err != nil {
		return false, err
	}
	return bucket.IsObjectExist(data.Key)
}

func (of *OssFile) UploadStream(fileResult *fileconfig.Result, data *fileconfig.Stream) error {
	//传入osskey, 文件路径, oss路径
	// 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
	client, err := oss.New(of.config.EndPoint, of.config.AccessKey, of.config.SecretKey)
	if err != nil {
		return err
	}

	bucket, err := client.Bucket(data.Bucket)
	if err != nil {
		return err
	}

	return of.StreamUpload(fileResult, data, bucket)
}
