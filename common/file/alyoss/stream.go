package alyoss

import (
	"fmt"
	fileconfig "gitee.com/h79/goutils/common/file/config"
	"gitee.com/h79/goutils/common/file/tools"
	"gitee.com/h79/goutils/common/logger"
	"gitee.com/h79/goutils/common/result"
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"io"
)

// StreamUpload 流式上传
func (of *OssFile) StreamUpload(fileResult *fileconfig.Result, data *fileconfig.Stream, bucket *oss.Bucket) error {

	if data.Size <= of.config.MinChunkSize {

		if _, err := data.Seeker.Seek(0, io.SeekStart); err != nil {
			return result.Error(result.ErrUpload,
				fmt.Sprintf("Stream: File Seek failure, err= %v", err))
		}

		if err := bucket.PutObject(data.Key, data.Reader); err != nil {
			return result.Error(result.ErrUpload,
				fmt.Sprintf("Stream: Bucket put object failure, err= %v", err))
		}
		logger.Debug("Stream: Completed put object")
		return nil
	}
	return of.streamPartUpload(fileResult, data, bucket)
}

func (of *OssFile) streamPartUpload(fileResult *fileconfig.Result, data *fileconfig.Stream, bucket *oss.Bucket) error {

	//分片处理
	imur, err := bucket.InitiateMultipartUpload(data.Key)
	if err != nil {
		return result.Error(result.ErrUpload,
			fmt.Sprintf("Stream: Bucket InitiateMultipartUpload failure, err= %v", err))
	}
	fileResult.Id = imur.UploadID

	chunks := tools.Chunk(data.Size, tools.ChunkNum(data.Size, of.config.MinChunkSize))

	// 对每个分片调用UploadPart方法上传。
	var parts []oss.UploadPart
	for _, chunk := range chunks {
		_, er := data.Seeker.Seek(chunk.Offset, io.SeekStart)
		if er != nil {
			return result.Error(result.ErrUpload,
				fmt.Sprintf("Stream: Bucket part file seek failure, err= %v", er))
		}
		part, er1 := bucket.UploadPart(imur, data.Reader, chunk.Size, chunk.Number)
		if er1 != nil {
			return result.Error(result.ErrUpload,
				fmt.Sprintf("Stream: Bucket part file failure, err= %v", er1))
		}
		parts = append(parts, part)
	}
	// 完成分片上传
	cmur, er2 := bucket.CompleteMultipartUpload(imur, parts)
	if er2 != nil {
		return result.Error(result.ErrUpload,
			fmt.Sprintf("Stream: Bucket complete part failure, err= %v", er2))
	}
	logger.Info("Stream: Complete part, data= %v", cmur)
	return nil
}
