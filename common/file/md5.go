package file

import (
	"crypto/md5"
	"fmt"
	"io"
	"os"
)

func MD5(filename string) string {
	file, err := os.Open(filename)
	defer file.Close()
	if err != nil {
		return ""
	}
	return MD5Reader(file)
}

func MD5Byte(src io.Reader) []byte {
	by, _ := MD5ByteSize(src)
	return by
}

// MD5ByteSize return md5 bytes and file's size
func MD5ByteSize(src io.Reader) ([]byte, int64) {
	hash := md5.New()
	ll, err := io.Copy(hash, src)
	if err != nil {
		return []byte{}, ll
	}
	return hash.Sum(nil), ll
}

func MD5Reader(src io.Reader) string {
	return fmt.Sprintf("%x", MD5Byte(src))
}

// MD5Size return md5 hex(32) and file's size
func MD5Size(src io.Reader) (string, int64) {
	has, ll := MD5ByteSize(src)
	return fmt.Sprintf("%x", has), ll
}
