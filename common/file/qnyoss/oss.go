package qnyoss

import (
	"context"
	"fmt"
	fileconfig "gitee.com/h79/goutils/common/file/config"
	"github.com/qiniu/api.v7/v7/auth"
	"github.com/qiniu/api.v7/v7/storage"
)

type OssFile struct {
	config fileconfig.Config
}

func NewOssFile(config fileconfig.Config) *OssFile {
	return &OssFile{config: config}
}

// UploadToken 获取七牛云Token
func (of *OssFile) UploadToken(bucket string, key string) string {
	putPolicy := storage.PutPolicy{
		Scope: fmt.Sprintf("%s:%s", bucket, key),
	}
	putPolicy.Expires = uint64(of.config.Expires)
	cred := auth.New(of.config.AccessKey, of.config.SecretKey)

	return putPolicy.UploadToken(cred)
}

func (of *OssFile) UploadStream(fileResult *fileconfig.Result, stream *fileconfig.Stream) error {

	upToken := of.UploadToken(stream.Bucket, stream.Key)

	region, exist := storage.GetRegionByID(storage.RegionID(of.config.Region))
	if !exist {
		return fmt.Errorf("region not found")
	}

	cfg := storage.Config{}
	// 空间对应的机房
	cfg.Zone = &region
	// 是否使用https域名
	cfg.UseHTTPS = of.config.UseHTTPS
	// 上传是否使用CDN上传加速
	cfg.UseCdnDomains = of.config.UseCdn

	formUploader := storage.NewFormUploader(&cfg)
	ret := storage.PutRet{}
	extra := storage.PutExtra{}

	if err := formUploader.Put(context.Background(),
		&ret, upToken, stream.Key, stream.Reader, stream.Size, &extra); err != nil {
		return err
	}
	fileResult.Key = ret.Key
	fileResult.Url = of.config.Cdn + fileResult.Key
	return nil
}
