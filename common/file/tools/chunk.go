package tools

import (
	fileconfig "gitee.com/h79/goutils/common/file/config"
)

func Chunk(size int64, chunkNum int) []fileconfig.Chunk {

	var chunks []fileconfig.Chunk
	var chunk = fileconfig.Chunk{}
	var num = int64(chunkNum)
	for i := int64(0); i < num; i++ {
		chunk.Number = int(i + 1)
		chunk.Offset = i * (size / num)
		if i == num-1 {
			chunk.Size = size/num + size%num
		} else {
			chunk.Size = size / num
		}
		chunks = append(chunks, chunk)
	}
	return chunks
}

func ChunkNum(size, minChunkSize int64) int {
	return (int)(size/minChunkSize + 1)
}
