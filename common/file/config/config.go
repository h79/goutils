package config

import (
	"io"
	"os"
	"time"
)

type Config struct {
	Access
	Type             string `yaml:"type" json:"type"` // aliyun, qiniu...
	Cdn              string `yaml:"fileCdn" json:"fileCdn"`
	TmpPath          string `yaml:"tmpPath" json:"tmpPath"` //本地的临时文件
	TokenCallbackUrl string `yaml:"tokenCallbackUrl" json:"tokenCallbackUrl"`
	MinChunkSize     int64  `yaml:"minChunkSize" json:"minChunkSize"`
	Expires          int64  `yaml:"expires" json:"expires"`   //TOKEN 过期秒
	UseHTTPS         bool   `yaml:"useHttps" json:"useHttps"` //是否使用https域名
	UseCdn           bool   `yaml:"useCdn" json:"useCdn"`     //上传是否使用CDN上传加速
}

type Access struct {
	AccessKey string `yaml:"acsKey" json:"acsKey"`
	SecretKey string `yaml:"secret" json:"secret"`
	EndPoint  string `yaml:"endpoint" json:"endpoint"`
	Region    string `yaml:"region" json:"region"`
}

type Base struct {
	Dir      string
	Key      string
	Bucket   string
	FileName string
}

type Bucket struct {
	Name             string `yaml:"name" json:"name"`
	Host             string `yaml:"host" json:"host"`
	DefaultUploadDir string `yaml:"defaultUploadDir" json:"defaultUploadDir"`
	AllowExt         string `yaml:"allowExt" json:"allowExt"`
	AppId            string `yaml:"appId" json:"appId"`
	ConfKey          string `yaml:"confKey" json:"confKey"`
}

// File is a file inside an archive.
type File struct {
	Info        FileInfo `yaml:"info,omitempty" json:"info,omitempty"`
	Source      string   `yaml:"src,omitempty" json:"src,omitempty"`
	Destination string   `yaml:"dst,omitempty" json:"dst,omitempty"`
	Dir         bool     `yaml:"dir,omitempty" json:"dir,omitempty"`
	StripParent bool     `yaml:"strip_parent,omitempty" json:"strip_parent,omitempty"`
	Default     bool     `yaml:"-" json:"-"`
}

// FileInfo is the file info of a file.
type FileInfo struct {
	Mode        os.FileMode `yaml:"mode,omitempty" json:"mode,omitempty"`
	Owner       string      `yaml:"owner,omitempty" json:"owner,omitempty"`
	Group       string      `yaml:"group,omitempty" json:"group,omitempty"`
	MTime       string      `yaml:"mtime,omitempty" json:"mtime,omitempty"`
	ParsedMTime time.Time   `yaml:"-" json:"-"`
}

// Unmarshal is a custom unmarshaler that wraps strings in arrays.
func (f *File) Unmarshal(unmarshal func(interface{}) error) error {
	type t File
	var str string
	if err := unmarshal(&str); err == nil {
		*f = File{Source: str}
		return nil
	}

	var file t
	if err := unmarshal(&file); err != nil {
		return err
	}
	*f = File(file)
	return nil
}

type ReaderStream interface {
	OnReader(r io.ReadSeeker)
}

type ReaderStreamFunc func(r io.ReadSeeker)

func (s ReaderStreamFunc) OnReader(r io.ReadSeeker) {
	s(r)
}

type WriteStream interface {
	OnWriter(r io.WriteSeeker)
}

type WriterStreamFunc func(r io.WriteSeeker)

func (s WriterStreamFunc) OnWriter(r io.WriteSeeker) {
	s(r)
}

// Stream 文件流
type Stream struct {
	Base
	Seeker io.Seeker
	Reader io.Reader
	Size   int64
}

func (sm *Stream) Seek(offset int64, whence int) (int64, error) {
	if sm.Seeker != nil {
		return sm.Seeker.Seek(offset, whence)
	}
	return 0, nil
}

type Result struct {
	TaskId   string `json:"task_id,omitempty"`  //异步时
	Id       string `json:"id,omitempty"`       //文件id值
	Key      string `json:"key"`                //文件key
	Url      string `json:"url,omitempty"`      //文件url
	Name     string `json:"name,omitempty"`     //文件名
	Type     string `json:"type"`               //文件类型
	Path     string `json:"-"`                  //本地文件路径
	Version  string `json:"version,omitempty"`  //版本
	Method   int32  `json:"method,omitempty"`   //存储方法
	Size     int64  `json:"size,omitempty"`     //文件大小
	CreateAt int64  `json:"create_at"`          //创建的时间
	Duration int64  `json:"duration"`           //所消耗时间豪秒
	Business any    `json:"business,omitempty"` //业务定义数据
}

type Results struct {
	Results []Result `json:"results"`
}

type ChunkResult struct {
	Key    string  `json:"key,omitempty"`
	Chunks []Chunk `json:"chunks"`
}

type Chunk struct {
	Number int   `json:"number"` // Chunk number
	Offset int64 `json:"offset"` // Chunk offset
	Size   int64 `json:"size"`   // Chunk size.
}

type Path struct {
	Id        string   `json:"id"`        //作品ID: vid-nums, 其它id
	Product   string   `json:"product"`   //产品名
	Biz       string   `json:"biz"`       //业务名
	Type      string   `json:"type"`      //类型
	Name      string   `json:"name"`      //原始文件名
	RealName  string   `json:"real_name"` //实际文件名
	Ext       string   `json:"ext"`       //扩展名
	Md5       string   `json:"md5"`       //md5
	Version   string   `json:"version"`   //版本
	Dir       []string `json:"dir"`       //多级目录
	Business  any      `json:"-"`         //业务自定义数据
	Method    int32    `json:"method"`
	TenantId  int64    `json:"tenantId"` // 租户id
	BizId     int32    `json:"biz_id"`   // 业务
	Source    int32    `json:"source"`   //`来源
	CreateAt  int64    `json:"create_at"`
	ProductId int64    `json:"product_id"` // 产品
	Size      int64    `json:"size"`
}
