package algorithm

import (
	"crypto/md5"
	"fmt"
)

// StringToMd5 string to md5
// Deprecated: this function Md5String
func StringToMd5(data string) string {
	return fmt.Sprintf("%x", md5.Sum([]byte(data)))
}

// BytesToMd5 bytes to md5
// Deprecated: this function Md5Bytes
func BytesToMd5(data []byte) string {
	return fmt.Sprintf("%x", md5.Sum(data))
}

func Md5String(data string) string {
	return fmt.Sprintf("%x", md5.Sum([]byte(data)))
}

func Md5Bytes(data []byte) string {
	return fmt.Sprintf("%x", md5.Sum(data))
}
