package algorithm

import "crypto/cipher"

type ecb struct {
	b         cipher.Block
	blockSize int
}

func newECB(b cipher.Block) *ecb {
	return &ecb{
		b:         b,
		blockSize: b.BlockSize(),
	}
}

type ecbEncrypt ecb

// NewECBEncrypt returns a BlockMode which encrypts in electronic code book
// mode, using the given Block.
func NewECBEncrypt(b cipher.Block) cipher.BlockMode {
	return (*ecbEncrypt)(newECB(b))
}

func (e *ecbEncrypt) BlockSize() int {
	return e.blockSize
}

func (e *ecbEncrypt) CryptBlocks(dst, src []byte) {
	if len(src)%e.blockSize != 0 {
		panic("cipher/ecb: input not full blocks")
	}

	if len(dst) < len(src) {
		panic("cipher/ecb: output smaller than input")
	}

	for len(src) > 0 {
		e.b.Encrypt(dst, src[:e.blockSize])
		src = src[e.blockSize:]
		dst = dst[e.blockSize:]
	}
}

type ecbDecrypter ecb

// NewECBDecrypter returns a BlockMode which decrypts in electronic code book
// mode, using the given Block.
func NewECBDecrypter(b cipher.Block) cipher.BlockMode {
	return (*ecbDecrypter)(newECB(b))
}

func (e *ecbDecrypter) BlockSize() int {
	return e.blockSize
}

func (e *ecbDecrypter) CryptBlocks(dst, src []byte) {
	if len(src)%e.blockSize != 0 {
		panic("cipher/ecb: input not full blocks")
	}

	if len(dst) < len(src) {
		panic("cipher/ecb: output smaller than input")
	}

	for len(src) > 0 {
		e.b.Decrypt(dst, src[:e.blockSize])
		src = src[e.blockSize:]
		dst = dst[e.blockSize:]
	}
}
