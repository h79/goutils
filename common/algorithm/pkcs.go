package algorithm

type PKCS interface {
	Encrypt(raw, key []byte) (string, error)
	Decrypt(raw string, key []byte) ([]byte, error)
}

func NewPKCS(ver int) PKCS {
	switch ver {
	case 1:
		return NewPKCS1()

	case 7:
		return NewPKCS7()
	}
	return nil
}
