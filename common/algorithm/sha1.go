package algorithm

import (
	"crypto/sha1"
	"fmt"
	"io"
	"sort"
	"strings"
)

func SignWithSha1(sep string, s ...string) (string, error) {
	sort.Strings(s)
	jn := strings.Join(s, sep)
	return Sha1(jn)
}

func Sha1(s string) (string, error) {
	hash := sha1.New()
	if _, err := io.WriteString(hash, s); err != nil {
		return "", err
	}
	return fmt.Sprintf("%x", hash.Sum(nil)), nil
}
