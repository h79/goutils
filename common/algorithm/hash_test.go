package algorithm

import "testing"

func TestHashCode(t *testing.T) {

	t.Log(HashCode("windows:prod:1.0.0"))
	t.Log(HashCode("windows:prod:1.1.0"))
	t.Log(HashCode("windows:prod:1.0.1"))
	t.Log(HashCode("windows:prod:3.2.1"))
	t.Log(HashCode("windows:dev:1.0.1"))
	t.Log(HashCode("windows:test:1.0.1"))
}
