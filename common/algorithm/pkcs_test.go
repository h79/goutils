package algorithm

import (
	"bytes"
	"fmt"
	"testing"
)

func TestCrypt_Decrypt(t *testing.T) {

	var privKey = []byte(`-----BEGIN RSA PRIVATE KEY-----
MIICXQIBAAKBgQCVZ625ZMlW8VmhH8K7R4qtJHZRgkzi0g+JZytEVUTLQ8aQ7Wl7
KZjHPnih1U8f5XBFEyn+LHsAcNvA77D/17igzLIZ5nIyfggIAOS26ie4HHEyH6Wl
pMA3F1o/+k4yGok//9tvdF/OboQGo1mXpeztQrM8GU3KPJFMKOg9skk7YwIDAQAB
AoGAR/45vEhQrNCH/Bgt4cjLjjsvuH8tRW8rhW5tbvQXvkreeB+u4GZe9EOqJXGA
O0J4IxDk/1G3w7Kecg4OIHp2wGfzoEPTFmv3uZ+KnQZTZ9wUudsgsS5kSvjQ9kLF
SPv/eD2EKC3DJWfJA1VcetDMB0WQdqZt3uMTl3qWoLBhn0ECQQCw2yHy7mhVQGrf
W1xvfIULXrkY+T/HUF/YhJEaPvnC0fVCZC6Up/SLLdXCdM0tR1cAuaD+GkTObKlv
4qRhmu17AkEA2EO4406oiqe5/udyqTp4bd6TfI1dNvwBbBGlPztzWMhH8YnBMuUw
V+MWVcoabCPRtpysI3970ximXoFGo1ChOQJAUuUDTpiMcpkU/sgV1XOp8KBfZeM+
D7AOnK/WS1UFQFmk+Y93fdoTd0DuloyktvpfP0nCuheZ9J9d88Fphw5VDQJBANPw
Omb5FQ8dJJf8dlAGLzNtJxiu6SUTfslL2afIOVhOnsLE/4NDPEdFEwRsFczYpZLS
o5f+PXk49QI0qucuA3ECQQCGWhNOznEoZX+4RvHj1NvjSwyvc5I4dfbhUukTfTgC
5xGIOb5kJ/5IsE39Bo6rxmOLurlbP9xA5urgYvEQ/iAZ
-----END RSA PRIVATE KEY-----`)

	var pubKey = []byte(`-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCVZ625ZMlW8VmhH8K7R4qtJHZR
gkzi0g+JZytEVUTLQ8aQ7Wl7KZjHPnih1U8f5XBFEyn+LHsAcNvA77D/17igzLIZ
5nIyfggIAOS26ie4HHEyH6WlpMA3F1o/+k4yGok//9tvdF/OboQGo1mXpeztQrM8
GU3KPJFMKOg9skk7YwIDAQAB
-----END PUBLIC KEY-----`)

	orig := "hello"

	pkcs := NewPKCS1()
	raw, er := pkcs.Encrypt([]byte(orig), pubKey)
	if er != nil {
		return
	}
	raw = "jD/WK7XW8JohMeEE7aBarPsZ8p/YChLaQJPzU0RPvyF2oX9zV6JlI8kMf1Jw0IbZSeNQoK+8JhNhQ9UJjFsqrbl+Nu738zURxZksaONHOeCOjnQGgxR/Wye9SBF05VAlz0x2brMLYpobftX0+CBWztsOY6g37/6OWsW44nGRx5E="
	orig2, err := pkcs.Decrypt(raw, privKey)
	if err != nil {
		return
	}
	t.Log("OK: ", string(orig2))
}

func TestNewPKCS(t *testing.T) {
	const key = "1r1tE7lZDHgKHK5A"
	pkc7 := NewPKCS7()
	xx, _ := pkc7.Encrypt([]byte("13234567890"), []byte(key))
	t.Log(xx)
	mobile, _ := pkc7.Decrypt("b9gAYgUCMkTQg+9v3Wb+NlVoHGksCaJM/Hz+brt+UvE=", []byte(key))
	t.Log(string(mobile))
}

func TestSignWithSha1(t *testing.T) {
	noncestr := "Wm3WZYTPz0wzccnW"
	jsapi_ticket := "sM4AOVdWfPE4DxkXGEs8VMCPGGVi4C3VM0P37wVUCFvkVAy_90u5h9nbSlYy3-Sl-HhTdfl2fzFy1AOcHKP7qg"
	timestamp := "1414587457"
	url := "http://mp.weixin.qq.com?params=value"

	noncestr = fmt.Sprintf("noncestr=%v", noncestr)
	timestamp = fmt.Sprintf("timestamp=%v", timestamp)
	jsapi_ticket = fmt.Sprintf("jsapi_ticket=%v", jsapi_ticket)
	url = fmt.Sprintf("url=%v", url)

	sign, _ := SignWithSha1("&", noncestr, jsapi_ticket, timestamp, url)
	t.Log("sign: ", sign)

}

func TestPKCS1_Generate(t *testing.T) {
	pkcs := NewPKCS1()

	pubKey := &bytes.Buffer{}
	priKey := &bytes.Buffer{}
	out := &RsaKey{
		Pub: pubKey,
		Pri: priKey,
	}
	err := pkcs.Generate(2048, out)
	if err != nil {
		return
	}
	orig := "hello"
	raw, er := pkcs.Encrypt([]byte(orig), pubKey.Bytes())
	if er != nil {
		return
	}
	orig2, err := pkcs.Decrypt(raw, priKey.Bytes())
	if err != nil {
		return
	}
	t.Log("OK: ", string(orig2))
}
