package algorithm

import (
	"encoding/base64"
	"fmt"
	"gitee.com/h79/goutils/common/stringutil"
)

// Crypt
/**
 * 1.第三方回复加密消息给公众平台；
 * 2.第三方收到公众平台发送的消息，验证消息的安全性，并对消息进行解密。
 */
type Crypt struct {
	Token  string
	AesKey []byte
}

// NewCrypt
/**
 * 构造函数
 * @param $token string 公众平台上，开发者设置的token
 * @param $encodingAesKey string 公众平台上，开发者设置的EncodingAESKey
 * @param $appId string 公众平台的appId
 */
func NewCrypt(token, aesKey string) *Crypt {

	ak, _ := base64.StdEncoding.DecodeString(aesKey + "=")

	return &Crypt{
		Token:  token,
		AesKey: ak,
	}
}

// Encrypt
/**
 * 将公众平台回复用户的消息加密打包.
 * <ol>
 *    <li>对要发送的消息进行AES-CBC加密</li>
 *    <li>生成安全签名</li>
 *    <li>将消息密文和安全签名打包成xml格式</li>
 * </ol>
 *
 * @param msg string 公众平台待回复用户的消息，xml格式的字符串
 * @param timeStamp string 时间戳，可以自己生成，也可以用URL参数的timestamp
 * @param nonce string 随机串，可以自己生成，也可以用URL参数的nonce
 *
 */
func (ct *Crypt) Encrypt(msg, timestamp, nonce string) (string, string, error) {

	pc := NewPKCS7()

	en, err := pc.Encrypt([]byte(msg), ct.AesKey)
	if err != nil {
		return "", "", err
	}

	sign := ""
	if sign, err = SignWithSha1("", ct.Token, timestamp, nonce, en); err != nil {
		return "", "", err
	}

	return sign, en, nil
}

// Decrypt
/**
 * 检验消息的真实性，并且获取解密后的明文.
 * <ol>
 *    <li>利用收到的密文生成安全签名，进行签名验证</li>
 *    <li>若验证通过，则提取xml中的加密消息</li>
 *    <li>对消息进行解密</li>
 * </ol>
 *
 * @param signature string 签名串，对应URL参数的msg_signature
 * @param timestamp string 时间戳 对应URL参数的timestamp
 * @param nonce string 随机串，对应URL参数的nonce
 * @param encrypt string 密文
 *
 */
func (ct *Crypt) Decrypt(signature, timestamp, nonce, encrypt string) ([]byte, error) {
	sign, err := SignWithSha1("", ct.Token, timestamp, nonce, encrypt)
	if err != nil {
		return nil, err
	}
	if signature != sign {
		return nil, fmt.Errorf("sign no match")
	}
	pc := NewPKCS7()
	buff, err := pc.Decrypt(encrypt, ct.AesKey)
	if err != nil {
		return nil, err
	}
	return buff, nil
}

// DecryptMsg
/**
 * @return @string 正文 @string appid @error
 */
func (ct *Crypt) DecryptMsg(signature, timestamp, nonce, encrypt string) ([]byte, string, error) {

	buff, err := ct.Decrypt(signature, timestamp, nonce, encrypt)
	if err != nil {
		return nil, "", err
	}
	l := stringutil.BytesToUInt32(buff[0:4])
	return buff[4 : l+4], string(buff[l+4:]), nil
}

// GenerateXmlMsg
/**
 * 生成xml消息
 * @param string $encrypt 加密后的消息密文
 * @param string $signature 安全签名
 * @param string $timestamp 时间戳
 * @param string $nonce 随机字符串
 */
func (ct *Crypt) GenerateXmlMsg(msg, timestamp, nonce string) string {
	signature, encrypt, er := ct.Encrypt(msg, timestamp, nonce)
	if er != nil {
		return ""
	}
	format := "<xml>" +
		"<Encrypt><![CDATA[%s]]></Encrypt>" +
		"<MsgSignature><![CDATA[%s]]></MsgSignature>" +
		"<TimeStamp>%s</TimeStamp>" +
		"<Nonce><![CDATA[%s]]></Nonce>" +
		"</xml>"
	return fmt.Sprintf(format, encrypt, signature, timestamp, nonce)
}
