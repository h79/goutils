package algorithm

import (
	"crypto/cipher"
	"encoding/base64"
	"github.com/tjfoc/gmsm/sm4"
)

type Sm4 struct {
}

func (s *Sm4) Encrypt(raw, key []byte) (string, error) {
	data, err := s.sm4Encrypt(raw, key)
	if err != nil {
		return "", err
	}
	return base64.StdEncoding.EncodeToString(data), nil
}

func (s *Sm4) Decrypt(raw string, key []byte) ([]byte, error) {
	data, err := base64.StdEncoding.DecodeString(raw)
	if err != nil {
		return nil, err
	}
	return s.sm4Decrypt(data, key)
}

// 加密
func (s *Sm4) sm4Encrypt(raw []byte, key []byte) ([]byte, error) {
	enc := NewECB(func(key []byte) (cipher.Block, error) {
		block, err := sm4.NewCipher(key)
		if err != nil {
			return nil, err
		}
		return block, nil
	})
	return enc.Encrypt(raw, key, nil)
}

// 解密
func (s *Sm4) sm4Decrypt(raw []byte, key []byte) ([]byte, error) {
	enc := NewECB(func(key []byte) (cipher.Block, error) {
		block, err := sm4.NewCipher(key)
		if err != nil {
			return nil, err
		}
		return block, nil
	})
	return enc.Decrypt(raw, key, nil)
}
