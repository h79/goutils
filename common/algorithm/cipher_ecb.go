package algorithm

import (
	"crypto/cipher"
	"errors"
)

// CipherECB 模式加密
type CipherECB struct {
	cipherFunc func(key []byte) (cipher.Block, error)
	//keySize    int
	//blockSize  int
	//identifier asn1.ObjectIdentifier
}

func NewECB(cipherFunc func(key []byte) (cipher.Block, error)) CipherECB {
	return CipherECB{cipherFunc: cipherFunc}
}

//// 值大小
//func (ecb CipherECB) KeySize() int {
//	return ecb.keySize
//}

//// oid
//func (ecb CipherECB) OID() asn1.ObjectIdentifier {
//	return ecb.identifier
//}

// Encrypt 加密
func (ecb CipherECB) Encrypt(plaintext, key []byte, iv []byte) ([]byte, error) {

	block, err := ecb.cipherFunc(key)
	if err != nil {
		return nil, errors.New("pkcs/cipher:" + err.Error() + " failed to create cipher")
	}
	// 加密数据补码
	plaintext = pkcs7Padding(plaintext, block.BlockSize())

	// 需要保存的加密数据
	encrypted := make([]byte, len(plaintext))
	mode := NewECBEncrypt(block)
	mode.CryptBlocks(encrypted, plaintext)

	return encrypted, nil
}

// Decrypt 解密
func (ecb CipherECB) Decrypt(ciphertext, key []byte, iv []byte) ([]byte, error) {
	block, err := ecb.cipherFunc(key)
	if err != nil {
		return nil, err
	}

	// 判断数据是否为填充数据
	l := len(ciphertext)
	if l == 0 || l%block.BlockSize() != 0 {
		return nil, errors.New("pkcs/cipher: invalid padding")
	}

	plaintext := make([]byte, len(ciphertext))
	mode := NewECBDecrypter(block)
	mode.CryptBlocks(plaintext, ciphertext)

	return pkcs7UnPadding(plaintext), nil
}
