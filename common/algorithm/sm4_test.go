package algorithm

import (
	"encoding/base64"
	"testing"
)

func TestSM4(t *testing.T) {
	key := "TvaBgrhE46sft3nZlfe7xw=="

	kk, _ := base64.StdEncoding.DecodeString(key)

	//	ct := `{"language”:"zh-CN","orderId":"QIAO-20200618-004"}TvaBgrhE46sft3nZlfe7xw==`
	//ct := `{"language”:“zh-CN”,“orderId”:“QIAO-20200618-004”}TvaBgrhE46sft3nZlfe7xw==`
	ct := "{\"language\":\"zh-CN\",\"orderId\":\"QIAO-20200618-004\"}TvaBgrhE46sft3nZlfe7xw=="
	res := "efB6PnjDpgHG4xfrvYlXonyBuMJoGTynkfasopHvbl2u3nmNeP+rznA3DyRwb/2GeZL7I3rL6HKD5+Tv3Uy6x8jIDISbYG8Bg14caH2flYE="

	sm4 := Sm4{}
	ret, err := sm4.Encrypt([]byte(ct), kk)
	if err != nil {
		return
	}
	t.Log(res == ret)
}

func TestSM42(t *testing.T) {
	key := "TvaBgrhE46sft3nZ"
	//	ct := `{"language”:"zh-CN","orderId":"QIAO-20200618-004"}TvaBgrhE46sft3nZlfe7xw==`
	//ct := `{"language”:“zh-CN”,“orderId”:“QIAO-20200618-004”}TvaBgrhE46sft3nZlfe7xw==`
	ct := "{\"language\":\"zh-CN\",\"orderId\":\"QIAO-20200618-004\"}TvaBgrhE46sft3nZ"
	//res := "efB6PnjDpgHG4xfrvYlXonyBuMJoGTynkfasopHvbl2u3nmNeP+rznA3DyRwb/2GeZL7I3rL6HKD5+Tv3Uy6x8jIDISbYG8Bg14caH2flYE="

	sm4 := Sm4{}
	ret, err := sm4.Encrypt([]byte(ct), []byte(key))
	if err != nil {
		return
	}

	ret1, err := sm4.Decrypt(ret, []byte(key))

	t.Log(string(ret1))
}
