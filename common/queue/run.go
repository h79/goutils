package queue

import (
	"gitee.com/h79/goutils/common/system"
	"sync"
	"time"
)

type Call struct {
	PopCount int
	Duration time.Duration
	Wait     func(dur time.Duration)
	Begin    func(count int) any
	Process  func(be any, data any)
	Quited   func(lock sync.Locker, qu Queue)
}

func Run(c Call, lock sync.Locker, qu Queue) {
	var count int
	var popCount = c.PopCount
	if popCount <= 0 {
		popCount = 10
	}
	if c.Wait == nil {
		c.Wait = func(dur time.Duration) {
			time.Sleep(dur)
		}
	}
	if c.Quited == nil {
		c.Quited = func(lock sync.Locker, qu Queue) {
		}
	}
	ten := make([]any, popCount)
	for {
		count = 0
		lock.Lock()
		for i := 0; i < popCount; i++ {
			v := qu.Pop()
			if v == nil {
				break
			}
			ten[i] = v
			count++
		}
		lock.Unlock()
		var bn = c.Begin(count)
		for i := 0; i < count; i++ {
			c.Process(bn, ten[i])
		}
		c.Wait(c.Duration)

		select {
		case <-system.Closed():
			c.Quited(lock, qu)
			return
		default:
		}
	}
}
