package queue

import (
	"gitee.com/h79/goutils/common/random"
	"testing"
)

func TestPQueue(t *testing.T) {

	pq := NewPriority()

	pq.Add(&priorityItem{Priority: 40, Value: 40})
	pq.Add(&priorityItem{Priority: 20, Value: 20})
	pq.Add(&priorityItem{Priority: 80, Value: 80})
	pq.Add(&priorityItem{Priority: 33, Value: 33})
	pq.Add(&priorityItem{Priority: 30, Value: 30})
	pq.Add(&priorityItem{Priority: 30, Value: 30})
	pq.Add(&priorityItem{Priority: 60, Value: 60})

	//for pq.Len() > 0 {

	//}
	for pq.Len() > 0 {
		t.Logf("%+v", pq.Peek())
		t.Logf("%+v", pq.Pop())
		t.Logf("----")
		//t.Logf("%+v", heap.Pop(pq))
		//fmt.Printf("%d ", heap.Pop())
	}
	//t.Logf("%+v", pq.Pop())
	//t.Logf("%+v", pq.Pop())
	//t.Logf("%+v", pq.Pop())
	//t.Logf("%+v", pq.Pop())
	//t.Logf("%+v", pq.Pop())
	//t.Logf("%+v", pq.Pop())
	//t.Logf("%+v", pq.Pop())
	//t.Logf("%+v", pq.Pop())
}

func TestSQueue(t *testing.T) {

	pq := NewCapacity(10)
	//t.Logf("%+v", pq.Peek())

	r := random.Random()
	for i := 0; i < 1000; i++ {
		v := r.Int63n(10000)
		pq.Add(&priorityItem{Priority: v, Value: v})
	}
	//
	//pq.Add(&priorityItem{Priority: 20, Value: 20})
	//pq.Add(&priorityItem{Priority: 30, Value: 30})
	//pq.Add(&priorityItem{Priority: 60, Value: 60})
	//pq.Add(&priorityItem{Priority: 201, Value: 201})
	//pq.Add(&priorityItem{Priority: 301, Value: 301})
	//pq.Add(&priorityItem{Priority: 601, Value: 601})
	//pq.Add(&priorityItem{Priority: 202, Value: 202})
	//pq.Add(&priorityItem{Priority: 401, Value: 401})
	//pq.Add(&priorityItem{Priority: 401, Value: 401})
	//pq.Add(&priorityItem{Priority: 401, Value: 401})
	//pq.Add(&priorityItem{Priority: 402, Value: 402})
	//pq.Add(&priorityItem{Priority: 302, Value: 302})
	//pq.Add(&priorityItem{Priority: 602, Value: 602})
	//pq.Add(&priorityItem{Priority: 203, Value: 203})
	//pq.Add(&priorityItem{Priority: 303, Value: 303})
	//pq.Add(&priorityItem{Priority: 502, Value: 202})
	//pq.Add(&priorityItem{Priority: 503, Value: 503})
	//pq.Add(&priorityItem{Priority: 504, Value: 504})
	//pq.Add(&priorityItem{Priority: 502, Value: 501})
	//pq.Add(&priorityItem{Priority: 402, Value: 402})
	//pq.Add(&priorityItem{Priority: 703, Value: 703})
	//pq.Add(&priorityItem{Priority: 302, Value: 302})
	//pq.Add(&priorityItem{Priority: 602, Value: 602})
	//pq.Add(&priorityItem{Priority: 203, Value: 203})
	//pq.Add(&priorityItem{Priority: 303, Value: 303})
	//pq.Add(&priorityItem{Priority: 603, Value: 603})
	i := 0
	for pq.Len() > 0 {
		t.Logf("%+v,%d", pq.Peek(), i)
		t.Logf("%+v", pq.Pop())
		t.Logf("----")
		i++
		//t.Logf("%+v", pq.Pop())
		//fmt.Printf("%d ", heap.Pop())
	}
	//t.Logf("%+v", pq.Pop())
	//t.Logf("%+v", pq.Pop())
	//t.Logf("%+v", pq.Pop())
	//t.Logf("%+v", pq.Pop())
}
