package queue

import "time"

var _ Queue = (*Capacity)(nil)

// Capacity 固定长度的队列
type Capacity struct {
	l        []IPriority
	location *time.Location
	count    int
}

func NewCapacity(capacity int) *Capacity {
	if capacity <= 0 {
		capacity = minCapacity
	}
	if capacity > maxCapacity {
		capacity = maxCapacity
	}
	return &Capacity{
		location: time.Local,
		l:        make([]IPriority, capacity),
	}
}

func (sq *Capacity) SetLocation(l *time.Location) {
	sq.location = l
}

func (sq *Capacity) Len() int {
	return sq.count
}

func (sq *Capacity) Push(elem any) error {
	if sq.count == len(sq.l) {
		sq.resize()
	}
	p := NewPriorityItem(elem, WithUnixNano(sq.location))
	index := sq.getInsertIndex(p.PValue(), 0, sq.count-1)
	copy(sq.l[index+1:], sq.l[index:])
	sq.l[index] = p
	sq.count++
	return nil
}

func (sq *Capacity) Add(elem any) error {
	return sq.Push(elem)
}

func (sq *Capacity) Peek() any {
	if sq.Empty() {
		return nil
	}
	return sq.l[0].GValue()
}

// Get index -1 refers to the last.
func (sq *Capacity) Get(index int) any {
	if index < 0 || index >= sq.count {
		return nil
	}
	return sq.l[index].GValue()
}

// Find return index -1 not found.
func (sq *Capacity) Find(fn IndexFunc) (any, int) {
	for i := 0; i < sq.count; i++ {
		if fn(sq.l[i], i) {
			return sq.l[i], i
		}
	}
	return nil, -1
}

func (sq *Capacity) Foreach(fn IndexFunc) {
	for i := 0; i < sq.count; i++ {
		if fn(sq.l[i], i) {
			return
		}
	}
}

func (sq *Capacity) Pop() any {
	if sq.Empty() {
		return nil
	}
	ret := sq.l[0]
	copy(sq.l[0:], sq.l[1:])
	sq.count--
	if sq.count < 0 {
		sq.count = 0
	} else {
		sq.l[sq.count] = nil
	}
	return ret.GValue()
}

func (sq *Capacity) Empty() bool {
	return sq.count <= 0
}

func (sq *Capacity) ToList() []any {
	var (
		buf = make([]any, sq.count)
	)
	for i := 0; i < sq.count; i++ {
		buf[i] = sq.Get(i)
	}
	return buf
}

func (sq *Capacity) resize() {
	if sq.count >= maxCapacity {
		return
	}
	size := sq.count << 1
	if size > maxCapacity {
		size = maxCapacity
	}
	newBuf := make([]IPriority, size)

	copy(newBuf, sq.l[:sq.count])

	sq.l = newBuf
}

func (sq *Capacity) getInsertIndex(priority int64, leftIndex, rightIndex int) (index int) {
	if sq.count <= 0 {
		// 如果当前优先级切片没有元素，则插入的index就是0
		return 0
	}

	length := rightIndex - leftIndex
	if sq.l[leftIndex].PValue() >= priority {
		// 如果当前切片中最小的元素都超过了插入的优先级，则插入位置应该是最左边
		return leftIndex
	}

	if sq.l[rightIndex].PValue() <= priority {
		// 如果当前切片中最大的元素都没超过插入的优先级，则插入位置应该是最右边
		return rightIndex + 1
	}

	if length == 1 && sq.l[leftIndex].PValue() < priority && sq.l[rightIndex].PValue() >= priority {
		// 如果插入的优先级刚好在仅有的两个优先级之间，则中间的位置就是插入位置
		return leftIndex + 1
	}

	middleVal := sq.l[leftIndex+length/2].PValue()

	// 这里用二分法递归的方式，一直寻找正确的插入位置
	if priority <= middleVal {
		return sq.getInsertIndex(priority, leftIndex, leftIndex+length/2)
	}
	return sq.getInsertIndex(priority, leftIndex+length/2, rightIndex)
}
