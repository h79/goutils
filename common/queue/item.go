package queue

import (
	"reflect"
	"time"
)

// priorityItem 优先级
type priorityItem struct {
	Value    any
	Priority int64
}

func (it *priorityItem) GValue() any {
	return it.Value
}

func (it *priorityItem) PValue() int64 {
	return it.Priority
}

type GeneratePriorityFn func() int64

func WithUnixNano(l *time.Location) GeneratePriorityFn {
	return func() int64 {
		return time.Now().In(l).UnixNano()
	}
}

func NewPriorityItem(elem any, fn GeneratePriorityFn) IPriority {
	if p, ok := elem.(*priorityItem); ok {
		return p
	}
	if it, ok := elem.(priorityItem); ok {
		return &it
	}
	val := reflect.ValueOf(elem)
	if val.CanInterface() && val.Type().Implements(PriorityType) {
		fm := val.Interface().(IPriority)
		return fm
	}
	return &priorityItem{Value: elem, Priority: fn()}
}
