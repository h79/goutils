package queue

import "errors"

var ErrIsReleased = errors.New("size is zero")
var ErrIsFull = errors.New("is full")

const minCapacity = 16
const maxCapacity = 65535
