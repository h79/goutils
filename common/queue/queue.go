package queue

import (
	"reflect"
)

type Queue interface {
	Add(elem any) error
	Push(elem any) error
	Pop() any
	Peek() any
	Empty() bool
	Len() int
	Find(fn IndexFunc) (any, int)
	Foreach(fn IndexFunc)
}

type IndexFunc func(v any, index int) bool

var PriorityType = reflect.TypeOf((*IPriority)(nil)).Elem()

type IPriority interface {
	GValue() any
	PValue() int64 //优先级值，越小越前
}
