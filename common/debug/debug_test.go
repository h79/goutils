package debug

import (
	"fmt"
	"testing"
)

func TestDebug_WithDetail(t *testing.T) {
	d := Debug{}
	d.WithDetail("ADDD")
	d.WithDetailFormat("ADDD=%d", 123242)
	d.WithError(fmt.Errorf("xxxxxeee"))

	t.Log(d.String())
}
