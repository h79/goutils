package debug

import (
	"errors"
	"fmt"
	"gitee.com/h79/goutils/common/config"
	"gitee.com/h79/goutils/common/json"
	"runtime/debug"
	"strings"
)

type Debug struct {
	Code   int32  `json:"code"`
	Title  string `json:"title"`
	Name   string `json:"name"`   //服务名称
	Module string `json:"module"` //模板名
	Detail string `json:"detail"` //简单描述
	Stack  string `json:"stack"`  //调用堆栈
	More   string `json:"more"`   //更多
	Level  Level  `json:"level"`  //级别（严重程度)
	ErrMsg string `json:"errMsg"`
	err    error  `json:"-"`
}

type Level int32

const (
	DUnkLevel       = Level(0)
	DNormalLevel    = Level(1)
	DImportantLevel = Level(2)
	DTightLevel     = Level(3) // 紧级
	DFatalLevel     = Level(4) // 致命
)

func NewStack(code int32) *Debug {
	return &Debug{
		Code:  code,
		Name:  config.AppName,
		Stack: string(debug.Stack()),
	}
}

func New(code int32) *Debug {
	return &Debug{
		Code: code,
		Name: config.AppName,
	}
}

func (d *Debug) String() string {
	return json.ToString(d)
}

func (d *Debug) Error() error {
	return d.newErr().err
}

func (d *Debug) newErr() *Debug {
	if d.err == nil && len(d.ErrMsg) > 0 {
		d.err = errors.New(d.ErrMsg)
	}
	return d
}

func (d *Debug) WithDetailFormat(format string, a ...interface{}) *Debug {
	d.Detail = fmt.Sprintf(format, a...)
	return d
}

func (d *Debug) WithDetail(detail string) *Debug {
	d.Detail = detail
	return d
}

func (d *Debug) WithError(er error) *Debug {
	d.err = er
	if er != nil {
		d.ErrMsg = er.Error()
	}
	return d
}

func (d *Debug) WithErrMsg(er string) *Debug {
	d.ErrMsg = er
	return d.newErr()
}

func (d *Debug) WithName(name string) *Debug {
	d.Name = name
	return d
}

func (d *Debug) WithModule(module string) *Debug {
	d.Module = module
	return d
}

func (d *Debug) WithLevel(l Level) *Debug {
	d.Level = l
	return d
}

func (d *Debug) WithTitle(title string) *Debug {
	d.Title = title
	return d
}

func (d *Debug) WithStack() *Debug {
	d.Stack = string(debug.Stack())
	return d
}

func (d *Debug) WithMoreFormat(more string, a ...interface{}) *Debug {
	d.More = fmt.Sprintf(more, a...)
	return d
}

func (d *Debug) WithMore(more string) *Debug {
	d.More = more
	return d
}

func (d *Debug) MoreTo() string {
	if len(d.Stack) == 0 {
		return d.More
	}
	if len(d.More) == 0 {
		return d.Stack
	}
	b := strings.Builder{}
	b.WriteString(d.More)
	b.WriteString("\r\n\r\nStack---------------\r\n")
	b.WriteString(d.Stack)
	return b.String()
}
