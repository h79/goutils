package archive

import (
	"fmt"
	"gitee.com/h79/goutils/common/archive/e7z"
	"gitee.com/h79/goutils/common/archive/gzip"
	"gitee.com/h79/goutils/common/archive/tar"
	"gitee.com/h79/goutils/common/archive/targz"
	"gitee.com/h79/goutils/common/archive/tarxz"
	"gitee.com/h79/goutils/common/archive/zip"
	fileconfig "gitee.com/h79/goutils/common/file/config"
	"io"
	"os"
)

type Config struct {
	Path   string
	File   string
	Format string
}

// Archive represents a compression archive files from disk can be written to.
type Archive interface {
	Close() error
	Add(f fileconfig.File, stream ...fileconfig.ReaderStream) error
}

type Builder interface {
	Ext() string
	New(w io.WriteCloser, conf Config) Archive
	Coping(r *os.File, w io.Writer, conf Config) (Archive, error)
}

func Register(format string, obj Builder) {
	builders[format] = obj
}

func UnRegister(format string) {
	delete(builders, format)
}

func Support(format string) bool {
	_, ok := builders[format]
	return ok
}

func Ext(format string) string {
	o, ok := builders[format]
	if ok {
		return o.Ext()
	}
	return ""
}

var builders = map[string]Builder{
	"zip":    &zipBuilder{},
	"tgz":    &tgzBuilder{ext: ".tgz"},
	"tar.gz": &tgzBuilder{ext: ".tar.gz"},
	"tar":    &tarBuilder{},
	"gz":     &gzBuilder{},
	"txz":    &txzBuilder{ext: ".txz"},
	"tar.xz": &txzBuilder{ext: ".tar.xz"},
	"7z":     &e7zBuilder{},
}

func NewBuilder(format string) Builder {
	switch format {
	case "zip":
		return &zipBuilder{}
	case "tgz":
		return &tgzBuilder{ext: ".tgz"}
	case "tar.gz":
		return &tgzBuilder{ext: ".tar.gz"}
	case "tar":
		return &tarBuilder{}
	case "gz":
		return &gzBuilder{}
	case "txz":
		return &txzBuilder{ext: ".txz"}
	case "tar.xz":
		return &txzBuilder{ext: ".tar.xz"}
	case "7z":
		return &e7zBuilder{}
	}
	return nil
}

func NewMultiBuilder(ext string, objs ...Builder) Builder {
	return &multiBuilder{
		ext:  ext,
		objs: append([]Builder{}, objs...),
	}
}

type multiBuilder struct {
	ext  string
	objs []Builder
}

func (o *multiBuilder) Ext() string {
	return o.ext
}

func (o *multiBuilder) New(w io.WriteCloser, conf Config) Archive {
	var ars []Archive
	for i := range o.objs {
		ar := o.objs[i].New(w, conf)
		ars = append(ars, ar)
	}
	return NewMulti(ars)
}

func (o *multiBuilder) Coping(r *os.File, w io.Writer, conf Config) (Archive, error) {
	var ars []Archive
	for i := range o.objs {
		ar, err := o.objs[i].Coping(r, w, conf)
		if err != nil {
			ars = append(ars, ar)
		}
	}
	return NewMulti(ars), nil
}

type e7zBuilder struct {
}

func (*e7zBuilder) Ext() string {
	return ".7z"
}

func (*e7zBuilder) New(w io.WriteCloser, conf Config) Archive {
	return e7z.New(w, conf.Path, conf.File)
}

func (*e7zBuilder) Coping(r *os.File, w io.Writer, conf Config) (Archive, error) {
	return nil, fmt.Errorf("not support")
}

type zipBuilder struct {
}

func (*zipBuilder) Ext() string {
	return ".zip"
}
func (*zipBuilder) New(w io.WriteCloser, conf Config) Archive {
	return zip.New(w)
}

func (*zipBuilder) Coping(r *os.File, w io.Writer, conf Config) (Archive, error) {
	return zip.Copying(r, w)
}

type tgzBuilder struct {
	ext string
}

func (t *tgzBuilder) Ext() string {
	return t.ext
}
func (*tgzBuilder) New(w io.WriteCloser, conf Config) Archive {
	return targz.New(w)
}

func (*tgzBuilder) Coping(r *os.File, w io.Writer, conf Config) (Archive, error) {
	return targz.Copying(r, w)
}

type tarBuilder struct {
}

func (t *tarBuilder) Ext() string {
	return ".tar"
}
func (*tarBuilder) New(w io.WriteCloser, conf Config) Archive {
	return tar.New(w)
}

func (*tarBuilder) Coping(r *os.File, w io.Writer, conf Config) (Archive, error) {
	return tar.Copying(r, w)
}

type gzBuilder struct {
}

func (t *gzBuilder) Ext() string {
	return ".gz"
}
func (*gzBuilder) New(w io.WriteCloser, conf Config) Archive {
	return gzip.New(w)
}

func (*gzBuilder) Coping(r *os.File, w io.Writer, conf Config) (Archive, error) {
	return nil, fmt.Errorf("not support")
}

type txzBuilder struct {
	ext string
}

func (t *txzBuilder) Ext() string {
	return t.ext
}
func (*txzBuilder) New(w io.WriteCloser, conf Config) Archive {
	return tarxz.New(w)
}

func (*txzBuilder) Coping(r *os.File, w io.Writer, conf Config) (Archive, error) {
	return nil, fmt.Errorf("not support")
}

// New archive.
func New(w io.WriteCloser, conf Config) (Archive, error) {
	if n, ok := builders[conf.Format]; ok {
		return n.New(w, conf), nil
	}
	return nil, fmt.Errorf("invalid archive format: %s", conf.Format)
}

// Copying copies the source archive into a new one, which can be appended at.
// Source needs to be in the specified format.
func Copying(r *os.File, w io.Writer, conf Config) (Archive, error) {
	if n, ok := builders[conf.Format]; ok {
		return n.Coping(r, w, conf)
	}
	return nil, fmt.Errorf("invalid archive format: %s", conf.Format)
}
