// Package targz implements the Archive interface providing tar.gz archiving
// and compression.
package targz

import (
	"compress/gzip"
	"gitee.com/h79/goutils/common/archive/tar"
	fileconfig "gitee.com/h79/goutils/common/file/config"
	"io"
)

// Archive as tar.gz.
type Archive struct {
	gw *gzip.Writer
	tw *tar.Archive
}

// New tar.gz archive.
func New(target io.Writer) Archive {
	// the error will be nil since the compression level is valid
	writer, _ := gzip.NewWriterLevel(target, gzip.BestCompression)
	tw := tar.New(writer)
	return Archive{
		gw: writer,
		tw: &tw,
	}
}

func Copying(source io.Reader, target io.Writer) (Archive, error) {
	// the error will be nil since the compression level is valid
	writer, _ := gzip.NewWriterLevel(target, gzip.BestCompression)
	src, err := gzip.NewReader(source)
	if err != nil {
		return Archive{}, err
	}
	tw, err := tar.Copying(src, writer)
	return Archive{
		gw: writer,
		tw: &tw,
	}, err
}

// Close all closeables.
func (a Archive) Close() error {
	if a.tw != nil {
		err := a.tw.Close()
		a.tw = nil
		if err != nil {
			return err
		}
	}
	if a.gw != nil {
		err := a.gw.Close()
		a.gw = nil
		return err
	}
	return nil
}

// Add file to the archive.
func (a Archive) Add(f fileconfig.File, stream ...fileconfig.ReaderStream) error {
	return a.tw.Add(f, stream...)
}
