package e7z

import (
	"context"
	"fmt"
	fileconfig "gitee.com/h79/goutils/common/file/config"
	"io"
	"os"
	"os/exec"
	"runtime"
)

// Archive as 7z.
type Archive struct {
	Path     string
	Filename string
}

// New tar archive.
func New(target io.WriteCloser, path, filename string) Archive {
	if target != nil {
		target.Close()
		os.Remove(filename)
	}
	return Archive{Path: path, Filename: filename}
}

// Close all closeables.
func (a Archive) Close() error {
	return nil
}

// Add file to the archive.
func (a Archive) Add(f fileconfig.File, stream ...fileconfig.ReaderStream) error {
	err := a.cmd(f)
	if err != nil {
		return err
	}
	return a.Stream(f, stream...)
}

func (a Archive) cmd(f fileconfig.File) error {
	cmd := "7z"
	switch runtime.GOOS {
	case "windows":
		cmd = "7z.exe"
	}
	return run(context.Background(), []string{cmd, "a", "-r", a.Filename, f.Source}, nil, "")
}

func (a Archive) Stream(f fileconfig.File, stream ...fileconfig.ReaderStream) error {
	if len(stream) == 0 {
		return nil
	}
	if f.Dir {
		return nil
	}
	file, err := os.Open(f.Source) // #nosec
	if err != nil {
		return fmt.Errorf("%s: %w", f.Source, err)
	}
	defer func(file *os.File) {
		err = file.Close()
		if err != nil {
		}
	}(file)
	for i := range stream {
		stream[i].OnReader(file)
	}
	return nil
}

func run(ctx context.Context, command, env []string, dir string) error {
	cmd := exec.CommandContext(ctx, command[0], command[1:]...)
	cmd.Env = env
	cmd.Dir = dir
	out, err := cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf("%w: %s", err, string(out))
	}
	return nil
}
