package e7z

import (
	"gitee.com/h79/goutils/common/file"
	"gitee.com/h79/goutils/common/file/config"
	"github.com/stretchr/testify/require"
	"path/filepath"
	"testing"
)

func Test7Z(t *testing.T) {
	tmp := t.TempDir()
	filename := filepath.Join(tmp, "test.7z")
	//f, err := os.Create()
	//require.NoError(t, err)
	//defer f.Close() // nolint: errcheck
	archive := New(nil, tmp, filename)
	defer archive.Close() // nolint: errcheck

	require.NoError(t, archive.Add(config.File{
		Source:      "../testdata/regular.txt",
		Destination: "regular.txt",
	}))
	require.NoError(t, archive.Add(config.File{
		Source:      "../testdata/sub1",
		Destination: "sub1",
	}))

	file.CopyFile(filename, "test.7z")
}
