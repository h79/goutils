package archive

import (
	fileconfig "gitee.com/h79/goutils/common/file/config"
)

type multiArchive struct {
	ars []Archive
}

func NewMulti(ar []Archive) Archive {
	return &multiArchive{ars: ar}
}

// Close al.
func (a *multiArchive) Close() error {
	for i := range a.ars {
		err := a.ars[i].Close()
		if err != nil {
			return err
		}
	}
	return nil
}

// Add file to the archive.
func (a *multiArchive) Add(f fileconfig.File, stream ...fileconfig.ReaderStream) error {
	for i := range a.ars {
		err := a.ars[i].Add(f, stream...)
		if err != nil {
			return err
		}
	}
	return nil
}
