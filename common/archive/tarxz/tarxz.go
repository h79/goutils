// Package tarxz implements the Archive interface providing tar.xz archiving
// and compression.
package tarxz

import (
	"gitee.com/h79/goutils/common/archive/tar"
	fileconfig "gitee.com/h79/goutils/common/file/config"
	"io"

	"github.com/ulikunitz/xz"
)

// Archive as tar.xz.
type Archive struct {
	xzw *xz.Writer
	tw  *tar.Archive
}

// New tar.xz archive.
func New(target io.Writer) Archive {
	xzw, _ := xz.WriterConfig{DictCap: 16 * 1024 * 1024}.NewWriter(target)
	tw := tar.New(xzw)
	return Archive{
		xzw: xzw,
		tw:  &tw,
	}
}

// Close all closeables.
func (a Archive) Close() error {
	if a.tw != nil {
		err := a.tw.Close()
		a.tw = nil
		if err != nil {
			return err
		}
	}
	if a.xzw != nil {
		err := a.xzw.Close()
		a.xzw = nil
		if err != nil {
			return err
		}
	}
	return nil
}

// Add file to the archive.
func (a Archive) Add(f fileconfig.File, stream ...fileconfig.ReaderStream) error {
	return a.tw.Add(f, stream...)
}
