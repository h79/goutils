package logger

import (
	"gitee.com/h79/goutils/common/tag"
	"go.uber.org/zap/zapcore"
	"reflect"
	"strconv"
	"strings"
)

type Decorate struct {
	S string
	E string
}

func (d *Decorate) UseObject() {
	d.S = "{"
	d.E = "}"
}

func (d *Decorate) UseSlice() {
	d.S = "["
	d.E = "]"
}

func ObjectTo(o any) string {
	d := Decorate{}
	s := objectDecorate(o, &d)
	if d.S != "" {
		return d.S + s + d.E
	}
	return s
}

func NObjectTo(o any) string {
	if !ndebugEnabled {
		return ""
	}
	return DObjectTo(o)
}

func DObjectTo(o any) string {
	if L().Core().Enabled(zapcore.DebugLevel) {
		return ObjectTo(o)
	}
	return ""
}

func WObjectTo(o any) string {
	if L().Core().Enabled(zapcore.WarnLevel) {
		return ObjectTo(o)
	}
	return ""
}

func IObjectTo(o any) string {
	if L().Core().Enabled(zapcore.InfoLevel) {
		return ObjectTo(o)
	}
	return ""
}

func EObjectTo(o any) string {
	if L().Core().Enabled(zapcore.ErrorLevel) {
		return ObjectTo(o)
	}
	return ""
}

func objectDecorate(o any, d *Decorate) string {
	b := strings.Builder{}
	objectTo(&b, o, d)
	return b.String()
}

func objectTo(b *strings.Builder, o any, d *Decorate) {
	value := reflect.ValueOf(o)
	switch value.Kind() {
	case reflect.Ptr:
		if value.IsNil() {
			b.WriteString("")
			return
		}
		v := value.Elem()
		objectTo(b, v.Interface(), d)
	case reflect.Struct:
		d.UseObject()
		structTo(b, value)
	case reflect.Slice:
		d.UseSlice()
		for i := 0; i < value.Len(); i++ {
			if i > 0 {
				b.WriteString(",")
			}
			b.WriteString(objectString(value.Index(i), &Decorate{}))
		}
	case reflect.Map:
		d.UseObject()
		k := value.MapKeys()
		for i := range k {
			if i > 0 {
				b.WriteString(",")
			}
			b.WriteString(objectString(k[i], &Decorate{}))
			b.WriteString(":")
			b.WriteString(objectString(value.MapIndex(k[i]), &Decorate{}))
		}
	default:
		b.WriteString(objectString(value, d))
	}
}

func structTo(b *strings.Builder, value reflect.Value) {
	begin := 0
	t := value.Type()
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		if field.PkgPath != "" {
			continue
		}
		l := field.Tag.Get("log")
		if l == "-" {
			continue
		}
		name, opts := tag.Parse(l)
		omitempty := opts.Has("omitempty")
		val := value.FieldByName(field.Name)

		de := Decorate{}
		vv := objectString(val, &de)
		if omitempty && (vv == "" || vv == "\"\"" || vv == "false" || vv == "0") {
			continue
		}
		if begin > 0 {
			b.WriteString(",")
		}
		begin++
		b.WriteString("\"")
		b.WriteString(name)
		b.WriteString("\":")
		if de.S != "" {
			b.WriteString(de.S)
		}
		b.WriteString(vv)
		if de.E != "" {
			b.WriteString(de.E)
		}
	}
}

func objectString(val reflect.Value, d *Decorate) string {
	switch val.Kind() {
	case reflect.String:
		return strconv.QuoteToASCII(val.String())
	case reflect.Int:
		fallthrough
	case reflect.Int8:
		fallthrough
	case reflect.Int16:
		fallthrough
	case reflect.Int32:
		fallthrough
	case reflect.Int64:
		return strconv.FormatInt(val.Int(), 10)

	case reflect.Uint:
		fallthrough
	case reflect.Uint8:
		fallthrough
	case reflect.Uint16:
		fallthrough
	case reflect.Uint32:
		fallthrough
	case reflect.Uint64:
		return strconv.FormatUint(val.Uint(), 10)
	case reflect.Float32:
		fallthrough
	case reflect.Float64:
		return strconv.FormatFloat(val.Float(), 'f', 10, 64)

	case reflect.Bool:
		if val.Bool() {
			return "true"
		} else {
			return "false"
		}
	case reflect.Invalid:
		return strconv.FormatUint(0, 10)
	case reflect.Interface:
		return objectDecorate(val.Interface(), d)
	case reflect.Struct:
		return objectDecorate(val.Interface(), d)
	case reflect.Slice:
		return objectDecorate(val.Interface(), d)
	case reflect.Chan:
	case reflect.Func:
	case reflect.Pointer:
		if !val.IsNil() {
			v := val.Elem()
			return objectDecorate(v.Interface(), d)
		}
	case reflect.Map:
		return objectDecorate(val.Interface(), d)
	case reflect.UnsafePointer:
	default:
	}
	return ""
}
