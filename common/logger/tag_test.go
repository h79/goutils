package logger

import "testing"

type Event struct {
	Log       string `log:"log"`
	TimeStamp int64  `log:"TimeStamp,omitempty"`
}
type info struct {
	Info  string            `log:"info"` //企业微信CorpID
	Event *Event            `log:"Event,omitempty"`
	Id    []string          `log:"id,omitempty"`
	Map   map[string]string `log:"map,omitempty"`
	In    interface{}       `log:"in,omitempty"`
}

func TestObjectTo(t *testing.T) {
	e := &info{Info: "2333", Event: &Event{TimeStamp: 133}, Id: []string{"133", "1333"}, Map: map[string]string{"xxx": "yyy", "aa": "bbb"}, In: Event{}}
	t.Logf("%s", ObjectTo(e))

}
