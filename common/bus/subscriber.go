package bus

type EventChan chan Event

// Subscriber ch is func or chan.(EventChan)
type Subscriber interface {
	SubscribeEventChan(topic string, ch EventChan) error
	Subscribe(topic string, ch interface{}) error
	SubscribeAsync(topic string, ch interface{}, transactional bool) error
	SubscribeOnce(topic string, ch interface{}) error
	SubscribeOnceAsync(topic string, ch interface{}) error
	UnSubscribe(topic string, ch interface{})
}
