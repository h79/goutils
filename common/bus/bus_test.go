package bus

import (
	"testing"
	"time"
)

type D struct {
	Name string
}

func TestBus(t *testing.T) {
	bus := New()

	ch := make(EventChan)
	err := bus.Subscribe("TEST", ch)
	if err != nil {
		return
	}
	err = bus.Subscribe("TEST", func(arg Event) {
		t.Logf("fn= %v", arg)
	})
	if err != nil {
		return
	}
	//bus.UnSubscribe("TEST", ch)

	go func() {
		for {
			select {
			case data := <-ch:
				t.Logf("out= %v", data)
			}
		}
	}()

	bus.Publish("TEST", Event{Cmd: "1", Data: D{Name: "1"}})
	bus.Publish("TEST", Event{Cmd: "2", Data: D{Name: "2"}})
	bus.Publish("TEST", Event{Cmd: "3", Data: D{Name: "3"}})
	bus.Publish("TEST", Event{Cmd: "4", Data: D{Name: "4"}})
	bus.Publish("TEST", Event{Cmd: "5", Data: D{Name: "5"}})

	time.Sleep(time.Second * 5)
}

func TestFunc(t *testing.T) {
	bus := New()

	err := bus.Subscribe("TEST", func(arg Event) {
		t.Logf("out= %v", arg)
	})
	if err != nil {
		return
	}

	bus.Publish("TEST", Event{Cmd: "1", Data: D{Name: "1"}})

	time.Sleep(time.Second * 5)
}
