package bus

type Event struct {
	Cmd  string
	Data interface{}
}
