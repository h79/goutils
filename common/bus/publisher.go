package bus

type Publisher interface {
	Publish(topic string, ev Event)
	PublishArgs(topic string, args ...interface{})
}
