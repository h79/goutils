package yaml

import (
	"gitee.com/h79/goutils/common/file"
	"gitee.com/h79/goutils/common/result"
	"gopkg.in/yaml.v3"
	"os"
)

func Write(name string, data interface{}, perm os.FileMode) error {
	return file.WriteFileName(name, perm, func(w *os.File) error {
		enc := yaml.NewEncoder(w)
		enc.SetIndent(2)
		if err := enc.Encode(data); err != nil {
			return result.Errorf(result.ErrYaml, "Encode %s yaml failed,err: %v", name, err).Log()
		}
		return nil
	})
}

func Read(name string, data interface{}) error {
	_, err := file.ReadFileName(name, func(r *os.File) error {
		dec := yaml.NewDecoder(r)
		if err := dec.Decode(data); err != nil {
			return result.Errorf(result.ErrYaml, "Decode %s yaml failed,err: %v", name, err).Log()
		}
		return nil
	})
	return err
}

type coder struct {
}

func (j *coder) Unmarshal(content []byte, v interface{}) error {
	return yaml.Unmarshal(content, v)
}

func (j *coder) Marshal(v interface{}) ([]byte, error) {
	return yaml.Marshal(v)
}

var DefaultCoder = &coder{}
