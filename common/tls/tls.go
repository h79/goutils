package tls

import (
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"gitee.com/h79/goutils/common/result"
	"golang.org/x/crypto/pkcs12"
	"os"
)

type Tls struct {
	Key      string `json:"key" yaml:"key" xml:"key"` //true,false,skip-verify,preferred
	CaFile   string `json:"caFile" yaml:"caFile" xml:"caFile"`
	CertFile string `json:"certFile" yaml:"certFile" xml:"certFile"`
	KeyFile  string `json:"keyFile" yaml:"keyFile" xml:"keyFile"`
}

type ServerPubKey struct {
	Key     string `json:"Key" yaml:"Key" xml:"Key"`
	PemFile string `json:"pemFile" yaml:"pemFile" xml:"pemFile"`
}

func (t *Tls) GetCredential() (tls.Certificate, *x509.CertPool, error) {
	if t.CaFile == "" || t.CertFile == "" || t.KeyFile == "" {
		return tls.Certificate{}, nil, errors.New("param error")
	}
	cert, err := tls.LoadX509KeyPair(t.CertFile, t.KeyFile)
	if err != nil {
		return tls.Certificate{}, nil, err
	}

	certPool := x509.NewCertPool()
	ca, err := os.ReadFile(t.CaFile)
	if err != nil {
		return tls.Certificate{}, nil, err
	}

	if ok := certPool.AppendCertsFromPEM(ca); !ok {
		return tls.Certificate{}, nil, err
	}
	return cert, certPool, nil
}

func GetCertificate(t *Tls) (tls.Certificate, *x509.CertPool, error) {
	if t.Key == "" || t.CaFile == "" || t.CertFile == "" || t.KeyFile == "" {
		return tls.Certificate{}, nil, result.RErrParam
	}
	cert, err := tls.LoadX509KeyPair(t.CertFile, t.KeyFile)
	if err != nil {
		return tls.Certificate{}, nil, err
	}

	certPool := x509.NewCertPool()
	ca, err := os.ReadFile(t.CaFile)
	if err != nil {
		return tls.Certificate{}, nil, err
	}

	if ok := certPool.AppendCertsFromPEM(ca); !ok {
		return tls.Certificate{}, nil, err
	}
	return cert, certPool, nil
}

func GetServerPubKey(pk *ServerPubKey) (*rsa.PublicKey, error) {
	if pk.Key == "" || pk.PemFile == "" {
		return nil, result.RErrParam
	}
	data, err := os.ReadFile(pk.PemFile)
	if err != nil {
		return nil, err
	}
	block, _ := pem.Decode(data)
	if block == nil || block.Type != "PUBLIC KEY" {
		return nil, errors.New("failed to decode PEM block containing public key")
	}
	pub, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return nil, err
	}
	if rsaPubKey, ok := pub.(*rsa.PublicKey); ok {
		return rsaPubKey, nil
	}
	return nil, errors.New("failure")
}

type P12 struct {
	Password string
}

func (p *P12) ReadCertificate(certFile, keyFile, pkcs12File any) (cert tls.Certificate, err error) {
	if certFile == nil && keyFile == nil && pkcs12File == nil {
		return cert, errors.New("cert parse failed or nil")
	}
	var (
		certPem, keyPem []byte
	)
	if certFile != nil && keyFile != nil {
		if _, ok := certFile.([]byte); ok {
			certPem = certFile.([]byte)
		} else {
			certPem, err = os.ReadFile(certFile.(string))
		}
		if _, ok := keyFile.([]byte); ok {
			keyPem = keyFile.([]byte)
		} else {
			keyPem, err = os.ReadFile(keyFile.(string))
		}
		if err != nil {
			return cert, fmt.Errorf("os.ReadFile：%w", err)
		}
	} else if pkcs12File != nil {
		var pfxData []byte
		if _, ok := pkcs12File.([]byte); ok {
			pfxData = pkcs12File.([]byte)
		} else {
			pfxData, err = os.ReadFile(pkcs12File.(string))
			if err != nil {
				return cert, fmt.Errorf("os.ReadFile：%w", err)
			}
		}
		blocks, err := pkcs12.ToPEM(pfxData, p.Password)
		if err != nil {
			return cert, fmt.Errorf("pkcs12.ToPEM：%w", err)
		}
		for _, b := range blocks {
			keyPem = append(keyPem, pem.EncodeToMemory(b)...)
		}
		certPem = keyPem
	}
	if certPem != nil && keyPem != nil {
		cert, err = tls.X509KeyPair(certPem, keyPem)
		if err != nil {
			return cert, fmt.Errorf("tls.LoadX509KeyPair：%w", err)
		}
		return cert, nil
	}
	return cert, errors.New("cert files must all nil or all not nil")
}
