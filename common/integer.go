package common

import (
	"reflect"
	"strconv"
)

type IntegerType interface {
	~int8 | ~int16 | ~int | ~int32 | ~int64 | ~uint8 | ~uint16 | ~uint | ~uint32 | ~uint64 | ~float32 | ~float64
}

func SliceToInt(arr []any) []int {
	return ToInt[int](arr)
}

func SliceToInt64(arr []any) []int64 {
	return ToInt[int64](arr)
}

func ToInt[T IntegerType](arr []any) []T {
	Int := make([]T, 0, len(arr))
	for i := range arr {
		Int = append(Int, T(ToInt64(arr[i])))
	}
	return Int
}

func WhereInt[T IntegerType](arr []any, where func(a any) (T, bool)) []T {
	Int := make([]T, len(arr))
	for i := range arr {
		if ret, ok := where(arr[i]); ok {
			Int = append(Int, ret)
		}
	}
	return Int
}

func ToInt64(value interface{}) int64 {
	reflectValue := reflect.Indirect(reflect.ValueOf(value))
	return ToInt64V2(reflectValue)
}

func ToInt64V2(reflectValue reflect.Value) int64 {
	switch reflectValue.Kind() {
	case reflect.String:
		if i, err := strconv.ParseInt(reflectValue.String(), 10, 64); err == nil {
			return i
		}
		return 0
	case reflect.Int:
		fallthrough
	case reflect.Int8:
		fallthrough
	case reflect.Int16:
		fallthrough
	case reflect.Int32:
		fallthrough
	case reflect.Int64:
		return reflectValue.Int()
	case reflect.Float32:
		fallthrough
	case reflect.Float64:
		return int64(reflectValue.Float())
	default:
		panic("unhandled default case")
	}
	return 0
}

func ToFloat64(value interface{}) float64 {
	reflectValue := reflect.Indirect(reflect.ValueOf(value))
	return ToFloat64V2(reflectValue)
}

func ToFloat64V2(reflectValue reflect.Value) float64 {
	switch reflectValue.Kind() {
	case reflect.String:
		f, _ := strconv.ParseFloat(reflectValue.String(), 4)
		return f
	case reflect.Int:
		fallthrough
	case reflect.Int8:
		fallthrough
	case reflect.Int16:
		fallthrough
	case reflect.Int32:
		fallthrough
	case reflect.Int64:
		return float64(reflectValue.Int())
	case reflect.Float32:
		fallthrough
	case reflect.Float64:
		return reflectValue.Float()
	default:
		panic("unhandled default case")
	}
	return 0.0
}

func ToUInt64(value interface{}) uint64 {
	reflectValue := reflect.Indirect(reflect.ValueOf(value))
	return ToUInt64V2(reflectValue)
}

func ToUInt64V2(reflectValue reflect.Value) uint64 {
	switch reflectValue.Kind() {
	case reflect.String:
		if i, err := strconv.ParseUint(reflectValue.String(), 10, 64); err == nil {
			return i
		}
		return 0
	case reflect.Uint:
		fallthrough
	case reflect.Uint8:
		fallthrough
	case reflect.Uint16:
		fallthrough
	case reflect.Uint32:
		fallthrough
	case reflect.Uint64:
		return reflectValue.Uint()
	default:
		panic("unhandled default case")
	}
	return 0
}
