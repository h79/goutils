package timer

import (
	"gitee.com/h79/goutils/common/system"
	"time"
)

// Ticker
// 定时调用
// Deprecated: please use system.Ctrl
func Ticker(tick time.Duration, fun func(any) bool, param any, funcDefer func(any) bool, paramDefer any) {
	Delay(0, tick, fun, param, funcDefer, paramDefer)
}

// Delay 超时调用
// Deprecated: please use system.Ctrl
func Delay(delay, tick time.Duration, fun func(any) bool, param any, funcDefer func(any) bool, paramDefer any) {
	system.Delay(delay, tick, fun, param, funcDefer, paramDefer)
}

// Call 超时调用
// Deprecated: please use system.Ctrl
func Call(timeout time.Duration, task system.Task) (any, error) {
	return system.RunAfter(timeout, task)
}
