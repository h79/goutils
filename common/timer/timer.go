package timer

import "time"

const FormatYMD = "2006-01-02 15:04:05"
const FormatWMD = "Mon Jan 2 15:04:05 2006"

// CurrentMS 豪秒
func CurrentMS() int64 {
	return time.Now().Local().UnixMilli()
}

// CurrentS 秒
func CurrentS() int64 {
	return time.Now().Local().Unix()
}

func CurrentTime() string {
	return time.Now().Local().Format(FormatYMD)
}

func MillToFormat(msec int64) string {
	t := time.UnixMilli(msec)
	return t.Local().Format(time.RFC3339)
}

func UnixToFormat(second int64) string {
	return TimeFormat(time.Unix(second, 0))
}

func TimeFormat(t time.Time) string {
	return t.Local().Format(FormatYMD)
}

// TimeFormatNoLocal
// 时间转换成日期+时间字符串 (time.Time to "2006-01-02 15:04:05")
func TimeFormatNoLocal(t time.Time) string {
	return t.Format(FormatYMD)
}

// GetTimeUnix
// 获取时间戳   return:1464059756
func GetTimeUnix(t time.Time) int64 {
	return t.Local().Unix()
}

// TodayLast
// 今天最后时间 23：59：59
func TodayLast() time.Time {
	now := time.Now()
	return time.Date(now.Year(), now.Month(), now.Day(), 23, 59, 59, 0, now.Location())
}

func TodayAt(d time.Duration) time.Time {
	return TodayLast().Add(d)
}

// Get24Time
// 获取当日晚上24点（次日0点）的时间
func Get24Time(t time.Time) time.Time {
	date := TimeToDate(t.Add(time.Hour * 24))
	return DateStringToTime(date)
}

// Get24TimeUnix
// 获取当日晚上24点（次日0点）的时间戳
func Get24TimeUnix(t time.Time) int64 {
	t24 := Get24Time(t)
	return t24.Local().Unix()
}

// GetToday24Time
// 获取今天晚上24点（次日0点）的时间
func GetToday24Time() time.Time {
	return Get24Time(time.Now())
}

// GetToday24TimeUnix
// 获取今天晚上24点（次日0点）的时间戳
func GetToday24TimeUnix() int64 {
	return Get24TimeUnix(time.Now())
}

// TimeToDate
// 时间转换成日期字符串 (time.Time to "2006-01-02")
func TimeToDate(t time.Time) string {
	return t.Format("2006-01-02")
}

// GetNowDateString
// 获取当前时间的日期字符串（"2006-01-02"）
func GetNowDateString() string {
	return TimeToDate(time.Now())
}

// GetNowString
// 获取当前的时期+时间字符串
func GetNowString() string {
	return TimeFormat(time.Now())
}

// DateStringToTime
// 日期字符串转换成时间 ("2006-01-02" to time.Time)
func DateStringToTime(d string) time.Time {
	t, _ := time.ParseInLocation("2006-01-02", d, time.Local)
	return t
}

// DateTimeStringToTime
// 日期+时间字符串转换成时间 ("2006-01-02 15:04:05" to time.Time)
func DateTimeStringToTime(dt string) time.Time {
	t, _ := time.ParseInLocation(FormatYMD, dt, time.Local)
	return t
}

// TodayStringToTime
// 时间字符串转换成时间 ("15:04:05" to time.Time)
func TodayStringToTime(dt string) time.Time {
	now := time.Now()
	nowDate := TimeToDate(now)
	return DateTimeStringToTime(nowDate + " " + dt)
}

// IsWeekend
// 是否是周末
func IsWeekend(t time.Time) bool {
	wd := t.Weekday()
	if wd == time.Sunday || wd == time.Saturday {
		return true
	}
	return false
}

// NowNano returns the current Unix time in nanoseconds.
func NowNano() int64 {
	return time.Now().UnixNano()
}

func Seconds(seconds int) time.Duration {
	return time.Duration(seconds) * time.Second
}

func Minutes(minutes int) time.Duration {
	return time.Duration(minutes) * time.Minute
}

func Hours(hours int) time.Duration {
	return time.Duration(hours) * time.Hour
}
