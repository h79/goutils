package timer

import (
	"time"
)

type Expire struct {
	p        int   //精度
	StartIn  int64 `json:"startIn"`
	ExpireIn int64 `json:"expireIn"` //过期时间点
}

func NowExpireWithMillSecond(msec int64) Expire {
	return NowMillExpireWithDuration(time.Millisecond * time.Duration(msec))
}

func NowExpireWithSecond(secondIn int64) Expire {
	return NowExpireWithDuration(time.Second * time.Duration(secondIn))
}

func NowExpireWithMinute(minuteIn int64) Expire {
	return NowExpireWithDuration(time.Minute * time.Duration(minuteIn))
}

func NowExpireWithHour(hourIn int64) Expire {
	return NowExpireWithDuration(time.Hour * time.Duration(hourIn))
}

// NowExpireWithDuration 秒
func NowExpireWithDuration(duration time.Duration) Expire {
	now := time.Now()
	expireIn := now.Add(duration).Unix()
	return Expire{StartIn: now.Unix(), ExpireIn: expireIn}
}

// NowMillExpireWithDuration 豪秒
func NowMillExpireWithDuration(duration time.Duration) Expire {
	now := time.Now()
	expireIn := now.Add(duration).UnixMilli()
	return Expire{p: 1, StartIn: now.UnixMilli(), ExpireIn: expireIn}
}

func (e *Expire) IsExpired() bool {
	return e.IsDiff(0)
}

func (e *Expire) IsDiff(diff int64) bool {
	if e.ExpireIn == -1 { //永久不过期
		return false
	}
	if e.p == 0 {
		return time.Now().Unix()+diff >= e.ExpireIn
	}
	return time.Now().UnixMilli()+diff > e.ExpireIn
}
