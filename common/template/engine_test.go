package template

import "testing"

func TestDefEngine(t *testing.T) {
	var xx = struct {
		When    string
		OrderBy string
	}{
		When:    "1",
		OrderBy: "",
	}
	data, err := DefEngine().LoadString("{{.When}} {{.OrderBy}};").OUTPUT("", &xx)
	if err != nil {
		t.Error("xxx")
		return
	}
	t.Logf("ok= %s", string(data.Bytes()))
}
