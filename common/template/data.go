package template

import (
	"bytes"
)

type Data struct {
	buf bytes.Buffer
}

// Writer interface
func (d *Data) Write(p []byte) (n int, err error) {
	return d.buf.Write(p)
}

func (d *Data) Bytes() []byte {
	return d.buf.Bytes()
}

// Reader interface
func (d *Data) Read(p []byte) (n int, err error) {
	return d.buf.Read(p)
}
