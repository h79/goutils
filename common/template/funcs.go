package template

import (
	"encoding/json"
	"strings"
	"time"
)

func JSONMarshal(v interface{}) string {
	b, err := json.Marshal(v)
	if err != nil {
		return ""
	}
	return string(b)
}

func Trim(v interface{}) string {
	var s = v.(string)
	return strings.Trim(s, " ")
}

func Escape(v interface{}) string {
	// escape tabs
	var str = strings.ReplaceAll(v.(string), "\t", "\\t")
	// replace " with \", and if that results in \\", replace that with \\\"
	str = strings.ReplaceAll(str, "\"", "\\\"")

	return strings.ReplaceAll(str, "\\\\\"", "\\\\\\\"")
}

func TimeCurrent() int64 {
	return time.Now().Unix()
}

func Chain(handlers ...CommandFunc) CommandFunc {
	n := len(handlers)

	if n == 0 {
		return func(data interface{}, opt *Option, handler HandlerFunc) error {
			return handler(data, opt)
		}
	}
	if n == 1 {
		return handlers[0]
	}
	last := n - 1
	return func(data interface{}, opt *Option, handler HandlerFunc) error {

		var (
			chain HandlerFunc
			cur   int
		)
		chain = func(data interface{}, opt *Option) error {
			if cur == last {
				return handler(data, opt)
			}
			cur++
			err := handlers[cur](data, opt, chain)
			cur--
			return err
		}
		return handlers[0](data, opt, chain)
	}
}
