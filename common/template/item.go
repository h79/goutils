package template

type Items struct {
	ItMap map[string]Item `json:"items"` //[appid]Item
}

type Item struct {
	Xml  ContentMap `json:"xml"`
	Json ContentMap `json:"json"`
}

type Content struct {
	Name string `json:"name"`
	Func string `json:"func"`
}

type ContentMap map[string]Content
