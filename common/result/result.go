package result

import (
	"context"
	"fmt"
	"go.uber.org/zap"
	"reflect"
)

var DefaultOk = ErrOk

var Type = reflect.TypeOf((*Object)(nil)).Elem()

type Object interface {
	ToResult() Result
}

type Result struct {
	Errno   int32  `json:"code"`    //业务错误码=0，表示正确，其它失败
	SubCode int32  `json:"subCode"` //业务子码
	Desc    string `json:"desc"`    //错误信息
	Msg     string `json:"msg"`     //提供给界面提示之类
}

func New(code int32, err string) error {
	return Error(code, err)
}

func Case(a any) (Result, bool) {
	val := reflect.ValueOf(a)
	if val.CanInterface() && val.Type().Implements(Type) {
		var v = val.Interface().(Object)
		return v.ToResult(), true
	}
	return Result{}, false
}

func Succeed() Result {
	return Result{Errno: DefaultOk, Desc: ""}
}

func Error(code int32, err string) Result {
	return Result{Errno: code, Desc: err, Msg: err}
}

func ErrCode(code int32) Result {
	return Result{Errno: code}
}

func Errorf(code int32, f string, arg ...any) Result {
	desc := fmt.Sprintf(f, arg...)
	return Result{Errno: code, Desc: desc, Msg: desc}
}

func WithErr(err error) Result {
	if err == nil {
		return Succeed()
	}
	if res, ok := err.(Result); ok {
		return res
	}
	if i18n, ok := err.(I18n); ok {
		r := Result{}
		r.Errno = ErrException
		r.Msg = i18n.Content
		return r
	}
	if res, ok := Case(err); ok {
		return res
	}
	if res, ok := err.(*Result); ok {
		return *res
	}
	if i18n, ok := err.(*I18n); ok {
		r := Result{}
		r.Errno = ErrException
		r.Msg = i18n.Content
		return r
	}
	r := Result{}
	r.Errno = ErrException
	r.Desc = err.Error()
	r.Msg = r.Desc
	return r
}

func GetCode(err error) int32 {
	if err == nil {
		return 0
	}
	res := WithErr(err)
	return res.Errno
}

func IsOK(code int32) bool {
	return code == DefaultOk || code == Success
}

func (r Result) Is(err error) bool {
	if err == nil {
		return false
	}
	if res, ok := err.(Result); ok {
		return res.Errno == r.Errno && res.SubCode == r.SubCode
	}
	if res, ok := err.(*Result); ok {
		return res.Errno == r.Errno && res.SubCode == r.SubCode
	}
	return false
}

// error interface
func (r Result) Error() string {
	return fmt.Sprintf("code: %d, msg: %s, desc: %s", r.Errno, r.Msg, r.Desc)
}

func (r Result) Ok() bool {
	return IsOK(r.Errno)
}

func (r Result) NotFound() bool {
	return r.Equal(ErrNotFound)
}

// NonFound
// 不是NOT FOUND的错误
func (r Result) NonFound() bool {
	return !r.Equal(ErrOk) && !r.Equal(ErrNotFound) && !r.Equal(Success)
}

func (r Result) Equal(code int32) bool {
	return code == r.Errno
}

func (r Result) WithCode(code int32) Result {
	r.Errno = code
	return r
}

func (r Result) WithSubCode(code int32) Result {
	r.SubCode = code
	return r
}

func (r Result) WithMsg(msg string) Result {
	r.Msg = msg
	return r
}

func (r Result) FormatMsg(format string, a ...any) Result {
	r.Msg = fmt.Sprintf(format, a...)
	return r
}

func (r Result) WithDesc(desc string) Result {
	r.Desc = desc
	return r
}

func (r Result) FormatDesc(format string, a ...any) Result {
	r.Desc = fmt.Sprintf(format, a...)
	return r
}

func (r Result) AppendMsg(msg string) Result {
	r.Msg += msg
	return r
}

func (r Result) AppendDesc(desc string) Result {
	r.Desc += desc
	return r
}

func (r Result) AppendFormatMsg(format string, a ...any) Result {
	r.Msg += fmt.Sprintf(format, a...)
	return r
}

func (r Result) AppendFormatDesc(format string, a ...any) Result {
	r.Desc += fmt.Sprintf(format, a...)
	return r
}

func (r Result) WithError(err error) Result {
	if err == nil {
		return Succeed()
	}
	if res, ok := err.(Result); ok {
		return res.Exception(r.Errno)
	}
	if i18n, ok := err.(I18n); ok {
		r.Msg = i18n.Content
		return r.Exception(ErrException)
	}
	if res, ok := Case(err); ok {
		return res.Exception(r.Errno)
	}
	if res, ok := err.(*Result); ok {
		return (*res).Exception(r.Errno)
	}
	if i18n, ok := err.(*I18n); ok {
		r.Msg = i18n.Content
		return r.Exception(ErrException)
	}
	r.Desc = err.Error()
	r.Msg = r.Desc
	return r.Exception(ErrException)
}

func (r Result) Exception(code int32) Result {
	if !r.Ok() {
		return r
	}
	if IsOK(code) {
		r.Errno = ErrException
	} else {
		r.Errno = code
	}
	return r
}

func (r Result) Log() Result {
	if !r.Ok() {
		zap.L().WithOptions(zap.AddCallerSkip(1)).Error("Result",
			zap.Int32("errno", r.Errno),
			zap.Int32("subCode", r.SubCode),
			zap.String("msg", r.Msg),
			zap.String("desc", r.Desc))
	}
	return r
}

func (r Result) LogWithContext(ctx context.Context) Result {
	if !r.Ok() {
		zap.L().WithOptions(zap.AddCallerSkip(1)).Error("Result",
			zap.Any("traceId", ctx.Value("traceId")),
			zap.Int32("errno", r.Errno),
			zap.Int32("subCode", r.SubCode),
			zap.String("msg", r.Msg),
			zap.String("desc", r.Desc))
	}
	return r
}

func (r Result) LogM(model string, d interface{}) Result {
	if !r.Ok() {
		zap.L().WithOptions(zap.AddCallerSkip(1)).Error("Result",
			zap.Any("data", d),
			zap.String("model", model),
			zap.Int32("errno", r.Errno),
			zap.Int32("subCode", r.SubCode),
			zap.String("msg", r.Msg),
			zap.String("desc", r.Desc))
		return r.LogMWithContext(context.Background(), model, d)
	}
	return r
}

func (r Result) LogMWithContext(ctx context.Context, model string, d interface{}) Result {
	if !r.Ok() {
		zap.L().WithOptions(zap.AddCallerSkip(1)).Error("Result",
			zap.Any("traceId", ctx.Value("traceId")),
			zap.Any("data", d),
			zap.String("model", model),
			zap.Int32("errno", r.Errno),
			zap.Int32("subCode", r.SubCode),
			zap.String("msg", r.Msg),
			zap.String("desc", r.Desc))
	}
	return r
}
