package result

type ErCode int32

func (ec ErCode) Int() int32 {
	return int32(ec)
}

// low bit16
const (
	ErrOk               = int32(0x000)
	ErrOk1              = int32(0x001) //为1，可能也定义成成功
	ErrAuth             = int32(0x002)
	ErrReg              = int32(0x003)
	ErrSession          = int32(0x004)
	ErrToken            = int32(0x005)
	ErrException        = int32(0x006)
	ErrReq              = int32(0x007)
	ErrLogin            = int32(0x008)
	ErrParam            = int32(0x009)
	ErrJson             = int32(0x00A)
	ErrNil              = int32(0x00B)
	ErrTimeout          = int32(0x00C)
	ErrNotFound         = int32(0x00D)
	ErrNotSupport       = int32(0x00E)
	ErrNotImplement     = int32(0x00F)
	ErrFileNotExist     = int32(0x010)
	ErrUpload           = int32(0x011)
	ErrIgnore           = int32(0x012)
	ErrSms              = int32(0x013)
	ErrIllegal          = int32(0x014)
	ErrSensitive        = int32(0x015)
	ErrVersion          = int32(0x016)
	ErrXml              = int32(0x017)
	ErrConfig           = int32(0x018)
	ErrYaml             = int32(0x019)
	ErrDeadlineExceeded = int32(0x01A)
	Success             = int32(0x0C8) //200 , httpCode = 200
)

var (
	ROk                  = New(ErrOk, "ok")
	RErrAuth             = New(ErrAuth, "non exist authorization")
	RErrReg              = New(ErrReg, "register failure")
	RErrSession          = New(ErrSession, "session failure")
	RErrToken            = New(ErrToken, "token failure")
	RErrException        = New(ErrException, "exception")
	RErrReq              = New(ErrReq, "request failure")
	RErrLogin            = New(ErrLogin, "login failure")
	RErrParam            = New(ErrParam, "param is not error")
	RErrJson             = New(ErrJson, "json error")
	RErrNil              = New(ErrNil, "is nil")
	RErrTimeout          = New(ErrTimeout, "time out")
	RErrNotFound         = New(ErrNotFound, "not found")
	RErrNotSupport       = New(ErrNotSupport, "not support")
	RErrNotImplement     = New(ErrNotImplement, "not implement")
	RErrFileNotExist     = New(ErrFileNotExist, "file not exist")
	RErrUpload           = New(ErrUpload, "upload failure")
	RErrIgnore           = New(ErrIgnore, "ignore")
	RErrSms              = New(ErrSms, "sms")
	RErrIllegal          = New(ErrIllegal, "illegal")
	RErrSensitive        = New(ErrSensitive, "sensitive")
	RErrVersion          = New(ErrVersion, "version is error")
	RErrXml              = New(ErrXml, "xml error")
	RErrConfig           = New(ErrConfig, "config error")
	RErrYaml             = New(ErrYaml, "yaml error")
	RErrDeadlineExceeded = New(ErrDeadlineExceeded, "deadline exceeded")
	RSuccess             = New(Success, "success") //200 , httpCode = 200
)

// 子项
const (
	RegInfo        = int32(0x001000) //注册信息不完
	RegExisted     = int32(0x002000) //已存在
	RegActivated   = int32(0x003000) //需要激活
	TokenMalformed = int32(0x004000) //TOKEN 格式错误
	TokenSigned    = int32(0x005000) //TOKEN 答名失败
	TokenValidate  = int32(0x006000) //TOKEN 验证失败
	TokenExpired   = int32(0x007000) //TOKEN 过期
	TokenOther     = int32(0x008000) //TOKEN
	TokenUnVerify  = int32(0x009000)
)

// 内部错误ID
const (
	ErrSaveInternal          = 0x10000000
	ErrMediaCodeInternal     = 0x11000000
	ErrServiceInternal       = 0x12000000
	ErrWxInternal            = 0x13000000
	ErrEmailInternal         = 0x14000000
	ErrSmsInternal           = 0x15000000
	ErrGroupInternal         = 0x16000000
	ErrInitInternal          = 0x17000000
	ErrDbInternal            = 0x18000000
	ErrMqInitInternal        = 0x19000000
	ErrMqStartInternal       = 0x1A000000
	ErrMqPublishInternal     = 0x1B000000
	ErrRdsStartInternal      = 0x1C000000
	ErrRdsPingInternal       = 0x1D000000
	ErrRdsCfgInternal        = 0x1E000000
	ErrRdsDelInternal        = 0x1F000000
	ErrRdsSetInternal        = 0x20000000
	ErrRdsGetInternal        = 0x21000000
	ErrDbStartInternal       = 0x22000000
	ErrDbCfgInternal         = 0x23000000
	ErrDbOpenInternal        = 0x24000000
	ErrDbExistInternal       = 0x25000000
	ErrEsStartInternal       = 0x26000000
	ErrEsCfgInternal         = 0x27000000
	ErrEsClientInternal      = 0x28000000
	ErrEsPingInternal        = 0x29000000
	ErrListenInternal        = 0x2A000000
	ErrServeInternal         = 0x2B000000
	ErrClientConnectInternal = 0x2C000000
	ErrClientNilInternal     = 0x2D000000
	ErrSendInternal          = 0x2E000000
	ErrReadInternal          = 0x2F000000
	ErrEtcdInternal          = 0x34000000
	ErrEtcdClosedInternal    = 0x35000000
	ErrEtcdStartInternal     = 0x36000000
	ErrEtcdStopInternal      = 0x37000000
	ErrTryInternal           = 0x38000000
	ErrWarningInternal       = 0x39000000
)
