package result

type I18n struct {
	Content string
}

// error interface
func (i18n I18n) Error() string {
	return i18n.Content
}

func (i18n I18n) Append(content string) I18n {
	i18n.Content += content
	return i18n
}

func WithI18n(content string) I18n {
	return I18n{Content: content}
}
