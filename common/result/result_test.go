package result

import "testing"

type xxxx struct {
	Result
}

func (x xxxx) ToResult() Result {
	return x.Result
}

func TestNew(t *testing.T) {

	res, _ := RErrAuth.(Result)

	t.Logf("Result:  %+v", res.Errno)

	r := WithErr(Result{
		Errno: 1,
	})
	t.Logf("Result:  %+v", r.Errno)

	x := xxxx{Result{Desc: "XXXXX"}}
	res1, _ := Case(x)

	t.Logf("%v", res1)
}
