package common

import (
	"errors"
	"reflect"
)

func ElementsTo[T EleType](c []T) []interface{} {
	var ret = make([]interface{}, len(c))
	for i := range c {
		ret[i] = c[i]
	}
	return ret
}

// IncludeElement 包含 找src在all
func IncludeElement[T EleType](all []T, src ...T) []T {
	if len(src) == 0 {
		return all
	}
	if len(all) == 0 {
		return []T{}
	}
	var rest []T
	for i := range src {
		for j := range all {
			if all[j] == src[i] {
				rest = append(rest, src[i])
				break
			}
		}
	}
	return rest
}

// ExcludeElement 排除 找src不存在all
func ExcludeElement[T EleType](all []T, src ...T) []T {
	if len(src) == 0 {
		return []T{}
	}
	if len(all) == 0 {
		return src
	}
	var rest []T
	for i := range src {
		found := false
		for j := range all {
			if all[j] == src[i] {
				found = true
				break
			}
		}
		if !found {
			rest = append(rest, src[i])
		}
	}
	return rest
}

// RemoveDuplicateElement 除重
func RemoveDuplicateElement[T EleType](values []T) []T {
	var result []T
	temp := map[T]struct{}{}
	for i := range values {
		if _, ok := temp[values[i]]; !ok {
			temp[values[i]] = struct{}{}
			result = append(result, values[i])
		}
	}
	return result
}

// RemoveElement 移除某元素
func RemoveElement[T EleType](all []T, src T) []T {
	return FilterElement(all, func(v T) bool {
		return src != v
	})
}

type FilterFunc[T EleType] func(v T) bool

// FilterElement 过滤某元素
func FilterElement[T EleType](all []T, cb FilterFunc[T]) []T {
	var rest []T
	for i := range all {
		if cb(all[i]) {
			rest = append(rest, all[i])
		}
	}
	return rest
}

func FindElement[T EleType](all []T, src T) bool {
	for i := range all {
		if src == all[i] {
			return true
		}
	}
	return false
}

type EleType interface {
	string | bool | int8 | int16 | int | int32 | int64 | uint8 | uint16 | uint | uint32 | uint64 | float32 | float64
}

type ReverseType interface {
	rune | bool | int8 | int16 | int64 | uint8 | uint16 | uint32 | uint64 | float32 | float64
}

func ReverseElement[T ReverseType](rs []T) []T {
	for i, j := 0, len(rs)-1; i < j; i, j = i+1, j-1 {
		rs[i], rs[j] = rs[j], rs[i]
	}
	return rs
}

// Defined checks if variable is defined in a struct
func Defined(data interface{}, field string) bool {
	t := reflect.Indirect(reflect.ValueOf(data)).Type()

	if t.Kind() != reflect.Struct {
		return false
	}
	_, b := t.FieldByName(field)
	return b
}

// Dict allows to send more than one variable into a template
func Dict(values ...interface{}) (map[string]interface{}, error) {
	if len(values)%2 != 0 {
		return nil, errors.New("invalid dict call")
	}
	dict := make(map[string]interface{}, len(values)/2)
	for i := 0; i < len(values); i += 2 {
		key, ok := values[i].(string)
		if !ok {
			return nil, errors.New("dict keys must be strings")
		}
		dict[key] = values[i+1]
	}
	return dict, nil
}

func IsNil(o interface{}) bool {
	if o == nil {
		return true
	}
	return IsNilReflect(reflect.ValueOf(o))
}

func IsNilReflect(v reflect.Value) bool {
	switch v.Kind() {
	case reflect.Pointer, reflect.UnsafePointer:
		fallthrough
	case reflect.Chan, reflect.Func, reflect.Map:
		fallthrough
	case reflect.Interface, reflect.Slice:
		return v.IsNil()
	default:
	}
	return false
}

// IsMap checks if some variable is a map
func IsMap(o interface{}) bool {
	if o == nil {
		return false
	}
	return reflect.ValueOf(o).Kind() == reflect.Map
}

// IsSlice checks if some variable is a slice
func IsSlice(o interface{}) bool {
	if o == nil {
		return false
	}
	return reflect.ValueOf(o).Kind() == reflect.Slice
}
