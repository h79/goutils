package data

import "net/url"

// Base
/** 数据基础 */
type Base interface {
	DataType() string
}

type D map[string]interface{}

func (d D) String(key string) string {
	if v, ok := d[key].(string); ok {
		return v
	}
	return ""
}

func (d D) Value(name string) interface{} {
	if v, ok := d[name]; ok {
		return v
	}
	return nil
}

func (d D) Append(src D) {
	for k, v := range src {
		d[k] = v
	}
}

func (d D) ToFormUrl() url.Values {
	values := url.Values{}
	for k, v := range d {
		if val, ok := v.(string); ok {
			values.Add(k, val)
		}
	}
	return values
}

type Options struct {
	Data D
}

type OptionsFunc func(options *Options)
