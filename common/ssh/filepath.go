package ssh

import (
	"gitee.com/h79/goutils/common/file"
	"io"
	"os"
	"path"
)

type Path struct {
	Mode  os.FileMode
	Name  string
	IsDir bool
}

type FileInfo struct {
	Name string
	Size int64
	Mode os.FileMode
}

func (fi *FileInfo) Receive(opt *Option, src io.Reader) error {
	dst, err := fi.CreateFile(opt)
	if err != nil {
		return err
	}
	defer dst.Close()

	n, err := file.CopyN(dst, src, fi.Size)
	if err != nil {
		return err
	}
	if n != fi.Size {
		return ErrCopy
	}
	return nil
}

func (fi *FileInfo) CreateFile(opt *Option) (io.WriteCloser, error) {

	// Create a local file to write to
	dst, err := os.OpenFile(localFile(opt), os.O_RDWR|os.O_CREATE, opt.Local.Mode|fi.Mode)
	if err != nil {
		return nil, err
	}
	return dst, nil
}

func remoteFile(opt *Option) string {
	if opt.Remote.IsDir {
		filename := path.Base(opt.Local.Name)
		opt.Remote.Name = path.Join(opt.Remote.Name, filename)
		opt.Remote.IsDir = false
		return filename
	}
	return path.Base(opt.Remote.Name)
}

func localFile(opt *Option) string {
	if opt.Local.IsDir {
		filename := path.Base(opt.Remote.Name)
		opt.Local.Name = path.Join(opt.Local.Name, filename)
		opt.Local.IsDir = false
	}
	return opt.Local.Name
}
