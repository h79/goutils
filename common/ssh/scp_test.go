package ssh

import (
	"golang.org/x/crypto/ssh"
	"net"
	"testing"
)

func TestScp(t *testing.T) {

	config, _ := WithPassword("root", "Hullo001@1010", func(hostname string, remote net.Addr, key ssh.PublicKey) error {
		return nil
	})
	scp := NewScp(NewSession("8.129.77.207:22", &config))
	res := scp.SendTo(0, &Option{Local: Path{Name: "./sftp.go"}, Remote: Path{Name: "/home/", IsDir: true, Mode: 0777}})

	t.Log(res)

	err := scp.ReceiveFrom(&Option{Local: Path{Name: "./sftpx", Mode: 0777}, Remote: Path{Name: "./home", IsDir: true}})

	t.Log(err)
}
