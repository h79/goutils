package ssh

import (
	"errors"
	"fmt"
	"github.com/pkg/sftp"
	"io"
	"os"
	"time"
)

type Sftp struct {
	session *Session
}

func NewSftp(session *Session) *Sftp {
	return &Sftp{session: session}
}

func (sf *Sftp) Exec(id int, localFilePath string, remoteFilePath string) *Result {

	result := &Result{
		Id:         id,
		Host:       sf.session.Host,
		LocalPath:  localFilePath,
		RemotePath: remoteFilePath,
	}
	start := time.Now()
	result.StartTime = start

	if err := sf.session.Connect(); err != nil {
		result.Error = err
		return result
	}

	var fileSize int64
	if s, err := os.Stat(localFilePath); err != nil {
		result.Error = err
		return result

	} else {
		fileSize = s.Size()
	}
	srcFile, err := os.Open(localFilePath)
	if err != nil {
		result.Error = err
		return result
	}

	defer srcFile.Close()

	sftpClient, err := sftp.NewClient(sf.session.Client)
	if err != nil {
		result.Error = err
		return result
	}
	defer sftpClient.Close()

	dstFile, err := sftpClient.Create(remoteFilePath)
	if err != nil {
		result.Error = err
		return result
	}
	defer dstFile.Close()
	n, err := io.Copy(dstFile, io.LimitReader(srcFile, fileSize))
	if err != nil {
		result.Error = err
		return result
	}
	if n != fileSize {
		result.Error = errors.New(fmt.Sprintf("copy: expected %v bytes, got %d", fileSize, n))
		return result
	}
	end := time.Now()
	result.EndTime = end
	return result
}
