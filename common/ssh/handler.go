package ssh

import (
	"errors"
	"fmt"
	"io"
	"os"
)

type Sender struct {
	w    io.WriteCloser
	info FileInfo
}

func (wf *Sender) Do(reply io.Reader, src io.Reader) (err error) {
	if err = wf.writeHead(); err != nil {
		return
	}
	if err = readReply(reply); err != nil {
		return
	}

	if err = wf.writeFile(src); err != nil {
		return
	}

	if err = wf.writeCompleted(); err != nil {
		return
	}
	err = readReply(reply)
	return
}

func (wf *Sender) writeHead() error {
	_, err := fmt.Fprintf(wf.w, "C%#4o %d %s\n", wf.info.Mode&os.ModePerm, wf.info.Size, wf.info.Name)
	return err
}

func (wf *Sender) writeFile(src io.Reader) error {
	_, err := io.Copy(wf.w, src)
	return err
}

func (wf *Sender) writeCompleted() error {
	_, err := fmt.Fprint(wf.w, "\x00")
	return err
}

type Receiver struct {
	opt *Option
}

func (rf *Receiver) Do(reply io.Reader, ack io.Writer) (err error) {
	var Ack = func(w io.Writer) error {
		var msg = []byte{0}
		n, err2 := w.Write(msg)
		if err2 != nil {
			return err2
		}
		if n < len(msg) {
			return ErrAck
		}
		return nil
	}

	if err = Ack(ack); err != nil {
		return
	}

	res, err := ParseReply(reply)
	if err != nil {
		return
	}

	if res.IsFailure() {
		return res
	}

	info, err := res.Parse()
	if err != nil {
		return
	}

	if err = Ack(ack); err != nil {
		return
	}

	if fi, ok := info.(*FileHeader); ok {
		//文件传输
		if err = fi.Receive(rf.opt, reply); err != nil {
			return
		}
	}
	if _, ok := info.(*DirHeader); ok {
		//目录传输
	}
	err = Ack(ack)
	return
}

//
//// ReceiveFromByWriter 下载
//func (scp *Scp) ReceiveFromByWriter(dst io.Writer, opt *Option) error {

var ErrAck = errors.New("failed to write ack buffer")
var ErrCopy = errors.New("failed to copy file")
