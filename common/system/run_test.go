package system

import (
	"testing"
	"time"
)

func TestMainRunning(t *testing.T) {

	MainRunning(func() {
		exit := Exit()
		for {
			select {
			case <-exit.Done():
				return

			case <-Closed():
				return
			}
		}
	})

	for i := 0; i < 2000; i++ {
		ChildRunning(func() {
			for {
				select {
				case <-Closed():
					t.Log("close")
					return
				}
			}
		})
	}
	ChildRunning(func() {
		time.Sleep(time.Second * 10)
		Close()
	})
	Go().Wait()
}

func TestChildRunning(t *testing.T) {
	var i map[string]int
	DefRecoverFunc = func(r any) {
		t.Log(r)
	}
	ChildRunning(func() {
		i["1"] = 1
	})
	Go().Wait()
}
