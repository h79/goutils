//go:build !not_quit_sys

package system

import "os"

func Quit(code int) {
	os.Exit(code)
}
