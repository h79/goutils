//go:build linux
// +build linux

package system

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

// unixProcess is an implementation of Process that contains Unix-specific
// fields and information.
type unixProcess struct {
	pid    int32
	ppid   int32
	binary string
	path   string
}

func (p *unixProcess) Pid() int32 {
	return p.pid
}

func (p *unixProcess) PPid() int32 {
	return p.ppid
}

func (p *unixProcess) Name() string {
	return p.binary
}

func (p *unixProcess) Path() string {
	return p.path
}

type processList struct {
	processes []Process
}

func (p *processList) fetchPID(path string) (int, error) {
	index := strings.LastIndex(path, "/")
	if index < 0 || len(path) < 7 {
		return -1, fmt.Errorf("fetch pid error, path: %v", path)
	}
	return strconv.Atoi(path[6:index])
}

func (p *processList) read(path string) (string, int, int, error) {
	// The status file contains the name of the process in its first line.
	// The line looks like "Name: theProcess".
	f, err := os.Open(path)
	if err != nil {
		return "", 0, 0, err
	}
	defer f.Close()

	var pidStr string
	var ppidStr string
	var name string
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		// read line by line
		line := string(scanner.Bytes())
		if strings.HasPrefix(line, "Name:") {
			name = strings.Split(line, ":")[1]
		} else if strings.HasPrefix(line, "PPid:") {
			ppidStr = strings.Split(line, ":")[1]
		} else if strings.HasPrefix(line, "Pid:") {
			pidStr = strings.Split(line, ":")[1]
		}
		if name != "" && ppidStr != "" && pidStr != "" {
			break
		}
	}
	pid, _ := strconv.Atoi(strings.Trim(pidStr, " "))
	ppid, _ := strconv.Atoi(strings.Trim(ppidStr, " "))
	return strings.Trim(name, " "), pid, ppid, nil
}

func (p *processList) walk(path string, info os.FileInfo, err error) error {
	// We just return in case of errors, as they are likely due to insufficient
	// privileges. We shouldn't get any errors for accessing the information we
	// are interested in. Run as root (sudo) and log the error, in case you want
	// this information.
	if err != nil {
		return nil
	}

	// We are only interested in files with a path looking like /proc/<pid>/status.
	if strings.Count(path, "/") != 3 {
		return nil
	}
	// Let's extract the middle part of the path with the <pid> and
	// convert the <pid> into an integer. Log an error if it fails.
	pid, err := p.fetchPID(path)
	if err != nil {
		return err
	}
	if strings.HasSuffix(path, "/status") {
		// Extract the process name from within the first line in the buffer
		name, pid_, ppid, err := p.read(path)
		if err != nil {
			return err
		}
		if pid_ != pid {
			return fmt.Errorf("pid not match, pid: %v, name: %v, ppid: %v", pid_, name, ppid)
		}
		pa, err := os.Readlink("/proc/" + strconv.Itoa(pid) + "/exe")
		if err != nil {
			return err
		}
		p.processes = append(p.processes, &unixProcess{pid: int32(pid), binary: name, ppid: int32(ppid), path: pa})
	}
	return nil
}

func getProcesses() ([]Process, error) {
	p := processList{}
	err := filepath.Walk("/proc", p.walk)
	return p.processes, err
}
