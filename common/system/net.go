package system

import (
	"fmt"
	"io"
	"net"
	"net/http"
	"time"
)

type GetPublicIPFunc func(url string) (net.IP, error)
type PingIPFunc func(ip string) error

var (
	xx = &IP{}
)

const (
	KLocalHost = "127.0.0.1"
	url        = "https://myexternalip.com/raw"
)

type IP struct {
	resolveIp        []net.IP
	publicIp         net.IP
	currentResolveIp net.IP
	publicIPFunc     GetPublicIPFunc
	pingIPFunc       PingIPFunc
	publicCount      int
}

func IPX() *IP {
	return xx
}

func NewIP() *IP {
	return &IP{}
}

func (p *IP) SetPublicIPFunc(fn GetPublicIPFunc) {
	p.publicIPFunc = fn
}

func (p *IP) SetPingIPFunc(fn PingIPFunc) {
	p.pingIPFunc = fn
}

func (p *IP) SetPublicIP(ip net.IP) {
	p.publicIp = ip
}

func (p *IP) ResetCurrentPublicIP() net.IP {
	p.publicIp = nil
	return p.GetCurrentPublicIP()
}

func (p *IP) GetCurrentPublicIP() net.IP {
	if len(p.publicIp) > 0 {
		return p.publicIp
	}
	if p.publicCount > 2 { //3次
		return net.IP{}
	}
	p.publicCount++
	var fn = p.publicIPFunc
	if fn == nil {
		fn = GetPublicIPWithUrl
	}
	ip, err := fn(url)
	if err == nil {
		p.publicIp = ip
	}
	return ip
}

func (p *IP) PublicIPValid() bool {
	ret := p.GetCurrentPublicIP()
	return len(ret) > 0
}

// SelfExternalIP 自己外部IP，可能是局域网IP，公网IP
// lan 局域网优先
func (p *IP) SelfExternalIP(lan bool) string {
	pip := p.GetCurrentPublicIP()
	rip := p.GetCurrentResolveIP()
	if lan && IsLanIP(rip) {
		return rip.String()
	}
	if IsPublicIP(pip) {
		return pip.String()
	}
	return rip.String()
}

func (p *IP) ResetCurrentResolveIP() net.IP {
	p.currentResolveIp = nil
	return p.GetCurrentResolveIP()
}

func (p *IP) GetAllResolveIP() []net.IP {
	if len(p.resolveIp) > 0 {
		return p.resolveIp
	}
	p.GetCurrentResolveIP()
	return p.resolveIp
}

func (p *IP) GetCurrentResolveIP() net.IP {
	if len(p.currentResolveIp) > 0 {
		return p.currentResolveIp
	}
	ping := p.pingIPFunc
	if ping == nil {
		ping = DefaultPingFunc
	}
	ret := p.SelectResolveIP(ping)
	if len(ret) > 0 {
		p.currentResolveIp = ret
	}
	return ret
}

func (p *IP) SetCurrentResolveIP(ip net.IP) {
	p.currentResolveIp = ip
}

func (p *IP) SelectResolveIP(pingFunc PingIPFunc) net.IP {
	var err error
	if len(p.resolveIp) == 0 {
		ips, err := p.GetResolveIP()
		if err != nil {
			return nil
		}
		p.resolveIp = append([]net.IP{}, ips...)
	}
	if len(p.resolveIp) == 1 {
		return p.resolveIp[0]
	}
	for i := range p.resolveIp {
		err = pingFunc(p.resolveIp[i].String())
		if err == nil {
			return p.resolveIp[i]
		}
	}
	return nil
}

func (p *IP) GetResolveIP() ([]net.IP, error) {
	var ips, err = GetResolveIPList(func(ip net.IP) bool {
		t := GetIPType(ip)
		return t == IptUnk || t == IptPub
	})
	if err != nil {
		return nil, err
	}
	if len(ips) > 0 {
		return ips, nil
	}
	return nil, fmt.Errorf("not exist")
}

func GetPublicIP() (net.IP, error) {
	return GetPublicIPWithUrl(url)
}

func GetResolveIP() (net.IP, error) {
	adds, err := net.InterfaceAddrs()
	if err != nil {
		return net.IP{}, err
	}
	return parseAddress(adds), nil
}

func GetResolveIPV3(tt IPType, index int) (net.IP, error) {
	var ips, err = GetResolveIPList(func(ip net.IP) bool {
		t := GetIPType(ip)
		return t != tt
	})
	if err != nil {
		return nil, err
	}
	if len(ips) > 0 {
		if index <= 0 || len(ips) <= index {
			return ips[0], nil
		}
		return ips[index], nil
	}
	return nil, fmt.Errorf("not exist")
}

func GetResolveIPList(filter func(ip net.IP) bool) ([]net.IP, error) {
	var ips []net.IP
	var ip, err = GetClientIP()
	if err != nil {
		return nil, err
	}
	for i := range ip {
		if filter(ip[i]) {
			continue
		}
		ips = append(ips, ip[i])
	}
	return ips, nil
}

// IsSelfIP 是否是自己
func IsSelfIP(ip string) string {
	pip := xx.GetCurrentPublicIP()
	rip := xx.GetCurrentResolveIP()
	if pip.String() == ip {
		return KLocalHost
	}
	if rip.String() == ip {
		return KLocalHost
	}
	return ip
}

// IPIsSelf 是否是自己
// Deprecated: call IsSelfIP
func IPIsSelf(ip string) string {
	return IsSelfIP(ip)
}

// IsLanIP 是否局域网 192.168.x.x
func IsLanIP(IP net.IP) bool {
	return GetIPType(IP) == IptPriC
}

func IsPublicIP(IP net.IP) bool {
	return GetIPType(IP) == IptPub
}

func GetClientIP() ([]net.IP, error) {
	var ips []net.IP
	faces, err := net.Interfaces()
	if err != nil {
		return nil, err
	}
	for _, face := range faces {
		if face.Flags&net.FlagUp == 0 {
			continue
		}
		if face.Flags&net.FlagLoopback != 0 {
			continue
		}
		adds, er := face.Addrs()
		if er != nil {
			return nil, er
		}
		ip := parseAddress(adds)
		if len(ip) > 0 {
			ips = append(ips, ip)
		}
	}
	return ips, nil
}

func parseAddress(adds []net.Addr) net.IP {
	for _, addr := range adds {
		ip := parseResolveIP(addr)
		if len(ip) > 0 {
			return ip
		}
	}
	return net.IP{}
}

func parseResolveIP(addr net.Addr) net.IP {
	var ip net.IP
	switch v := addr.(type) {
	case *net.IPNet:
		ip = v.IP
	case *net.IPAddr:
		ip = v.IP
	}
	if ip == nil || ip.IsLoopback() || ip.IsLinkLocalMulticast() || ip.IsLinkLocalUnicast() {
		return nil
	}
	return ip.To4()
}

type IPType int8

const (
	IptUnk  = IPType(0)
	IptPub  = IPType(1)
	IptPriA = IPType(2)
	IptPriB = IPType(3)
	IptPriC = IPType(4)
)

func GetIPType(ip net.IP) IPType {
	if len(ip) == 0 {
		return IptUnk
	}
	if ip.IsLoopback() || ip.IsLinkLocalMulticast() || ip.IsLinkLocalUnicast() {
		return IptUnk
	}
	if ip4 := ip.To4(); ip4 != nil {
		switch true {
		case ip4[0] == 10:
			return IptPriA
		case ip4[0] == 172 && ip4[1]&0xf0 == 16:
			return IptPriB
		case ip4[0] == 192 && ip4[1] == 168:
			return IptPriC
		default:
			return IptPub
		}
	}
	if len(ip) == net.IPv6len && ip[0]&0xfe == 0xfc {
		return IptPriC
	}
	return IptPub
}

/**
 * 私有IP
A 类 10.0.0.0/8：10.0.0.0～10.255.255.255
B 类 172.16.0.0/12：172.16.0.0～172.31.255.255
C 192.168.0.0/16：192.168.0.0～192.168.255.255
*/

func DefaultPingFunc(ip string) error {
	return nil
}

func GetPublicIPWithUrl(url string) (net.IP, error) {
	client := http.Client{Timeout: time.Second * 5}
	resp, err := client.Get(url)
	if err != nil {
		return net.IP{}, err
	}
	defer resp.Body.Close()
	ip, err := io.ReadAll(resp.Body)
	if err != nil {
		return net.IP{}, err
	}
	return net.ParseIP(string(ip)), nil
}

func PortsUsed(ports []int) []bool {
	var used = make([]bool, len(ports))
	for i := range ports {
		used[i] = PortUsed(ports[i])
	}
	return used
}

func PortUsed(port int) bool {
	return PortUsedV2("", port)
}

func PortUsedV2(ip string, port int) bool {
	var host = fmt.Sprintf("%s:%d", ip, port)
	if conn, _ := net.DialTimeout("tcp", host, 3*time.Second); conn != nil {
		_ = conn.Close()
		return true
	}
	return false
}

func PingCheck(port int) PingIPFunc {
	return func(ip string) error {
		var host = fmt.Sprintf("%s:%d", ip, port)
		var lis, err = net.Listen("tcp", host)
		if err != nil {
			return err
		}
		_ = lis.Close()
		return nil
	}
}
