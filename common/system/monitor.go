package system

import "time"

type Monitor struct {
	Dur      int    `json:"dur" yaml:"dur"` //单位秒
	Diff     int64  `json:"diff" yaml:"diff"`
	Count    int    `json:"count" yaml:"count"`
	Cpu      int    `json:"cpu" yaml:"cpu"`
	Mem      int    `json:"mem" yaml:"mem"`
	Disk     int    `json:"disk" yaml:"disk"`
	DiskPath string `json:"diskPath" yaml:"diskPath"`
}

func (mon *Monitor) RunMonitor(call func(d []DiskPath, m *MemInfo, cpuPercent float64, flag int) int) {
	if call == nil {
		return
	}
	if mon.Dur <= 0 {
		mon.Dur = 1
	}
	ti := time.NewTimer(time.Second * time.Duration(mon.Dur))
	defer ti.Stop()
	for {
		select {

		case <-Closed():
			return

		case <-ti.C:
			flag := 0
			c := CpuPercent()
			if int(c) >= mon.Cpu {
				flag = 1
			}
			var disk []DiskPath
			if mon.DiskPath == "" { //所有磁盘
				d := DiskAll()
				for i := range d {
					if int(d[i].UsedPercent) >= mon.Disk {
						flag |= 2
						disk = append(disk, d[i])
					}
				}
			} else { //当前运行磁盘
				d := DiskWithPath(mon.DiskPath)
				if int(d.UsedPercent) >= mon.Disk {
					flag |= 2
					disk = append(disk, d)
				}
			}
			m := Mem()
			if int(m.UsedPercent) >= mon.Mem {
				flag |= 4
			}

			if 0 != call(disk, &m, c, flag) {
				return
			}
		}
	}
}
