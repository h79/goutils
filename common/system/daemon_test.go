package system

import (
	"testing"
)

func Back(args []string, opts ...ArgOptionFunc) []string {
	opt := ArgOption{
		Id:   0,
		Args: args,
	}
	for i := range opts {
		opts[i](&opt)
	}
	return opt.Args
}

func TestDaemon(t *testing.T) {
	var args = []string{"test", "-1", "-2", "-3", "xx"}
	xx := Back(args, func(arg *ArgOption) {
		arg.Args = StripArgs(arg.Args, "-3")
	})
	t.Logf("xx: %v", xx)
	t.Logf("old: %v", args)
}
