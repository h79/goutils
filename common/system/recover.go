package system

type RecoverFunc func(r any)

var DefRecoverFunc RecoverFunc = func(r any) {
}

func Recover(cleanups ...func()) {
	for i := range cleanups {
		cleanups[i]()
	}
	if r := recover(); r != nil {
		if DefRecoverFunc != nil {
			DefRecoverFunc(r)
		}
	}
}
