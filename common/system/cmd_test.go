package system

import "testing"

func TestStripArgs(t *testing.T) {

	args := []string{"c.exe", "-serviceName", "dim", "-serviceInstall", "true"}
	args = StripArgs(args, "-serviceInstall")

	t.Log(args)
}

func TestCommand(t *testing.T) {
	res := SyncExec("cp cmd_test.go xxx.txt", 2)
	t.Logf("result: %v", res)
}
