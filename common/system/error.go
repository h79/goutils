package system

import "errors"

var (
	TimeoutError = errors.New("operation timed out")
	ClosedError  = errors.New("process closed")
)
