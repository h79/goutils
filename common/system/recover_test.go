package system

import (
	"gitee.com/h79/goutils/common"
	"testing"
	"time"
)

func TestNil(t *testing.T) {
	order := "sdd"
	common.IsNil(order)
}

func TestRecover(t *testing.T) {
	type XXX struct {
		I int
	}
	go func() {
		defer Recover()
		var xx *XXX

		xx.I = 0
	}()
	time.Sleep(time.Second * 5)
}
