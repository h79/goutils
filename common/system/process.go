package system

import "errors"

// Process is the generic interface that is implemented on every platform
// and provides common operations for processes.
type Process interface {
	// Pid is the process ID for this process.
	Pid() int32

	// PPid is the parent process ID for this process.
	PPid() int32

	// Name name running this process. This is not a path to the
	Name() string

	Path() string
}

// Processes returns all processes.
//
// This of course will be a point-in-time snapshot of when this method was
// called. Some operating systems don't provide snapshot capability of the
// process table, in which case the process table returned might contain
// ephemeral entities that happened to be running when this was called.
func Processes() ([]Process, error) {
	return getProcesses()
}

// FindProcessByPId looks up a single process by pid.
//
// Process will be nil and error will be nil if a matching process is
// not found.
func FindProcessByPId(pid int32) (Process, error) {
	ps, err := getProcesses()
	if err != nil {
		return nil, err
	}

	for _, p := range ps {
		if p.Pid() == pid {
			return p, nil
		}
	}

	return nil, errors.New("process not found")
}

// FindProcessByName looks up a single process by pid.
//
// Process will be nil and error will be nil if a matching process is
// not found.
func FindProcessByName(name string) (Process, error) {
	ps, err := getProcesses()
	if err != nil {
		return nil, err
	}

	for _, p := range ps {
		if p.Name() == name {
			return p, nil
		}
	}
	return nil, errors.New("process not found")
}
