//go:build darwin
// +build darwin

package system

import (
	"bytes"
	"encoding/binary"
	"syscall"
	"unsafe"
)

type darwinProcess struct {
	pid    int32
	ppid   int32
	binary string
}

func (p *darwinProcess) Pid() int32 {
	return p.pid
}

func (p *darwinProcess) PPid() int32 {
	return p.ppid
}

func (p *darwinProcess) Name() string {
	return p.binary
}

func (p *darwinProcess) Path() string {
	return ""
}

func getProcesses() ([]Process, error) {
	buf, err := darwinSyscall()
	if err != nil {
		return nil, err
	}

	procs := make([]*kernInfoProc, 0, 50)
	k := 0
	for i := kernInfoStructSize; i < buf.Len(); i += kernInfoStructSize {
		proc := &kernInfoProc{}
		err = binary.Read(bytes.NewBuffer(buf.Bytes()[k:i]), binary.LittleEndian, proc)
		if err != nil {
			return nil, err
		}
		k = i
		procs = append(procs, proc)
	}

	result := make([]Process, 0, len(procs))
	for _, p := range procs {
		result = append(result, &darwinProcess{
			pid:    p.Pid,
			ppid:   p.PPid,
			binary: darwinCString(p.Comm),
		})
	}
	return result, nil
}

func darwinCString(s [16]byte) string {
	i := 0
	for _, b := range s {
		if b != 0 {
			i++
		} else {
			break
		}
	}

	return string(s[:i])
}

func darwinSyscall() (*bytes.Buffer, error) {
	mib := [4]int32{ctrlKern, kernProc, kernProcAll, 0}
	size := uintptr(0)

	_, _, errno := syscall.Syscall6(
		syscall.SYS___SYSCTL,
		uintptr(unsafe.Pointer(&mib[0])),
		4,
		0,
		uintptr(unsafe.Pointer(&size)),
		0,
		0)

	if errno != 0 {
		return nil, errno
	}

	bs := make([]byte, size)
	_, _, errno = syscall.Syscall6(
		syscall.SYS___SYSCTL,
		uintptr(unsafe.Pointer(&mib[0])),
		4,
		uintptr(unsafe.Pointer(&bs[0])),
		uintptr(unsafe.Pointer(&size)),
		0,
		0)

	if errno != 0 {
		return nil, errno
	}

	return bytes.NewBuffer(bs[0:size]), nil
}

const (
	ctrlKern           = 1
	kernProc           = 14
	kernProcAll        = 0
	kernInfoStructSize = 648
)

type kernInfoProc struct {
	_    [40]byte
	Pid  int32
	_    [199]byte
	Comm [16]byte
	_    [301]byte
	PPid int32
	_    [84]byte
}
