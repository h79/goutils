package system

import (
	"fmt"
	"golang.org/x/sys/windows"
	"path/filepath"
	"syscall"
	"unsafe"
)

type windowsProcess struct {
	pid  int32
	ppid int32
	exe  string
	path string
}

func (p *windowsProcess) Pid() int32 {
	return p.pid
}

func (p *windowsProcess) PPid() int32 {
	return p.ppid
}

func (p *windowsProcess) Name() string {
	return p.exe
}

func (p *windowsProcess) Path() string {
	return p.path
}

func getProcessPath(pid int32, name string) (string, error) {
	snap, err := windows.CreateToolhelp32Snapshot(windows.TH32CS_SNAPMODULE, uint32(pid))
	if err != nil {
		return "", err
	}
	defer windows.CloseHandle(snap)
	var pe32 windows.ModuleEntry32
	pe32.Size = uint32(unsafe.Sizeof(pe32))
	if err = windows.Module32First(snap, &pe32); err != nil {
		return "", err
	}
	for {
		module := windows.UTF16ToString(pe32.Module[:])
		if pe32.ProcessID == uint32(pid) && module == name {
			path := windows.UTF16ToString(pe32.ExePath[:])
			return filepath.Dir(path), nil
		}
		if err = windows.Module32Next(snap, &pe32); err != nil {
			break
		}
	}
	return "", fmt.Errorf("couldn't find pid: %d", pid)
}

func newProcessFromEntry(entry *windows.ProcessEntry32) Process {
	if entry == nil {
		return nil
	}
	na := getProcessName(entry)
	pa, _ := getProcessPath(int32(entry.ProcessID), na)
	return &windowsProcess{path: pa, exe: na, pid: int32(entry.ProcessID), ppid: int32(entry.ParentProcessID)}
}

func getProcessName(entry *windows.ProcessEntry32) string {
	return syscall.UTF16ToString(entry.ExeFile[:])
}

func newProcessEntry() *windows.ProcessEntry32 {
	var entry windows.ProcessEntry32
	entry.Size = uint32(unsafe.Sizeof(entry))
	return &entry
}

func getProcesses() ([]Process, error) {
	const snapProcess = 0x00000002
	handle, err := windows.CreateToolhelp32Snapshot(snapProcess, 0)
	if err != nil {
		return nil, err
	}
	defer windows.CloseHandle(handle)

	entry := newProcessEntry()
	// take first process
	err = windows.Process32First(handle, entry)
	if err != nil {
		return nil, err
	}
	results := make([]Process, 0, 50)
	for {
		if entry.ProcessID > 0 {
			results = append(results, newProcessFromEntry(entry))
		}
		if err = windows.Process32Next(handle, entry); err != nil {
			// catch syscall.ERROR_NO_MORE_FILES on the end of process list
			if err == syscall.ERROR_NO_MORE_FILES {
				break
			}
			return nil, err
		}
	}
	return results, nil
}
