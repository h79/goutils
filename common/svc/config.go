package svc

type Config struct {
	ServiceName string `json:"serviceName" yaml:"serviceName" xml:"serviceName"`
	ServiceDesc string `json:"serviceDesc" yaml:"serviceDesc" xml:"serviceDesc"`
}
