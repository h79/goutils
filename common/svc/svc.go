//go:build !windows

package svc

func DoService(conf Config, startService func(isWindowsService bool), stopService func()) {

	startService(false)

	stopService()
}
