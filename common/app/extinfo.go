package app

import (
	"encoding/base64"
	"encoding/json"
)

type ExtInfo map[string]interface{}

func (e ExtInfo) Decode(info string) error {
	by, er := base64.StdEncoding.DecodeString(info)
	if er != nil {
		return er
	}
	return json.Unmarshal(by, &e)
}

func (e ExtInfo) Encode() string {
	by, er := json.Marshal(&e)
	if er != nil {
		return ""
	}
	return base64.StdEncoding.EncodeToString(by)
}
