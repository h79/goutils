package app

// Info 应用app信息
type Info struct {
	AppId    string `form:"appId" binding:"-" json:"appId" yaml:"appId" xml:"appId"`
	Source   string `form:"source" binding:"-" json:"source" yaml:"source" xml:"source"` //来自己client ,`如果 server，表示具体服务名
	Terminal string `form:"terminal" binding:"-" json:"terminal" yaml:"terminal" xml:"terminal"`
	Version  string `form:"version" binding:"-" json:"version" yaml:"version" xml:"version"` //应用程序版本
	Channel  string `form:"channel" binding:"-" json:"channel" yaml:"channel" xml:"channel"` //渠道（小米、华为、360、VIVO、OPPO、APPLE)
	ExtInfo  string `form:"extInfo" binding:"-" json:"extInfo" yaml:"extInfo" xml:"extInfo"` //base64 json
}

func (i Info) HasValid() bool {
	return len(i.AppId) > 0 && len(i.Source) > 0 && len(i.Terminal) > 0 && len(i.Version) > 0
}

func (i Info) HasExtInfo() bool {
	return len(i.ExtInfo) > 0
}

// System 用户系统
type System struct {
	DeviceId string `form:"deviceId" binding:"-" json:"deviceId" yaml:"deviceId"`
	SysVer   string `form:"sysVersion" binding:"-" json:"sysVersion" yaml:"sysVer"`
	Model    string `form:"model" binding:"-" json:"model" yaml:"model"`
	Wifi     string `form:"wifi" binding:"-" json:"wifi" yaml:"wifi"`
}

func (i System) HasValid() bool {
	return len(i.DeviceId) > 0 && len(i.SysVer) > 0
}
