package random

import (
	"math/rand"
	"strings"
	"time"
)

var allChar = []byte("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
var letterChar = []byte("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
var numberChar = []byte{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'}

func init() {
}

func Source() rand.Source {
	return rand.NewSource(time.Now().UnixNano())
}

func Random() *rand.Rand {
	return rand.New(Source())
}

func generateString(bytes []byte, length int) string {
	return generateStringV2(Random(), bytes, length)
}

func generateStringV2(r *rand.Rand, bytes []byte, length int) string {
	l := len(bytes)
	if l <= 0 {
		return ""
	}
	build := &strings.Builder{}
	for i := 0; i < length; i++ {
		build.WriteByte(bytes[r.Intn(l)])
	}
	return build.String()
}

// GenerateString
/**
 * 返回一个定长的随机字符串(只包含大小写字母、数字)
 *
 * @param length
 *            随机字符串长度
 * @return 随机字符串
 */
func GenerateString(length int) string {
	return generateString(allChar, length)
}
func GenerateStringV2(r *rand.Rand, length int) string {
	return generateStringV2(r, allChar, length)
}

// GenerateNumString
/**
 * 返回一个定长的随机数字字符串(只包含数字)
 *
 * @param length
 *            随机字符串长度
 * @return 随机字符串
 */
func GenerateNumString(length int) string {
	return generateString(numberChar, length)
}
func GenerateNumStringV2(r *rand.Rand, length int) string {
	return generateStringV2(r, numberChar, length)
}

// GenerateMixString
/**
 * 返回一个定长的随机纯字母字符串(只包含大小写字母)
 *
 * @param length
 *            随机字符串长度
 * @return 随机字符串
 */
func GenerateMixString(length int) string {
	return generateString(letterChar, length)
}
func GenerateMixStringV2(r *rand.Rand, length int) string {
	return generateStringV2(r, letterChar, length)
}

// GenerateLowerString
/**
 * 返回一个定长的随机纯小写字母字符串(只包含大小写字母)
 *
 * @param length
 *            随机字符串长度
 * @return 随机字符串
 */
func GenerateLowerString(length int) string {
	return strings.ToLower(GenerateMixString(length))
}
func GenerateLowerStringV2(r *rand.Rand, length int) string {
	return strings.ToLower(GenerateMixStringV2(r, length))
}

// GenerateUpperString
/**
 * 返回一个定长的随机纯大写字母字符串(只包含大小写字母)
 *
 * @param length
 *            随机字符串长度
 * @return 随机字符串
 **/
func GenerateUpperString(length int) string {
	return strings.ToUpper(GenerateMixString(length))
}
func GenerateUpperStringV2(r *rand.Rand, length int) string {
	return strings.ToUpper(GenerateMixStringV2(r, length))
}
