package tag

import (
	"reflect"
	"strings"
)

func StructVal(s interface{}) reflect.Value {
	v := reflect.ValueOf(s)

	// if pointer get the underlying element≤
	for v.Kind() == reflect.Ptr {
		v = v.Elem()
	}

	if v.Kind() != reflect.Struct {
		panic("not struct")
	}

	return v
}

func StructFields(value reflect.Value, tagName string) []reflect.StructField {
	t := value.Type()

	var f []reflect.StructField

	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		// we can't access the value of unexported fields
		if field.PkgPath != "" {
			continue
		}
		// don't check if it's omitted
		if tag := field.Tag.Get(tagName); tag == "-" {
			continue
		}
		f = append(f, field)
	}
	return f
}

// Tags contains a slice of tag options
type Tags []string

// Has returns true if the given option is available in Tags
func (t Tags) Has(opt string) bool {
	for i := range t {
		if t[i] == opt {
			return true
		}
	}
	return false
}

func Parse(tag string) (string, Tags) {
	// tag is one of followings:
	// ""
	// "name"
	// "name,opt"
	// "name,opt,opt2"
	// ",opt"

	res := strings.Split(tag, ",")
	return res[0], res[1:]
}
