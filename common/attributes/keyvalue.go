package attributes

type KV struct {
	Key interface{} `json:"key"`
	Val interface{} `json:"value"`
}
