package version

import (
	"gitee.com/h79/goutils/common/stringutil"
	"strings"
)

type Version struct {
	version string
	major   int32
	minor   int32
}

func Build(version string) Version {
	v := strings.Split(version, ".")

	var vInt []int32
	for _, val := range v {
		vInt = append(vInt, stringutil.StringToInt32(val))
	}
	major := int32(0)
	minor := int32(0)
	vLen := len(vInt)
	if vLen == 1 {
		major = vInt[0]
	} else if vLen == 2 {
		major = vInt[0]<<8 + vInt[1]
	} else if vLen == 3 {
		major = vInt[0]<<8 + vInt[1]
		minor = vInt[2]
	} else if vLen >= 4 {
		major = vInt[0]<<8 + vInt[1]
		minor = vInt[2]<<8 + vInt[3]
	}
	return Version{version: version, major: major, minor: minor}
}

func (v Version) Compare(that Version) int {
	if v.major > that.major {
		return 1
	} else if v.major < that.major {
		return -1
	}
	if v.minor > that.minor {
		return 1
	} else if v.minor < that.minor {
		return -1
	}
	return 0
}

func (v Version) Major() int32 {
	return v.major
}

func (v Version) Minor() int32 {
	return v.minor
}

func (v Version) Int32() int32 {
	return v.major<<16 + v.minor
}
