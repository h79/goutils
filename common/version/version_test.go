package version

import "testing"

func TestVersion(t *testing.T) {

	ver := Build("1.0.0.1")
	t.Logf("%d", ver.Int32())
}
