package stringutil

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"reflect"
	"regexp"
	"strconv"
	"strings"
)

type Builder struct {
	strings.Builder
}

func (b *Builder) WriteUInt64(n uint64) (int, error) {
	return b.WriteString(UInt64ToString(n))
}

func (b *Builder) WriteInt64(n int64) (int, error) {
	return b.WriteString(Int64ToString(n))
}

func (b *Builder) WriteUInt32(n uint32) (int, error) {
	return b.WriteString(UInt32ToString(n))
}

func (b *Builder) WriteInt32(n int32) (int, error) {
	return b.WriteString(Int32ToString(n))
}

func (b *Builder) WriteUInt(n uint) (int, error) {
	return b.WriteString(UInt32ToString(uint32(n)))
}

func (b *Builder) WriteInt(n int) (int, error) {
	return b.WriteString(IntToString(n))
}

func (b *Builder) WriteF64(n float64) (int, error) {
	return b.WriteString(F64ToString(n))
}

func StringToInt(a string) int {
	if i, err := strconv.Atoi(a); err == nil {
		return i
	}
	return 0
}

func StringToInt32(a string) int32 {
	if i, err := strconv.ParseInt(a, 10, 64); err == nil {
		return int32(i)
	}
	return 0
}

func StringToUInt32(a string) uint32 {
	if i, err := strconv.ParseUint(a, 10, 64); err == nil {
		return uint32(i)
	}
	return 0
}

func StringToInt64(a string) int64 {
	if i, err := strconv.ParseInt(a, 10, 64); err == nil {
		return i
	}
	return 0
}

func StringToUInt64(a string) uint64 {
	if i, err := strconv.ParseUint(a, 10, 64); err == nil {
		return i
	}
	return 0
}

func IntToString(a int) string {
	return strconv.Itoa(a)
}

func UInt32ToString(a uint32) string {
	return strconv.FormatUint(uint64(a), 10)
}

func Int32ToString(a int32) string {
	return strconv.FormatInt(int64(a), 10)
}

func UInt64ToString(a uint64) string {
	return strconv.FormatUint(a, 10)
}

func Int64ToString(a int64) string {
	return strconv.FormatInt(a, 10)
}

func F64ToString(a float64) string {
	return F64ToStringV(a, 2, 64)
}

func F64ToStringV(a float64, prec, bitSize int) string {
	return strconv.FormatFloat(a, 'f', prec, bitSize)
}

func StringToF64(a string) float64 {
	return StringToF64V(a, 64)
}

func Decimal(f float64) float64 {
	f, err := strconv.ParseFloat(fmt.Sprintf("%.3f", f), 64)
	if err != nil {
		return 0.0
	}
	return f
}

func StringToF64V(a string, bitSize int) float64 {
	f, err := strconv.ParseFloat(a, bitSize)
	if err != nil {
		return 0.0
	}
	return f
}

func DefaultString(a string, def string) string {
	if a == "" {
		return def
	}
	return a
}

func UInt32ToBytes(n uint32) []byte {
	return ToBytes[uint32](n)
}

func BytesToUInt32(bys []byte) uint32 {
	return binary.BigEndian.Uint32(bys)
}

func UInt64ToBytes(n uint64) []byte {
	return ToBytes[uint64](n)
}

func BytesToUInt64(bys []byte) uint64 {
	return binary.BigEndian.Uint64(bys)
}

// BytesTo BType not int
func BytesTo[T BType](bys []byte, def T) T {
	var data T
	buf := bytes.NewBuffer(bys)
	if er := binary.Read(buf, binary.BigEndian, &data); er != nil {
		return def
	}
	return data
}

func ToBytes[T BType](n T) []byte {
	buf := bytes.NewBuffer([]byte{})
	if er := binary.Write(buf, binary.BigEndian, n); er != nil {
		return nil
	}
	return buf.Bytes()
}

type BType interface {
	int8 | int16 | int32 | int64 |
		uint8 | uint16 | uint32 | uint64 |
		float64 | float32 | bool
}

const (
	// 带分机号
	regPhoneExt = "^(13\\d|15\\d|17\\d|18\\d|16\\d|19\\d|14\\d)\\d{8}(-\\d{3,6})$"
	regPhone    = "^(13\\d|15\\d|17\\d|18\\d|16\\d|19\\d|14\\d)\\d{8}(-\\d{3,6})?$"
	regEmail    = "^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+"
	regName     = "^[a-z0-9_-]{3,20}$"
)

var (
	emailCompile    = regexp.MustCompile(regEmail)
	phoneCompile    = regexp.MustCompile(regPhone)
	phoneExtCompile = regexp.MustCompile(regPhoneExt)
	nameCompile     = regexp.MustCompile(regName)

	hasHKPhoneCompile    = regexp.MustCompile("(5|6|8|9)\\d{7}")
	hasChinaPhoneCompile = regexp.MustCompile("(13\\d|15\\d|17\\d|18\\d|16\\d|19\\d|14\\d)\\d{8}")
)

// IsEmailValidate 邮箱合规
func IsEmailValidate(email string) bool {
	return emailCompile.MatchString(email)
}

// IsPhoneValidate 手机验证
func IsPhoneValidate(mobile string) bool {
	return phoneCompile.MatchString(mobile)
}

// IsPhoneExtValidate 带分机
func IsPhoneExtValidate(mobile string) bool {
	return phoneExtCompile.MatchString(mobile)
}

// IsNameValidate 用户名验证
func IsNameValidate(name string) bool {
	return nameCompile.MatchString(name)
}

/**
 * 字符串是否有手机
 * \d 匹配一个或多个数字&#xff0c;其中 \ 要转义&#xff0c;所以是 \\d
 * $ 匹配输入字符串结尾的位置
 */

func HasPhoneLegal(num string) bool {
	return HasChinaPhoneLegal(num) || HasHKPhoneLegal(num)
}

func HasChinaPhoneLegal(num string) bool {
	return hasChinaPhoneCompile.MatchString(num)
}

func GetChinaPhone(num string) string {
	return hasChinaPhoneCompile.FindString(num)
}

func HasHKPhoneLegal(num string) bool {
	return hasHKPhoneCompile.MatchString(num)
}

func GetHKPhone(num string) string {
	return hasHKPhoneCompile.FindString(num)
}

type Number struct {
	Num     int64
	Decimal float64
}

func NumberWithI64(v int64) Number {
	return Number{Num: v}
}

func NumberWithF64(v float64, prec int) Number {
	var ff = strings.Split(F64ToStringV(v, prec, 64), ".")
	return Number{Num: StringToInt64(ff[0]), Decimal: StringToF64("0." + ff[1])}
}

func NumberWithString(number string, prec int) Number {
	if strings.HasSuffix(number, "%") {
		return NumberWithF64(StringToF64(strings.TrimSuffix(number, "%"))/100, prec)
	}
	if strings.HasPrefix(number, "%") {
		return NumberWithF64(StringToF64(strings.TrimPrefix(number, "%"))/100, prec)
	}
	var nr = Number{Decimal: 0}
	sp := strings.SplitN(number, ".", 2)
	if len(sp) >= 2 { //有小数点
		nr.Decimal = StringToF64("0." + sp[1])
	}
	nr.Num = StringToInt64(sp[0])
	return nr
}

func (n Number) String() string {
	return n.ToString(4)
}

func (n Number) ToString(prec int) string {
	var out = ""
	if n.Num > 0 {
		out = Int64ToString(n.Num)
	} else if n.Num == 0 {
		out = "0"
	}
	if n.Decimal <= 0 {
		return out
	}
	dec := F64ToStringV(n.Decimal, prec, 64)
	ff := strings.Split(dec, ".")
	if ff[0] == "0" {
		out += "." + ff[1]
	} else {
		out += "." + ff[0]
	}
	return out
}

func (n Number) ToF64(prec int) float64 {
	return StringToF64(n.ToString(prec))
}

func (n Number) ToI64() int64 {
	return int64(StringToF64(n.ToString(0)))
}

func NumberValue(number string) (num int64, decimal float64) {
	var n = NumberWithString(number, 4)
	return n.Num, n.Decimal
}

// PercentIf
// 是否按比例分，是返回,比例值，true
func PercentIf(number string) (float64, bool) {
	num, decimal := NumberValue(number)
	if num == 0 {
		return decimal, true
	}
	return float64(num), false
}

func ATo(arr []interface{}) []string {
	var res = make([]string, len(arr))
	for i := range arr {
		res[i] = ToString(arr[i])
	}
	return res
}

func ToString(value interface{}) string {
	reflectValue := reflect.Indirect(reflect.ValueOf(value))
	return ToStringV2(reflectValue)
}

func ToStringV2(reflectValue reflect.Value) string {

	switch reflectValue.Kind() {
	case reflect.String:
		return reflectValue.String()

	case reflect.Int:
		fallthrough
	case reflect.Int8:
		fallthrough
	case reflect.Int16:
		fallthrough
	case reflect.Int32:
		fallthrough
	case reflect.Int64:
		return strconv.FormatInt(reflectValue.Int(), 10)

	case reflect.Uint:
		fallthrough
	case reflect.Uint8:
		fallthrough
	case reflect.Uint16:
		fallthrough
	case reflect.Uint32:
		fallthrough
	case reflect.Uint64:
		return strconv.FormatUint(reflectValue.Uint(), 10)

	case reflect.Float32:
		fallthrough
	case reflect.Float64:
		return strconv.FormatFloat(reflectValue.Float(), 'f', 5, 64)

	case reflect.Bool:
		if reflectValue.Bool() {
			return "true"
		} else {
			return "false"
		}
	default:
		msg := fmt.Sprintf("not support the '%v' data", reflectValue.Kind())
		panic(msg)
	}
	return ""
}
