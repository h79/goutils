package stringutil

import (
	"regexp"
	"testing"
)

func TestStringValidate(t *testing.T) {
	if !IsNameValidate("hullo001") {
		t.Error("name is invalidate")
	}

	if !IsPhoneValidate("23622283547") {
		t.Error("23622283547 number is invalidate")
	}

	if !IsPhoneValidate("13844486306-44") {
		t.Error("13844486306 number is invalidate")
	}

	if !IsPhoneExtValidate("13844486306-44466") {
		t.Error("13844486306 number is invalidate")
	}

	if !HasChinaPhoneLegal("sdsxvxvx13844486306-444456") {
		t.Error("13844486306 number is invalidate")
	}

	if !IsEmailValidate("sybgo@sina.com") {
		t.Error("email is invalidate")
	}
	//
	var text = "SFSF<a href=\"https://wx889ba45762981adb.wxcp.qidian.com/ubz/QKcs9\">惊喜+1，充9.9送9.9</a>\n\n<a href=\"https://wx889ba45762981adb.wxcp.qidian.com/pov/QKcs5\">惊喜+2，充45送45</a>\n\n<a href=\"https://wx889ba45762981adb.wxcp.qidian.com/ykv/QKcsl\">惊喜+3，充95送95</a>"

	reg, er := regexp.Compile(`<a href=(.*)>(.*)</a>`)
	if er != nil {
		t.Error("xxx", er)
		return
	}
	aa := reg.FindAllStringSubmatch(text, 10)
	t.Log("regexp:", aa)
}

func TestBytesTo(t *testing.T) {
	buff := []byte{0, 0, 0, 19, 57, 48}
	buff1 := []byte{0, 12, 0, 19, 57, 48}

	l := BytesTo[int32](buff[0:4], 0)
	t.Log(l)
	ll := BytesToUInt32(buff1[0:4])
	t.Log(ll)
}

func TestNumberValue(t *testing.T) {
	num := NumberWithString("%0.2", 3)
	t.Logf("%v", num.ToString(3))

	t.Log(Decimal(num.Decimal))

	var xx = []interface{}{1, 2, 3, 5}
	t.Log(ATo(xx))
}
