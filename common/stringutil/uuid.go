package stringutil

import (
	"github.com/google/uuid"
	"strings"
)

func Uuid() string {
	if id, err := uuid.NewUUID(); err == nil {
		return id.String()
	}
	return ""
}

func UuidNot() string {

	return strings.Join(strings.Split(Uuid(), "-"), "")
}
