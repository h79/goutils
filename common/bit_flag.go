package common

type Bit[T BitType] interface {
	Int() T
}

type BitType interface {
	int8 | int | int32 | int64 | uint8 | uint | uint32 | uint64
}

func IsBit[T BitType](v T, b T) bool {
	return v&b == b
}

type Flag[T BitType] struct {
	FInt T `json:"flag,omitempty"`
}

func WithFlag[T BitType](flag T) Flag[T] {
	return Flag[T]{FInt: flag}
}

func (f Flag[T]) Int() T {
	return f.FInt
}

func (f Flag[T]) IsBit(bit Bit[T]) bool {
	return f.FInt&bit.Int() == bit.Int()
}

func (f Flag[T]) Must(i T) Flag[T] {
	f.FInt |= i
	return f
}

func (f Flag[T]) MustFlag(i Flag[T]) Flag[T] {
	f.FInt |= i.Int()
	return f
}
