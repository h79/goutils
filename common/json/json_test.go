package json

import (
	"encoding/json"
	"strconv"
	"strings"
	"testing"
)

func TestQuote(t *testing.T) {
	var i = strconv.Quote("\"xxxx\n")

	t.Log(i)

	var j, _ = strconv.Unquote(i)

	t.Log(j)

	var jj = "{\"uid\":\"334\",\"xx\":\"deddddd\n\"}"
	jj = strings.ReplaceAll(jj, "\n", "\\n")
	var out = map[string]string{}
	var _ = json.Unmarshal([]byte(jj), &out)
	t.Log(out)
}
