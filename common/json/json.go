package json

import (
	"bytes"
	"encoding/json"
	"gitee.com/h79/goutils/common/file"
	"gitee.com/h79/goutils/common/result"
	"os"
	"strings"
	"unicode"
	"unicode/utf16"
	"unicode/utf8"
)

type DumpOption func(enc *json.Encoder)

func EscapeHTML(escape bool) DumpOption {
	return func(enc *json.Encoder) {
		enc.SetEscapeHTML(escape)
	}
}

// Indent sets the indentation level (passed as string of spaces) for DumpBytes/DumpString
func Indent(prefix, indent string) DumpOption {
	return func(enc *json.Encoder) {
		enc.SetIndent(prefix, indent)
	}
}

// DumpBytes convert Go data object to JSON []byte
func DumpBytes(obj interface{}, options ...DumpOption) ([]byte, error) {
	var b bytes.Buffer
	enc := json.NewEncoder(&b)
	enc.SetEscapeHTML(false)

	for _, opt := range options {
		opt(enc)
	}
	err := enc.Encode(obj)
	return b.Bytes(), err
}

// DumpString encode Go data object to JSON string
func DumpString(obj interface{}, options ...DumpOption) (string, error) {
	if res, err := DumpBytes(obj, options...); err != nil {
		return "", err
	} else {
		return string(res), nil
	}
}

// MustDumpBytes encode Go data object to JSON []byte (panic in case of error)
func MustDumpBytes(obj interface{}, options ...DumpOption) []byte {
	if res, err := DumpBytes(obj, options...); err != nil {
		panic(err)
	} else {
		return res
	}
}

// MustDumpString encode Go data object to JSON string (panic in case of error)
func MustDumpString(obj interface{}, options ...DumpOption) string {
	if res, err := DumpString(obj, options...); err != nil {
		panic(err)
	} else {
		return res
	}
}

// ToString 序列化参数
func ToString(o interface{}) string {
	// 序列化一次
	raw, _ := json.Marshal(o)

	// 反序列化为map
	m := make(map[string]interface{})
	reader := bytes.NewReader(raw)
	decode := json.NewDecoder(reader)
	decode.UseNumber()
	_ = decode.Decode(&m)

	// 重新做一次序列化，并禁用Html Escape
	buffer := bytes.NewBufferString("")
	encoder := json.NewEncoder(buffer)
	encoder.SetEscapeHTML(false)
	_ = encoder.Encode(m)

	// Trim掉末尾的换行符
	return strings.TrimSpace(buffer.String())
}

func Write(name string, data interface{}, perm os.FileMode) error {
	return file.WriteFileName(name, perm, func(w *os.File) error {
		enc := json.NewEncoder(w)
		if err := enc.Encode(data); err != nil {
			return result.Errorf(result.ErrJson, "Encode %s json failed,err: %v", name, err).Log()
		}
		return nil
	})
}

func Read(name string, data interface{}) error {
	_, err := file.ReadFileName(name, func(r *os.File) error {
		dec := json.NewDecoder(r)
		if err := dec.Decode(data); err != nil {
			return result.Errorf(result.ErrJson, "Decode %s json failed,err: %v", name, err).Log()
		}
		return nil
	})
	return err
}

func UnQuoteBytes(s []byte) (t []byte, ok bool) {
	if len(s) < 2 || s[0] != '"' || s[len(s)-1] != '"' {
		return s, true
	}
	s = s[1 : len(s)-1]

	// Check for unusual characters. If there are none,
	// then no unquoting is needed, so return a slice of the
	// original bytes.
	r := 0
	for r < len(s) {
		c := s[r]
		if c == '\\' || c == '"' || c < ' ' {
			break
		}
		if c < utf8.RuneSelf {
			r++
			continue
		}
		rr, size := utf8.DecodeRune(s[r:])
		if rr == utf8.RuneError && size == 1 {
			break
		}
		r += size
	}
	if r == len(s) {
		return s, true
	}

	b := make([]byte, len(s)+2*utf8.UTFMax)
	w := copy(b, s[0:r])
	for r < len(s) {
		// Out of room? Can only happen if s is full of
		// malformed UTF-8 and we're replacing each
		// byte with RuneError.
		if w >= len(b)-2*utf8.UTFMax {
			nb := make([]byte, (len(b)+utf8.UTFMax)*2)
			copy(nb, b[0:w])
			b = nb
		}
		switch c := s[r]; {
		case c == '\\':
			r++
			if r >= len(s) {
				return
			}
			switch s[r] {
			default:
				return
			case '"', '\\', '/', '\'':
				b[w] = s[r]
				r++
				w++
			case 'b':
				b[w] = '\b'
				r++
				w++
			case 'f':
				b[w] = '\f'
				r++
				w++
			case 'n':
				b[w] = '\n'
				r++
				w++
			case 'r':
				b[w] = '\r'
				r++
				w++
			case 't':
				b[w] = '\t'
				r++
				w++
			case 'u':
				r--
				rr := getU4(s[r:])
				if rr < 0 {
					return
				}
				r += 6
				if utf16.IsSurrogate(rr) {
					rr1 := getU4(s[r:])
					if dec := utf16.DecodeRune(rr, rr1); dec != unicode.ReplacementChar {
						// A valid pair; consume.
						r += 6
						w += utf8.EncodeRune(b[w:], dec)
						break
					}
					// Invalid surrogate; fall back to replacement rune.
					rr = unicode.ReplacementChar
				}
				w += utf8.EncodeRune(b[w:], rr)
			}

		// Quote, control characters are invalid.
		case c == '"', c < ' ':
			return

		// ASCII
		case c < utf8.RuneSelf:
			b[w] = c
			r++
			w++

		// Coerce to well-formed UTF-8.
		default:
			rr, size := utf8.DecodeRune(s[r:])
			r += size
			w += utf8.EncodeRune(b[w:], rr)
		}
	}
	return b[0:w], true
}

// getU4 decodes \uXXXX from the beginning of s, returning the hex value,
// or it returns -1.
func getU4(s []byte) rune {
	if len(s) < 6 || s[0] != '\\' || s[1] != 'u' {
		return -1
	}
	var r rune
	for _, c := range s[2:6] {
		switch {
		case '0' <= c && c <= '9':
			c = c - '0'
		case 'a' <= c && c <= 'f':
			c = c - 'a' + 10
		case 'A' <= c && c <= 'F':
			c = c - 'A' + 10
		default:
			return -1
		}
		r = r*16 + rune(c)
	}
	return r
}

type coder struct {
}

func (j *coder) Unmarshal(content []byte, v interface{}) error {
	return json.Unmarshal(content, v)
}

func (j *coder) Marshal(v interface{}) ([]byte, error) {
	return json.Marshal(v)
}

var DefaultCoder = &coder{}
