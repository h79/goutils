package coder

import "io"

type Decoder interface {
	Decode(dest string) string
}

type DecodeFunc func(dest string) string

func (f DecodeFunc) Decode(dest string) string {
	return f(dest)
}

type Encoder interface {
	Encode(src string) string
}

type EncoderFunc func(dest string) string

func (f EncoderFunc) Encode(dest string) string {
	return f(dest)
}

type DecoderReader interface {
	Decode(data any, r io.Reader) error
}

type DecodeReaderFunc func(data any, r io.Reader) error

func (f DecodeReaderFunc) Decode(data any, r io.Reader) error {
	return f(data, r)
}

type EncoderWriter interface {
	Encode(data any, r io.Writer) error
}

type EncoderWriterFunc func(data any, r io.Writer) error

func (f EncoderWriterFunc) Encode(data any, r io.Writer) error {
	return f(data, r)
}

type Unmarshal interface {
	Unmarshal(content []byte, v interface{}) error
}

type UnmarshalFunc func(content []byte, v interface{}) error

func (f UnmarshalFunc) Unmarshal(content []byte, v interface{}) error {
	return f(content, v)
}

type Marshal interface {
	Marshal(v interface{}) ([]byte, error)
}
type MarshalFunc func(v interface{}) ([]byte, error)

func (f MarshalFunc) Marshal(v interface{}) ([]byte, error) {
	return f(v)
}

type Coder interface {
	Unmarshal
	Marshal
}
