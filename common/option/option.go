package option

type Option interface {
	String() string
	Type() int
	Value() any
}

type Opt[T any] struct {
	v T
	d string
	t int
}

func WithOpt[T any](v T, desc string, t int) Option {
	return &Opt[T]{v: v, d: desc, t: t}
}

func (o Opt[T]) String() string {
	return o.d
}
func (o Opt[T]) Type() int  { return o.t }
func (o Opt[T]) Value() any { return o.v }

func Filter(filter func(opt Option) bool, opts ...Option) {
	if len(opts) == 0 {
		return
	}
	for i := range opts {
		if opts[i] != nil && filter(opts[i]) {
			return
		}
	}
}

func Exist(ty int, opts ...Option) (Option, bool) {
	if len(opts) == 0 {
		return nil, false
	}
	for i := range opts {
		if opts[i] != nil && opts[i].Type() == ty {
			return opts[i], true
		}
	}
	return nil, false
}

func Select[T any](t int, defValue T, opts ...Option) T {
	if len(opts) == 0 {
		return defValue
	}
	r, ok := Exist(t, opts...)
	if !ok {
		return defValue
	}
	if ret, o := r.Value().(T); o {
		return ret
	}
	return defValue
}

func SelectV2[T any](t int, defValue T, opts ...Option) (T, bool) {
	if len(opts) == 0 {
		return defValue, false
	}
	r, ok := Exist(t, opts...)
	if !ok {
		return defValue, false
	}
	if ret, o := r.Value().(T); o {
		return ret, true
	}
	return defValue, false
}
