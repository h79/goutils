package xml

import (
	"encoding/xml"
	"gitee.com/h79/goutils/common/file"
	"gitee.com/h79/goutils/common/result"
	"os"
)

func Write(name string, data interface{}, perm os.FileMode) error {
	return file.WriteFileName(name, perm, func(w *os.File) error {
		enc := xml.NewEncoder(w)
		if err := enc.Encode(data); err != nil {
			return result.Errorf(result.ErrXml, "Encode %s xml failed,err: %v", name, err).Log()
		}
		return nil
	})
}

func Read(name string, data interface{}) error {
	_, err := file.ReadFileName(name, func(r *os.File) error {
		dec := xml.NewDecoder(r)
		if err := dec.Decode(data); err != nil {
			return result.Errorf(result.ErrXml, "Decode %s xml failed,err: %v", name, err).Log()
		}
		return nil
	})
	return err
}

type coder struct {
}

func (j *coder) Unmarshal(content []byte, v interface{}) error {
	return xml.Unmarshal(content, v)
}

func (j *coder) Marshal(v interface{}) ([]byte, error) {
	return xml.Marshal(v)
}

var DefaultCoder = &coder{}
