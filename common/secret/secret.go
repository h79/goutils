package secret

var _ Key = (*Secret)(nil)

type Secret struct {
	Value  string `json:"key"`
	Expire int64  `json:"expireIn"`
}

func (s *Secret) Key() []byte {
	return []byte(s.Value)
}

func (s *Secret) ExpireIn() int64 {
	return s.Expire
}
