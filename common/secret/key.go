package secret

type Key interface {
	Key() []byte
	ExpireIn() int64
}
