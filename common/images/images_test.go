package images

import "testing"

func TestHistogram(t *testing.T) {
	hist1 := NewHistogram()
	if er := hist1.OpenFile("./2.png"); er != nil {
		t.Error(er)
	}

	hist2 := NewHistogram()
	if er := hist2.OpenFile("./3.png"); er != nil {
		t.Error(er)
	}

	t.Log("match: ", hist1.Match(hist2))
}

func TestDistance(t *testing.T) {
	hash1 := NewPHash()
	if er := hash1.OpenFile("./1.jpg"); er != nil {
		t.Error(er)
	}

	hash2 := NewPHash()
	if er := hash2.OpenFile("./1-1.jpg"); er != nil {
		t.Error(er)
	}

	t.Log("match: ", hash1.Match(hash2))
}
