package tls

import (
	"crypto/tls"
	commontls "gitee.com/h79/goutils/common/tls"
	"google.golang.org/grpc/credentials"
)

type Server struct {
	commontls.Tls
}

func (t *Server) GetCredentialsByCA() (credentials.TransportCredentials, error) {
	cert, certPool, err := t.GetCredential()
	if err != nil {
		return nil, err
	}

	c := credentials.NewTLS(&tls.Config{
		Certificates: []tls.Certificate{cert},
		ClientAuth:   tls.RequireAndVerifyClientCert,
		ClientCAs:    certPool,
	})

	return c, err
}

func (t *Server) GetTLSCredentials() (credentials.TransportCredentials, error) {
	c, err := credentials.NewServerTLSFromFile(t.CertFile, t.KeyFile)
	if err != nil {
		return nil, err
	}

	return c, err
}
