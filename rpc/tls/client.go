package tls

import (
	"crypto/tls"
	commontls "gitee.com/h79/goutils/common/tls"
	"google.golang.org/grpc/credentials"
)

type Client struct {
	commontls.Tls
	ServerName string `json:"serverName" yaml:"serverName" xml:"serverName"`
}

func (t *Client) GetCredentialsByCA() (credentials.TransportCredentials, error) {
	cert, certPool, err := t.GetCredential()
	if err != nil {
		return nil, err
	}

	return credentials.NewTLS(&tls.Config{
		Certificates: []tls.Certificate{cert},
		ServerName:   t.ServerName,
		RootCAs:      certPool,
	}), nil
}

func (t *Client) GetTLSCredentials() (credentials.TransportCredentials, error) {
	c, err := credentials.NewClientTLSFromFile(t.CertFile, t.ServerName)
	if err != nil {
		return nil, err
	}
	return c, err
}
