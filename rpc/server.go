package rpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func Reflection(grpcServer *grpc.Server) {
	reflection.Register(grpcServer)
}
