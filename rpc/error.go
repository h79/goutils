package rpc

import (
	"gitee.com/h79/goutils/common/result"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func WithStatus(err error) *status.Status {
	if st, ok := status.FromError(err); ok {
		return st
	}
	return nil
}

func IsDeadlineExceeded(s *status.Status) bool {
	return s != nil && s.Code() == codes.DeadlineExceeded
}

func IsUnimplemented(s *status.Status) bool {
	return s != nil && s.Code() == codes.Unimplemented
}

func IsClosed(s *status.Status) bool {
	return s != nil && s.Code() == codes.Canceled
}

func IsNeedAlarm(s *status.Status) bool {
	return s != nil && (s.Code() == codes.DeadlineExceeded || s.Code() == codes.Unimplemented)
}

func ErrIsDeadlineExceeded(err error) bool {
	return result.ErrCode(result.ErrDeadlineExceeded).Is(err)
}
