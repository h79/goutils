package rpc

import (
	"context"
	"fmt"
	"gitee.com/h79/goutils/alarm"
	"gitee.com/h79/goutils/common/logger"
	"gitee.com/h79/goutils/common/result"
	"gitee.com/h79/goutils/common/system"
	"google.golang.org/grpc"
	"google.golang.org/grpc/connectivity"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/health/grpc_health_v1"
	"sync"
	"time"
)

func NewClient(addr string, opts ...grpc.DialOption) (*grpc.ClientConn, error) {
	logger.D("rpc", "NewClient, addr= %s", addr)
	var dialOpts []grpc.DialOption
	if len(opts) <= 0 {
		dialOpts = append(dialOpts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	} else {
		dialOpts = append(dialOpts, opts...)
	}
	conn, err := grpc.NewClient(addr, dialOpts...)
	if err != nil {
		return nil, result.Errorf(result.ErrClientConnectInternal, "did not connect failure, err= %v", err).Log()
	}
	return conn, nil
}

func Close(client *grpc.ClientConn) {
	if client != nil {
		_ = client.Close()
	}
}

type Client struct {
	Config
	health HealthClient
	conn   *grpc.ClientConn
	rm     sync.Mutex
}

func (cli *Client) CreateConnect(opts ...grpc.DialOption) (*grpc.ClientConn, error) {
	cli.rm.Lock()
	defer cli.rm.Unlock()
	err := cli.connect(opts...)
	return cli.conn, err
}

// Idle：表示客户端连接处于空闲状态。此时连接可能还没有开始建立，或者之前的连接已经关闭，等待新的连接请求。
// Connecting：表示客户端正在尝试建立与服务器的连接。在这个状态下，客户端会发送连接请求，等待服务器的响应。
// Ready：表示客户端连接已经成功建立，并且可以正常进行 RPC 调用。此时客户端可以向服务器发送请求并接收响应。
// TransientFailure：表示客户端连接遇到了临时故障，但预计可以恢复。例如，网络暂时中断、服务器暂时不可用等。在这种情况下，客户端可能会尝试重新连接。
// Shutdown：表示客户端连接已经开始关闭。一旦进入这个状态，连接将不再接受新的请求，并且会逐步释放相关资源。
func (cli *Client) connect(opts ...grpc.DialOption) error {
	if cli.conn == nil {
		conn, err := NewClient(cli.Server, opts...)
		if err != nil {
			return err
		}
		cli.conn = conn
	}
	return cli.CheckConnect()
}

func (cli *Client) CheckConnect() error {
	state := cli.conn.GetState()
	switch state {
	case connectivity.Shutdown:
		// 重连
		logger.D("rpc", "connect state is SHUTDOWN")
		cli.conn.Connect()
		err := cli.waitConnect(30)
		if err != nil {
			return result.Errorf(result.ErrClientConnectInternal, err.Error())
		}
	case connectivity.Ready:
		// 准备好
	case connectivity.Connecting:
		// 正在连
		logger.D("rpc", "connect state is CONNECTING")
		err := cli.waitConnect(10)
		if err != nil {
			return err
		}
		logger.D("rpc", "CONNECTING to wait connect completed")

	case connectivity.Idle:
		// 还没有连接,需要等待吗，卡10秒
		logger.D("rpc", "connect state is IDLE")
		cli.conn.Connect()
		err := cli.waitConnect(30)
		if err != nil {
			return err
		}
		logger.D("rpc", "IDLE to wait connect completed")

	case connectivity.TransientFailure:
		logger.D("rpc", "connect state is TRANSIENT FAILURE")
		err := cli.waitConnect(120)
		if err != nil {
			return err
		}
		logger.D("rpc", "TRANSIENT FAILURE to wait connect completed")
	default:
		logger.D("rpc", "connect state is %v, it is ignored", state)
	}
	return nil
}

func (cli *Client) waitStateChanged(timeout time.Duration, state connectivity.State) bool {
	ctx, cancel := context.WithTimeout(context.Background(), timeout*time.Second)
	defer cancel()
	return cli.conn.WaitForStateChange(ctx, state)
}

func (cli *Client) waitConnect(timeout time.Duration) error {
	errCh := make(chan error, 1)
	go func() {
		for {
			if cli.conn == nil {
				errCh <- result.ErrCode(int32(connectivity.Shutdown))
				return
			}
			state := cli.conn.GetState()
			if state == connectivity.Ready {
				errCh <- nil
				return
			}
			//wait 状态有变化
			if !cli.waitStateChanged(timeout, state) {
				errCh <- result.ErrCode(result.ErrDeadlineExceeded)
				return
			}
		}
	}()
	dead := time.NewTimer((timeout + 2) * time.Second)
	defer dead.Stop()
	select {
	case err := <-errCh:
		return err
	case <-dead.C:
		return result.ErrCode(result.ErrDeadlineExceeded)
	case <-system.Closed():
		return system.ClosedError
	}
}

func (cli *Client) Close() {
	cli.close(cli.conn)
}

func (cli *Client) close(conn *grpc.ClientConn) {
	var c = conn
	cli.rm.Lock()
	if conn == cli.conn {
		c = cli.conn
		cli.conn = nil
	}
	cli.rm.Unlock()
	Close(c)
}

func (cli *Client) CheckHealth() (int, error) {
	start := time.Now()
	conn, err := cli.CreateConnect()
	if err != nil {
		return 0, err
	}
	// Set up a connection to the server.
	if conn == nil {
		return int(grpc_health_v1.HealthCheckResponse_UNKNOWN), result.RErrNil
	}

	rsp, err := cli.health.Check(conn, cli.Service, cli.Timeout)

	if err != nil {
		const method = "Check"
		return rsp, cli.HandlerError(conn, err, start, method)
	}
	return rsp, nil
}

func (cli *Client) WatchHealth() (grpc_health_v1.Health_WatchClient, error) {
	start := time.Now()
	conn, err := cli.CreateConnect()
	if err != nil {
		return nil, err
	}
	// Set up a connection to the server.
	if conn == nil {
		return nil, result.RErrNil
	}

	rsp, err := cli.health.Watch(conn, cli.Service, cli.Timeout)

	if err != nil {
		const method = "Watch"
		return nil, cli.HandlerError(conn, err, start, method)
	}
	return rsp, nil
}

// HandlerError "conn state":"IDLE","Status":"rpc error: code = Unknown desc = rpc error: code = DeadlineExceeded desc = context deadline exceeded"}
// HandlerError "conn state= SHUTDOWN, status= rpc error: code = Canceled desc = grpc: the client connection is closing
func (cli *Client) HandlerError(conn *grpc.ClientConn, err error, start time.Time, method string) error {
	logger.E("rpc", "HandlerError conn state= %v, err= %s", conn.GetState(), err)
	st := WithStatus(err)
	if st == nil {
		return err
	}
	if IsNeedAlarm(st) {
		cli.Alarm(method, time.Now().Sub(start), err)
	}
	if IsDeadlineExceeded(st) {
		return result.ErrCode(result.ErrDeadlineExceeded).WithError(err)
	}
	return err
}

func (cli *Client) Alarm(method string, latency time.Duration, err error) {
	alarm.Fatal(context.Background(), -1, "rpc", cli.Title, fmt.Sprintf("method= '%s',url= '%s',latency= '%v'", method, cli.Server, latency), err)
}
