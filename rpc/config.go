package rpc

import (
	"time"
)

type Config struct {
	Timeout time.Duration
	Server  string
	Service string
	Title   string
}

type OptionFunc func(conf *Config)
