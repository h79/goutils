package rpc

import (
	"context"
	"google.golang.org/grpc"
)

type AuthFunc func(ctx context.Context, req interface{}, fullMethodName string) (context.Context, error)

// ServiceAuthOverride allows a given gRPC service implementation to override the global `AuthFunc`.
//
// If a service implements the AuthFuncOverride method, it takes precedence over the `AuthFunc` method,
// and will be called instead of AuthFunc for all method invocations within that service.
type ServiceAuthOverride interface {
	AuthFuncOverride(ctx context.Context, req interface{}, fullMethodName string) (context.Context, error)
}

// UnaryServerInterceptor returns a new unary server interceptors that performs per-request auth.
func UnaryServerInterceptor(authFunc AuthFunc) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		var newCtx context.Context
		var err error
		if overrideSrv, ok := info.Server.(ServiceAuthOverride); ok {
			newCtx, err = overrideSrv.AuthFuncOverride(ctx, req, info.FullMethod)
		} else {
			newCtx, err = authFunc(ctx, req, info.FullMethod)
		}
		if err != nil {
			return nil, err
		}
		return handler(newCtx, req)
	}
}
