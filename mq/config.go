package mq

type ClientConfig struct {
	GroupID      string `json:"groupId" yaml:"groupId" xml:"groupId"`
	Server       string `json:"server" yaml:"server" xml:"server"`
	Domain       string `json:"domain" yaml:"domain" xml:"domain"`
	GroupName    string `json:"groupName" yaml:"groupName" xml:"groupName"`
	InstanceName string `json:"instance" yaml:"instance" xml:"instance"`
}

type Credentials struct {
	AccessKey   string `json:"accessKey" yaml:"accessKey" xml:"accessKey"`
	SecretKey   string `json:"secretKey" yaml:"secretKey" xml:"secretKey"`
	SecretToken string `json:"secretToken" yaml:"secretToken" xml:"secretToken"`
}

type Config struct {
	Client       ClientConfig   `json:"client" yaml:"client" xml:"client"`
	Credentials  Credentials    `json:"credentials" yaml:"credentials" xml:"credentials"`
	LogLevel     int            `json:"logLevel" yaml:"logLevel" xml:"logLevel"`
	ProducerConf ProducerConfig `json:"producer" yaml:"producer" xml:"producer"`
	ConsumerConf ConsumerConfig `json:"consumer" yaml:"consumer" xml:"consumer"`
}

type ProducerConfig struct {
	Timeout  int `json:"timeout" yaml:"timeout" xml:"timeout"`
	Compress int `json:"compress" yaml:"compress" xml:"compress"`
	Size     int `json:"size" yaml:"size" xml:"size"`
	Retry    int `json:"retry" yaml:"retry" xml:"retry"`
}

type ConsumerConfig struct {
	ThreadCount int `json:"threadCount" yaml:"threadCount" xml:"threadCount"`
	Model       int `json:"model" yaml:"model" xml:"model"`
	Size        int `json:"size" yaml:"size" xml:"size"`
}
