package mq

type Topic struct {
	Topic      string
	BrokerName string
	QueueId    int
}

type Expression struct {
	Type string
	Exp  string
}

type Message struct {
	MsgId    string
	QueueId  int
	Topic    string
	Tags     string
	Keys     string
	Body     string
	Property map[string]string
}

type Status int

const (
	ConOk   = Status(0)
	ConFail = Status(1)
)

type Type int

const (
	ConTypePush = Type(0)
	ConTypePull = Type(1)
)

type Consumer interface {
	Start() error

	Stop()

	//Subscribe 建议外面使用 go goroutine
	Subscribe(topic Topic, exp Expression, call func(msg *Message) Status) error
}

func NewConsumer(con Type, cfg Config) Consumer {
	switch con {
	case ConTypePush:
		return NewPushConsumer(cfg)
	case ConTypePull:
		return NewPullConsumer(cfg)
	}
	return nil
}
