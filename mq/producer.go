package mq

type Producer interface {
	Start() error

	Stop()

	SendJson(topic string, event interface{}) error

	SendBytes(topic string, data []byte) error

	SendAsync(topic string, data []byte) error
}
