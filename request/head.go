package request

import "time"

// Head
/** 为了让 api.Head 及rpc.Request(proto)都能 */
type Head interface {
	GetSource() string
	GetSeqId() string
	GetVersion() string
}

type HeadV2 interface {
	Head
	GetTimeOut() time.Duration
	SetTimeOut(duration time.Duration)
	GetRecvAt() int64 // GetRecvAt 服务收到时间
	GetReqAt() int64  // GetReqAt 请求者时间
	SetSource(source string)
}
