package request

type Query struct {
	AppId       string `form:"appid" binding:"required" json:"appid"` //appid
	ComponentId string `form:"componentId" binding:"-" json:"componentId"`
}

type Content struct {
	Type    string `json:"type,omitempty"`
	Content []byte `json:"content,omitempty"`
}

func (c *Content) GetType() string {
	return c.Type
}

func (c *Content) GetContent() []byte {
	return c.Content
}
