package sensitive

import (
	"regexp"
	"testing"
)

func TestSensitive(t *testing.T) {
	rege := NewRegexp()

	reg := regexp.MustCompile("(13\\d|15\\d|17\\d|18\\d|16\\d|19\\d|14\\d)\\d{8}")
	b := reg.MatchString("验证零13822283547和非零开头")
	t.Log(b)

	rege.Add("(13\\d|15\\d|17\\d|18\\d|16\\d|19\\d|14\\d)\\d{8}")

	_, err := rege.Add("我是谁")
	if err != nil {
		t.Error(err)
	}
	tx := "和非零和非零和非零和非零验证\r\n零13822283547和非零13822283547开头\r\n和非零13822283547开头\r\n和非零和非零和非零和非零和非零和非零"
	filter := rege.Filter(tx)
	t.Log(filter)

	filter = rege.Replace(tx, "***")
	t.Log(filter)

	ok, filt := rege.FindIn(tx)
	t.Logf("FindIn ok= %v,f=%v", ok, filt)

	group := rege.FindAll(tx)
	for _, g := range group {
		t.Logf("group= %+v\n", g)
	}
	t.Log(tx[:9])
}

//验证数字：^[0-9]*$
//验证n位的数字：^\d{n}$
//验证至少n位数字：^\d{n,}$
//验证m-n位的数字：^\d{m,n}$
//验证零和非零开头的数字：^(0|[1-9][0-9]*)$
//验证有两位小数的正实数：^[0-9]+(.[0-9]{2})?$
//验证有1-3位小数的正实数：^[0-9]+(.[0-9]{1,3})?$
//验证非零的正整数：^\+?[1-9][0-9]*$

//验证电话号码: ^(\(\d{3,4}\)|\d{3,4}-)?\d{7,8}$
//手机号: /^(0|86|17951)?(13[0-9]|15[012356789]|17[013678]|18[0-9]|14[57])[0-9]{8}$/
//验证身份证号 ^\d{15}|\d{}18&
