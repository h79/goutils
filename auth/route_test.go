package auth

import (
	"gitee.com/h79/goutils/auth/jwt"
	"gitee.com/h79/goutils/common/secret"
	"testing"
)

func TestMgr_Load(t *testing.T) {
	mgr := NewSecretMgr()
	if err := mgr.LoadFromFile("./secret.json"); err != nil {
		t.Error(err)
	}
}

func TestNewMgr_Add(t *testing.T) {
	mgr := NewSecretMgr()
	mgr.AddItem("jwt",
		&Item{
			Enabled: true,
			Secret: secret.Secret{
				Value:  "SFSF",
				Expire: 1223,
			},
		})

	Engine().Client().Use(jwt.New(nil))
}
