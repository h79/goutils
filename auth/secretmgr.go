package auth

import (
	"gitee.com/h79/goutils/auth/token"
	"gitee.com/h79/goutils/common/secret"
	"gitee.com/h79/goutils/loader"
	"sync"
)

// 保证 SecretMgr struct implement token.Secret
var _ token.Secret = (*SecretMgr)(nil)

type ItemSecret interface {
	GetSecret() secret.Secret
	SetEnabled(enable bool)
	// EnableFlag return 0= disable, 1=enable, 2=not exist
	EnableFlag() int
}

type SecretMgr struct {
	locker sync.Mutex
	items  map[string]ItemSecret
}

func NewSecretMgr() *SecretMgr {
	return &SecretMgr{
		items: make(map[string]ItemSecret, 0),
	}
}

func (mgr *SecretMgr) LoadFromFile(file string) error {
	items := map[string]Item{}
	ll := loader.NewFiler(file, "json", &items)
	if err, _ := ll.Read(); err != nil {
		return err
	}
	return mgr.Load(items)
}

func (mgr *SecretMgr) Load(items map[string]Item) error {
	for k, item := range items {
		mgr.AddItem(k, &item)
	}
	return nil
}

func (mgr *SecretMgr) AddSecret(key string, secret secret.Secret) {
	mgr.AddItem(key, &Item{Enabled: true, Secret: secret})
}

func (mgr *SecretMgr) AddItem(key string, item ItemSecret) {
	if len(key) <= 0 {
		return
	}
	{
		mgr.locker.Lock()
		defer mgr.locker.Unlock()
		if _, ok := mgr.items[key]; ok {
			return
		}
		mgr.items[key] = item
	}
}

// GetSecret implement token.Secret interface
func (mgr *SecretMgr) GetSecret(key string) secret.Secret {
	mgr.locker.Lock()
	defer mgr.locker.Unlock()
	if st, ok := mgr.items[key]; ok {
		return st.GetSecret()
	}
	return secret.Secret{}
}

// SetEnabled implement token.Secret interface
func (mgr *SecretMgr) SetEnabled(key string, enable bool) {
	mgr.locker.Lock()
	defer mgr.locker.Unlock()
	if st, ok := mgr.items[key]; ok {
		st.SetEnabled(enable)
	}
}

// EnableFlag implement token.Secret interface
func (mgr *SecretMgr) EnableFlag(key string) int {
	mgr.locker.Lock()
	defer mgr.locker.Unlock()
	if st, ok := mgr.items[key]; ok {
		return st.EnableFlag()
	}
	return 2
}
