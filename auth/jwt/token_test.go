package jwt

import (
	"gitee.com/h79/goutils/common/secret"
	"testing"
)

func TestToken(t *testing.T) {

	sec := secret.Secret{
		Value:  "abcdefghijklmn",
		Expire: 864000,
	}
	t.Log(sec)
	//tk1 := NewToken(&sec)

	//_, _ = tk1.Update()

	//t.Log("token 1: ", tk1.Token, "\r\n")

	token := "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzb3VyY2UiOnsiYXBwaWQiOiJ0ZXN0QXBwSWQiLCJzb3VyY2UiOiJ0ZXN0U291cmNlIn0sInNlc3Npb24iOiJiYmIiLCJleHRlbmQiOiJhYWEiLCJzdGFuZGFyZENsYWltcyI6eyJzdWJqZWN0IjozLCJpc3N1ZWRBdCI6MiwiZXhwaXJlc0F0IjoxfX0.jL7QLb5MztsZV07vEG_GW8Tb6hWycDfRXU7rvH5baIc"
	tk, er := DecodeTokenNoBase64(token)
	if er != nil {
		t.Error("er : ", er, "\r\n")
		return
	}

	t.Log(tk)
}
