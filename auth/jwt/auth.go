package jwt

import (
	"gitee.com/h79/goutils/auth/token"
)

// 保证 Authenticate struct implement token.Authenticate
var _ token.Authenticate = (*Authenticate)(nil)

const Type = "jwt"

type Authenticate struct {
	method  string
	secret  token.Secret
	factory token.Factory
}

func New(secret token.Secret) *Authenticate {
	return NewWith(secret, Type, &defJWTFactory)
}

func NewWith(secret token.Secret, method string, factory token.Factory) *Authenticate {
	if factory == nil {
		factory = &defJWTFactory
	}
	return &Authenticate{method: method, secret: secret, factory: factory}
}

func (t *Authenticate) Type() string {
	return t.method
}

// Create implement token.Authenticate interface
func (t *Authenticate) Create(secretKey string, expireSeconds int64, opts ...token.Option) (token.Token, error) {
	sec := t.secret.GetSecret(secretKey)
	if expireSeconds > 0 {
		sec.Expire = expireSeconds
	}
	return t.factory.Create(&sec, opts...)
}

// Decode implement token.Authenticate interface
func (t *Authenticate) Decode(tok string, opts ...token.Option) (token.Token, error) {
	return t.factory.Decode(tok, opts...)
}

// Check implement token.Authenticate interface
func (t *Authenticate) Check(secretKey string, tok string, opts ...token.Option) (token.Token, error) {
	sec := t.secret.GetSecret(secretKey)
	return t.factory.Check(tok, &sec, opts...)
}

// SetEnabled implement token.Authenticate interface
func (t *Authenticate) SetEnabled(secretKey string, enable bool) {
	t.secret.SetEnabled(secretKey, enable)
}

// EnableFlag implement token.Authenticate interface
func (t *Authenticate) EnableFlag(secretKey string) int {
	return t.secret.EnableFlag(secretKey)
}
