package auth

import (
	"gitee.com/h79/goutils/common/secret"
)

var _ ItemSecret = (*Item)(nil)

type Item struct {
	Enabled bool          `json:"enabled"`
	Secret  secret.Secret `json:"secret"`
}

func (it *Item) GetSecret() secret.Secret {
	return it.Secret
}

func (it *Item) SetEnabled(enable bool) {
	it.Enabled = enable
}

// EnableFlag return 0= disable, 1=enable, 2=not exist
func (it *Item) EnableFlag() int {
	if it.Enabled {
		return 1
	}
	return 0
}
