package auth

import (
	"gitee.com/h79/goutils/auth/token"
)

var _ token.Engine = (*engine)(nil)

type engine struct {
	client route
	server route
}

var defEngine = &engine{
	client: route{auths: authMap{}},
	server: route{auths: authMap{}},
}

func Engine() token.Engine {
	return defEngine
}

func (eng *engine) Client() token.Route {
	return &eng.client
}

func (eng *engine) Server() token.Route {
	return &eng.server
}
