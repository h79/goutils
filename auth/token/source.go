package token

type Source struct {
	AppId  string `json:"appid"`
	Source string `json:"source"`
}
