package token

import (
	"gitee.com/h79/goutils/common/secret"
)

type Key secret.Key

type Secret interface {
	GetSecret(key string) secret.Secret
	SetEnabled(key string, enable bool)
	// EnableFlag return 0= disable, 1=enable, 2=not exist
	EnableFlag(key string) int
}
