package token

const (
	CSource    = "source"
	CSession   = "session"
	CExtend    = "extend"
	CUid       = "uid"
	CBase64    = "base64"
	CCustom    = "custom"
	CIssuedAt  = "issue_at"
	CExpiresAt = "expire_at"
)

type Token interface {
	// WithVar key= CSource value= token.Source
	//         key= CSession value= string
	//         key= CExtend value= string
	//         key= CUid value= string
	//         key= CCustom value = interface 自定义数据
	//         key= CIssuedAt value= int64 秒
	//         key= CExpiresAt value= int64 秒
	WithVar(key string, value interface{}) Token
	GetVar(key string) interface{}
	GetToken() string
	Update() (string, error)
	// ExpireIn 根据配置，产生一个过期时间
	ExpireIn() int64
}

type Factory interface {
	Create(key Key, opts ...Option) (Token, error)
	Decode(tk string, opts ...Option) (Token, error)
	Check(tk string, key Key, opts ...Option) (Token, error)
}

type Authenticate interface {
	Type() string
	// Create secretKey不是那个密钥，而是 Secret interface 的key,通过这个key值，获取到真实的密钥
	Create(secretKey string, expireSeconds int64, opts ...Option) (Token, error)
	// Check secretKey不是那个密钥，而是 Secret interface 的key,通过这个key值，获取到真实的密钥
	Check(secretKey string, tk string, opts ...Option) (Token, error)
	// Decode not need sign, only parse
	Decode(tk string, opts ...Option) (Token, error)
	// EnableFlag return 0= disable, 1=enable, 2=not exist
	// secretKey不是那个密钥，而是 Secret interface 的key, 通过这个key值，获取到真实的密钥
	EnableFlag(secretKey string) int
	// SetEnabled secretKey不是那个密钥，而是 Secret interface 的key, 通过这个key值，获取到真实的密钥
	SetEnabled(secretKey string, enable bool)
}

type Route interface {
	Use(a Authenticate) Route
	HasAuth(method string) bool
	Auth(method string) (Authenticate, error)
}

type Engine interface {
	Client() Route
	Server() Route
}

type Option struct {
	Base64 bool //是否使用base64编码
}

func IsBase64Enabled(opts ...Option) bool {
	if len(opts) == 0 {
		// 默认base64
		return true
	}
	for i := range opts {
		if opts[i].Base64 {
			return true
		}
	}
	return false
}
