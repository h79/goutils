package auth

import (
	"gitee.com/h79/goutils/auth/token"
	"gitee.com/h79/goutils/common/result"
)

type authMap map[string]token.Authenticate

var (
	_ token.Route = (*route)(nil)
)

type route struct {
	auths authMap
}

func (auth *route) HasAuth(method string) bool {
	_, ok := auth.auths[method]
	return ok
}

func (auth *route) Use(a token.Authenticate) token.Route {
	auth.auths[a.Type()] = a
	return auth
}

func (auth *route) Auth(method string) (token.Authenticate, error) {
	if len(auth.auths) <= 0 {
		return nil, result.Error(result.ErrNil, "Not exist auth")
	}
	item, ok := auth.auths[method]
	if !ok {
		return nil, result.RErrNotFound
	}
	return item, nil
}
