package alarm

import (
	"context"
	"gitee.com/h79/goutils/common/debug"
	"gitee.com/h79/goutils/plugins"
)

func Do(ctx context.Context, code int32, module, title, detail string, err error) {
	Handle(ctx, debug.DNormalLevel, code, module, title, detail, err)
}

func Important(ctx context.Context, code int32, module, title, detail string, err error) {
	Handle(ctx, debug.DImportantLevel, code, module, title, detail, err)
}
func Tight(ctx context.Context, code int32, module, title, detail string, err error) {
	Handle(ctx, debug.DTightLevel, code, module, title, detail, err)
}

func Fatal(ctx context.Context, code int32, module, title, detail string, err error) {
	Handle(ctx, debug.DFatalLevel, code, module, title, detail, err)
}

func Handle(ctx context.Context, l debug.Level, code int32, module, title, detail string, err error) {
	d := debug.New(code).
		WithModule(module).
		WithDetail(detail).
		WithTitle(title).
		WithError(err).
		WithLevel(l)
	_, _ = plugins.Do(plugins.KAlarm, ctx, "debug", d)
}
