package plugins

import (
	"context"
	"gitee.com/h79/goutils/common/result"
)

type defaultPlugin struct {
}

func (plu *defaultPlugin) Do(ctx context.Context, cmd string, d interface{}) (interface{}, error) {
	return nil, result.RErrNotImplement
}

func (plu *defaultPlugin) DoChan(ctx context.Context, cmd string, d interface{}) (Chan, error) {
	return nil, result.RErrNotImplement
}
