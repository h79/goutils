package plugins

import (
	"context"
	"sync"
)

const (
	KAlarm = "alarm"
)

type Command struct {
	Cmd  string
	Data interface{}
}

type Data struct {
	Error error
	Data  interface{}
}
type Chan chan Data

type Plugin interface {
	// Do
	// name is plugin's name
	// key parameter is custom defined
	// d parameter is can use Command or custom
	// return is custom
	Do(ctx context.Context, key string, d interface{}) (interface{}, error)
	DoChan(ctx context.Context, key string, d interface{}) (Chan, error)
}

type Plugins struct {
	locker sync.RWMutex
	plus   map[string]Plugin
}

var plugins *Plugins
var def *defaultPlugin

func init() {
	def = &defaultPlugin{}
	plugins = &Plugins{
		plus: make(map[string]Plugin),
	}
}

func Get(name string) Plugin {
	plugins.locker.RLock()
	defer plugins.locker.RUnlock()

	if p, exist := plugins.plus[name]; exist {
		return p
	}
	return def
}

func Add(name string, p Plugin) {
	plugins.locker.Lock()
	defer plugins.locker.Unlock()
	plugins.plus[name] = p
}

func Remove(name string) {
	plugins.locker.Lock()
	defer plugins.locker.Unlock()
	delete(plugins.plus, name)
}

func Do(name string, ctx context.Context, key string, d interface{}) (interface{}, error) {
	p := Get(name)
	return p.Do(ctx, key, d)
}

func DoChan(name string, ctx context.Context, key string, d interface{}) (Chan, error) {
	p := Get(name)
	return p.DoChan(ctx, key, d)
}

// DoWithError 只关心error
func DoWithError(name string, ctx context.Context, key string, d interface{}) error {
	_, err := Do(name, ctx, key, d)
	return err
}
